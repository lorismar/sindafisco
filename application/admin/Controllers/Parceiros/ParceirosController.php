<?php
/**
 * User: Iuri Gustavo
 * @property Arquivos tbl
 */

class ParceirosController extends admin_Controller
{
    function beforeAction()
    {
        $this->tbl = new Arquivos(NULL);
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
    }

    function Listar()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Parceiros");
        $this->view->setMenu("parceiros_listar");

        $listaArquivos = Arquivos::ListarTodos("WHERE id_CategoriasPrincipal_fk = 11");
        $this->set("listaArquivos", $listaArquivos);

        return "Parceiros";
    }

    function Editar()
    {
        if (isset($_GET['id'])) {
            $this->tbl->setIdArquivos($_GET['id']);
            $this->tbl->VisualizarPorId();
        }
        return $this->Listar();
    }

    function Salvar()
    {
        try {
            $tbl = new Arquivos(NULL);
            if ($_POST["id_Arquivos"] != NULL && $_POST["id_Arquivos"] != "") {
                $tbl->setIdArquivos($_POST["id_Arquivos"]);
                $tbl->VisualizarPorId();
            }
            $tbl->setIdCategoriasPrincipalFk(11); // Parceiros
            $tbl->setDsTitulo($_POST['ds_Titulo']);
            $tbl->setDsUrlLink($_POST['ds_UrlLink']);
            //UPLOAD
            $get_imagem = $_FILES["file_Arquivo"];
            if ($get_imagem['name'] == "")
                $get_imagem = "";
            if (!empty($get_imagem)) {
                $get_imagem = enviarArquivo($get_imagem, 'Parceiros', NULL, NULL);
                $tbl->setFileArquivo($get_imagem);
            } else if ($_POST['file_Arquivo_original'] != NULL && $_POST["file_Arquivo_original"] != "") {
                $tbl->setFileArquivo($_POST['file_Arquivo_original']);
            }
            $tbl->setDtCadastro(DATE);
            $_POST["is_Ativo"] == "TRUE" ? $tbl->setIsAtivo(1) : $tbl->setIsAtivo(0);

            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setIdUsuariosFk($u->getIdUsuarios());
            $tbl->setLogALTERADOPOR($u->getDsNome());

            if ($tbl->Salvar()) {
                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }
        return $this->Editar();
    }

    function Deletar()
    {

        if (isset($_GET['did'])) {
            Arquivos::Remover($_GET['did']);
            GUI::msgAlerta("Registro removido com sucesso!");
        }
        return $this->Listar();

    }

    function RemoverFoto()
    {

        Arquivos::Remover($_GET['foto']);
        GUI::msgAlerta("Registro removido com sucesso!");

        return $this->Listar();

    }


}