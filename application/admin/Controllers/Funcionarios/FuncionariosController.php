<?php
/**
 * Created by PhpStorm.
 * @property Pessoas tbl
 * @property Usuarios tblUsuario
 * User: user
 */

class FuncionariosController extends admin_Controller
{
    function beforeAction()
    {
        $this->tbl = new Pessoas(NULL);
        $this->tbl->setUsuarios(new Usuarios(NULL));
        $this->listaTodasSit = SitPessoas::ListarTodos();
        $this->listaPerfis   = Perfis::ListarTodos();
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
        $this->set("listaTodasSit", $this->listaTodasSit);
        $this->set("listaPerfis", $this->listaPerfis);

    }

    function Listar()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Funcionários");
//        $this->view->Grid("Funcionarios", "Grid",array(0));
        $this->view->setMenu("funcionarios_listar");

        $this->view->GridEstatico();
        $this->listaFiliados = Pessoas::ListarTodos("Where id_TipoPessoas_fk = 2 order by id_Pessoas DESC");
        $this->set("listaFiliados", $this->listaFiliados);


        return "Funcionarios";
    }


    function Editar()
    {
        if (isset($_GET['id'])) {
            $this->tbl->setIdPessoas($_GET['id']);
            $this->tbl->VisualizarPorId();
            $this->tbl->getUsuarios()->VisualizarPorIdPessoa();
        }
        return $this->Listar();
    }


    function Salvar()
    {
        try {
            $tbl = new Pessoas(NULL);
            if ($_POST["id_Pessoas"] != NULL && $_POST["id_Pessoas"] != "") {
                $tbl->setIdPessoas($_POST["id_Pessoas"]);
                $tbl->VisualizarPorId();
            }
            $tbl->setDsNome($_POST['ds_Nome']);
            $tbl->setDsCpf($_POST['ds_Cpf']);
            $tbl->setDsRg($_POST['ds_Rg']);
            $tbl->setDsOrgaoExpedidor($_POST['ds_OrgaoExpedidor']);
            $tbl->setDtNascimento($_POST['dt_Nascimento']);
            $tbl->setDsSexo($_POST['ds_Sexo']);
            $tbl->setDsEstadoCivil($_POST['ds_EstadoCivil']);
            $tbl->setDsNomeConjuge($_POST['ds_NomeConjuge']);
            $tbl->setDsEmail($_POST['ds_Email']);
            $tbl->setDsEmailAlternativo($_POST['ds_EmailAlternativo']);
            $tbl->setDsEndCompleto($_POST['ds_End_Completo']);
            $tbl->setDsEndBairro($_POST['ds_End_Bairro']);
            $tbl->setDsEndCep($_POST['ds_End_Cep']);
            $tbl->setDsEndCidade($_POST['ds_End_Cidade']);
            $tbl->setDsEndUf($_POST['ds_End_UF']);
            $tbl->setDsTelefone($_POST['ds_Telefone']);
            $tbl->setDsCelular($_POST['ds_Celular']);
            $tbl->setIdSitPessoasFk($_POST['id_SitPessoas_fk']);
            $tbl->setIdTipoPessoasFk(2);

            //UPLOAD
            $get_imagem = $_FILES["file_Foto"];
            if ($get_imagem['name'] == "")
                $get_imagem = "";
            if (!empty($get_imagem)) {
                $get_imagem = enviarArquivo($get_imagem, 'Pessoas', NULL, NULL);
                $tbl->setFileFoto($get_imagem);
            } else if ($_POST['file_Foto_original'] != NULL && $_POST["file_Foto_original"] != "") {
                $tbl->setFileFoto($_POST['file_Foto_original']);
            }

            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setLogALTERADOPOR($u->getDsNome());


            if ($tbl->Salvar()) {
                $usuario = new Usuarios(NULL);
                $usuario->setIdPessoasFk($tbl->getIdPessoas());
                $usuario->VisualizarPorIdPessoa();
                $usuario->setDsNome($tbl->getDsNome());
                $usuario->setDsUsuario($tbl->getDsCpf());
                $usuario->setDsSenha(mycrypt_encode($_POST['ds_Senha']));
                $usuario->setIdPerfisFk($_POST['id_Perfis_fk']);
                $usuario->setLogALTERADOPOR($u->getDsNome());
                $usuario->setIsAtivo(1);
                $usuario->Salvar();
                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }
        return $this->Listar();
    }

    function Deletar()
    {
        if (isset($_GET['did'])) {
            Pessoas::Remover($_GET['did']);
            GUI::msgAlerta("Registro removido com sucesso!");

        }
        return $this->Listar();

    }

    function Grid()
    {

        $aColunas        = array('id_Pessoas', 'ds_Nome', 'ds_Cpf', 'ds_Descricao');
        $aColunasFiltros = array('ds_Nome', 'ds_Cpf', 'ds_Descricao');
        $sColunaIndex    = "id_Pessoas";
        $sTabela         = "Pessoas LEFT JOIN SitPessoas ON id_SitPessoas = id_SitPessoas_fk";
        $sCondicao       = "WHERE id_TipoPessoas_fk = 2";

        echo Grid::Gera($aColunas, $aColunasFiltros, $sColunaIndex, $sTabela, $sCondicao, NULL, array(Grid::Editar()));
    }
} 