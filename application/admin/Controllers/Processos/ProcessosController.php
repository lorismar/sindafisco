<?php
/**
 * User: Iuri Gustavo
 * @property Conteudos tbl
 */

class ProcessosController extends admin_Controller
{
    function beforeAction()
    {
        $this->tbl          = new Conteudos(NULL);
        $this->listaPessoas = Pessoas::ListarTodos("WHERE id_TipoPessoas_fk = 1");
        $this->u            = unserialize($_SESSION['usuario']);

    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
        $this->set("listaPessoas", $this->listaPessoas);
        $this->set("u", $this->u);
    }


    function Editar()
    {
        if (isset($_GET['id'])) {
            $this->tbl->setIdConteudos($_GET['id']);
            $this->tbl->VisualizarPorId();
            $listaPessoasProcessos = Conteudos_tem_Pessoas::ListarTodosPorConteudo($_GET['id']);
            $this->set("listaPessoasProcessos", $listaPessoasProcessos);
        }
        return $this->Todos();
    }

    function Anexar()
    {

        try {
            $tbl = new Arquivos(NULL);
            $tbl->setIdConteudosFk($_POST["id_Conteudos_fk"]);
            $tbl->setDsTitulo($_POST['ds_Titulo']);
            $tbl->setDtCadastro($_POST['dt_Cadastro']);

            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setIdUsuariosFk($u->getIdUsuarios());
            $tbl->setLogALTERADOPOR($u->getDsNome());


            $get_imagem = $_FILES["file_Arquivo"];
            if ($get_imagem['name'] == "")
                $get_imagem = "";
            if (!empty($get_imagem)) {
                $get_imagem = enviarArquivo($get_imagem, 'Processos', NULL, NULL);
                $tbl->setFileArquivo($get_imagem);
            }
            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setIdUsuariosFk($u->getIdUsuarios());

            if ($tbl->Salvar()) {
                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                // GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }

        return $this->Editar();
    }

    function RemoverAnexo()
    {
        if (!empty($_GET['anexo'])) {
            Arquivos::Remover($_GET['anexo']);
        }
        return $this->Editar();
    }

    function Deletar()
    {

        if (isset($_GET['did'])) {
            Comentarios::RemoverPorConteudo($_GET['did']);
            Conteudos_tem_Pessoas::RemoverPorConteudo($_GET['did']);
            Conteudos::Remover($_GET['did']);
            GUI::msgAlerta("Registro removido com sucesso!");
        }
        return $this->Todos();

    }

    function Salvar()
    {
        try {
            $tbl = new Conteudos(NULL);
            if ($_POST["id_Conteudos"] != NULL && $_POST["id_Conteudos"] != "") {
                $tbl->setIdConteudos($_POST["id_Conteudos"]);
                $tbl->VisualizarPorId();
            }

            $tbl->setTxResumo($_POST['ds_NuProcesso']);
            $tbl->setDsTitulo($_POST['ds_Titulo']);
            $tbl->setTxConteudo($_POST['tx_Descricao']);
            $tbl->setIdCategoriasPrincipalFk(13); // Processos
            $tbl->setDhCadastro(DATETIME);

            $_POST["is_Publico"] == "TRUE" ? $tbl->setIsPublico(1) : $tbl->setIsPublico(0);


//
            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setIdUsuariosFk($u->getIdUsuarios());
            $tbl->setLogALTERADOPOR($u->getDsNome());

            if ($tbl->Salvar()) {

                Conteudos_tem_Pessoas::RemoverPorConteudo($tbl->getIdConteudos());
                $_GET['id'] = $tbl->getIdConteudos();

                foreach ($_POST['id_Pessoas_fk'] as $pf) {
                    $ptp = new Conteudos_tem_Pessoas(NULL, NULL);
                    $ptp->setIdPessoasFk($pf);
                    $ptp->setIdConteudosFk($tbl->getIdConteudos());
                    $ptp->Salvar();
                }

                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }
        return $this->Acompanhar();
    }

    function Listar()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Processos");

        return "Processos";
    }

    function Todos()
    {
        $this->view->setDescricao("Todos os processos");
//        $this->view->Grid("Processos", "Grid?sit=todos",array(0));

        $this->view->setMenu("processos_todos");
        $this->view->GridEstatico();
        $listaProcessos = Conteudos::ListarTodos("Where id_Conteudos is NOT NULL AND id_CategoriasPrincipal_fk = 13");
        $this->set("listaProcessos", $listaProcessos);

        return $this->Listar();
    }

    function Sindicato()
    {
        $this->view->setDescricao("Todos os processos do Sindicato");
//        $this->view->Grid("Processos", "Grid?sit=sindicato",array(0));
        $this->view->setMenu("processos_sindicato");

        $this->view->GridEstatico();
        $listaProcessos = Conteudos::ListarTodos("Where is_Publico = TRUE AND id_CategoriasPrincipal_fk = 13");
        $this->set("listaProcessos", $listaProcessos);

        return $this->Listar();
    }

    function Meus()
    {
        $this->view->setDescricao("Todos os meus processos");
//        $this->view->Grid("Processos", "Grid?sit=meus",array(0));
        $this->view->setMenu("processos_meus");

        /** @var $u Usuarios */
        $u = unserialize($_SESSION['usuario']);
        $this->view->GridEstatico();

        $listaProcessos = Conteudos::ListarTodos("WHERE is_Publico = FALSE and id_Conteudos in (select id_Conteudos_fk from Conteudos_tem_Pessoas WHERE id_Pessoas_fk = " . $u->getIdPessoasFk() . ") AND id_CategoriasPrincipal_fk = 13");
        $this->set("listaProcessos", $listaProcessos);

        return $this->Listar();
    }

    function Acompanhar()
    {
        $this->view->setTemplate("Admin");
        $this->view->setMenu("processos_menu");

        if (isset($_GET['id'])) {
            $this->tbl->setIdConteudos($_GET['id']);
            $this->tbl->VisualizarPorId();

            $this->view->setTitulo("Processo # " . $this->tbl->getTxResumo());
            $this->view->setDescricao($this->tbl->getDsTitulo());
        }
        return "Acompanhar";
    }

    function Comentar()
    {
        $mov = new Comentarios(NULL);
        $mov->setIdConteudosFk($_POST['id_Processos_fk']);
        $mov->setTxComentario($_POST['tx_Movimentacao']);
        $mov->setDhComentario($_POST['dh_Movimentacao']);
        /** @var $u Usuarios */
        $u = unserialize($_SESSION['usuario']);
        $mov->setIdUsuariosFk($u->getIdPessoasFk());
        $mov->Salvar();

        $_GET['id'] = $mov->getIdConteudosFk();

        return $this->Acompanhar();
    }

    function ExportarExcel()
    {
        $arquivo        = 'Processos-' . DATAHORA2 . '.xls';
        $listaProcessos = Conteudos::ListarTodos("WHERE id_CategoriasPrincipal_fk = 13");
        // Criamos uma tabela HTML com o formato da planilha para excel
        $tabela = '<table border="1">';
        $tabela .= "<thead>";
        $tabela .= "<tr>";
        $tabela .= "<th>N�mero</th>";
        $tabela .= "<th>T�tulo</th>";
        $tabela .= "<th>Descri��o</th>";
        $tabela .= "<th>Data de Cadastro</th>";
        $tabela .= "<th>Andamento</th>";
        $tabela .= "</tr>";
        $tabela .= "</thead>";
        $tabela .= "<tbody>";
        /** @var $p Conteudos */
        foreach ($listaProcessos as $p) {
            $tabela .= "<tr>";
            $tabela .= "<td>" . $p->getTxResumo() . "</td>";
            $tabela .= "<td>" . $p->getDsTitulo() . "</td>";
            $tabela .= "<td>" . $p->getTxConteudo() . "</td>";
            $tabela .= "<td>" . $p->getDhCadastro() . "</td>";
            $tabela .= "<td>";
            $tabela .= '<table border="1">';
            $tabela .= "<thead>";
            $tabela .= "<th>Movimenta�ao</th>";
            $tabela .= "<th>Data</th>";
            $tabela .= "</thead>";
            $tabela .= "<tbody>";
            /** @var $mov Comentarios */
            foreach ($p->Comentarios() as $mov) {
                $tabela .= "<tr>";
                $tabela .= "<td>" . $mov->getTxComentario() . "</td>";
                $tabela .= "<td>" . $mov->getDhComentario() . "</td>";
                $tabela .= "</tr>";
            }
            $tabela .= "</tbody>";
            $tabela .= '</table>';
            $tabela .= "</td>";
            $tabela .= "</tr>";
        }
        $tabela .= "</tbody>";
        $tabela .= '</table>';

        // For�a o Download do Arquivo Gerado
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename=\"{$arquivo}\"");
        echo $tabela;


    }

    function Grid()
    {
        /** @var $u Usuarios */
        $u = unserialize($_SESSION['usuario']);

        $aColunas        = array('id_Processos', 'ds_NuProcesso', 'ds_Titulo');
        $aColunasFiltros = array('ds_NuProcesso', 'ds_Titulo');
        $sColunaIndex    = "id_Processos";
        $sTabela         = "Processos ";
        if ($_GET['sit'] == "sindicato")
            $sCondicao = "Where is_Publico = TRUE";
        else if ($_GET['sit'] == "meus")
            $sCondicao = "Where is_Publico = FALSE and id_Processos in (select id_Processos_fk from Processos_tem_Pessoas WHERE id_Pessoas_fk = " . $u->getIdPessoasFk() . ")";
        else if ($_GET['sit'] == "todos")
            $sCondicao = "WHERE id_Processos is NOT NULL";
        else
            $sCondicao = "Where is_Publico = FALSE and id_Processos in (select id_Processos_fk from Processos_tem_Pessoas WHERE id_Pessoas_fk = " . $u->getIdPessoasFk() . ")";

        $sBotaoExtra = array("<a href='../Acompanhar/?id={0}#tab_1_3' class='btn default btn-xs green'><i class='fa fa-search'> Acompanhar</i></a>");
        echo Grid::Gera($aColunas, $aColunasFiltros, $sColunaIndex, $sTabela, $sCondicao, NULL, $sBotaoExtra);

    }

}