<?php
/**
 * Created by PhpStorm.
 * @property Usuarios tblUsuario
 * @property Usuarios tbl
 * User: user
 */

class UsuariosController extends admin_Controller
{
    function beforeAction()
    {
        $this->tbl         = new Usuarios(NULL);
        $this->listaPerfis = Perfis::ListarTodos();
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
        $this->set("listaPerfis", $this->listaPerfis);
    }

    function Listar()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Usu�rios");
        $this->view->Grid("Usuarios", "Grid", array(0));
        $this->view->setMenu("usuarios_listar");

        return "Usuarios";
    }


    function Editar()
    {
        if (isset($_GET['id'])) {
            $this->tbl->setIdUsuarios($_GET['id']);
            $this->tbl->VisualizarPorId();
        }
        return $this->Listar();
    }

    function  Salvar()
    {
        try {
            $tbl = new Usuarios(NULL);
            if ($_POST["id_Usuarios"] != NULL && $_POST["id_Usuarios"] != "") {
                $tbl->setIdUsuarios($_POST["id_Usuarios"]);
                $tbl->VisualizarPorId();
            }
            $tbl->setDsNome($_POST['ds_Nome']);
            $tbl->setDsSenha(mycrypt_encode($_POST['ds_Senha']));
            $tbl->setDsUsuario($_POST['ds_Usuario']);
            $_POST["is_Ativo"] == "TRUE" ? $tbl->setIsAtivo(1) : $tbl->setIsAtivo(0);
            $tbl->setIdPerfisFk($_POST['id_Perfis_fk']);

            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setLogALTERADOPOR($u->getDsNome());


            if ($tbl->Salvar()) {
                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }
        return $this->Listar();
    }

    function Grid()
    {

        $aColunas        = array('id_Usuarios', 'ds_Nome', 'ds_Descricao');
        $aColunasFiltros = array('ds_Nome', 'ds_Descricao');
        $sColunaIndex    = "id_Usuarios";
        $sTabela         = "Usuarios LEFT JOIN Perfis ON id_Perfis = id_Perfis_fk";
        $sCondicao       = NULL;

        echo Grid::Gera($aColunas, $aColunasFiltros, $sColunaIndex, $sTabela, $sCondicao, NULL, array(Grid::Editar()));
    }
} 