<?php
/**
 * User: Iuri Gustavo
 * @property Parametros tbl
 */

class ParametrosController extends admin_Controller
{
    function beforeAction()
    {
        $this->tbl = new Parametros(NULL);
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
    }

    function Listar()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Parametros");
        $this->view->Grid("Parametros", "Grid", array(0));
        $this->view->setMenu("parametros_listar");

        return "Parametros";
    }

    function Editar()
    {
        if (isset($_GET['id'])) {
            $this->tbl->setIdParametros($_GET['id']);
            $this->tbl->VisualizarPorId();
        }
        return $this->Listar();
    }

    function Salvar()
    {
        try {
            $tbl = new Parametros(NULL);
            if ($_POST["id_Parametros"] != NULL && $_POST["id_Parametros"] != "") {
                $tbl->setIdParametros($_POST["id_Parametros"]);
            }
            $tbl->setDsDescricao($_POST['ds_Descricao']);
            $tbl->setDsValor($_POST['ds_Valor']);

            if ($tbl->Salvar()) {
                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }
        return $this->Listar();
    }

    function Deletar()
    {

        if (isset($_GET['did'])) {
            Parametros::Remover($_GET['did']);
            GUI::msgAlerta("Registro removido com sucesso!");
        }
        return $this->Listar();

    }


    function Grid()
    {
        $aColunas        = array('id_Parametros', 'ds_Descricao', 'ds_Valor');
        $aColunasFiltros = array('ds_Descricao');
        $sColunaIndex    = "id_Parametros";
        $sTabela         = "Parametros";
        echo Grid::Gera($aColunas, $aColunasFiltros, $sColunaIndex, $sTabela, NULL, NULL, array(Grid::Editar(), Grid::Remover()));
    }

}