<?php
/**
 * User: Iuri Gustavo
 * @property Categorias tbl
 */

class CategoriasController extends admin_Controller
{
    function beforeAction()
    {
        $this->tbl = new Categorias(NULL);
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
    }

    function Listar()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Categorias");
        $this->view->setMenu("categorias_listar");

        $listaCategorias = Categorias::ListarTodos("WHERE id_Categorias_fk = 1");
        $this->set("listaCategorias", $listaCategorias);

        return "Categorias";
    }

    function Editar()
    {
        if (isset($_GET['id'])) {
            $this->tbl->setIdCategorias($_GET['id']);
            $this->tbl->VisualizarPorId();
            if ($this->tbl->getIdCategoriasFk() != 1) {
                unset($this->tbl);
                $this->tbl = new Categorias(NULL);
            }
        }
        return $this->Listar();
    }

    function Salvar()
    {
        try {
            $tbl = new Categorias(NULL);

            if ($_POST["id_Categorias"] != NULL && $_POST["id_Categorias"] != "") {
                $tbl->setIdCategorias($_POST["id_Categorias"]);
            }
            $tbl->setDsDescricao($_POST['ds_Descricao']);
            $tbl->setIdCategoriasFk(1);

            if ($tbl->Salvar()) {
                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }
        return $this->Listar();
    }

    function Grid()
    {

        $aColunas        = array('id_Conteudos', 'ds_Titulo', 'dh_Cadastro');
        $aColunasFiltros = array('ds_Titulo');
        $sColunaIndex    = "id_Conteudos";
        $sTabela         = "Conteudos";
        $sCondicao       = "";
        echo Grid::Gera($aColunas, $aColunasFiltros, $sColunaIndex, $sTabela, $sCondicao);
    }

}