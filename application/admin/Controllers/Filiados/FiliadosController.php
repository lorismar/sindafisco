<?php
/**
 * Created by PhpStorm.
 * @property Pessoas tbl
 * @property Usuarios tblUsuario
 * User: user
 */

class FiliadosController extends admin_Controller
{
    function beforeAction()
    {
        $this->tbl = new Pessoas(NULL);
        $this->tbl->setUsuarios(new Usuarios(NULL));
        $this->listaTodasSit    = SitPessoas::ListarTodos();
        $this->listaDependentes = array();
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
        $this->set("listaTodasSit", $this->listaTodasSit);
        $this->set("listaDependentes", $this->listaDependentes);
    }

    function Listar()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Filiados");
        // $this->view->Grid("Filiados", "Grid?sit=ativo",array(0));
        $this->view->setMenu("filiados_listar");

        $this->view->GridEstatico();
        $this->listaFiliados = Pessoas::ListarTodos("WHERE id_TipoPessoas_fk = 1 order by id_Pessoas DESC");
        $this->set("listaFiliados", $this->listaFiliados);

        return "Filiados";
    }

    function Funcionarios()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Funcion�rios");
//        $this->view->Grid("Filiados", "Grid?sit=funcionarios",array(0));
        $this->view->setMenu("filiados_funcionarios");
        $this->listaPerfis = Perfis::ListarTodos();
        $this->set("listaPerfis", $this->listaPerfis);

        $this->view->GridEstatico();
        $this->listaFiliados = Pessoas::ListarTodos("Where id_TipoPessoas_fk = 2 order by id_Pessoas DESC");
        $this->set("listaFiliados", $this->listaFiliados);

        return "Funcionarios";
    }


    function ListarSolicitacoes()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Filiados");
        $this->view->setDescricao("Solicita��es de novos filiados");
//        $this->view->Grid("Filiados", "Grid?sit=solicitacoes",array(0));
        $this->view->setMenu("filiados_listarsolicitacoes");

        $this->view->GridEstatico();
        $this->listaFiliados = Pessoas::ListarTodos("WHERE  id_SitPessoas_fk = 2 and id_TipoPessoas_fk = 1 order by id_Pessoas DESC");
        $this->set("listaFiliados", $this->listaFiliados);

        return "Filiados";
    }

    function Editar()
    {
        if (isset($_GET['id'])) {
            $this->tbl->setIdPessoas($_GET['id']);
            $this->tbl->VisualizarPorId();
            $this->tbl->getUsuarios()->VisualizarPorIdPessoa();
            $this->listaDependentes = Dependentes::ListarTodos("WHERE id_Pessoas_fk = " . $this->tbl->getIdPessoas());
        }
        return $this->Listar();
    }

    function Deletar()
    {
        if (isset($_GET['did'])) {
            Usuarios::RemoverPorIdPessoas($_GET['did']);
            Dependentes::RemoverPorTitular($_GET['did']);
            Pessoas::Remover($_GET['did']);
            GUI::msgAlerta("Registro removido com sucesso!");

        }
        return $this->Listar();

    }

    function  Salvar()
    {
        try {
            $tbl = new Pessoas(NULL);
            if ($_POST["id_Pessoas"] != NULL && $_POST["id_Pessoas"] != "") {
                $tbl->setIdPessoas($_POST["id_Pessoas"]);
                $tbl->VisualizarPorId();
            }
            $tbl->setDsNome($_POST['ds_Nome']);
            $tbl->setDsCpf($_POST['ds_Cpf']);
            $tbl->setDsRg($_POST['ds_Rg']);
            $tbl->setDsOrgaoExpedidor($_POST['ds_OrgaoExpedidor']);
            $tbl->setDsMatricula($_POST['ds_Matricula']);
            $tbl->setDtNascimento($_POST['dt_Nascimento']);
            $tbl->setDsSexo($_POST['ds_Sexo']);
            $tbl->setDsFormacao($_POST['ds_Formacao']);
            $tbl->setDsEstadoCivil($_POST['ds_EstadoCivil']);
            $tbl->setDsNomeConjuge($_POST['ds_NomeConjuge']);
            $tbl->setNuDependentes($_POST['nu_Dependentes']);
            $tbl->setDsEmail($_POST['ds_Email']);
            $tbl->setDsEmailAlternativo($_POST['ds_EmailAlternativo']);
            $tbl->setDsEndCompleto($_POST['ds_End_Completo']);
            $tbl->setDsEndBairro($_POST['ds_End_Bairro']);
            $tbl->setDsEndCep($_POST['ds_End_Cep']);
            $tbl->setDsEndCidade($_POST['ds_End_Cidade']);
            $tbl->setDsEndUf($_POST['ds_End_UF']);
            $tbl->setDsTelefone($_POST['ds_Telefone']);
            $tbl->setDsCelular($_POST['ds_Celular']);
            $tbl->setDsTelefoneTrabalho($_POST['ds_TelefoneTrabalho']);
            $tbl->setDsLotacao($_POST['ds_Lotacao']);
            $tbl->setIdSitPessoasFk($_POST['id_SitPessoas_fk']);
            $tbl->setIdTipoPessoasFk(1);

            //UPLOAD
            $get_imagem = $_FILES["file_Foto"];
            if ($get_imagem['name'] == "")
                $get_imagem = "";
            if (!empty($get_imagem)) {
                $get_imagem = enviarArquivo($get_imagem, 'Pessoas', NULL, NULL);
                $tbl->setFileFoto($get_imagem);
            } else if ($_POST['file_Foto_original'] != NULL && $_POST["file_Foto_original"] != "") {
                $tbl->setFileFoto($_POST['file_Foto_original']);
            }

            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setLogALTERADOPOR($u->getDsNome());


            if ($tbl->Salvar()) {
                Dependentes::RemoverPorTitular($tbl->getIdPessoas());
                if (is_numeric($_POST['nu_Dependentes'])) {
                    for ($x = 1; $x <= $_POST['nu_Dependentes']; $x++) {
                        $dep = new Dependentes(NULL);
                        $dep->setDsNome($_POST['dep_ds_Nome_' . $x]);
                        $dep->setDtNascimento($_POST['dep_dt_Nascimento_' . $x]);
                        $dep->setDsParentesco($_POST['dep_ds_Parentesco_' . $x]);
                        $x++;
                        $dep->setIdPessoasFk($tbl->getIdPessoas());
                        $dep->Salvar();
                    }
                }
                $usuario = new Usuarios(NULL);
                $usuario->setIdPessoasFk($tbl->getIdPessoas());
                $usuario->VisualizarPorIdPessoa();
                $usuario->setDsNome($tbl->getDsNome());
                $usuario->setDsUsuario($tbl->getDsCpf());
                $usuario->setDsSenha(mycrypt_encode($_POST['ds_Senha']));
                $usuario->setIdPerfisFk(99);
                $usuario->setIsAtivo(1);
                $usuario->setLogALTERADOPOR($u->getDsNome());
                $usuario->Salvar();
                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }
        return $this->Listar();
    }

    function  SalvarFuncionario()
    {
        try {
            $tbl = new Pessoas(NULL);
            if ($_POST["id_Pessoas"] != NULL && $_POST["id_Pessoas"] != "") {
                $tbl->setIdPessoas($_POST["id_Pessoas"]);
                $tbl->VisualizarPorId();
            }
            $tbl->setDsNome($_POST['ds_Nome']);
            $tbl->setDsCpf($_POST['ds_Cpf']);
            $tbl->setDsRg($_POST['ds_Rg']);
            $tbl->setDsOrgaoExpedidor($_POST['ds_OrgaoExpedidor']);
            $tbl->setDtNascimento($_POST['dt_Nascimento']);
            $tbl->setDsSexo($_POST['ds_Sexo']);
            $tbl->setDsEstadoCivil($_POST['ds_EstadoCivil']);
            $tbl->setDsNomeConjuge($_POST['ds_NomeConjuge']);
            $tbl->setDsEmail($_POST['ds_Email']);
            $tbl->setDsEmailAlternativo($_POST['ds_EmailAlternativo']);
            $tbl->setDsEndCompleto($_POST['ds_End_Completo']);
            $tbl->setDsEndBairro($_POST['ds_End_Bairro']);
            $tbl->setDsEndCep($_POST['ds_End_Cep']);
            $tbl->setDsEndCidade($_POST['ds_End_Cidade']);
            $tbl->setDsEndUf($_POST['ds_End_UF']);
            $tbl->setDsTelefone($_POST['ds_Telefone']);
            $tbl->setDsCelular($_POST['ds_Celular']);
            $tbl->setIdSitPessoasFk($_POST['id_SitPessoas_fk']);
            $tbl->setIdTipoPessoasFk(2);

            //UPLOAD
            $get_imagem = $_FILES["file_Foto"];
            if ($get_imagem['name'] == "")
                $get_imagem = "";
            if (!empty($get_imagem)) {
                $get_imagem = enviarArquivo($get_imagem, 'Pessoas', NULL, NULL);
                $tbl->setFileFoto($get_imagem);
            } else if ($_POST['file_Foto_original'] != NULL && $_POST["file_Foto_original"] != "") {
                $tbl->setFileFoto($_POST['file_Foto_original']);
            }

            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setLogALTERADOPOR($u->getDsNome());


            if ($tbl->Salvar()) {
                $usuario = new Usuarios(NULL);
                $usuario->setIdPessoasFk($tbl->getIdPessoas());
                $usuario->VisualizarPorIdPessoa();
                $usuario->setDsNome($tbl->getDsNome());
                $usuario->setDsUsuario($tbl->getDsCpf());
                $usuario->setDsSenha(mycrypt_encode($_POST['ds_Senha']));
                $usuario->setIdPerfisFk($_POST['id_Perfis_fk']);
                $usuario->setLogALTERADOPOR($u->getDsNome());
                $usuario->setIsAtivo(1);
                $usuario->Salvar();
                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }
        return $this->Funcionarios();
    }

    function ExportarExcel()
    {
        $arquivo = 'Sindicalizados-'.DATAHORA2.'.xls';
        $c       = Conexao::getInstance();
        $sql     = "SELECT ds_Nome, ds_Cpf, ds_Rg, ds_OrgaoExpedidor, ds_Matricula, DATE_FORMAT(dt_Nascimento, '%d/%m/%Y') dt_Nascimento, ds_Sexo,
        ds_Formacao, ds_EstadoCivil, ds_NomeConjuge, nu_Dependentes, ds_Email, ds_EmailAlternativo, ds_End_Completo, ds_End_Bairro, ds_End_Cep, ds_End_Cidade, ds_End_Uf, ds_Telefone, ds_Celular, ds_TelefoneTrabalho, ds_Lotacao, ds_NomePai, ds_NomeMae, ds_Descricao FROM Pessoas
        LEFT JOIN SitPessoas ON SitPessoas.id_SitPessoas = Pessoas.id_SitPessoas_fk where id_TipoPessoas_fk = 1";
        $c->Consulta($sql);
        // Criamos uma tabela HTML com o formato da planilha para excel
        $tabela = '<table border="1">';
        $tabela .= "<thead>";
        $tabela .= "<tr>";
        $tabela .= "<th>Nome</th>";
        $tabela .= "<th>CPF</th>";
        $tabela .= "<th>RG</th>";
        $tabela .= "<th>Org�o Expedidor</th>";
        $tabela .= "<th>Matr�cula</th>";
        $tabela .= "<th>Data de Nascimento</th>";
        $tabela .= "<th>Sexo</th>";
        $tabela .= "<th>Forma��o</th>";
        $tabela .= "<th>Estado Civ�l</th>";
        $tabela .= "<th>Nome Conjuge</th>";
        $tabela .= "<th>N�mero de Dependentes</th>";
        $tabela .= "<th>E-Mail</th>";
        $tabela .= "<th>E-Mail Alternativo</th>";
        $tabela .= "<th>Endere�o Completo</th>";
        $tabela .= "<th>Bairro</th>";
        $tabela .= "<th>CEP</th>";
        $tabela .= "<th>Cidade</th>";
        $tabela .= "<th>UF</th>";
        $tabela .= "<th>Telefone</th>";
        $tabela .= "<th>Celular</th>";
        $tabela .= "<th>Telefone Trabalho</th>";
        $tabela .= "<th>Lota��o</th>";
        $tabela .= "<th>Nome Pai</th>";
        $tabela .= "<th>Nome M�e</th>";
        $tabela .= "<th>Situa��o</th>";
        $tabela .= "</tr>";
        $tabela .= "</thead>";
        $tabela .= "<tbody>";
        while ($c->Resultado()) {
            $tabela .= "<tr>";
            $tabela .= "<td>" . $c->linha['ds_Nome'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_Cpf'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_Rg'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_OrgaoExpedidor'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_Matricula'] . "</td>";
            $tabela .= "<td>" . $c->linha['dt_Nascimento'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_Sexo'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_Formacao'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_EstadoCivil'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_NomeConjuge'] . "</td>";
            $tabela .= "<td>" . $c->linha['nu_Dependentes'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_Email'] . "l</td>";
            $tabela .= "<td>" . $c->linha['ds_EmailAlternativo'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_End_Completo'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_End_Bairro'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_End_Cep'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_End_Cidade'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_End_Uf'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_Telefone'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_Celular'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_TelefoneTrabalho'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_Lotacao'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_NomePai'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_NomeMae'] . "</td>";
            $tabela .= "<td>" . $c->linha['ds_Descricao'] . "</td>";
            $tabela .= "</tr>";
        }
        $tabela .= "</tbody>";
        $tabela .= '</table>';

        // For�a o Download do Arquivo Gerado
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename=\"{$arquivo}\"");
        echo $tabela;


    }

    function Grid()
    {

        $aColunas        = array('id_Pessoas', 'ds_Nome', 'ds_Cpf', 'ds_Descricao');
        $aColunasFiltros = array('ds_Nome', 'ds_Cpf', 'ds_Descricao');
        $sColunaIndex    = "id_Pessoas";
        $sTabela         = "Pessoas LEFT JOIN SitPessoas ON id_SitPessoas = id_SitPessoas_fk";
        $sCondicao       = "WHERE id_TipoPessoas_fk = 1";
        if ($_GET['sit'] == "solicitacoes")
            $sCondicao = "Where id_SitPessoas_fk = 2 and id_TipoPessoas_fk = 1";
        if ($_GET['sit'] == "funcionarios")
            $sCondicao = "Where id_TipoPessoas_fk = 2";

        echo Grid::Gera($aColunas, $aColunasFiltros, $sColunaIndex, $sTabela, $sCondicao, NULL, array(Grid::Editar()));
    }
} 