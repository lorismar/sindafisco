<?php
/**
 * Created by PhpStorm.
 * @property Pessoas tbl
 * @property Usuarios tblUsuario
 * User: user
 */

class PerfilController extends admin_Controller
{
    function beforeAction()
    {
        $this->tbl = new Pessoas(NULL);
        $this->tbl->setUsuarios(new Usuarios(NULL));
        $this->listaDependentes = array();
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
        $this->set("listaDependentes", $this->listaDependentes);
    }

    function PrimeiroAcesso()
    {

        $this->view->setTemplate("EmBranco");
        $this->view->setTitulo("Primeiro Acesso");
        GUI::msgInformacao("Bem-vindo ao seu primeiro acesso, ter� de completar seus dados antes de avan�ar.");
        /** @var $u Usuarios */
        $u = unserialize($_SESSION['usuario']);

        $this->tbl->setIdPessoas($u->getIdPessoasFk());
        $this->tbl->VisualizarPorId();
        $this->tbl->getUsuarios()->VisualizarPorIdPessoa();
        $this->listaDependentes = Dependentes::ListarTodos("WHERE id_Pessoas_fk = " . $this->tbl->getIdPessoas());

        return "PrimeiroAcesso";
    }

    function Editar()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Editar Dados");

        /** @var $u Usuarios */
        $u = unserialize($_SESSION['usuario']);

        $this->tbl->setIdPessoas($u->getIdPessoasFk());
        $this->tbl->VisualizarPorId();
        $this->tbl->getUsuarios()->VisualizarPorIdPessoa();
        $this->listaDependentes = Dependentes::ListarTodos("WHERE id_Pessoas_fk = " . $this->tbl->getIdPessoas());
        return "Perfil";
    }

    function  Salvar()
    {
        try {
            $tbl = new Pessoas(NULL);
            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setIdPessoas($u->getIdPessoasFk());
            $tbl->VisualizarPorId();
            $tbl->setDsNome($_POST['ds_Nome']);
            $tbl->setDsRg($_POST['ds_Rg']);
            $tbl->setDsOrgaoExpedidor($_POST['ds_OrgaoExpedidor']);
            $tbl->setDsMatricula($_POST['ds_Matricula']);
            $tbl->setDtNascimento($_POST['dt_Nascimento']);
            $tbl->setDsSexo($_POST['ds_Sexo']);
            $tbl->setDsFormacao($_POST['ds_Formacao']);
            $tbl->setDsEstadoCivil($_POST['ds_EstadoCivil']);
            $tbl->setDsNomeConjuge($_POST['ds_NomeConjuge']);
            $tbl->setNuDependentes($_POST['nu_Dependentes']);
            $tbl->setDsNomePai($_POST['ds_NomePai']);
            $tbl->setDsNomeMae($_POST['ds_NomeMae']);
            $tbl->setDsEmail($_POST['ds_Email']);
            $tbl->setDsEmailAlternativo($_POST['ds_EmailAlternativo']);
            $tbl->setDsEndCompleto($_POST['ds_End_Completo']);
            $tbl->setDsEndBairro($_POST['ds_End_Bairro']);
            $tbl->setDsEndCep($_POST['ds_End_Cep']);
            $tbl->setDsEndCidade($_POST['ds_End_Cidade']);
            $tbl->setDsEndUf($_POST['ds_End_UF']);
            $tbl->setDsTelefone($_POST['ds_Telefone']);
            $tbl->setDsCelular($_POST['ds_Celular']);
            $tbl->setDsTelefoneTrabalho($_POST['ds_TelefoneTrabalho']);
            $tbl->setDsLotacao($_POST['ds_Lotacao']);
            $tbl->setIsPrimeiroAcesso(0);

            //UPLOAD
            $get_imagem = $_FILES["file_Foto"];
            if ($get_imagem['name'] == "")
                $get_imagem = "";
            if (!empty($get_imagem)) {
                $get_imagem = enviarArquivo($get_imagem, 'Pessoas', NULL, NULL);
                $tbl->setFileFoto($get_imagem);
            } else if ($_POST['file_Foto_original'] != NULL && $_POST["file_Foto_original"] != "") {
                $tbl->setFileFoto($_POST['file_Foto_original']);
            }

            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setLogALTERADOPOR($u->getDsNome());


            if ($tbl->Salvar()) {
                Dependentes::RemoverPorTitular($tbl->getIdPessoas());
                if (is_numeric($_POST['nu_Dependentes'])) {
                    for ($x = 1; $x <= $_POST['nu_Dependentes']; $x++) {
                        $dep = new Dependentes(NULL);
                        $dep->setDsNome($_POST['dep_ds_Nome_' . $x]);
                        $dep->setDtNascimento($_POST['dep_dt_Nascimento_' . $x]);
                        $dep->setDsParentesco($_POST['dep_ds_Parentesco_' . $x]);
                        $x++;
                        $dep->setIdPessoasFk($u->getIdPessoasFk());
                        $dep->Salvar();
                    }
                }
                $usuario = new Usuarios(NULL);
                $usuario->setIdUsuarios($u->getIdUsuarios());
                $usuario->setIdPessoasFk($tbl->getIdPessoas());
                $usuario->VisualizarPorIdPessoa();
                $usuario->setDsNome($tbl->getDsNome());
                $usuario->setDsUsuario($tbl->getDsCpf());
                $usuario->setDsSenha(mycrypt_encode($_POST['ds_Senha']));
                $usuario->setLogALTERADOPOR($u->getDsNome());
                $usuario->Salvar();
                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }

        echo "<script>window.location.replace('../../Principal/Dashboard/?cad=ok');</script>";
        header("location: ../../Principal/Dashboard/?cad=ok");
        exit();
    }


} 