<?php
/**
 * Created by PhpStorm.
 * @property FaleConosco tbl
 */

class FaleConoscoController extends admin_Controller
{
    function beforeAction()
    {
        $this->tbl = new FaleConosco(NULL);
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
    }

    function Listar()
    {
        $this->view->setTemplate("Admin");
        $this->view->setMenu("faleconosco_listar");

        return "Mensagens";
    }

    function ListarRespondidas()
    {
        $this->view->setTitulo("Mensagens Respondidas");
        $this->view->Grid("FaleConosco", "Grid?sit=Respondidas",array(0));

        return $this->Listar();
    }

    function ListarNaoRespondidas()
    {
        $this->view->setTitulo("Mensagens N�o Respondidas");
        $this->view->Grid("FaleConosco", "Grid?sit=NaoRespondidas",array(0));

        return $this->Listar();
    }

    function Editar()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Responder");

        if (isset($_GET['id'])) {
            $this->tbl->setIdFaleConosco($_GET['id']);
            $this->tbl->VisualizarPorId();
        }
        return "Responder";
    }

    function Salvar()
    {
        try {
            $tbl = new FaleConosco(NULL);
            if ($_POST["id_FaleConosco"] != NULL && $_POST["id_FaleConosco"] != "") {
                $tbl->setIdFaleConosco($_POST["id_FaleConosco"]);
                $tbl->VisualizarPorId();
            }
            $tbl->setTxResposta($_POST['tx_Resposta']);
            $tbl->setDhResposta($_POST['dh_Resposta']);

            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setIdUsuariosFk($u->getIdUsuarios());

            if ($tbl->Salvar()) {
                Email::RespostaContato($tbl->getDsNome(), $tbl->getDsEmail(), $tbl->getTxResposta(), $tbl->getTxTexto());
                GUI::msgSucesso("Respondido com Sucesso para " . $tbl->getDsEmail());
            } else {
                GUI::msgErro("Erro ao Responder!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }
        return $this->ListarNaoRespondidas();
    }

    function Grid()
    {
        $aColunas        = array('id_FaleConosco', 'ds_Nome', 'DATE_FORMAT(dh_Postagem, \'%d/%m/%Y %H:%i\')');
        $aColunasFiltros = array('ds_Nome');
        $sColunaIndex    = "id_FaleConosco";
        $sTabela         = "FaleConosco";
        $sCondicao       = NULL;
        if ($_GET['sit'] == "Respondidas")
            $sCondicao = "WHERE dh_Resposta is NOT NULL";
        else if ($_GET['sit'] == "NaoRespondidas")
            $sCondicao = "WHERE dh_Resposta is NULL";


        echo Grid::Gera($aColunas, $aColunasFiltros, $sColunaIndex, $sTabela, $sCondicao,NULL, array(Grid::Editar("Responder")));
    }


}