<?php
/**
 * User: Iuri Gustavo
 * @property Conteudos tbl
 */

class PaginasController extends admin_Controller
{
    function beforeAction()
    {
        $this->tbl = new Conteudos(NULL);
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
    }

    function Listar()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Paginas");
        $this->view->setMenu("paginas_listar");

        $listaPaginas = Conteudos::ListarTodos("WHERE id_CategoriasPrincipal_fk = 2 and id_Conteudos_fk IS NULL");
        $this->set("listaPaginas", $listaPaginas);
        return "Paginas";
    }

    function Editar()
    {
        if (isset($_GET['id'])) {
            $this->tbl->setIdConteudos($_GET['id']);
            $this->tbl->VisualizarPorId();
        }
        return $this->Listar();
    }

    function Salvar()
    {
        try {
            $tbl = new Conteudos(NULL);
            if ($_POST["id_Conteudos"] != NULL && $_POST["id_Conteudos"] != "") {
                $tbl->setIdConteudos($_POST["id_Conteudos"]);
            }
            $tbl->setIdCategoriasPrincipalFk(2); // P�ginas
            $tbl->setDsTitulo($_POST['ds_Titulo']);

            $tbl->setTxConteudo($_POST['tx_Conteudo']);
            $tbl->setDhCadastro(DATETIME);
            $tbl->setIsPublico(1);
            $_POST["is_Ativo"] == "TRUE" ? $tbl->setIsAtivo(1) : $tbl->setIsAtivo(0);

            $tbl->setDsPermalink(StringUtils::makeSlugs($_POST['ds_Titulo']));
            $tbl->setIdConteudosFk(NULL);

            $_POST['id_Conteudos_fk'] > 0 ? $tbl->setIdConteudosFk($_POST['id_Conteudos_fk']) : $tbl->setIdConteudosFk(NULL);

            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setIdUsuariosFk($u->getIdUsuarios());
            $tbl->setLogALTERADOPOR($u->getDsNome());

            if ($tbl->Salvar()) {
                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }
        return $this->Listar();
    }



}