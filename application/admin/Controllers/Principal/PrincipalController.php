<?php
/**
 * User: Iuri Gustavo
 * @property Conteudos tbl
 */

class PrincipalController extends admin_Controller
{
    function beforeAction()
    {
        $this->tbl = new Conteudos(NULL);
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
    }

    function Dashboard()
    {

        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Dashboard");
        $this->view->setMenu("principal_dashboard");

        /** @var $u Usuarios */
        $u                         = unserialize($_SESSION['usuario']);
        $listaInformativos         = Conteudos::ListarTodos("WHERE id_CategoriasPrincipal_fk = 4 and is_Ativo = TRUE ORDER BY Conteudos.dh_Cadastro DESC LIMIT 0,20");
        $listaInformativosContabil = Arquivos::ListarTodos("WHERE id_CategoriasPrincipal_fk = 5 and is_Ativo = TRUE and file_Arquivo is NOT NULL  ORDER BY Arquivos.dt_Cadastro DESC LIMIT 0,20");
        $listaProcessosMeus        = Conteudos::ListarTodos("Where is_Publico = FALSE and id_Conteudos in (select id_Conteudos_fk from Conteudos_tem_Pessoas WHERE id_Pessoas_fk = " . $u->getIdPessoasFk() . ") and id_CategoriasPrincipal_fk = 13");
        $listaProcessosSindicado   = Conteudos::ListarTodos("Where is_Publico = TRUE and id_CategoriasPrincipal_fk = 13");
        $listaFeeds                = Feeds::ListarTodos("Where id_Usuarios_fk = " . $u->getIdUsuarios() . " LIMIT 0,20");
        $listaPerfis               = Perfis::ListarTodos();

        $this->set("listaInformativos", $listaInformativos);
        $this->set("listaInformativosContabil", $listaInformativosContabil);
        $this->set("listaProcessosMeus", $listaProcessosMeus);
        $this->set("listaProcessosSindicado", $listaProcessosSindicado);
        $this->set("listaFeeds", $listaFeeds);
        $this->set("listaPerfis", $listaPerfis);
        $this->set("u", $u);

        if(isset($_GET['cad'])){
            GUI::msgSucesso("Perfil Salvo com Sucesso!");
        }

        return "Dashboard";
    }

    function TrocarPerfil()
    {
        /** @var $u Usuarios */
        $u = unserialize($_SESSION['usuario']);
        $u->setIdPerfisFk($_POST['id_Perfis_fk']);
        $u->getPerfis()->setIdPerfis($_POST['id_Perfis_fk']);
        $u->getPerfis()->VisualizarPorId();
        if (isset($_SESSION['usuario']))
            unset($_SESSION['usuario']);
        $_SESSION['usuario'] = serialize($u);

        echo "<script>window.location.replace('../../Principal/Dashboard/');</script>";
        header("location: ../../Principal/Dashboard/");
        exit();
    }

}