<?php
/**
 * User: Iuri Gustavo
 * @property Conteudos tbl
 */

class InformativosController extends admin_Controller
{
    function beforeAction()
    {
        $this->tbl = new Conteudos(NULL);
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
    }

    function Listar()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Informativos");
        $this->view->Grid("Informativos", "Grid", array(0));
        $this->view->setMenu("informativos_listar");

        $this->set("listaTodasSit", SitPessoas::ListarTodos());

        return "Informativos";
    }

    function Filiados()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Informativos");
        $this->view->Grid("Informativos", "GridInfo", array(0, 1));
        $this->view->setMenu("informativos_filiados");

        return "InformativosFiliados";
    }

    function Escape()
    {
        $this->view->setTemplate("Admin");
        $this->tbl->VisualizarPorPermalinkCategoriaPrivado($_GET['id'], 4);
        $this->view->setTitulo($this->tbl->getDsTitulo());
        $this->view->setMenu("informativos_filiados");

        return "Visualizar";
    }

    function Editar()
    {
        if (isset($_GET['id'])) {
            $this->tbl->setIdConteudos($_GET['id']);
            $this->tbl->VisualizarPorId();
        }
        return $this->Listar();
    }

    function Anexar()
    {

        try {
            $tbl = new Arquivos(NULL);
            $tbl->setIdConteudosFk($_POST["id_Conteudos_fk"]);
            $tbl->setDsTitulo($_POST['ds_Titulo']);
            $tbl->setDtCadastro($_POST['dt_Cadastro']);

            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setIdUsuariosFk($u->getIdUsuarios());
            $tbl->setLogALTERADOPOR($u->getDsNome());


            $get_imagem = $_FILES["file_Arquivo"];
            if ($get_imagem['name'] == "")
                $get_imagem = "";
            if (!empty($get_imagem)) {
                $get_imagem = enviarArquivo($get_imagem, 'Anexos', NULL, NULL);
                $tbl->setFileArquivo($get_imagem);
            }
            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setIdUsuariosFk($u->getIdUsuarios());

            if ($tbl->Salvar()) {
                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                // GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }

        return $this->Editar();
    }

    function Salvar()
    {
        try {
            $tbl = new Conteudos(NULL);
            if ($_POST["id_Conteudos"] != NULL && $_POST["id_Conteudos"] != "") {
                $tbl->setIdConteudos($_POST["id_Conteudos"]);
            }
            $tbl->setIdCategoriasPrincipalFk(4); // Informativos
            $tbl->setIdCategoriasFk(4); // Informativos
            $tbl->setDsTitulo($_POST['ds_Titulo']);
            //UPLOAD
            $get_imagem = $_FILES["file_Capa"];
            if ($get_imagem['name'] == "")
                $get_imagem = "";
            if (!empty($get_imagem)) {
                $get_imagem = enviarArquivo($get_imagem, 'Conteudos', NULL, NULL);
                $tbl->setFileCapa($get_imagem);
            } else if ($_POST['file_Capa_original'] != NULL && $_POST["file_Capa_original"] != "") {
                $tbl->setFileCapa($_POST['file_Capa_original']);
            }
            $tbl->setTxResumo($_POST['tx_Resumo']);
            $tbl->setTxConteudo($_POST['tx_Conteudo']);
            $tbl->setDhCadastro($_POST['dh_Cadastro']);
            $_POST["is_Ativo"] == "TRUE" ? $tbl->setIsAtivo(1) : $tbl->setIsAtivo(0);
            $_POST["is_Publico"] == "TRUE" ? $tbl->setIsPublico(1) : $tbl->setIsPublico(0);
            $_POST["is_Destaque"] == "TRUE" ? $tbl->setIsDestaque(1) : $tbl->setIsDestaque(0);
            $tbl->setIsLiberarComentarios(0);
            $tbl->setDsPermalink(StringUtils::makeSlugs($_POST['ds_Titulo']));
            $tbl->setIdConteudosFk(NULL);

            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setIdUsuariosFk($u->getIdUsuarios());
            $tbl->setLogALTERADOPOR($u->getDsNome());

            if ($tbl->Salvar()) {
                GUI::msgSucesso("Cadastrado com Sucesso!");
                if ($_POST['id_SitPessoas_fk'] > 0) {
                    // TODO: ENVIAR EMAILS PARA TODOS DA SIT


                }
            } else {
                GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }
        return $this->Listar();
    }

    function Deletar()
    {

        if (isset($_GET['did'])) {
            Conteudos::Remover($_GET['did']);
            GUI::msgAlerta("Registro removido com sucesso!");
        }
        return $this->Listar();

    }

    function RemoverAnexo()
    {
        if (!empty($_GET['anexo'])) {
            Arquivos::Remover($_GET['anexo']);
        }
        return $this->Editar();
    }

    function Grid()
    {
        $aColunas        = array('id_Conteudos', 'ds_Titulo', 'DATE_FORMAT(dh_Cadastro, \'%d/%m/%Y %H:%i\') dh_Cadastro');
        $aColunasFiltros = array('ds_Titulo');
        $sColunaIndex    = "id_Conteudos";
        $sTabela         = "Conteudos";
        $sCondicao       = "Where id_CategoriasPrincipal_fk = 4";
        $sOrdecao = array("Conteudos.dh_Cadastro DESC");
        echo Grid::Gera($aColunas, $aColunasFiltros, $sColunaIndex, $sTabela, $sCondicao, $sOrdecao, array(Grid::Editar(), Grid::Remover()));
    }

    function GridInfo()
    {
        $aColunas        = array('id_Conteudos', 'ds_PermaLink', 'ds_Titulo', 'DATE_FORMAT(dh_Cadastro, \'%d/%m/%Y %H:%i\') dh_Cadastro');
        $aColunasFiltros = array('ds_Titulo');
        $sColunaIndex    = "id_Conteudos";
        $sTabela         = "Conteudos";
        $sCondicao       = "Where id_CategoriasPrincipal_fk = 4 and is_Ativo = TRUE";
        $sOrdecao = array("Conteudos.dh_Cadastro DESC");
        echo Grid::Gera($aColunas, $aColunasFiltros, $sColunaIndex, $sTabela, $sCondicao, $sOrdecao, array(Grid::Botao("../{1}/", "Visualizar", "blue", "searcg", "_self")));
    }

}