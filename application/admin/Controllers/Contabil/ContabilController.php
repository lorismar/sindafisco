<?php
/**
 * User: Iuri Gustavo
 * @property Categorias Categoria
 * @property Arquivos tbl
 */

class ContabilController extends admin_Controller
{
    function beforeAction()
    {
        $this->Categoria = new Categorias(NULL);
        $this->tbl       = new Arquivos(NULL);
    }

    function afterAction()
    {
        $this->set("Categoria", $this->Categoria);
        $this->set("tbl", $this->tbl);
    }

    function EnviarArquivos()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Enviar Arquivos");
        $this->view->Grid("Contabil", "Grid", array(0, 3));
        $this->view->setMenu("contabil_manutencao");

        $listaCategorias = Categorias::ListarTodos("WHERE id_Categorias_fk = 5");
        $this->set("listaCategorias", $listaCategorias);

        return "Arquivos";
    }

//    function InformacoesFinanceiras()
//    {
//        $this->view->setTemplate("Admin");
//        $this->view->setTitulo("Informacoes Financeiras");
//        if (isset($_GET['id_Categorias_fk']) && is_numeric($_GET['id_Categorias_fk']))
//        $this->view->Grid("Contabil", "GridInfo?id_Categorias_fk=" . $_GET['id_Categorias_fk'], array(0, 3));
//    else
//        $this->view->Grid("Contabil", "GridInfo", array(0, 3));
//        $this->view->setMenu("contabil_informacoesfinanceiras");
//        $this->set("listaCategorias", Categorias::ListarTodos("WHERE id_Categorias_fk =5"));
//
//
//        return "InformacoesFinanceiras";
//    }

    function InformacoesFinanceiras()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Informacoes Financeiras");
        $this->view->setMenu("contabil_informacoesfinanceiras");


        return "PastaVisualizacao";
    }

    function Editar()
    {
        if (isset($_GET['id'])) {
            $this->tbl->setIdArquivos($_GET['id']);
            $this->tbl->VisualizarPorId();
        }
        return $this->Pasta();
    }

//
//    function Salvar()
//    {
//        try {
//            $tbl = new Arquivos(NULL);
//            if ($_POST["id_Arquivos"] != NULL && $_POST["id_Arquivos"] != "") {
//                $tbl->setIdArquivos($_POST["id_Arquivos"]);
//            }
//            $tbl->setIdCategoriasPrincipalFk(5);
//            $tbl->setDsTitulo($_POST['ds_Titulo']);
//            //UPLOAD
//            $get_arquivo = $_FILES["file_Arquivo"];
//            if ($get_arquivo['name'] == "")
//                $get_arquivo = "";
//            if (!empty($get_arquivo)) {
//                $get_arquivo = enviarArquivo($get_arquivo, 'Contabil', NULL, NULL);
//                $tbl->setFileArquivo($get_arquivo);
//            } else if ($_POST['file_Arquivo_original'] != NULL && $_POST["file_Arquivo_original"] != "") {
//                $tbl->setFileArquivo($_POST['file_Arquivo_original']);
//            }
//
//            $tbl->setDsDescricao($_POST['ds_Descricao']);
//            $tbl->setDtCadastro($_POST['dt_Cadastro']);
//            $_POST["is_Ativo"] == "TRUE" ? $tbl->setIsAtivo(1) : $tbl->setIsAtivo(0);
//            $tbl->setIdCategoriasFk($_POST['id_Categorias_fk']);
//
//            /** @var $u Usuarios */
//            $u = unserialize($_SESSION['usuario']);
//            $tbl->setIdUsuariosFk($u->getIdUsuarios());
//            $tbl->setLogALTERADOPOR($u->getDsNome());
//
//            if ($tbl->Salvar()) {
//                GUI::msgSucesso("Cadastrado com Sucesso!");
//            } else {
//                GUI::msgErro("Erro ao Cadastrar!");
//            }
//        } catch (Exception $ext) {
//            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
//        }
//        return $this->EnviarArquivos();
//    }


    function Salvar()
    {
        try {
            $tbl = new Arquivos(NULL);
            if ($_POST["id_Arquivos"] != NULL && $_POST["id_Arquivos"] != "") {
                $tbl->setIdArquivos($_POST["id_Arquivos"]);
            }
            $tbl->setIdCategoriasPrincipalFk(5);
            $tbl->setDsTitulo($_POST['ds_Titulo']);
            //UPLOAD
            $get_arquivo = $_FILES["file_Arquivo"];
            if ($get_arquivo['name'] == "")
                $get_arquivo = "";
            if (!empty($get_arquivo)) {
                $get_arquivo = enviarArquivo($get_arquivo, 'Contabil', NULL, NULL);
                $tbl->setFileArquivo($get_arquivo);
            } else if ($_POST['file_Arquivo_original'] != NULL && $_POST["file_Arquivo_original"] != "") {
                $tbl->setFileArquivo($_POST['file_Arquivo_original']);
            }

            $tbl->setDtCadastro($_POST['dt_Cadastro']);
            $tbl->setIsAtivo(1);
            is_numeric($_POST['id_Arquivos_fk']) ? $tbl->setIdArquivosFk($_POST['id_Arquivos_fk']) : $tbl->setIdArquivosFk(NULL);

            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setIdUsuariosFk($u->getIdUsuarios());
            $tbl->setLogALTERADOPOR($u->getDsNome());

            if ($tbl->Salvar()) {
                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }
        return $this->Pasta();
    }


    function ListarCategorias()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Categorias");
        $this->view->setMenu("contabil_manutencao");

        $listaCategorias = Categorias::ListarTodos("WHERE id_Categorias_fk = 5");
        $this->set("listaCategorias", $listaCategorias);
        return "Categorias";
    }

    function EditarCategoria()
    {
        if (isset($_GET['id'])) {
            $this->Categoria->setIdCategorias($_GET['id']);
            $this->Categoria->VisualizarPorId();
//            if ($this->Categoria->getIdCategoriasFk() != 5) {
//                unset($this->Categoria);
//                $this->Categoria = new Categorias(NULL);
//            }
        }
        return $this->ListarCategorias();
    }

    function DeletarCategoria()
    {
        if (isset($_GET['did'])) {
            Arquivos::Remover($_GET['did']);
            GUI::msgSucesso("Removido com Sucesso!");
        }
        return $this->ListarCategorias();

    }

    function SalvarCategoria()
    {
        try {
            $tbl = new Categorias(NULL);

            if ($_POST["id_Categorias"] != NULL && $_POST["id_Categorias"] != "") {
                $tbl->setIdCategorias($_POST["id_Categorias"]);
            }
            $tbl->setDsDescricao($_POST['ds_Descricao']);
            $tbl->setIdCategoriasFk(5);

            if ($tbl->Salvar()) {
                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }
        return $this->ListarCategorias();
    }

    function Pasta()
    {

        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Pastas");
        $this->view->setMenu("contabil_manutencao");


        $contador         = 0;
        $contador_posicao = $contador;
        $arvore           = "";

        /** @var $lista Arquivos */
        foreach (Arquivos::ListarTodos("WHERE id_CategoriasPrincipal_fk = 5 and id_Arquivos_fk IS NULL ORDER BY id_Arquivos ASC") as $lista) {
            $contador++;
            $icone = retornaIcone($lista->getFileArquivo());
            $arvore .= "\nd.add({$contador},{$contador_posicao},'{$lista->getDsTitulo()}','../Editar/?id={$lista->getIdArquivos()}&dtree=adicionar','','','{$icone}');";
            $contador_pai = $contador;

            /** @var $f1 Arquivos */
            foreach (Arquivos::ListarTodosIdPai(5,$lista->getIdArquivos()) as $f1) {
                $contador++;
                $icone = retornaIcone($f1->getFileArquivo());
                $arvore .= "\nd.add({$contador},{$contador_pai},'{$f1->getDsTitulo()}','../Editar/?id={$f1->getIdArquivos()}&dtree=adicionar','','','{$icone}');";
                $contador_f1 = $contador;

                /** @var $f2 Arquivos */
                foreach (Arquivos::ListarTodosIdPai(5,$f1->getIdArquivos()) as $f2) {
                    $contador++;
                    $icone = retornaIcone($f2->getFileArquivo());
                    $arvore .= "\nd.add({$contador},{$contador_f1},'{$f2->getDsTitulo()}','../Editar/?id={$f2->getIdArquivos()}&dtree=adicionar','','','{$icone}');";
                    $contador_f2 = $contador;

                    /** @var $f3 Arquivos */
                    foreach (Arquivos::ListarTodosIdPai(5,$f2->getIdArquivos()) as $f3) {
                        $contador++;
                        $icone = retornaIcone($f3->getFileArquivo());
                        $arvore .= "\nd.add({$contador},{$contador_f2},'{$f3->getDsTitulo()}','../Editar/?id={$f3->getIdArquivos()}&dtree=editar','','','{$icone}');";
                    }

                }

            }
        }
        $this->set("arvore", $arvore);


        return "Pasta";
    }

    function Adicionar()
    {
        $this->tbl->setArquivosPai(new Arquivos(NULL));
        if (isset($_GET['fk'])) {
            $this->tbl->setIdArquivosFk($_GET['fk']);
            $this->tbl->getArquivosPai()->VisualizarPorId();
        }

        return $this->Pasta();
    }

    function Deletar()
    {

        if (isset($_GET['did'])) {
            Arquivos::Remover($_GET['did']);
            GUI::msgAlerta("Registro removido com sucesso!");
        }
        return $this->Pasta();

    }

    function Grid()
    {
        $aColunas        = array('id_Arquivos', 'ds_Titulo', 'c.ds_Descricao', 'file_Arquivo', 'DATE_FORMAT(dt_Cadastro, \'%d/%m/%Y\')');
        $aColunasFiltros = array('ds_Titulo', 'c.ds_Descricao');
        $sColunaIndex    = "id_Arquivos";
        $sTabela         = "Arquivos LEFT JOIN Categorias c ON Arquivos.id_Categorias_fk = c.id_Categorias";
        $sCondicao       = "Where id_CategoriasPrincipal_fk = 5";
        echo Grid::Gera($aColunas, $aColunasFiltros, $sColunaIndex, $sTabela, $sCondicao, NULL, array(Grid::Editar(), Grid::Remover(), Grid::Botao(CONTENT . "upload/Contabil/{3}", "Download", "blue", "download", "_new")));
    }

    function GridInfo()
    {
        $aColunas        = array('id_Arquivos', 'ds_Titulo', 'c.ds_Descricao', 'file_Arquivo', 'DATE_FORMAT(dt_Cadastro, \'%d/%m/%Y\')');
        $aColunasFiltros = array('ds_Titulo', 'c.ds_Descricao');
        $sColunaIndex    = "id_Arquivos";
        $sTabela         = "Arquivos LEFT JOIN Categorias c ON Arquivos.id_Categorias_fk = c.id_Categorias";
        if (isset($_GET['id_Categorias_fk']))
            $sCondicao = "Where id_CategoriasPrincipal_fk = 5 and is_Ativo = TRUE and Arquivos.id_Categorias_fk = " . $_GET['id_Categorias_fk'];
        else
            $sCondicao = "Where id_CategoriasPrincipal_fk = 5 and is_Ativo = TRUE";
        echo Grid::Gera($aColunas, $aColunasFiltros, $sColunaIndex, $sTabela, $sCondicao,NULL, array(Grid::Botao(CONTENT . "upload/Contabil/{3}", "Download", "blue", "download", "_blank")));

    }


}