<?php
/**
 * Created by PhpStorm.
 * @property Usuarios tbl
 * User: user
 */

class LoginController extends admin_Controller
{
    function beforeAction()
    {
        $this->tbl = new Usuarios(NULL);
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
    }

    function Logar()
    {
        $usuario = aplicarMascara($_POST['cpf'],'###.###.###-##');
        $senha   = mycrypt_encode($_POST['senha']);

        $u = new Usuarios(NULL);
        $u->setDsUsuario($usuario);
        $u->setDsSenha($senha);

        if ($u->Logar()) {
            $u->getPerfis()->VisualizarPorId();
            $u->getPessoas()->VisualizarPorId();
            if ($u->getPessoas()->getIdSitPessoasFk() == 1 || $u->getPessoas()->getIdSitPessoasFk() == 4) {
                if ($u->getPessoas()->getIsPrimeiroAcesso()) {
                    $_SESSION['usuario'] = serialize($u);
                    echo "<script>window.location.replace('../../Perfil/Editar/');</script>";
                    header("location: ../../Perfil/PrimeiroAcesso/");
                    exit();
                }
                $_SESSION['usuario'] = serialize($u);
                echo "<script>window.location.replace('../../Principal/Dashboard/');</script>";
                header("location: ../../Principal/Dashboard/");
                exit();
            } else {
                echo "<script>window.location.replace('../../../Site/Principal/?erro=102');</script>";
                header("location: ../../../Site/Principal/?erro=102");
                exit();
            }
        } else {
            echo "<script>window.location.replace('../../../Site/Principal/?erro=101');</script>";
            header("location: ../../../Site/Principal/?erro=101");
            exit();
        }
    }

    function Logout()
    {
        session_destroy();
        echo "<script>window.location.replace('../../../Site/Principal/');</script>";
        header("location: ../../../Site/Principal/");
        exit();
    }


}