<?php
/**
 * User: Iuri Gustavo
 * @property Conteudos tbl
 */

class GaleriasController extends admin_Controller
{
    function beforeAction()
    {
        $this->tbl = new Conteudos(NULL);
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
    }

    function Listar()
    {
        $this->view->setTemplate("Admin");
        $this->view->setTitulo("Galerias");
        $this->view->Grid("Galerias", "Grid", array(0));
        $this->view->setMenu("galerias_listar");
        return "Galerias";
    }

    function Editar()
    {
        if (isset($_GET['id'])) {
            $this->tbl->setIdConteudos($_GET['id']);
            $this->tbl->VisualizarPorId();
            $listaArquivos = Arquivos::ListarTodos("WHERE id_Conteudos_fk = " . $_GET['id']);
            $this->set("listaArquivos", $listaArquivos);
        }
        return $this->Listar();
    }

    function Salvar()
    {
        try {
            $tbl = new Conteudos(NULL);
            if ($_POST["id_Conteudos"] != NULL && $_POST["id_Conteudos"] != "") {
                $tbl->setIdConteudos($_POST["id_Conteudos"]);
            }
            $tbl->setIdCategoriasPrincipalFk(3); // Galerias
            $tbl->setDsTitulo($_POST['ds_Titulo']);
            //UPLOAD
            $get_imagem = $_FILES["file_Capa"];
            if ($get_imagem['name'] == "")
                $get_imagem = "";
            if (!empty($get_imagem)) {
                $get_imagem = enviarArquivo($get_imagem, 'Galerias', NULL, NULL);
                $tbl->setFileCapa($get_imagem);
            } else if ($_POST['file_Capa_original'] != NULL && $_POST["file_Capa_original"] != "") {
                $tbl->setFileCapa($_POST['file_Capa_original']);
            }
            $tbl->setTxConteudo($_POST['tx_Conteudo']);
            $tbl->setDhCadastro($_POST['dh_Cadastro']);
            $_POST["is_Ativo"] == "TRUE" ? $tbl->setIsAtivo(1) : $tbl->setIsAtivo(0);
            $_POST["is_LiberarComentarios"] == "TRUE" ? $tbl->setIsLiberarComentarios(1) : $tbl->setIsLiberarComentarios(0);
            $tbl->setIsPublico(1);
            $tbl->setDsPermalink(StringUtils::makeSlugs($_POST['ds_Titulo']));

            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $tbl->setIdUsuariosFk($u->getIdUsuarios());
            $tbl->setLogALTERADOPOR($u->getDsNome());

            if ($tbl->Salvar()) {
                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }
        return $this->Editar();
    }

    function Upload()
    {
        try {
            $img = new Arquivos(NULL);
            $img->setIdCategoriasFk(3);
            $img->setIdConteudosFk($_GET['id']);
            $img->setDtCadastro(DATE);
            $img->setDsTitulo("Album[" . $_GET['id'] . "]");

            $get_imagem = $_FILES["file"];
            if ($get_imagem['name'] == "")
                $get_imagem = "";
            if (!empty($get_imagem)) {
                $get_imagem = enviarArquivo($get_imagem, intval($_GET['id']), NULL, NULL);
                $img->setFileArquivo($get_imagem);
            }
            /** @var $u Usuarios */
            $u = unserialize($_SESSION['usuario']);
            $img->setIdUsuariosFk($u->getIdUsuarios());
            $img->setLogALTERADOPOR($u->getDsNome());
            if ($img->Salvar()) {
                GUI::msgSucesso("Cadastrado com Sucesso!");
            } else {
                GUI::msgErro("Erro ao Cadastrar!");
            }
        } catch (Exception $ext) {
            echo "<script>window.alert('" . addslashes($ext->getMessage()) . "');</script>";
        }
    }

    function RemoverFoto()
    {

        Arquivos::Remover($_GET['foto']);
        GUI::msgAlerta("Foto removido com sucesso!");

        return $this->Editar();

    }


    function Deletar()
    {

        if (isset($_GET['did'])) {
            Conteudos::Remover($_GET['did']);
            GUI::msgAlerta("Registro removido com sucesso!");
        }
        return $this->Listar();

    }

    function Grid()
    {
        $aColunas        = array('id_Conteudos', 'ds_Titulo', 'DATE_FORMAT(dh_Cadastro, \'%d/%m/%Y %H:%i\')');
        $aColunasFiltros = array('ds_Titulo');
        $sColunaIndex    = "id_Conteudos";
        $sTabela         = "Conteudos";
        $sCondicao       = "WHERE id_CategoriasPrincipal_fk = 3";
        $sBotaoExtra     = array("<a href='../Editar/?id={0}#tab_1_3' class='btn default btn-xs green'><i class='fa fa-camera'> Fotos</i></a> ",Grid::Remover());
        $sOrdecao = array("Conteudos.dh_Cadastro DESC");
        echo Grid::Gera($aColunas, $aColunasFiltros, $sColunaIndex, $sTabela, $sCondicao, $sOrdecao, $sBotaoExtra);
    }

}