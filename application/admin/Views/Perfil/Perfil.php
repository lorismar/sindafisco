<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var Pessoas $tbl
 */




?>

<div class="tabbable-custom ">

    <ul class="nav nav-tabs">
        <li id="tab_registros">
            <a href="#tab_1_1" data-toggle="tab">Edi��o</a>
        </li>

    </ul>

    <div class="tab-content" style="border: 1px solid #ddd;">

        <div class="tab-pane portlet-body form" id="tab_1_1">
            <form action="../Salvar/" method="post" id="validate" class="form-horizontal" enctype="multipart/form-data">

                <div class="form-body">

                    <h3 class="form-section">Informa��es</h3>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Cpf">CPF:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Cpf" id="ds_Cpf" class="form-control validate[required] maskCpf" readonly maxlength="14" value="<?= $tbl->getDsCpf() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Senha">Senha:</label>

                        <div class="col-md-9 ">
                            <input type="password" name="ds_Senha" id="ds_Senha" class="form-control" maxlength="20" value="<?= mycrypt_decode($tbl->getUsuarios()->getDsSenha()) ?>"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Rg">RG:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Rg" id="ds_Rg" class="form-control validate[required]" maxlength="20" value="<?= $tbl->getDsRg() ?>"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_OrgaoExpedidor">Org�o Expedidor:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_OrgaoExpedidor" id="ds_OrgaoExpedidor" class="form-control validate[required]" maxlength="10" value="<?= $tbl->getDsOrgaoExpedidor() ?>"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Nome">Nome:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Nome" id="ds_Nome" class="form-control validate[required]" maxlength="100" value="<?= $tbl->getDsNome() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="file_Foto">Foto</label>

                        <div class="col-md-9">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="<?= $tbl->getFileFoto() != NULL ? "../../../public/upload/Pessoas/thumbs/" . $tbl->getFileFoto() : ""; ?>" alt=""/>
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
													<span class="btn default btn-file">
													<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar Imagem</span>
													<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
													<input type="file" class="default" id="file_Foto" name="file_Foto"/>
                                                          <input type="hidden" id="file_Foto_original" name="file_Foto_original"
                                                                 value="<?= $tbl->getFileFoto() ?>">
													</span>
                                    <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a>
                                </div>
                            </div>
                            <span class="label label-danger">NOTA!</span>
											<span>
											Suportado somente nas �ltimas vers�es do Firefox, Chrome, Opera,
											Safari e Internet Explorer 10.
											</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Matricula">Matr�cula:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Matricula" id="ds_Matricula" class="form-control validate[required]" maxlength="20" value="<?= $tbl->getDsMatricula() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="dt_Nascimento">Data de Nascimento:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="dt_Nascimento" id="dt_Nascimento" class="form-control validate[required] maskDate" maxlength="30" value="<?= $tbl->getDtNascimento() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Sexo">Sexo</label>

                        <div class="col-md-9 ">
                            <select name="ds_Sexo" id="ds_Sexo" class="form-control validate[required]">
                                <option value="M" <?= $tbl->getDsSexo() == "M" ? "selected" : "" ?>>Masculino</option>
                                <option value="F" <?= $tbl->getDsSexo() == "F" ? "selected" : "" ?>>Feminino</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_EstadoCivil">Estado Civ�l</label>

                        <div class="col-md-9 ">
                            <select name="ds_EstadoCivil" id="ds_EstadoCivil" class="form-control validate[required]">
                                <option value="Solteiro" <?= $tbl->getDsEstadoCivil() == "Solteiro" ? "selected" : "" ?>>Solteiro</option>
                                <option value="Casado" <?= $tbl->getDsEstadoCivil() == "Casado" ? "selected" : "" ?>>Casado</option>
                                <option value="Divorciado" <?= $tbl->getDsEstadoCivil() == "Divorciado" ? "selected" : "" ?>>Divorciado</option>
                                <option value="Outros" <?= $tbl->getDsEstadoCivil() == "Outros" ? "selected" : "" ?>>Outros</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Formacao">Forma�ao:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Formacao" id="ds_Formacao" class="form-control validate[required]" maxlength="100" value="<?= $tbl->getDsFormacao() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_NomePai">Nome do Pai:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_NomePai" id="ds_NomePai" class="form-control" maxlength="100" value="<?= $tbl->getDsNomePai() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_NomeMae">Nome da M�e:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_NomeMae" id="ds_NomeMae" class="form-control" maxlength="100" value="<?= $tbl->getDsNomeMae() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_NomeConjuge">Nome do C�njuge:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_NomeConjuge" id="ds_NomeConjuge" class="form-control" maxlength="100" value="<?= $tbl->getDsNomeConjuge() ?>"/>
                        </div>
                    </div>
                    <h3 class="form-section">Dependentes</h3>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="nu_Dependentes">N�mero de Dependentes:</label>
                        <div class="col-md-9 ">
                            <input type="text" name="nu_Dependentes" id="nu_Dependentes" class="form-control validate[custom[onlyNumberSp]]" maxlength="2" value="<?= $tbl->getNuDependentes() ?>" onkeyup="AlteraNumeroDependentes()"/>
                        </div>
                    </div>

                    <div id="dependentes" class="form-group">
                        <?php /** @var $dep Dependentes */
                        $x = 1;
                        foreach ($listaDependentes as $dep):?>
                        <div class="col-md-5 col-lg-5"><div class="form-group"><label class="control-label" for="dep_ds_Nome_<?=$x?>">Nome do Dependente:</label><input type="text" name="dep_ds_Nome_<?=$x?>" id="dep_ds_Nome_<?=$x?>" class="form-control validate[required]" maxlength="100" value="<?= $dep->getDsNome() ?>"/></div></div>
                        <div class="col-md-3 col-lg-3"><div class="form-group"><label class="control-label" for="dep_ds_Parentesco_<?=$x?>">Parentesco:</label><input type="text" name="dep_ds_Parentesco_<?=$x?>" id="dep_ds_Parentesco_<?=$x?>" class="form-control validate[required]" maxlength="100" value="<?=$dep->getDsParentesco()?>"/></div></div>
                        <div class="col-md-3 col-lg-3"><div class="form-group"><label class="control-label" for="dep_dt_Nascimento_<?=$x?>">Data de Nascimento:</label><input type="text" name="dep_dt_Nascimento_<?=$x?>" id="dep_dt_Nascimento_<?=$x?>" class="form-control maskDate validate[required]" maxlength="100" value="<?= $dep->getDtNascimento() ?>"/></div></div>
                        <?php
                        $x++;
                        endforeach;
                        ?>
                    </div>

                    <h3 class="form-section">Contato</h3>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Email">E-Mail:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Email" id="ds_Email" class="form-control validate[custom[email],required]" maxlength="100" value="<?= $tbl->getDsEmail() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_EmailAlternativo">E-Mail Alternativo:</label>

                        <div class="col-md-9 ">
                        <input type="text" name="ds_EmailAlternativo" id="ds_EmailAlternativo" class="form-control validate[custom[email]]" maxlength="100" value="<?= $tbl->getDsEmailAlternativo() ?>"/>
                    </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Telefone">Telefone:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Telefone" id="ds_Telefone" class="form-control validate[required] maskTel" maxlength="14" value="<?= $tbl->getDsTelefone() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Celular">Celular:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Celular" id="ds_Celular" class="form-control validate[required] maskCel" maxlength="15" value="<?= $tbl->getDsCelular() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_TelefoneTrabalho">Telefone Trabalho:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_TelefoneTrabalho" id="ds_TelefoneTrabalho" class="form-control maskTel" maxlength="14" value="<?= $tbl->getDsTelefoneTrabalho() ?>"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Lotacao">Lota��o:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Lotacao" id="ds_Lotacao" class="form-control" maxlength="100" value="<?= $tbl->getDsLotacao() ?>"/>
                        </div>
                    </div>

                    <h3 class="form-section">Endere�o Residencial</h3>



                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_End_Completo">Endere�o</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_End_Completo" id="ds_End_Completo" class="form-control validate[required]" maxlength="200" value="<?= $tbl->getDsEndCompleto() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_End_Bairro">Bairro</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_End_Bairro" id="ds_End_Bairro" class="form-control validate[required]" maxlength="100" value="<?= $tbl->getDsEndBairro() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_End_Cep">CEP</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_End_Cep" id="ds_End_Cep" class="form-control validate[required] maskCep" maxlength="9" value="<?= $tbl->getDsEndCep() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_End_Cidade">Cidade</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_End_Cidade" id="ds_End_Cidade" class="form-control" maxlength="60" value="<?= $tbl->getDsEndCidade() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_End_UF">UF</label>

                        <div class="col-md-9">
                            <input type="text" name="ds_End_UF" id="ds_End_UF" class="form-control" maxlength="2" value="<?= $tbl->getDsEndUf() ?>"/>
                        </div>
                    </div>





                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green" name="action" value="Editar"><i class="fa fa-pencil"></i> Salvar</button>
                            </div>
                        </div>
                    </div>



                </div>

            </form>
        </div>

    </div>
</div>
