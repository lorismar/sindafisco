<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var Conteudos $tbl
 */

?>
<link href="<?= CONTENT_ADMIN ?>assets/css/pages/blog.css" rel="stylesheet" type="text/css"/>


<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12 blog-page">
        <div class="row">
            <div class="col-md-12 article-block">
                <div class="blog-tag-data">
                    <?php if ($tbl->getFileCapa() != NULL): ?>
                        <img src="../../../public/upload/Conteudos/<?= $tbl->getFileCapa() ?>" class="img-responsive" alt="">
                    <?php endif; ?>
                    <div class="row">

                        <div class="col-md-12 blog-tag-data-inner">
                            <ul class="list-inline">
                                <li>
                                    <i class="fa fa-calendar"></i><a href="#"><?= $tbl->getDhCadastro() ?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--end news-tag-data-->
                <div>
                   <?= $tbl->getTxConteudo() ?>
                </div>
                <hr>
                <?php
                $listaArquivos = $tbl->Arquivos();
                if ($listaArquivos): ?>
                    <section class="post-content">
                        <h3>Lista de Anexos:</h3>
                        <ul class="">
                            <?php /** @var $anexo Arquivos */
                            foreach ($listaArquivos as $anexo): ?>
                                <li><i class="icon-arrow-down"></i> <a href="<?= CONTENT ?>upload/Anexos/<?= $anexo->getFileArquivo() ?>" target="_blank">Fazer download de <?= $anexo->getDsTitulo() ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </section>
                <?php endif; ?>
            </div>
            <!--end col-md-9-->

        </div>
    </div>
</div>