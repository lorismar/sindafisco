<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var Conteudos $tbl
 */

?>

<div class="tabbable-custom ">
    <ul class="nav nav-tabs">
        <li id="tab_registros">
            <a href="#tab_1_1" data-toggle="tab">Registros</a>
        </li>
        <li id="tab_editcad">
            <a href="#tab_1_2" data-toggle="tab"> <?= isset($_GET["id"]) != NULL ? "Editar" : "Cadastrar" ?></a>
        </li>

    </ul>

    <div class="tab-content" style="border: 1px solid #ddd;">
        <div class="tab-pane" id="tab_1_1">
            <div class="portlet-body">
                <div class="table-responsive">
                    <!--COME�O TABELA PRINCIPAL ID->tabelaPrincipal  -->
                    <table class="table table-condensed table-hover" id="tabelaPrincipal">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Titulo</th>
                            <th>Data de Cadastro</th>
                            <th>A�ao</th>

                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <!--FIM TABELA PRINCIPAL -->
                </div>
            </div>
        </div>
        <div class="tab-pane portlet-body form" id="tab_1_2">
            <form action="<?= $Html->ActionLink(NULL, "Salvar") ?>" method="post" id="validate" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-body">

                    <div class="form-group display-hide">
                        <label class="control-label col-md-3 col-lg-1" for="id_Conteudos">ID</label>

                        <div class="col-md-2">
                            <input type="text" name="id_Conteudos" id="id_Conteudos" class="form-control" readonly value="<?= $tbl->getIdConteudos() ?>"/>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="ds_Titulo">T�tulo do Conte�do</label>

                    <div class="col-md-9 ">
                        <input type="text" name="ds_Titulo" id="ds_Titulo" class="form-control validate[required]" maxlength="100" value="<?= $tbl->getDsTitulo() ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="file_Capa">Foto da Capa</label>

                    <div class="col-md-9">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="<?= $tbl->getFileCapa() != NULL ? "../../../public/upload/Conteudos/thumbs/" . $tbl->getFileCapa() : ""; ?>" alt=""/>
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            <div>
													<span class="btn default btn-file">
													<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar Imagem</span>
													<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
													<input type="file" class="default" id="file_Capa" name="file_Capa"/>
                                                          <input type="hidden" id="file_Capa_original" name="file_Capa_original"
                                                                 value="<?= $tbl->getFileCapa() ?>">
													</span>
                                <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a>
                            </div>
                        </div>
                        <span class="label label-danger">NOTA!</span>
											<span>
											Suportado somente nas �ltimas vers�es do Firefox, Chrome, Opera,
											Safari e Internet Explorer 10.
											</span>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="tx_Resumo">Resumo do Texto</label>

                    <div class="col-md-9">
                        <textarea class="form-control" name="tx_Resumo" id="tx_Resumo" rows="6"><?= $tbl->getTxResumo() ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="tx_Conteudo">Texto</label>

                    <div class="col-md-9">
                        <textarea class="ckeditor form-control validate[required]" name="tx_Conteudo" id="tx_Conteudo" rows="6"><?= $tbl->getTxConteudo() ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="dh_Cadastro">Data da Publica��o</label>

                    <div class="col-md-9 ">
                        <input type="text" name="dh_Cadastro" id="dh_Cadastro" class="form-control maskDateTime validate[required]" value="<?= $tbl->getDhCadastro() == NULL ? DATETIME : $tbl->getDhCadastro() ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="is_Destaque">Destaque ?</label>

                    <div class="col-md-9">
                        <div class="make-switch" data-on-label="Sim" data-off-label="N�o">
                            <input type="checkbox" name="is_Destaque" id="is_Destaque" value="TRUE"  <?= $tbl->getIsDestaque() == "0" ? "" : "checked" ?> class="toggle"/>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="is_Publico">Publico ?</label>

                    <div class="col-md-9">
                        <div class="make-switch" data-on-label=" P�blico " data-off-label=" Privado ">
                            <input type="checkbox" name="is_Publico" id="is_Publico" value="TRUE"  <?= $tbl->getIsPublico() == "0" ? "" : "checked" ?> class="toggle"/>
                        </div>
                        <span class="label label-danger">NOTA!</span>
											<span>
											Marque "P�blico" para ser exibido no site ou "Privado" para somente na Intranet.
											</span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="is_Ativo">Ativo ?</label>

                    <div class="col-md-9">
                        <div class="make-switch" data-on-label="Sim" data-off-label="N�o">
                            <input type="checkbox" name="is_Ativo" id="is_Ativo" value="TRUE"  <?= $tbl->getIsAtivo() == "0" ? "" : "checked" ?> class="toggle"/>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="id_SitPessoas_fk">Enviar Informativo Para:</label>

                    <div class="col-md-9 ">
                        <select name="id_SitPessoas_fk" id="id_SitPessoas_fk" class="form-control">
                            <option value=""></option>
                            <?php
                            /** @var $sit SitPessoas */
                            foreach ($listaTodasSit as $sit) {
                                $selected = "";

                                ?>
                                <option value="<?= $sit->getIdSitPessoas() ?>"  <?= $selected ?> ><?=  $sit->getDsDescricao() ?></option>

                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <?php if (isset($_GET["id"]) == NULL) { ?>
                                    <button type="submit" class="btn blue" name="action" value="Salvar"><i class="fa fa-check"></i> Cadastrar</button>
                                <?php } else { ?>
                                    <button type="submit" class="btn green" name="action" value="Editar"><i class="fa fa-pencil"></i> Salvar</button>
                                <?php } ?>
                                <button type="button" class="btn default" onclick="location.href='../Listar/'">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>

            <?php if ($tbl->getIdConteudos() > 0): ?>
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-comments"></i>Anexos do Conte�do</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <form method="post" class="form-horizontal" name="form" enctype="multipart/form-data" id="validate"
                              autocomplete="off"
                              action="../Anexar/?id=<?= $_GET['id'] ?>">

                            <div class="control-group display-hide">
                                <label for="id_Arquivos" class="control-label">ID</label>

                                <div class="controls">
                                    <input name="id_Conteudos_fk" id="id_Conteudos_fk" type="text" style="background-color: #bbbbbb"
                                           readonly="readonly" class="span6 m-wrap"
                                           value="<?= $tbl->getIdConteudos() ?>"/>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-lg-1" for="dh_Cadastro">Data da Publica��o</label>

                                <div class="col-md-9 ">
                                    <input type="text" name="dt_Cadastro" id="dt_Cadastro" class="form-control maskDate validate[required]" value="<?= DATE ?>"/>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="ds_Nome" class="control-label col-md-3 col-lg-1">Nome do Arquivo</label>

                                <div class="col-md-9">
                                    <input class="form-control m-wrap validate[required]" name="ds_Titulo" id="ds_Titulo"
                                           type="text"
                                           maxlength="100" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-lg-1">Arquivo</label>

                                <div class="col-md-9">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <input type="file" id="file_Arquivo" name="file_Arquivo">
                                        <!--<span>Nenhum arquivo</span>-->


                                    </div>


                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue" name="action" value="Salvar">Enviar Arquivo</button>
                                    </div>
                                </div>
                            </div>


                        </form>


                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>T�tulo</th>
                                <th>Arquivo</th>
                                <th>A��o</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php /** @var $anexo Arquivos */
                            foreach ($tbl->Arquivos() as $anexo): ?>
                                <tr>
                                    <td><?= $anexo->getIdArquivos() ?></td>
                                    <td><?= $anexo->getDsTitulo() ?></td>
                                    <td><a href="<?= CONTENT ?>upload/Anexos/<?= $anexo->getFileArquivo() ?>" target="_blank"><?= $anexo->getDsTitulo() ?></a></td>
                                    <td>
                                        <a href='../RemoverAnexo/?id=<?= $anexo->getIdConteudosFk() ?>&anexo=<?= $anexo->getIdArquivos() ?>' class='btn mini red'><i class='icon-trash'></i> Remover</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php endif; ?>


        </div>

    </div>
</div>
