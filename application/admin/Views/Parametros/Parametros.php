<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var Parametros $tbl
 */


?>

<div class="tabbable-custom ">
    <ul class="nav nav-tabs">
        <li id="tab_registros">
            <a href="#tab_1_1" data-toggle="tab">Registros</a>
        </li>
        <li id="tab_editcad">
            <a href="#tab_1_2" data-toggle="tab"> <?= isset($_GET["id"]) != NULL ? "Editar" : "Cadastrar" ?></a>
        </li>

    </ul>

    <div class="tab-content" style="border: 1px solid #ddd;">
        <div class="tab-pane" id="tab_1_1">
            <div class="portlet-body">
                <div class="table-responsive">
                    <!--COME�O TABELA PRINCIPAL ID->tabelaPrincipal  -->
                    <table class="table table-condensed table-hover" id="tabelaPrincipal">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Parametro</th>
                            <th>Valor</th>
                            <th>A�ao</th>

                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <!--FIM TABELA PRINCIPAL -->
                </div>
            </div>
        </div>
        <div class="tab-pane portlet-body form" id="tab_1_2">
            <form action="<?= $Html->ActionLink(NULL, "Salvar") ?>" method="post" id="validate" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-body">

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="id_Parametros">ID</label>

                        <div class="col-md-2">
                            <input type="text" name="id_Parametros" id="id_Parametros" class="form-control" readonly value="<?= $tbl->getIdParametros() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Descricao">Parametro</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Descricao" id="ds_Descricao" class="form-control validate[required]" maxlength="100" value="<?= $tbl->getDsDescricao() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Valor">Valor</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Valor" id="ds_Valor" class="form-control" maxlength="100" value="<?= $tbl->getDsValor() ?>"/>
                        </div>
                    </div>


                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-offset-3 col-md-9">
                                    <?php if (isset($_GET["id"]) == NULL) { ?>
                                        <button type="submit" class="btn blue" name="action" value="Salvar"><i class="fa fa-check"></i> Cadastrar</button>
                                    <?php } else { ?>
                                        <button type="submit" class="btn green" name="action" value="Editar"><i class="fa fa-pencil"></i> Salvar</button>
                                    <?php } ?>
                                    <button type="button" class="btn default" onclick="location.href='../Listar/'">Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </form>
        </div>

    </div>
</div>
