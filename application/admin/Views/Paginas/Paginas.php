<?php
/**
 * User: Gustavo
 * @var Conteudos $tbl
 */

?>

<div class="tabbable-custom ">
    <ul class="nav nav-tabs">
        <li id="tab_registros">
            <a href="#tab_1_1" data-toggle="tab">Registros</a>
        </li>
        <li id="tab_editcad">
            <a href="#tab_1_2" data-toggle="tab"> <?= isset($_GET["id"]) != NULL ? "Editar" : "Cadastrar" ?></a>
        </li>

    </ul>

    <div class="tab-content" style="border: 1px solid #ddd;">
        <div class="tab-pane" id="tab_1_1">
            <div class="portlet-body">
                <div class="table-responsive">
                    <!--COME�O TABELA PRINCIPAL ID->tabelaPrincipal  -->
                    <table class="table table-condensed table-hover" id="tabelaPrincipal">
                        <thead>
                        <tr>
                            <th>Titulo</th>
                            <th>Data de Cadastro</th>
                            <th>A�ao</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php /** @var $pag Conteudos */
                        foreach (Conteudos::ListarTodos("Where id_CategoriasPrincipal_fk = 2 and id_Conteudos_fk IS NULL") as $pag):
                            ?>
                            <tr>
                                <td><?= $pag->getDsTitulo() ?></td>
                                <td><?= $pag->getDhCadastro() ?></td>
                                <td><a href="../Editar/?id=<?= $pag->getIdConteudos() ?>" class="btn default btn-xs purple"><i class="fa fa-edit"> Editar</i></a></td>
                            </tr>
                            <?php /** @var $pag2 Conteudos */
                            foreach (Conteudos::ListarTodos("Where id_CategoriasPrincipal_fk = 2 and id_Conteudos_fk = ". $pag->getIdConteudos()) as $pag2):
                                ?>
                                <tr>
                                    <td>-- <?= $pag2->getDsTitulo() ?></td>
                                    <td><?= $pag2->getDhCadastro() ?></td>
                                    <td><a href="../Editar/?id=<?= $pag2->getIdConteudos() ?>" class="btn default btn-xs purple"><i class="fa fa-edit"> Editar</i></a></td>
                                </tr>

                            <?php endforeach; ?>

                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <!--FIM TABELA PRINCIPAL -->
                </div>
            </div>
        </div>
        <div class="tab-pane portlet-body form" id="tab_1_2">
            <form action="../Salvar/" method="post" id="validate" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-body">

                    <div class="form-group display-hide">
                        <label class="control-label col-md-3 col-lg-1" for="id_Conteudos">ID</label>

                        <div class="col-md-2">
                            <input type="text" name="id_Conteudos" id="id_Conteudos" class="form-control" readonly value="<?= $tbl->getIdConteudos() ?>"/>
                        </div>
                    </div>

                </div>

                <?php if (isset($_GET['id'])) {
                    if ($_GET['id'] != NULL): ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-lg-1" for="url">Link Externo</label>

                            <div class="col-md-9 ">
                                <a href="<?= BASE_PATH ?>Pagina/<?= $tbl->getDsPermalink() ?>/" target="_blank" class=""><?= BASE_PATH ?>Pagina/<?= $tbl->getDsPermalink() ?>/</a>
                            </div>
                        </div>
                    <?php endif;
                } ?>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="ds_Titulo">T�tulo da P�gina</label>

                    <div class="col-md-9 ">
                        <input type="text" name="ds_Titulo" id="ds_Titulo" class="form-control validate[required]" maxlength="100" value="<?= $tbl->getDsTitulo() ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="tx_Conteudo">Texto</label>

                    <div class="col-md-9">
                        <textarea class="ckeditor form-control validate[required]" name="tx_Conteudo" id="tx_Conteudo" rows="6"><?= $tbl->getTxConteudo() ?></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="id_Conteudos_fk">P�gina Pai</label>

                    <div class="col-md-9 ">
                        <select name="id_Conteudos_fk" id="id_Conteudos_fk" class="form-control">
                            <option value=""></option>
                            <?php
                            /** @var $cat Conteudos */
                            foreach ($listaPaginas as $cat) {
                                $selected = "";
                                if ($tbl->getIdConteudosFk() == $cat->getIdConteudos()) {
                                    $selected = "selected";
                                }

                                ?>
                                <option value="<?= $cat->getIdConteudos() ?>"  <?= $selected ?> ><?= $cat->getDsTitulo() ?></option>

                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="is_Ativo">Ativo ?</label>

                    <div class="col-md-9">
                        <div class="make-switch" data-on-label="Sim" data-off-label="N�o">
                            <input type="checkbox" name="is_Ativo" id="is_Ativo" value="TRUE"  <?= $tbl->getIsAtivo() == "0" ? "" : "checked" ?> class="toggle"/>
                        </div>
                    </div>
                </div>


                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <?php if (isset($_GET["id"]) == NULL) { ?>
                                    <button type="submit" class="btn blue" name="action" value="Salvar"><i class="fa fa-check"></i> Cadastrar</button>
                                <?php } else { ?>
                                    <button type="submit" class="btn green" name="action" value="Editar"><i class="fa fa-pencil"></i> Salvar</button>
                                <?php } ?>
                                <button type="button" class="btn default" onclick="location.href='../Listar/'">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>

    </div>
</div>
