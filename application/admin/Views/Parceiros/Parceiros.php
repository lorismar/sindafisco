<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var Arquivos $tbl
 */

?>

<div class="tabbable-custom ">
    <ul class="nav nav-tabs">
        <li id="tab_registros">
            <a href="#tab_1_1" data-toggle="tab">Registros</a>
        </li>
        <li id="tab_editcad">
            <a href="#tab_1_2" data-toggle="tab"> <?= isset($_GET["id"]) != NULL ? "Editar" : "Cadastrar" ?></a>
        </li>
    </ul>

    <div class="tab-content" style="border: 1px solid #ddd;">
        <div class="tab-pane" id="tab_1_1">
            <div class="portlet-body">
                <div class="table-responsive">
                    <!--COME�O TABELA PRINCIPAL ID->tabelaPrincipal  -->
                    <table class="table table-condensed table-bordered table-hover" id="tabelaPrincipal">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Imagem</th>
                            <th>A�ao</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php /** @var $arq Arquivos */
                        foreach ($listaArquivos as $arq): ?>

                            <tr>
                                <td><?= $arq->getDsTitulo() ?></td>
                                <td><img src="../../../public/upload/Parceiros/<?= $arq->getFileArquivo() ?>" alt=""/></td>
                                <td><a href="../Editar/?id=<?=$arq->getIdArquivos()?>" class="btn default btn-xs purple"><i class="fa fa-edit"> Editar</i></a> <button onclick="Remover('../Deletar/?did=<?=$arq->getIdArquivos()?>')" class="btn default btn-xs red"><i class="fa fa-trash-o"> Remover</i></button></td>
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                    </table>
                    <!--FIM TABELA PRINCIPAL -->
                </div>
            </div>
        </div>
        <div class="tab-pane portlet-body form" id="tab_1_2">
            <form action="<?= $Html->ActionLink(NULL, "Salvar") ?>" method="post" id="validate" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-body">

                    <div class="form-group display-hide">
                        <label class="control-label col-md-3 col-lg-1" for="id_Arquivos">ID</label>

                        <div class="col-md-2">
                            <input type="text" name="id_Arquivos" id="id_Arquivos" class="form-control" readonly value="<?= $tbl->getIdArquivos() ?>"/>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="ds_Titulo">T�tulo do Conte�do</label>

                    <div class="col-md-9 ">
                        <input type="text" name="ds_Titulo" id="ds_Titulo" class="form-control validate[required]" maxlength="100" value="<?= $tbl->getDsTitulo() ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="ds_UrlLink">Link do Banner</label>

                    <div class="col-md-9 ">
                        <input type="text" name="ds_UrlLink" id="ds_UrlLink" class="form-control validate[required]" maxlength="100" value="<?= $tbl->getDsUrlLink() ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="file_Arquivo">Foto do Banner do Parceiro</label>

                    <div class="col-md-9">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="<?= $tbl->getFileArquivo() != NULL ? "../../../public/upload/Parceiros/thumbs/" . $tbl->getFileArquivo() : ""; ?>" alt=""/>
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            <div>
													<span class="btn default btn-file">
													<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar Imagem</span>
													<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
													<input type="file" class="default" id="file_Arquivo" name="file_Arquivo"/>
                                                          <input type="hidden" id="file_Arquivo_original" name="file_Arquivo_original"
                                                                 value="<?= $tbl->getFileArquivo() ?>">
													</span>
                                <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a>
                            </div>
                        </div>
                        <span class="label label-danger">NOTA!</span>
											<span>
											A imagem � anexo � suportado nas �ltimas vers�es do Firefox, Chrome, Opera,
											Safari e Internet Explorer 10.
											</span>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="is_Ativo">Ativo ?</label>

                    <div class="col-md-9">
                        <div class="make-switch" data-on-label="Sim" data-off-label="N�o">
                            <input type="checkbox" name="is_Ativo" id="is_Ativo" value="TRUE"  <?= $tbl->getIsAtivo() == "0" ? "" : "checked" ?> class="toggle"/>
                        </div>
                    </div>
                </div>



                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <?php if (isset($_GET["id"]) == NULL) { ?>
                                    <button type="submit" class="btn blue" name="action" value="Salvar"><i class="fa fa-check"></i> Cadastrar</button>
                                <?php } else { ?>
                                    <button type="submit" class="btn green" name="action" value="Editar"><i class="fa fa-pencil"></i> Salvar</button>
                                <?php } ?>
                                <button type="button" class="btn default" onclick="location.href='../Listar/'">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>

    </div>
</div>
