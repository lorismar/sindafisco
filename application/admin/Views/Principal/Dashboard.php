<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var Parametros $tbl
 * @var Usuarios $u
 */


?>
<?php if ($u->getPessoas()->getIdTipoPessoasFk() == 3): ?>
    <div class="col-md-12 col-sm-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-sun-o"></i>Simular Perfil
                </div>
            </div>
            <div class="portlet-body">
                <form method="post" class="form-horizontal" name="form" enctype="multipart/form-data"
                      action="../TrocarPerfil/">
                    <select name="id_Perfis_fk" id="id_Perfis_fk" class="form-control">
                        <option value=""></option>
                        <?php
                        /** @var $per Perfis */
                        foreach ($listaPerfis as $per) {
                            ?>
                            <option value="<?= $per->getIdPerfis() ?>"><?= $per->getDsDescricao() ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <button class="btn btn-success" type="submit">Trocar</button>
                </form>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bell-o"></i>Informativos (últimos 10)
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>
                    <a href="" class="remove"></a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="scroller" style="height: 200px;" data-always-visible="1" data-rail-visible="0">
                    <ul class="feeds">
                        <?php /** @var $inf Conteudos */
                        foreach ($listaInformativos as $inf):
                            ?>
                            <li>
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm label-info">
                                                <i class="fa fa-check"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc">
                                                <a href="../../Informativos/<?= $inf->getDsPermalink() ?>/"><?= $inf->getDsTitulo() ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="date">
                                        <?= substr($inf->getDhCadastro(), 0, 10) ?>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>

                    </ul>
                </div>
                <div class="scroller-footer">
                    <div class="pull-right">
                        <a href="<?= $Html->ActionLink("Informativos", "Filiados") ?>">Visualizar Todos <i class="m-icon-swapright m-icon-gray"></i></a> &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money"></i>Informativo Contábil (últimos 10)
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>
                    <a href="" class="remove"></a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="scroller" style="height: 200px;" data-always-visible="1" data-rail-visible="0">
                    <ul class="feeds">
                        <?php /** @var $inf Arquivos */
                        foreach ($listaInformativosContabil as $inf):
                            ?>
                            <li>
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm label-info">
                                                <i class="fa fa-check"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc">
                                                <a href="<?= CONTENT ?>upload/Contabil/<?= $inf->getFileArquivo() ?>" target="_blank"><?= $inf->getDsTitulo() ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="date">
                                        <?= $inf->getDtCadastro() ?>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>

                    </ul>
                </div>
                <div class="scroller-footer">
                    <div class="pull-right">
                        <a href="<?= $Html->ActionLink("Contabil", "InformacoesFinanceiras") ?>">Visualizar Todos <i class="m-icon-swapright m-icon-gray"></i></a> &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
<div class="col-md-6 col-sm-6">
    <!-- BEGIN PORTLET-->
    <div class="portlet paddingless">
        <div class="portlet-title line">
            <div class="caption">
                <i class="fa fa-folder-open"></i>Processos
            </div>
            <div class="tools">
                <a href="" class="collapse"></a>
                <a href="" class="remove"></a>
            </div>
        </div>
        <div class="portlet-body">
            <!--BEGIN TABS-->
            <div class="tabbable tabbable-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_1_1" data-toggle="tab">Meus</a>
                    </li>
                    <li>
                        <a href="#tab_1_2" data-toggle="tab">Sindicato</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1_1">
                        <div class="scroller" style="height: 290px;" data-always-visible="1" data-rail-visible="0">
                            <ul class="feeds">
                                <?php /** @var $pro Conteudos */
                                foreach ($listaProcessosMeus as $pro):
                                    ?>
                                    <li>
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-success">
                                                        <i class="fa fa-bell-o"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc">
                                                        <a href="../../Processos/Acompanhar/?id=<?= $pro->getIdConteudos() ?>"><?= $pro->getDsTitulo() ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_1_2">
                        <div class="scroller" style="height: 290px;" data-always-visible="1" data-rail-visible1="1">
                            <ul class="feeds">
                                <?php /** @var $pro Conteudos */
                                foreach ($listaProcessosSindicado as $pro):
                                    ?>
                                    <li>
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-success">
                                                        <i class="fa fa-bell-o"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc">
                                                        <a href="../../Processos/Acompanhar/?id=<?= $pro->getIdConteudos() ?>"><?= $pro->getDsTitulo() ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--END TABS-->
        </div>
    </div>
    <!-- END PORTLET-->
</div>

<div class="col-md-6 col-sm-6">
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-bell-o"></i>Feeds
            </div>
            <div class="tools">
                <a href="" class="collapse"></a>
                <a href="" class="remove"></a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                <ul class="feeds">
                    <?php /** @var $feed Feeds */
                    foreach ($listaFeeds as $feed):
                        ?>
                        <li>
                            <div class="col1">
                                <div class="cont">
                                    <div class="cont-col1">
                                        <div class="label label-sm label-info">
                                            <i class="fa fa-check"></i>
                                        </div>
                                    </div>
                                    <div class="cont-col2">
                                        <div class="desc">
                                            <a href="<?= $feed->getDsUrlLink() ?>"><?= $feed->getDsTitulo() ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col2">
                                <div class="date">
                                    <?= $feed->getDhCadastro() ?>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>

                </ul>
            </div>

        </div>
    </div>
</div>
</div>