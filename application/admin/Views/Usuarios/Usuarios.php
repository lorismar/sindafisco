<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var Usuarios $tbl
 */


?>

<div class="tabbable-custom ">

    <ul class="nav nav-tabs">
        <li id="tab_registros">
            <a href="#tab_1_1" data-toggle="tab">Registros</a>
        </li>
        <?php if (isset($_GET["id"]) != NULL) { ?>
        <li id="tab_editcad">
            <a href="#tab_1_2" data-toggle="tab"> <?= isset($_GET["id"]) != NULL ? "Editar" : "Cadastrar" ?></a>
        </li>
        <?php } ?>

    </ul>

    <div class="tab-content" style="border: 1px solid #ddd;">
        <div class="tab-pane" id="tab_1_1">
            <div class="portlet-body">
                <div class="table-responsive">
                    <!--COME�O TABELA PRINCIPAL ID->tabelaPrincipal  -->
                    <table class="table table-condensed table-hover" id="tabelaPrincipal">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nome</th>
                            <th>Perfil</th>
                            <th>A�ao</th>

                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <!--FIM TABELA PRINCIPAL -->
                </div>
            </div>
        </div>
        <?php if (isset($_GET["id"]) != NULL) : ?>
        <div class="tab-pane portlet-body form" id="tab_1_2">
            <form action="../Salvar/" method="post" id="validate" class="form-horizontal" autocomplete="off" enctype="multipart/form-data">

                <div class="form-body">

                    <div class="form-group display-hide">
                        <label class="control-label col-md-3 col-lg-1" for="id_Usuarios">ID</label>

                        <div class="col-md-2">
                            <input type="text" name="id_Usuarios" id="id_Usuarios" class="form-control" readonly value="<?= $tbl->getIdUsuarios() ?>"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Nome">Nome:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Nome" id="ds_Nome" class="form-control validate[required]" maxlength="100" value="<?= $tbl->getDsNome() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Usuario">Usu�rio (CPF):</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Usuario" id="ds_Usuario" class="form-control validate[required] maskCpf" maxlength="14" value="<?= $tbl->getDsUsuario() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Senha">Senha:</label>

                        <div class="col-md-9 ">
                            <input type="password" name="ds_Senha" id="ds_Senha" class="form-control" maxlength="20" value="<?= mycrypt_decode($tbl->getDsSenha()) ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="is_Ativo">Ativo ?</label>

                        <div class="col-md-9">
                            <div class="make-switch" data-on-label="Sim" data-off-label="N�o">
                                <input type="checkbox" name="is_Ativo" id="is_Ativo" value="TRUE"  <?= $tbl->getIsAtivo() == "0" ? "" : "checked" ?> class="toggle"/>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="id_Perfis_fk">Perfil</label>

                        <div class="col-md-9 ">
                            <select name="id_Perfis_fk" id="id_Perfis_fk" class="form-control validate[required]">
                                <option value=""></option>
                                <?php
                                /** @var $per Perfis */
                                foreach ($listaPerfis as $per) {
                                    $selected = "";
                                    if ($tbl->getIdPerfisFk() == $per->getIdPerfis()) {
                                        $selected = "selected";
                                    }

                                    ?>
                                    <option value="<?= $per->getIdPerfis() ?>"  <?= $selected ?> ><?=  $per->getDsDescricao() ?></option>

                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <?php if (isset($_GET["id"]) == NULL) { ?>
                                    <button type="submit" class="btn blue" name="action" value="Salvar"><i class="fa fa-check"></i> Cadastrar</button>
                                <?php } else { ?>
                                    <button type="submit" class="btn green" name="action" value="Editar"><i class="fa fa-pencil"></i> Salvar</button>
                                <?php } ?>
                                <button type="button" class="btn default" onclick="location.href='../Listar/'">Cancelar</button>
                            </div>
                        </div>
                    </div>


                </div>

            </form>
        </div>
        <?php endif; ?>

    </div>
</div>
