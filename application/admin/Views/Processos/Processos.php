<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var Conteudos $tbl
 * @var Usuarios $u
 */


?>

<div class="tabbable-custom ">
<ul class="nav nav-tabs">
    <li id="tab_registros">
        <a href="#tab_1_1" data-toggle="tab">Registros</a>
    </li>
    <li id="tab_editcad">
        <a href="#tab_1_2" data-toggle="tab"> <?= isset($_GET["id"]) != NULL ? "Editar" : "Cadastrar" ?></a>
    </li>
    <li id="tab_download">
        <a href="#tab_1_3" data-toggle="tab">Exportar</a>
    </li>
</ul>

<div class="tab-content" style="border: 1px solid #ddd;">
<div class="tab-pane" id="tab_1_1">
    <div class="portlet-body">
        <div class="table-responsive">
            <!--COME�O TABELA PRINCIPAL ID->tabelaPrincipal  -->
            <table class="table table-condensed table-hover" id="tabelaPrincipal">
                <thead>
                <tr>
                    <th>N� do Processo</th>
                    <th>Titulo</th>
                    <th>A�ao</th>
                </tr>
                </thead>
                <tbody>
                <?php /** @var $p Conteudos */
                foreach ($listaProcessos as $p): ?>
                    <tr>
                        <td><?= $p->getTxResumo() ?></td>
                        <td><?= $p->getDsTitulo() ?></td>
                        <td>
                            <a href="../Acompanhar/?id=<?= $p->getIdConteudos() ?>#tab_1_3" class="btn default btn-xs green"><i class="fa fa-search"> Acompanhar</i></a>
                            <?php if ($u->getIdPerfisFk() == 5 || $u->getIdPerfisFk() == 1 || $u->getIdPerfisFk() == 2): ?>    <a href='../Editar/?id=<?= $p->getIdConteudos() ?>' class='btn default btn-xs purple'><i class='fa fa-edit'> Editar</i></a><?php endif; ?>
                            <?php if ($u->getIdPerfisFk() == 5 || $u->getIdPerfisFk() == 1 || $u->getIdPerfisFk() == 2): ?>
                                <button onclick="javascript:Remover('../Deletar/?did=<?= $p->getIdConteudos() ?>')" class="btn default btn-xs red"><i class="fa fa-trash-o"> Deletar</i></button> <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <!--FIM TABELA PRINCIPAL -->
        </div>
    </div>
</div>
<div class="tab-pane portlet-body form" id="tab_1_2">
<form action="../Salvar/" method="post" id="validate" class="form-horizontal" enctype="multipart/form-data">

    <div class="form-actions fluid">

        <div class="form-body">

            <div class="form-group display-hide">
                <label class="control-label col-md-3 col-lg-1" for="id_Conteudos">ID</label>

                <div class="col-md-2">
                    <input type="text" name="id_Conteudos" id="id_Conteudos" class="form-control" readonly value="<?= $tbl->getIdConteudos() ?>"/>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-lg-1" for="ds_NuProcesso">N�mero do Processo</label>

            <div class="col-md-9 ">
                <input type="text" name="ds_NuProcesso" id="ds_NuProcesso" class="form-control validate[required]" maxlength="100" value="<?= $tbl->getTxResumo() ?>"/>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-lg-1" for="ds_Titulo">T�tulo do Processo</label>

            <div class="col-md-9 ">
                <input type="text" name="ds_Titulo" id="ds_Titulo" class="form-control validate[required]" maxlength="100" value="<?= $tbl->getDsTitulo() ?>"/>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-lg-1" for="tx_Descricao">Texto de Descri��o</label>

            <div class="col-md-9">
                <textarea class="ckeditor form-control validate[required]" name="tx_Descricao" id="tx_Descricao" rows="6"><?= $tbl->getTxConteudo() ?></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-lg-1" for="is_Publico">� p�blico ?</label>

            <div class="col-md-9">
                <div class="make-switch" data-on-label="Sim" data-off-label="N�o">
                    <input type="checkbox" name="is_Publico" id="is_Publico" value="TRUE"  <?= $tbl->getIsPublico() == "0" ? "" : "checked" ?> class="toggle"/>
                </div>
                <span class="label label-danger">NOTA!</span>
											<span>
											Todos os Sindicalizados ir�o poder visualizar.
											</span>
            </div>

        </div>

        <div class="form-group last">
            <label class="control-label col-md-3 col-lg-1">Quem tem acesso?</label>

            <div class="col-md-9">
                <label for="my_multi_select3"></label>
                <select name="id_Pessoas_fk[]" class="multi-select" multiple="" id="my_multi_select3">

                    <?php /** @var $sit SitPessoas */
                    foreach (SitPessoas::ListarTodos() as $sit): ?>
                        <optgroup label="Situa��o: <?= strtoupper($sit->getDsDescricao()) ?>">

                            <?php
                            /** @var $pessoas Pessoas */
                            foreach (Pessoas::ListarTodos("WHERE id_TipoPessoas_fk = 1 and id_SitPessoas_fk = {$sit->getIdSitPessoas()}") as $pessoas):
                                $selected = "";
                                if ($pessoas->getIdPessoas() == $u->getIdPessoasFk())
                                    $selected = "selected";
                                /** @var $p Conteudos_tem_Pessoas */
                                foreach ($listaPessoasProcessos as $p):
                                    if ($pessoas->getIdPessoas() == $p->getIdPessoasFk())
                                        $selected = "selected";
                                endforeach;

                                ?>
                                <option value="<?= $pessoas->getIdPessoas() ?>" <?= $selected ?>><?= $pessoas->getDsNome() ?></option>
                            <?php
                            endforeach;
                            ?>


                        </optgroup>
                    <?php endforeach; ?>





                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-offset-3 col-md-9">
                    <?php if (isset($_GET["id"]) == NULL) { ?>
                        <button type="submit" class="btn blue" name="action" value="Salvar"><i class="fa fa-check"></i> Cadastrar</button>
                    <?php } else { ?>
                        <button type="submit" class="btn green" name="action" value="Editar"><i class="fa fa-pencil"></i> Salvar</button>
                    <?php } ?>
                    <button type="button" class="btn default" onclick="location.href='../Todos/'">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

</form>

<?php if ($tbl->getIdConteudos() > 0): ?>
    <div class="portlet box purple">
        <div class="portlet-title">
            <div class="caption"><i class="icon-comments"></i>Anexos do Processo</div>
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
            </div>
        </div>
        <div class="portlet-body">

            <form method="post" class="form-horizontal" name="form" enctype="multipart/form-data" id="validate"
                  autocomplete="off"
                  action="../Anexar/?id=<?= $_GET['id'] ?>">

                <div class="control-group display-hide">
                    <label for="id_Arquivos" class="control-label">ID</label>

                    <div class="controls">
                        <input name="id_Conteudos_fk" id="id_Conteudos_fk" type="text" style="background-color: #bbbbbb"
                               readonly="readonly" class="span6 m-wrap"
                               value="<?= $tbl->getIdConteudos() ?>"/>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="dh_Cadastro">Data da Publica��o</label>

                    <div class="col-md-9 ">
                        <input type="text" name="dt_Cadastro" id="dt_Cadastro" class="form-control maskDate validate[required]" value="<?= DATE ?>"/>
                    </div>
                </div>


                <div class="form-group">
                    <label for="ds_Nome" class="control-label col-md-3 col-lg-1">Nome do Arquivo</label>

                    <div class="col-md-9">
                        <input class="form-control m-wrap validate[required]" name="ds_Titulo" id="ds_Titulo"
                               type="text"
                               maxlength="100" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1">Arquivo</label>

                    <div class="col-md-9">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <input type="file" id="file_Arquivo" name="file_Arquivo">
                            <!--<span>Nenhum arquivo</span>-->


                        </div>


                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn blue" name="action" value="Salvar">Enviar Arquivo</button>
                        </div>
                    </div>
                </div>


            </form>


            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>T�tulo</th>
                    <th>Arquivo</th>
                    <th>A��o</th>
                </tr>
                </thead>
                <tbody>
                <?php /** @var $anexo Arquivos */
                foreach ($tbl->Arquivos() as $anexo): ?>
                    <tr>
                        <td><?= $anexo->getIdArquivos() ?></td>
                        <td><?= $anexo->getDsTitulo() ?></td>
                        <td><a href="<?= CONTENT ?>upload/Processos/<?= $anexo->getFileArquivo() ?>" target="_blank"><?= $anexo->getDsTitulo() ?></a></td>
                        <td>
                            <a href='../RemoverAnexo/?id=<?= $anexo->getIdConteudosFk() ?>&anexo=<?= $anexo->getIdArquivos() ?>' class='btn mini red'><i class='icon-trash'></i> Remover</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>


</div>
<div class="tab-pane portlet-body form" id="tab_1_3">
    <a href="<?= $Html->ActionLink("Processos","ExportarExcel") ?>" target="_blank" class="btn btn-default"><i class="fa fa-download"></i> Exportar para Excel</a>
</div>
</div>
</div>
