<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var Conteudos $tbl
 * @var Usuarios $u
 */


?>



<!-- BEGIN PORTLET-->
<div class="portlet box blue">
    <div class="portlet-title line">
        <div class="caption">
            <i class="fa fa-search"></i> Acompanhamento
        </div>

    </div>
    <div class="portlet-body">
        <h2><?= $tbl->getDsTitulo() ?></h2>
        <hr/>
        <?= $tbl->getTxConteudo() ?>

        <br/><br/><br/>

        <div class="portlet">
            <div class="portlet-title line">
                <div class="caption">
                    <i class="fa fa-download"></i>Arquivos do Processo
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>

                    <a href="" class="remove"></a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-hover">


                    <tbody>
                    <?php /** @var $anexo Arquivos */
                    foreach ($tbl->Arquivos() as $anexo): ?>
                        <tr>
                            <td><a href="<?= CONTENT ?>upload/Processos/<?= $anexo->getFileArquivo() ?>" target="_blank"><?= $anexo->getDsTitulo() ?></a></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- BEGIN PORTLET-->
        <div class="portlet">
            <div class="portlet-title line">
                <div class="caption">
                    <i class="fa fa-comments"></i>Movimentação
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>

                    <a href="" class="remove"></a>
                </div>
            </div>

            <div class="portlet-body" id="chats">
                <div class="scroller" style="height: 435px;" data-always-visible="1" data-rail-visible1="1">
                    <ul class="chats">
                        <?php
                        /** @var $mov Comentarios */
                        foreach ($tbl->Comentarios() as $mov):
//                            $mov->getUsuarios()->VisualizarPorId();

                            ?>
                            <li class="in">

                                <div class="message" style="margin-left: 0px;">

                            											<span class="datetime">
                            												<?= $mov->getDhComentario() ?>
                            											</span>
                            											<span class="body">
                                                                            <?= $mov->getTxComentario() ?>
                            											</span>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <?php if ($u->getIdPerfisFk() == "5" || $u->getIdPerfisFk() == "1" || $u->getIdPerfisFk() == "2"): ?>
                    <form id="validate" method="POST" action="../Comentar/" enctype="multipart/form-data">
                        <div class="chat-form">
                            <div class="input-cont">
                                <input type="hidden" name="id_Processos_fk" value="<?= $tbl->getIdConteudos() ?>"/>
                                <input class="form-control validate[required]" type="text" name="tx_Movimentacao" id="tx_Movimentacao" placeholder="Digite o acompanhemento aqui..."/>
                                <input class="form-control validate[required] maskDateTime" type="text" name="dh_Movimentacao" id="dh_Movimentacao" value="<?= DATETIME ?>"/>
                            </div>
                            <div class="btn-cont">
									<span class="arrow">
									</span>
                                <button type="submit" class="btn blue icn-only"><i class="fa fa-check icon-white"></i></button>
                            </div>
                        </div>
                    </form>
                <?php endif; ?>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
<!-- END PORTLET-->

