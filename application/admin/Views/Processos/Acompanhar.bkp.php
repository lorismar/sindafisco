<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var Processos $tbl
 */





?>



<!-- BEGIN PORTLET-->
<div class="portlet box blue">
    <div class="portlet-title line">
        <div class="caption">
            <i class="fa fa-search"></i> Acompanhamento
        </div>

    </div>
    <div class="portlet-body">
        <h2><?= $tbl->getDsTitulo() ?></h2>
        <hr/>
        <?= $tbl->getTxDescricao() ?>

        <br/><br/><br/>
        <!-- BEGIN PORTLET-->
        <div class="portlet">
            <div class="portlet-title line">
                <div class="caption">
                    <i class="fa fa-comments"></i>Movimentação
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>

                    <a href="" class="remove"></a>
                </div>
            </div>
            <div class="portlet-body" id="chats">
                <div class="scroller" style="height: 435px;" data-always-visible="1" data-rail-visible1="1">
                    <ul class="chats">
                        <?php
                        /** @var $mov ProcessosMovimentacao */
                        foreach ($tbl->Movimentacoes() as $mov):
                            $mov->getPessoas()->VisualizarPorId();
                            if ($mov->getPessoas()->getFileFoto() == "") {
                                $mov->getPessoas()->setFileFoto("default_avatar.jpg");
                            }
                            ?>
                            <li class="in">
                                <img class="avatar img-responsive" alt="" src="<?= CONTENT ?>upload/Pessoas/<?= $mov->getPessoas()->getFileFoto() ?>"/>

                                <div class="message">
                            											<span class="arrow">
                            											</span>
                                    <a href="#" class="name"><?= $mov->getPessoas()->getDsNome() ?></a>
                            											<span class="datetime">
                            												em <?= $mov->getDhMovimentacao() ?>
                            											</span>
                            											<span class="body">
                                                                            <?= $mov->getTxMovimentacao() ?>
                            											</span>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <form id="validate" method="POST" action="../Comentar/" enctype="multipart/form-data">
                    <div class="chat-form">
                        <div class="input-cont">
                            <input type="hidden" name="id_Processos_fk" value="<?= $tbl->getIdProcessos() ?>"/>
                            <input class="form-control validate[required]" type="text" name="tx_Movimentacao" id="tx_Movimentacao" placeholder="Digite o acompanhemento aqui..."/>
                            <input class="form-control validate[required] maskDateTime"  type="text" name="dh_Movimentacao" id="dh_Movimentacao" value="<?=DATETIME?>" />
                        </div>
                        <div class="btn-cont">
									<span class="arrow">
									</span>
                            <button type="submit" class="btn blue icn-only"><i class="fa fa-check icon-white"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
<!-- END PORTLET-->

