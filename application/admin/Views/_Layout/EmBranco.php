    <?php

/**
 * @var $Html Html
 * @var $usuario Usuarios
 */

include("../application/admin/Controllers/_Layout/AdminController.php");
$index = new AdminController();
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html lang="pt-br" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="pt-br" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="ISO-8859-1"/>
    <title>SINDAFISCO | <?= $Html->Layout->getTitle() ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="HAVIT SOLU��ES TECNOL�GICAS" name="author"/>
    <meta name="MobileOptimized" content="320">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="<?= CONTENT_ADMIN ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= CONTENT_ADMIN ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= CONTENT_ADMIN ?>assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="<?= CONTENT_ADMIN ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/>
    <link rel="stylesheet" type="text/css" href="<?= CONTENT_ADMIN ?>assets/plugins/select2/select2_metro.css"/>
    <link rel="stylesheet" href="<?= CONTENT_ADMIN ?>assets/plugins/data-tables/DT_bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="<?= CONTENT_ADMIN ?>assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
    <link rel="stylesheet" type="text/css" href="<?= CONTENT_ADMIN ?>assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
    <link rel="stylesheet" type="text/css" href="<?= CONTENT_ADMIN ?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css"/>
    <link href="<?= CONTENT_ADMIN ?>assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
    <link href="<?= CONTENT_ADMIN ?>assets/css/pages/portfolio.css" rel="stylesheet" type="text/css"/>
    <link href="<?= CONTENT_ADMIN ?>assets/plugins/dropzone/css/dropzone.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="<?= CONTENT_ADMIN ?>assets/plugins/jquery-multi-select/css/multi-select.css"/>
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?= CONTENT_ADMIN ?>assets/css/style-metronic.css" rel="stylesheet" type="text/css"/>
    <link href="<?= CONTENT_ADMIN ?>assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?= CONTENT_ADMIN ?>assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?= CONTENT_ADMIN ?>assets/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?= CONTENT_ADMIN ?>assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?= CONTENT_ADMIN ?>assets/css/custom.css" rel="stylesheet" type="text/css"/>
    <link href="<?= CONTENT_ADMIN ?>custom/css/cssValidate.css" rel="stylesheet" type="text/css" id="style_color"/>

    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-full-width">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">

    <!-- BEGIN PAGE -->
    <div class="page-content">


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?= $Html->Layout->getTitle() ?>
                    <small> <?= $Html->Layout->getDescription() ?></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">


                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <?= GUI::retornaErro(); ?>
                <?= $Html->Layout->RenderBody() ?>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="footer-inner">
        2013 &copy; <a href="http://www.havit.com.br">HAVIT - Soluções Tecnol�gicas</a>
    </div>
    <div class="footer-tools">
         <span class="go-top">
         <i class="icon-angle-up"></i>
         </span>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?=CONTENT_ADMIN?>assets/plugins/respond.min.js"></script>
<script src="<?=CONTENT_ADMIN?>assets/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="<?= CONTENT_ADMIN ?>assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?= CONTENT_ADMIN ?>assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?= CONTENT_ADMIN ?>assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?= CONTENT_ADMIN ?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= CONTENT_ADMIN ?>assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?= CONTENT_ADMIN ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?= CONTENT_ADMIN ?>assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?= CONTENT_ADMIN ?>assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
<script src="<?= CONTENT_ADMIN ?>assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/ckfinder/ckfinder.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/bootstrap-markdown/lib/markdown.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>custom/js/custom.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>custom/js/jquery.currency.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>custom/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>custom/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>custom/js/jquery.validationEngine-en.js"></script>
<script src="<?= CONTENT_ADMIN ?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script src="<?= CONTENT_ADMIN ?>assets/plugins/dropzone/dropzone.js"></script>
<script type="text/javascript" src="<?= CONTENT_ADMIN ?>assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script src="<?= CONTENT_ADMIN ?>assets/scripts/app.js"></script>
<script src="<?= CONTENT_ADMIN ?>assets/scripts/form-components.js"></script>
<script src="<?= CONTENT_ADMIN ?>assets/scripts/table-managed.js"></script>
<script src="<?= CONTENT_ADMIN ?>assets/scripts/portfolio.js"></script>
<script src="<?= CONTENT_ADMIN ?>assets/scripts/form-dropzone.js"></script>
<script src="<?= CONTENT_ADMIN ?>assets/scripts/form-wizard.js"></script>
<script>
    jQuery(document).ready(function () {
        App.init();
        FormComponents.init();
        Portfolio.init();
        FormDropzone.init();
        FormWizard.init();
    });
</script>
<script>
    <?= GUI::retornaJS() ?>
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>