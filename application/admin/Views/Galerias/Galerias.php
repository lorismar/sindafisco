<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var Conteudos $tbl
 */

?>

<div class="tabbable-custom ">
    <ul class="nav nav-tabs">
        <li id="tab_registros">
            <a href="#tab_1_1" data-toggle="tab">Registros</a>
        </li>
        <li id="tab_editcad">
            <a href="#tab_1_2" data-toggle="tab"> <?= isset($_GET["id"]) != NULL ? "Editar" : "Cadastrar" ?></a>
        </li>
        <?php if (isset($_GET["id"]) != NULL) { ?>
            <li id="tab_galerias">
                <a href="#tab_1_3" data-toggle="tab">Fotos</a>
            </li>
        <?php } ?>
    </ul>

    <div class="tab-content" style="border: 1px solid #ddd;">
        <div class="tab-pane" id="tab_1_1">
            <div class="portlet-body">
                <div class="table-responsive">
                    <!--COME�O TABELA PRINCIPAL ID->tabelaPrincipal  -->
                    <table class="table table-condensed table-hover" id="tabelaPrincipal">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Titulo</th>
                            <th>Data de Cadastro</th>
                            <th>A�ao</th>

                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <!--FIM TABELA PRINCIPAL -->
                </div>
            </div>
        </div>
        <div class="tab-pane portlet-body form" id="tab_1_2">
            <form action="<?= $Html->ActionLink(NULL, "Salvar") ?>" method="post" id="validate" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-body">

                    <div class="form-group display-hide">
                        <label class="control-label col-md-3 col-lg-1" for="id_Conteudos">ID</label>

                        <div class="col-md-2">
                            <input type="text" name="id_Conteudos" id="id_Conteudos" class="form-control" readonly value="<?= $tbl->getIdConteudos() ?>"/>
                        </div>
                    </div>

                </div>
                <?php if (isset($_GET['id'])) {
                    if ($_GET['id'] != NULL): ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-lg-1" for="url">Link Externo</label>

                            <div class="col-md-9 ">
                                <a href="<?= BASE_PATH ?>Eventos/<?= $tbl->getDsPermalink() ?>/" target="_blank" class=""><?= BASE_PATH ?>Eventos/<?= $tbl->getDsPermalink() ?>/</a>
                            </div>
                        </div>
                    <?php endif;
                } ?>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="ds_Titulo">T�tulo do Conte�do</label>

                    <div class="col-md-9 ">
                        <input type="text" name="ds_Titulo" id="ds_Titulo" class="form-control validate[required]" maxlength="100" value="<?= $tbl->getDsTitulo() ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="file_Capa">Foto da Capa</label>

                    <div class="col-md-9">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="<?= $tbl->getFileCapa() != NULL ? "../../../public/upload/Galerias/thumbs/" . $tbl->getFileCapa() : ""; ?>" alt=""/>
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            <div>
													<span class="btn default btn-file">
													<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar Imagem</span>
													<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
													<input type="file" class="default" id="file_Capa" name="file_Capa"/>
                                                          <input type="hidden" id="file_Capa_original" name="file_Capa_original"
                                                                 value="<?= $tbl->getFileCapa() ?>">
													</span>
                                <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a>
                            </div>
                        </div>
                        <span class="label label-danger">NOTA!</span>
											<span>
											A imagem � anexo � suportado nas �ltimas vers�es do Firefox, Chrome, Opera,
											Safari e Internet Explorer 10.
											</span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="tx_Conteudo">Texto</label>

                    <div class="col-md-9">
                        <textarea class="ckeditor form-control validate[required]" name="tx_Conteudo" id="tx_Conteudo" rows="6"><?= $tbl->getTxConteudo() ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="dh_Cadastro">Data da Publica��o</label>

                    <div class="col-md-9 ">
                        <input type="text" name="dh_Cadastro" id="dh_Cadastro" class="form-control maskDateTime validate[required]" value="<?= $tbl->getDhCadastro() == NULL ? DATETIME : $tbl->getDhCadastro() ?>"/>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="is_LiberarComentarios">Liberar Coment�rios ?</label>

                    <div class="col-md-9">
                        <div class="make-switch" data-on-label="Sim" data-off-label="N�o">
                            <input type="checkbox" name="is_LiberarComentarios" id="is_LiberarComentarios" value="TRUE"  <?= $tbl->getIsLiberarComentarios() == "0" ? "" : "checked" ?> class="toggle"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="is_Ativo">Ativo ?</label>

                    <div class="col-md-9">
                        <div class="make-switch" data-on-label="Sim" data-off-label="N�o">
                            <input type="checkbox" name="is_Ativo" id="is_Ativo" value="TRUE"  <?= $tbl->getIsAtivo() == "0" ? "" : "checked" ?> class="toggle"/>
                        </div>
                    </div>
                </div>


                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <?php if (isset($_GET["id"]) == NULL) { ?>
                                    <button type="submit" class="btn blue" name="action" value="Salvar"><i class="fa fa-check"></i> Cadastrar</button>
                                <?php } else { ?>
                                    <button type="submit" class="btn green" name="action" value="Editar"><i class="fa fa-pencil"></i> Salvar</button>
                                <?php } ?>
                                <button type="button" class="btn default" onclick="location.href='../Listar/'">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
        <?php if (isset($_GET["id"]) != NULL) { ?>
            <div class="tab-pane portlet-body" id="tab_1_3">
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            <span class="label label-danger">NOTA!</span>&nbsp;
                            Suportado somente nas �ltimas vers�es do Firefox, Chrome, Opera, Safari e Internet Explorer 10.
                        </p>

                        <form action="<?= $Html->ActionLink(NULL, "Upload") ?>?id=<?= $tbl->getIdConteudos() ?>" enctype="multipart/form-data" class="dropzone" id="my-dropzone">

                        </form>
                    </div>
                </div>
                <div class="row ">
                     <div class="col-md-12">
                         <p>
                        <button class="btn blue" onclick="javascript:window.location.reload()">Atualizar</button>
                         </p>
                    </div>
                </div>
                <div class="row mix-grid thumbnails">
                    <?php /** @var $arq Arquivos */
                    foreach ($listaArquivos as $arq): ?>

                        <div class="col-md-3 col-sm-4 mix">
                            <div class="mix-inner">
                                <img class="img-responsive" src="../../../public/upload/Galerias/<?= $arq->getIdConteudosFk() ?>/thumbs/<?= $arq->getFileArquivo() ?>" alt="">

                                <div class="mix-details">
                                    <a class="mix-link" href="../RemoverFoto/?id=<?= $tbl->getIdConteudos() ?>&foto=<?=$arq->getIdArquivos()?>#tab_1_3"><i class="fa fa-trash-o"></i></a>
                                    <a class="mix-preview fancybox-button" href="../../../public/upload/Galerias/<?= $arq->getIdConteudosFk() ?>/<?= $arq->getFileArquivo() ?>" title="Project Name" data-rel="fancybox-button"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; ?>


                </div>
            </div>
        <?php } ?>
    </div>
</div>
