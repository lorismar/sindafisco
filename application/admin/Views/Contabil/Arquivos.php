<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var Arquivos $tbl
 */



?>

<div class="tabbable-custom ">
    <ul class="nav nav-tabs">
        <li id="tab_registros">
            <a href="#tab_1_1" data-toggle="tab">Registros</a>
        </li>
        <li id="tab_editcad">
            <a href="#tab_1_2" data-toggle="tab"> <?= isset($_GET["id"]) != NULL ? "Editar" : "Cadastrar" ?></a>
        </li>

    </ul>

    <div class="tab-content" style="border: 1px solid #ddd;">
        <div class="tab-pane" id="tab_1_1">
            <div class="portlet-body">
                <div class="table-responsive">
                    <!--COME�O TABELA PRINCIPAL ID->tabelaPrincipal  -->
                    <table class="table table-condensed table-hover" id="tabelaPrincipal">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Titulo</th>
                            <th>Categoria</th>
                            <th>Arquivo</th>
                            <th>Data de Cadastro</th>
                            <th>A�ao</th>

                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <!--FIM TABELA PRINCIPAL -->
                </div>
            </div>
        </div>
        <div class="tab-pane portlet-body form" id="tab_1_2">
            <form action="../Salvar/" method="post" id="validate" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-body">

                    <div class="form-group display-hide">
                        <label class="control-label col-md-3 col-lg-1" for="id_Arquivos">ID</label>

                        <div class="col-md-2">
                            <input type="text" name="id_Arquivos" id="id_Arquivos" class="form-control" readonly value="<?= $tbl->getIdArquivos() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Titulo">T�tulo</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Titulo" id="ds_Titulo" class="form-control validate[required]" maxlength="100" value="<?= $tbl->getDsTitulo() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="file_Capa">Arquivo</label>

                        <div class="col-md-9">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-group">
													<span class="input-group-btn">
														<span class="uneditable-input">
															<i class="fa fa-file fileupload-exists"></i>
															<span class="fileupload-preview">
															</span>
														</span>
													</span>
													<span class="btn default btn-file">
														<span class="fileupload-new">
															<i class="fa fa-paper-clip"></i> Selecione o Arquivo
														</span>
														<span class="fileupload-exists">
															<i class="fa fa-undo"></i> Alterar
														</span>
														<input type="file" class="default" id="file_Arquivo" name="file_Arquivo"/>
                                                         <input type="hidden" id="file_Arquivo_original" name="file_Arquivo_original"
                                                                value="<?= $tbl->getFileArquivo() ?>">
													</span>
                                    <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remover</a>
                                </div>
                            </div>
                            <?php if ($tbl->getFileArquivo() != NULL) { ?>
                                <span class="label label-danger">Arquivo Cadastrado!</span>
                                <span>
											<a href="<?= CONTENT ?>upload/Contabil/<?= $tbl->getFileArquivo() ?>" target="_blank"><?= $tbl->getFileArquivo() ?></a>
											</span>
                            <?php } ?>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Descricao">Descri��o</label>

                        <div class="col-md-9">
                            <textarea class="wysihtml5 form-control validate[required]" name="ds_Descricao" id="ds_Descricao" rows="6"><?= $tbl->getDsDescricao() ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="dt_Cadastro">Data da Publica��o</label>

                        <div class="col-md-9 ">
                            <input type="text" name="dt_Cadastro" id="dt_Cadastro" class="form-control maskDate validate[required]" value="<?= $tbl->getDtCadastro() == NULL ? DATE : $tbl->getDtCadastro() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="id_Categorias_fk">Situa��o</label>

                        <div class="col-md-9 ">
                            <select name="id_Categorias_fk" id="id_Categorias_fk" class="form-control validate[required]">
                                <option value=""></option>
                                <?php
                                /** @var $c Categorias */
                                foreach ($listaCategorias as $c) {
                                    $selected = "";
                                    if ($tbl->getIdCategoriasFk() == $c->getIdCategorias()) {
                                        $selected = "selected";
                                    }

                                    ?>
                                    <option value="<?= $c->getIdCategorias() ?>"  <?= $selected ?> ><?= $c->getDsDescricao() ?></option>

                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="is_Ativo">Habilitado ?</label>

                        <div class="col-md-9">
                            <div class="make-switch" data-on-label="Sim" data-off-label="N�o">
                                <input type="checkbox" name="is_Ativo" id="is_Ativo" value="TRUE"  <?= $tbl->getIsAtivo() == "0" ? "" : "checked" ?> class="toggle"/>
                            </div>
                        </div>
                    </div>


                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-offset-3 col-md-9">
                                    <?php if (isset($_GET["id"]) == NULL) { ?>
                                        <button type="submit" class="btn blue" name="action" value="Salvar"><i class="fa fa-check"></i> Cadastrar</button>
                                    <?php } else { ?>
                                        <button type="submit" class="btn green" name="action" value="Editar"><i class="fa fa-pencil"></i> Salvar</button>

                                    <?php } ?>
                                    <button type="button" class="btn default" onclick="location.href='../Listar/'">Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>

    </div>
</div>
