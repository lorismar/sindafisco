<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var Categorias $Categoria
 * @var Arquivos $tbl
 */

?>

    <div class="dtree">

        <p><a href="javascript: d.openAll();">Expandir tudo</a> | <a href="javascript: d.closeAll();">Fechar tudo</a>
        </p>
        <br/>
        <hr/>
        <br/>
        <script type="text/javascript">
            <!--

            d = new dTree('d');
            d.closeAll();
            d.add(0, -1, 'Contabilidade', '');
            <?php
                    $contador = 0;
                    $contador_posicao = $contador;

            /** @var $lista Arquivos */
    foreach (Arquivos::ListarTodos("WHERE id_CategoriasPrincipal_fk = 5 and id_Arquivos_fk IS NULL ORDER BY id_Arquivos ASC") as $lista) {
                        $titulo   = $lista->getDsTitulo();
                        $id       = $lista->getIdArquivos();
                        $contador++;
                        $icone = "";
                        $icone = retornaIcone($lista->getFileArquivo());
                        $link= "";
                        if ($lista->getFileArquivo() != NULL)
                        $link = CONTENT."upload/Contabil/{$lista->getFileArquivo()}";

                        echo "\nd.add({$contador},{$contador_posicao},'{$titulo}','{$link}','','blank','{$icone}');";
                        $contador_pai = $contador;
                        /** @var $f1 Arquivos */
                        foreach (Arquivos::ListarTodos("WHERE id_CategoriasPrincipal_fk = 5 and id_Arquivos_fk = {$id} ORDER BY id_Arquivos ASC") as $f1) {
                            $contador++;
                            $icone = retornaIcone($f1->getFileArquivo());
                            $link = "";
                            if ($f1->getFileArquivo() != NULL)
                        $link = CONTENT."upload/Contabil/{$f1->getFileArquivo()}";
                            echo "\nd.add({$contador},{$contador_pai},'{$f1->getDsTitulo()}','{$link}','','blank','{$icone}');";
                            $contador_f1 = $contador;
                             /** @var $f2 Arquivos */
                        foreach (Arquivos::ListarTodos("WHERE id_CategoriasPrincipal_fk = 5 and id_Arquivos_fk = {$f1->getIdArquivos()} ORDER BY id_Arquivos ASC") as $f2) {
                            $contador++;
                            $icone = retornaIcone($f2->getFileArquivo());
                            $link = "";
  if ($f2->getFileArquivo() != NULL)
                        $link = CONTENT."upload/Contabil/{$f2->getFileArquivo()}";
                            echo "\nd.add({$contador},{$contador_f1},'{$f2->getDsTitulo()}','{$link}','','blank','{$icone}');";
                                  $contador_f2 = $contador;
                             /** @var $f3 Arquivos */
                        foreach (Arquivos::ListarTodos("WHERE id_CategoriasPrincipal_fk = 5 and id_Arquivos_fk = {$f2->getIdArquivos()} ORDER BY id_Arquivos ASC") as $f3) {
                            $contador++;
                            $icone = "";
                         $icone = retornaIcone($f3->getFileArquivo());
                        $link = "";
                        if ($f3->getFileArquivo() != NULL)
                        $link = CONTENT."upload/Contabil/{$f3->getFileArquivo()}";
                            echo "\nd.add({$contador},{$contador_f2},'{$f3->getDsTitulo()}','{$link}','','blank','{$icone}');";
                        }
                        }

                        }
                    }

            ?>

            document.write(d);

            //-->
        </script>
    </div>

<?php if (isset($_GET['dtree'])) : ?>
    <hr/>
    <form action="../Salvar/" method="post" id="validate" class="form-horizontal" enctype="multipart/form-data">
        <div class="form-body">

            <div class="form-group display-hide">
                <label class="control-label col-md-3 col-lg-1" for="id_Arquivos">ID</label>

                <div class="col-md-2">
                    <input type="text" name="id_Arquivos" id="id_Arquivos" class="form-control" readonly value="<?= $tbl->getIdArquivos() ?>"/>
                </div>
            </div>

            <div class="form-group display-hide">
                <label class="control-label col-md-3 col-lg-1" for="id_Arquivos_fk">ID</label>

                <div class="col-md-2">
                    <input type="text" name="id_Arquivos_fk" id="id_Arquivos_fk" class="form-control" readonly value="<?= $tbl->getIdArquivosFk() ?>"/>
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-md-3 col-lg-1" for="ds_Titulo">T�tulo</label>

                <div class="col-md-9 ">
                    <input type="text" name="ds_Titulo" id="ds_Titulo" class="form-control validate[required]" maxlength="100" value="<?= $tbl->getDsTitulo() ?>"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-lg-1" for="file_Capa">Arquivo</label>

                <div class="col-md-9">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="input-group">
													<span class="input-group-btn">
														<span class="uneditable-input">
															<i class="fa fa-file fileupload-exists"></i>
															<span class="fileupload-preview">
															</span>
														</span>
													</span>
													<span class="btn default btn-file">
														<span class="fileupload-new">
															<i class="fa fa-paper-clip"></i> Selecione o Arquivo
														</span>
														<span class="fileupload-exists">
															<i class="fa fa-undo"></i> Alterar
														</span>
														<input type="file" class="default" id="file_Arquivo" name="file_Arquivo"/>
                                                         <input type="hidden" id="file_Arquivo_original" name="file_Arquivo_original"
                                                                value="<?= $tbl->getFileArquivo() ?>">
													</span>
                            <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remover</a>
                        </div>
                    </div>
                    <?php if ($tbl->getFileArquivo() != NULL) { ?>
                        <span class="label label-danger">Arquivo Cadastrado!</span>
                        <span>
											<a href="<?= CONTENT ?>upload/Contabil/<?= $tbl->getFileArquivo() ?>" target="_blank"><?= $tbl->getFileArquivo() ?></a>
											</span>
                    <?php } ?>
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-md-3 col-lg-1" for="dt_Cadastro">Data da Publica��o</label>

                <div class="col-md-9 ">
                    <input type="text" name="dt_Cadastro" id="dt_Cadastro" class="form-control maskDate validate[required]" value="<?= $tbl->getDtCadastro() == NULL ? DATE : $tbl->getDtCadastro() ?>"/>
                </div>
            </div>


            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-offset-3 col-md-9">
                            <?php if (isset($_GET["id"]) == NULL) { ?>
                                <button type="submit" class="btn blue" name="action" value="Salvar"><i class="fa fa-check"></i> Cadastrar</button>
                            <?php } else { ?>
                                <button type="submit" class="btn green" name="action" value="Editar"><i class="fa fa-pencil"></i> Salvar</button>
                                <?php if ($_GET['dtree'] != "editar"): ?>
                                    <a class="btn purple" href="../Adicionar/?fk=<?= $tbl->getIdArquivos() ?>&dtree=adicionar"><i class="fa fa-plus"></i> Adicionar Filho</a>
                                <?php endif; ?>
                                <a class="btn red" href="../Deletar/?did=<?= $tbl->getIdArquivos() ?>"><i class="fa fa-trash-o"></i> Deletar</a>

                            <?php } ?>
                            <button type="button" class="btn default" onclick="location.href='../Listar/'">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
<?php endif; ?>