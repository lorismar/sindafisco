<?php
/**
 * User: Gustavo
 * @var Arquivos $tbl
 */

?>

<div class="portlet default">
    <div class="portlet-body">

        <div class="table-responsive">

            <div class="row">
                <form method="get" name="form" enctype="multipart/form-data">
                    <div class="col-lg-3 col-md-3 col-sm-1">
                        <select name="id_Categorias_fk" id="id_Categorias_fk" class="form-control col-md-1">
                            <option value="">Todos</option>
                            <?php
                            /** @var $per Categorias */
                            foreach ($listaCategorias as $per) {
                                $selected = "";
                                if (isset($_GET['id_Categorias_fk'])) {
                                    if ($per->getIdCategorias() == $_GET['id_Categorias_fk']) {
                                        $selected = "selected";
                                    }
                                }
                                ?>
                                <option value="<?= $per->getIdCategorias() ?>" <?= $selected ?>><?= $per->getDsDescricao() ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                    <button class="btn btn-success" type="submit">Filtrar</button>
                </form>
            </div>
            <hr/>
            <!--COME�O TABELA PRINCIPAL ID->tabelaPrincipal  -->
            <table class="table table-condensed table-hover" id="tabelaPrincipal">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Titulo</th>
                    <th>Categoria</th>
                    <th>Arquivo</th>
                    <th>Data de Cadastro</th>
                    <th>A�ao</th>

                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
            <!--FIM TABELA PRINCIPAL -->
        </div>
    </div>
</div>