<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var Categorias $Categoria
 */

?>

<div class="tabbable-custom ">
    <ul class="nav nav-tabs">
        <li id="tab_registros">
            <a href="#tab_1_1" data-toggle="tab">Registros</a>
        </li>
        <li id="tab_editcad">
            <a href="#tab_1_2" data-toggle="tab"> <?= isset($_GET["id"]) != NULL ? "Editar" : "Cadastrar" ?></a>
        </li>
    </ul>
    <div class="tab-content" style="border: 1px solid #ddd;">
        <div class="tab-pane" id="tab_1_1">
            <div class="portlet-body">
                <div class="table-responsive">
                    <!--COME�O TABELA PRINCIPAL ID->tabelaPrincipal  -->
                    <table class="table table-condensed table-hover" id="tabelaPrincipal">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>A�ao</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        /** @var $Cat1 Categorias */
                        foreach ($listaCategorias as $Cat1) {
                            ?>
                            <tr>
                                <td><?= $Cat1->getDsDescricao() ?></td>
                                <td>
                                    <a href='../EditarCategoria/?id=<?= $Cat1->getIdCategorias() ?>' class='btn default btn-xs purple'><i class='fa fa-edit'> Editar</i></a>
                                    <button onclick="Remover('../DeletarCategoria/?did=<?= $Cat1->getIdCategorias() ?>')" class='btn default btn-xs red'><i class='fa fa-trash-o'> Remover</i></button>
                                </td>
                            </tr>
                            <?php
                            /** @var $Cat2 Categorias */
                            foreach (Categorias::ListarTodos("WHERE id_Categorias_fk = {$Cat1->getIdCategorias()}") as $Cat2) {
                                ?>
                                <tr>
                                    <td><?= $Cat1->getDsDescricao() ?></td>
                                    <td>
                                        <a href='../EditarCategoria/?id=<?= $Cat2->getIdCategorias() ?>' class='btn default btn-xs purple'><i class='fa fa-edit'> Editar</i></a>
                                        <button onclick="Remover('../DeletarCategoria/?did=<?= $Cat2->getIdCategorias() ?>')" class='btn default btn-xs red'><i class='fa fa-trash-o'> Remover</i></button>
                                    </td>
                                </tr>
                            <?php
                                /** @var $Cat3 Categorias */
                                foreach (Categorias::ListarTodos("WHERE id_Categorias_fk = {$Cat2->getIdCategorias()}") as $Cat3) {
                                    ?>
                                    <tr>
                                        <td><?= $Cat1->getDsDescricao() ?></td>
                                        <td>
                                            <a href='../EditarCategoria/?id=<?= $Cat3->getIdCategorias() ?>' class='btn default btn-xs purple'><i class='fa fa-edit'> Editar</i></a>
                                            <button onclick="Remover('../DeletarCategoria/?did=<?= $Cat3->getIdCategorias() ?>')" class='btn default btn-xs red'><i class='fa fa-trash-o'> Remover</i></button>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    <!--FIM TABELA PRINCIPAL -->
                </div>
            </div>
        </div>
        <div class="tab-pane portlet-body form" id="tab_1_2">
            <form action="../SalvarCategoria/" method="post" id="validate" class="form-horizontal">
                <div class="form-body">

                    <div class="form-group display-hide">
                        <label class="control-label col-md-3 col-lg-1" for="id_PRO_Categorias">ID</label>

                        <div class="col-md-2">
                            <input type="text" name="id_Categorias" id="id_Categorias" class="form-control" readonly value="<?= $Categoria->getIdCategorias() ?>"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Descricao">Nome da Categoria</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Descricao" id="ds_Descricao" class="form-control validate[required]" maxlength="45" value="<?= $Categoria->getDsDescricao() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="id_Categorias_fk">Categoria Pai</label>

                        <div class="col-md-9 ">
                            <select name="id_Categorias_fk" id="id_Categorias_fk" class="form-control">
                                <option value=""></option>
                                <?php
                                /** @var $cat Categorias */
                                foreach ($listaCategorias as $cat) :
                                    $selected = "";
                                    if ($Categoria->getIdCategoriasFk() == $cat->getIdCategorias()) :
                                        $selected = "selected";
                                    endif;

                                    ?>
                                    <option value="<?= $cat->getIdCategorias() ?>"  <?= $selected ?> ><?= $cat->getDsDescricao() ?></option>
                                    <?php

                                    /** @var $cat2 Categorias */
                                    foreach (Categorias::ListarTodos("WHERE id_Categorias_fk = {$cat->getIdCategorias()}") as $cat2) :
                                        $selected = "";
                                        if ($Categoria->getIdCategoriasFk() == $cat2->getIdCategorias()) :
                                            $selected = "selected";
                                        endif;

                                        ?>
                                        <option value="<?= $cat2->getIdCategorias() ?>"  <?= $selected ?> ><?= $cat2->getDsDescricao() ?></option>

                                    <?php endforeach; ?>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>


                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <?php if (isset($_GET["id"]) == NULL) { ?>
                                    <button type="submit" class="btn blue" name="action" value="Salvar"><i class="fa fa-check"></i> Cadastrar</button>
                                <?php } else { ?>
                                    <button type="submit" class="btn green" name="action" value="Editar"><i class="fa fa-pencil"></i> Salvar</button>
                                <?php } ?>
                                <button type="button" class="btn default" onclick="location.href='../ListarCategorias/'">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>

    </div>
</div>
