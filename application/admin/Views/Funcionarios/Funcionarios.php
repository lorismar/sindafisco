<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var Pessoas $tbl
 */




?>

<div class="tabbable-custom ">

    <ul class="nav nav-tabs">
        <li id="tab_registros">
            <a href="#tab_1_1" data-toggle="tab">Registros</a>
        </li>
        <li id="tab_editcad">
            <a href="#tab_1_2" data-toggle="tab"> <?= isset($_GET["id"]) != NULL ? "Editar" : "Cadastrar" ?></a>
        </li>

    </ul>

    <div class="tab-content" style="border: 1px solid #ddd;">
        <div class="tab-pane" id="tab_1_1">
            <div class="portlet-body">
                <div class="table-responsive">
                    <!--COME�O TABELA PRINCIPAL ID->tabelaPrincipal  -->
                    <table class="table table-condensed table-hover" id="tabelaPrincipal">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>CPF</th>
                            <th>Situa��o</th>
                            <th>A�ao</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php /** @var $filiado Pessoas */
                        foreach ($listaFiliados as $filiado): $filiado->getSitPessoas()->VisualizarPorId(); ?>
                            <tr>
                                <td><?= $filiado->getDsNome() ?></td>
                                <td><?= $filiado->getDsCpf() ?></td>
                                <td><?= $filiado->getSitPessoas()->getDsDescricao() ?></td>
                                <td><a href='../Editar/?id=<?= $filiado->getIdPessoas() ?>' class='btn default btn-xs purple'><i class='fa fa-edit'> Editar</i></a> <button onclick="javascript:Remover('../Deletar/?did=<?= $filiado->getIdPessoas() ?>')" class="btn default btn-xs red"><i class="fa fa-trash-o"> Deletar</i></button> </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <!--FIM TABELA PRINCIPAL -->
                </div>
            </div>
        </div>
        <div class="tab-pane portlet-body form" id="tab_1_2">
            <form action="../Salvar/" method="post" id="validate" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">

                <div class="form-body">

                <div class="form-group display-hide">
                        <label class="control-label col-md-3 col-lg-1" for="id_Pessoas">ID</label>

                        <div class="col-md-2">
                            <input type="text" name="id_Pessoas" id="id_Pessoas" class="form-control" readonly value="<?= $tbl->getIdPessoas() ?>"/>
                        </div>
                    </div>

                    <h3 class="form-section">Informa��es</h3>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Cpf">CPF:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Cpf" id="ds_Cpf" class="form-control validate[required] maskCpf" maxlength="14" value="<?= $tbl->getDsCpf() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Senha">Senha:</label>

                        <div class="col-md-9 ">
                            <input type="password" name="ds_Senha" id="ds_Senha" class="form-control" maxlength="20" value="<?= mycrypt_decode($tbl->getUsuarios()->getDsSenha()) ?>"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Rg">RG:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Rg" id="ds_Rg" class="form-control" maxlength="20" value="<?= $tbl->getDsRg() ?>"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_OrgaoExpedidor">Org�o Expedidor:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_OrgaoExpedidor" id="ds_OrgaoExpedidor" class="form-control" maxlength="10" value="<?= $tbl->getDsOrgaoExpedidor() ?>"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Nome">Nome:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Nome" id="ds_Nome" class="form-control validate[required]" maxlength="100" value="<?= $tbl->getDsNome() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="file_Foto">Foto</label>

                        <div class="col-md-9">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="<?= $tbl->getFileFoto() != NULL ? "../../../public/upload/Pessoas/thumbs/" . $tbl->getFileFoto() : ""; ?>" alt=""/>
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
													<span class="btn default btn-file">
													<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar Imagem</span>
													<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
													<input type="file" class="default" id="file_Foto" name="file_Foto"/>
                                                          <input type="hidden" id="file_Foto_original" name="file_Foto_original"
                                                                 value="<?= $tbl->getFileFoto() ?>">
													</span>
                                    <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a>
                                </div>
                            </div>
                            <span class="label label-danger">NOTA!</span>
											<span>
											Suportado somente nas �ltimas vers�es do Firefox, Chrome, Opera,
											Safari e Internet Explorer 10.
											</span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="dt_Nascimento">Data de Nascimento:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="dt_Nascimento" id="dt_Nascimento" class="form-control validate[required] maskDate" maxlength="30" value="<?= $tbl->getDtNascimento() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Sexo">Sexo</label>

                        <div class="col-md-9 ">
                            <select name="ds_Sexo" id="ds_Sexo" class="form-control validate[required]">
                                <option value="M" <?= $tbl->getDsSexo() == "M" ? "selected" : "" ?>>Masculino</option>
                                <option value="F" <?= $tbl->getDsSexo() == "F" ? "selected" : "" ?>>Feminino</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_EstadoCivil">Estado Civ�l</label>

                        <div class="col-md-9 ">
                            <select name="ds_EstadoCivil" id="ds_EstadoCivil" class="form-control validate[required]">
                                <option value="Solteiro" <?= $tbl->getDsEstadoCivil() == "Solteiro" ? "selected" : "" ?>>Solteiro</option>
                                <option value="Casado" <?= $tbl->getDsEstadoCivil() == "Casado" ? "selected" : "" ?>>Casado</option>
                                <option value="Divorciado" <?= $tbl->getDsEstadoCivil() == "Divorciado" ? "selected" : "" ?>>Divorciado</option>
                                <option value="Outros" <?= $tbl->getDsEstadoCivil() == "Outros" ? "selected" : "" ?>>Outros</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_NomePai">Nome do Pai:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_NomePai" id="ds_NomePai" class="form-control" maxlength="100" value="<?= $tbl->getDsNomePai() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_NomeMae">Nome da M�e:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_NomeMae" id="ds_NomeMae" class="form-control" maxlength="100" value="<?= $tbl->getDsNomeMae() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_NomeConjuge">Nome do C�njuge:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_NomeConjuge" id="ds_NomeConjuge" class="form-control" maxlength="100" value="<?= $tbl->getDsNomeConjuge() ?>"/>
                        </div>
                    </div>


                    <h3 class="form-section">Contato</h3>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Email">E-Mail:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Email" id="ds_Email" class="form-control validate[custom[email],required]" maxlength="100" value="<?= $tbl->getDsEmail() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_EmailAlternativo">E-Mail Alternativo:</label>

                        <div class="col-md-9 ">
                        <input type="text" name="ds_EmailAlternativo" id="ds_EmailAlternativo" class="form-control validate[custom[email]]" maxlength="100" value="<?= $tbl->getDsEmailAlternativo() ?>"/>
                    </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Telefone">Telefone:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Telefone" id="ds_Telefone" class="form-control maskTel" maxlength="14" value="<?= $tbl->getDsTelefone() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_Celular">Celular:</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_Celular" id="ds_Celular" class="form-control maskCel" maxlength="15" value="<?= $tbl->getDsCelular() ?>"/>
                        </div>
                    </div>



                    <h3 class="form-section">Endere�o Residencial</h3>



                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_End_Completo">Endere�o</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_End_Completo" id="ds_End_Completo" class="form-control" maxlength="200" value="<?= $tbl->getDsEndCompleto() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_End_Bairro">Bairro</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_End_Bairro" id="ds_End_Bairro" class="form-control" maxlength="100" value="<?= $tbl->getDsEndBairro() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_End_Cep">CEP</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_End_Cep" id="ds_End_Cep" class="form-control maskCep" maxlength="9" value="<?= $tbl->getDsEndCep() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_End_Cidade">Cidade</label>

                        <div class="col-md-9 ">
                            <input type="text" name="ds_End_Cidade" id="ds_End_Cidade" class="form-control" maxlength="60" value="<?= $tbl->getDsEndCidade() ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="ds_End_UF">UF</label>

                        <div class="col-md-9">
                            <input type="text" name="ds_End_UF" id="ds_End_UF" class="form-control" maxlength="2" value="<?= $tbl->getDsEndUf() ?>"/>
                        </div>
                    </div>

                    <h3 class="form-section"></h3>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="id_SitPessoas_fk">Situa��o</label>

                        <div class="col-md-9 ">
                            <select name="id_SitPessoas_fk" id="id_SitPessoas_fk" class="form-control validate[required]">
                                <option value=""></option>
                                <?php
                                /** @var $sit SitPessoas */
                                foreach ($listaTodasSit as $sit) {
                                    $selected = "";
                                    if ($tbl->getIdSitPessoasFk() == $sit->getIdSitPessoas()) {
                                        $selected = "selected";
                                    }

                                    ?>
                                    <option value="<?= $sit->getIdSitPessoas() ?>"  <?= $selected ?> ><?=  $sit->getDsDescricao() ?></option>

                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-1" for="id_Perfis_fk">Perfil</label>

                        <div class="col-md-9 ">
                            <select name="id_Perfis_fk" id="id_Perfis_fk" class="form-control validate[required]">
                                <option value=""></option>
                                <?php
                                /** @var $per Perfis */
                                foreach ($listaPerfis as $per) {
                                    $selected = "";
                                    if ($tbl->getUsuarios()->getIdPerfisFk() == $per->getIdPerfis()) {
                                        $selected = "selected";
                                    }

                                    ?>
                                    <option value="<?= $per->getIdPerfis() ?>"  <?= $selected ?> ><?=  $per->getDsDescricao() ?></option>

                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <?php if (isset($_GET["id"]) == NULL) { ?>
                                    <button type="submit" class="btn blue" name="action" value="Salvar"><i class="fa fa-check"></i> Cadastrar</button>
                                <?php } else { ?>
                                    <button type="submit" class="btn green" name="action" value="Editar"><i class="fa fa-pencil"></i> Salvar</button>
                                <?php } ?>
                                <button type="button" class="btn default" onclick="location.href='../Listar/'">Cancelar</button>
                            </div>
                        </div>
                    </div>



                </div>

            </form>
        </div>

    </div>
</div>
