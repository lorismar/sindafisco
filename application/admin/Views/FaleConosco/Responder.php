<?php
/**
 * User: Gustavo
 * @var FaleConosco $tbl
 */



?>

<div class="tabbable-custom ">
    <ul class="nav nav-tabs">
        <li id="tab_editcad">
            <a href="#tab_1_2" data-toggle="tab">Responder</a>
        </li>
    </ul>

    <div class="tab-content" style="border: 1px solid #ddd;">

        <div class="tab-pane portlet-body form" id="tab_1_2">
            <form action="<?= $Html->ActionLink(NULL, "Salvar") ?>" method="post" id="validate" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-body">

                    <div class="form-group display-hide">
                        <label class="control-label col-md-3 col-lg-1" for="id_FaleConosco">ID</label>

                        <div class="col-md-2">
                            <input type="text" name="id_FaleConosco" id="id_FaleConosco" class="form-control" readonly value="<?= $tbl->getIdFaleConosco() ?>"/>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="ds_Assunto">Assunto</label>

                    <div class="col-md-9 ">
                        <input type="text" name="ds_Assunto" id="ds_Assunto" class="form-control validate[required]"  readonly maxlength="45" value="<?= $tbl->getDsAssunto() ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="ds_Nome">Nome</label>

                    <div class="col-md-9 ">
                        <input type="text" name="ds_Nome" id="ds_Nome" class="form-control validate[required]"  readonly maxlength="100" value="<?= $tbl->getDsNome() ?>"/>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="ds_Email">E-Mail</label>

                    <div class="col-md-9 ">
                        <input type="text" name="ds_Email" id="ds_Email" class="form-control validate[required]"  readonly maxlength="100" value="<?= $tbl->getDsEmail() ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="dh_Postagem">Data da Mensagem</label>

                    <div class="col-md-9 ">
                        <input type="text" name="dh_Postagem" id="dh_Postagem" class="form-control maskDateTime validate[required]" readonly value="<?= $tbl->getDhPostagem() ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="tx_Texto">Mensagem</label>

                    <div class="col-md-9">
                        <textarea class="form-control" name="tx_Texto" id="tx_Texto" rows="6" readonly><?= $tbl->getTxTexto() ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="tx_Resposta">Texto de Resposta</label>

                    <div class="col-md-9">
                        <textarea class="ckeditor form-control validate[required]" name="tx_Resposta" id="tx_Resposta" rows="6"><?= $tbl->getTxResposta() ?></textarea>
                    </div>
                </div>



                <div class="form-group">
                    <label class="control-label col-md-3 col-lg-1" for="dh_Resposta">Data da Resposta</label>

                    <div class="col-md-9 ">
                        <input type="text" name="dh_Resposta" id="dh_Resposta" class="form-control maskDateTime validate[required]" value="<?= $tbl->getDhResposta() == NULL ? DATETIME : $tbl->getDhResposta() ?>"/>
                    </div>
                </div>





                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">

                                    <button type="submit" class="btn green" name="action" value="Responder"><i class="fa fa-mail-forward"></i> Responder</button>

                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>

    </div>
</div>
