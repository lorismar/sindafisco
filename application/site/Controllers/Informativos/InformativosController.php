<?php
/**
 * User: Iuri Gustavo
 * Date: 29/08/13
 * Time: 12:57
 * @property Conteudos tbl
 */

class InformativosController extends site_Controller
{
    function beforeAction()
    {
        $this->tbl = new Conteudos(NULL);
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
    }

    function Listar()
    {
        $this->view->setTemplate("Site");
        $this->view->setTitulo("Listar Informativos");
        return "Listar";
    }

    function Escape()
    {
        $this->view->setTemplate("Site");
        $this->tbl->VisualizarPorPermalinkCategoria($_GET['id'], 4);
        $this->view->setTitulo($this->tbl->getDsTitulo());
        return "Informativo";
    }

    function Grid()
    {
        $aColunas        = array(array("id_Conteudos", '
        <div class="eight columns alpha">
                    <article class="recent-blog">
                        <section class="date">
                            <span class="day">{2}</span>
                            <span class="mesano">{3}/{9}</span>
                        </section>
                        <h4><a href="../{7}/">{4}</a></h4>
                        <p>{5}</p>
                        <a href="?categoria={8}"><span style="margin-left: 60px;" class="highlight color">{6}</span></a>
                    </article>
                </div>
        '), 'IFNULL(file_Capa,\'blank.png\')', 'DATE_FORMAT(dh_Cadastro, \'%d\')', 'DATE_FORMAT(dh_Cadastro, \'%m\')', 'ds_Titulo', 'tx_Resumo', 'ds_Descricao', 'ds_PermaLink', 'id_Categorias', 'DATE_FORMAT(dh_Cadastro, \'%Y\')');
        $aColunasFiltros = array('ds_Titulo');
        $sColunaIndex    = "id_Conteudos";
        $sTabela         = "Conteudos LEFT JOIN Categorias ON Categorias.id_Categorias = Conteudos.id_Categorias_fk";
        $sCondicao       = "Where Conteudos.id_CategoriasPrincipal_fk = 4 and Conteudos.is_Ativo = TRUE and Conteudos.is_Publico = TRUE ";
        if (isset($_GET['categoria'])) {
            if (is_numeric($_GET['categoria']))
                $sCondicao .= " and id_Categorias = " . $_GET['categoria'];
        }
        $aOrdenacao = array("Conteudos.dh_Cadastro DESC");
        echo Grid::Gera($aColunas, $aColunasFiltros, $sColunaIndex, $sTabela, $sCondicao,$aOrdenacao, array());
    }


}