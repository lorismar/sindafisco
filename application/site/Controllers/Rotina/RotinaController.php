<?php

    /**
     * User: Iuri Gustavo
     * Date: 29/08/13
     * Time: 12:57
     * @property Conteudos tbl
     */
    class RotinaController extends site_Controller
    {
        function beforeAction()
        {
        }

        function afterAction()
        {
        }


        /**
         *
         */
        function EnviarNewsletter()
        {

            //Obtem lista de conte�do de 7 dias atras
            $conteudo = tblConteudos::ListarTodos("WHERE id_CategoriasPrincipal_fk = 1 and is_Ativo = TRUE and Conteudos.dh_Cadastro BETWEEN CURRENT_DATE()-7 AND CURRENT_DATE() ORDER BY Conteudos.dh_Cadastro DESC");
            $this->set("conteudo", $conteudo);

            // Obtem lista de filiados
            $listaFiliados = Pessoas::ListarTodos("WHERE id_TipoPessoas_fk IN (1,2,3) and ds_Email <> '' and  LENGTH(ds_Email) > 5 order by id_Pessoas asc");

            ob_start();
            include(ROOT . DS . 'application' . DS . 'site' . DS . 'Views' . DS . 'Site' . DS . 'Newsletter.php');
            $a = ob_get_contents();

            //modificado a linha 119 vendor/mail/class.smtp.php colocando $port

            echo Email::EnviaNewsletters($listaFiliados, $a);

            exit;

        }


    }