<?php
/**
 * User: Iuri Gustavo
 * Date: 29/08/13
 * Time: 12:57
 * @property Conteudos tbl
 */

class PaginaController extends site_Controller
{
    function beforeAction()
    {
        $this->tbl = new Conteudos(NULL);
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
    }

    function Escape()
    {
        $this->view->setTemplate("Site");
        $this->tbl->VisualizarPorPermalinkCategoria($_GET['id'],2);
        $this->view->setTitulo($this->tbl->getDsTitulo());

        $idPai = $this->tbl->getIdConteudosFk() > 0 ? $this->tbl->getIdConteudosFk() : $this->tbl->getIdConteudos();
        $pai = new Conteudos($idPai);
        $pai->VisualizarPorId();
        $this->set("pai", $pai);

        $listaFilhos = Conteudos::ListarTodos("WHERE is_Ativo = TRUE and id_Conteudos_fk = ". $idPai);

        $this->set("listaFilhos", $listaFilhos);

        return "Pagina";
    }

}