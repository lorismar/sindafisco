<?php
/**
 * User: Iuri Gustavo
 * Date: 29/08/13
 * Time: 12:57
 */

class SiteController extends site_Controller
{
    function beforeAction()
    {
    }

    function afterAction()
    {
    }

    function Principal()
    {

        $this->view->setTemplate("Site");
        $this->view->setTitulo("Principal");

        $youtube = new Youtube();
        $youtube->VisualizarPorUltimoVideo();

        $this->set("youtube", $youtube);


        $listaDestaque = tblConteudos::ListarTodos("WHERE id_CategoriasPrincipal_fk in (1,4) and is_Ativo = TRUE and is_Destaque = TRUE  ORDER BY Conteudos.dh_Cadastro DESC LIMIT 0,3");
        $this->set("listaDestaque", $listaDestaque);

        $listaNormal = tblConteudos::ListarTodos("WHERE id_CategoriasPrincipal_fk = 1 and is_Ativo = TRUE ORDER BY Conteudos.dh_Cadastro DESC LIMIT 0,5");
        $this->set("listaNormal", $listaNormal);

        $listaInformativos = tblConteudos::ListarTodos("WHERE id_CategoriasPrincipal_fk = 4 and is_Ativo = TRUE and is_Publico = TRUE ORDER BY Conteudos.dh_Cadastro DESC LIMIT 0,8");
        $this->set("listaInformativos", $listaInformativos);

        $listaEventos = tblConteudos::ListarTodos("WHERE id_CategoriasPrincipal_fk = 3 and is_Ativo = TRUE ORDER BY Conteudos.dh_Cadastro DESC LIMIT 0,1");
        $this->set("listaEventos", $listaEventos);

        $listaParceiros = Arquivos::ListarTodos("WHERE id_CategoriasPrincipal_fk = 11 and is_Ativo = TRUE");
        $this->set("listaParceiros", $listaParceiros);

        $listaAniversariantes = Pessoas::ListarTodos("WHERE date_format(now(), '%m') = date_format(dt_Nascimento, '%m') AND id_TipoPessoas_fk = 1 ORDER by date_format(dt_Nascimento, '%m%d') ");
        $this->set("listaAniversariantes", $listaAniversariantes);

        if (isset($_GET['erro'])) {
            if ($_GET['erro'] == "101")
                GUI::msg('<div class="notification error closeable" id="notification_1" style="display: block;"><p><span>Erro!</span> Usu�rio/senha inv�lido.</p><a class="close" href="#"><i class="icon-remove"></i></a></div>');
        }

        return "Principal";
    }

    function RecuperarSenha()
    {
        $this->view->setTemplate("Site");
        $this->view->setTitulo("Recuperar Senha");

        if (isset($_POST['cpf'])) {
            $usuario = new Usuarios(NULL);
            $usuario->setDsUsuario($_POST['cpf']);
            if ($usuario->VisualizarPorUsuario()) {
                $usuario->getPessoas()->VisualizarPorId();

                if ($_POST['email'] == $usuario->getPessoas()->getDsEmail()) {
                    Email::EnviaEmail($usuario->getDsNome(), $usuario->getPessoas()->getDsEmail(), "Recupera��o de Senha <br/> Usu�rio: " . $usuario->getDsUsuario() . " <br /> Senha: " . mycrypt_decode($usuario->getDsSenha()));
                    GUI::msg('<div class="notification success closeable" id="notification_1" style="display: block;"><p><span>Sucesso!</span> E-Mail com seus dados foi enviado com sucesso.</p><a class="close" href="#"><i class="icon-remove"></i></a></div>');
                } else {
                    GUI::msg('<div class="notification error closeable" id="notification_1" style="display: block;"><p><span>Erro!</span> Usu�rio/E-Mail inv�lido.</p><a class="close" href="#"><i class="icon-remove"></i></a></div>');
                }

            } else {
                GUI::msg('<div class="notification error closeable" id="notification_1" style="display: block;"><p><span>Erro!</span> Usu�rio/E-Mail inv�lido.</p><a class="close" href="#"><i class="icon-remove"></i></a></div>');
            }
            return $this->Principal();
        }

        return "RecuperarSenha";
    }


}