<?php
/**
 * User: Iuri Gustavo
 * Date: 29/08/13
 * Time: 12:57
 */

class FormularioController extends site_Controller
{
    function beforeAction()
    {
    }

    function afterAction()
    {
    }

    function Cadastro()
    {
        $this->view->setTemplate("Site");
        $this->view->setTitulo("Contato");

        return "Cadastro";
    }

    function CadastroSalvar()
    {
        $this->view->setTemplate("Site");

        if (isset($_POST['ds_Nome'])) {
            $pessoa = new Pessoas(NULL);
            $pessoa->setDsNome($_POST['ds_Nome']);
            $pessoa->setDsCpf($_POST['ds_Cpf']);
            $pessoa->setDsRg($_POST['ds_Rg']);
            $pessoa->setDsOrgaoExpedidor($_POST['ds_OrgaoExpedidor']);
            $pessoa->setDsMatricula($_POST['ds_Matricula']);
            $pessoa->setDtNascimento($_POST['dt_Nascimento']);
            $pessoa->setDsSexo($_POST['ds_Sexo']);
            $pessoa->setDsFormacao($_POST['ds_Formacao']);
            $pessoa->setDsEstadoCivil($_POST['ds_EstadoCivil']);
            $pessoa->setDsNomeConjuge($_POST['ds_NomeConjuge']);
            $pessoa->setNuDependentes($_POST['nu_Dependentes']);
            $pessoa->setDsEmail($_POST['ds_Email']);
            $pessoa->setDsEmailAlternativo($_POST['ds_EmailAlternativo']);
            $pessoa->setDsEndCompleto($_POST['ds_End_Completo']);
            $pessoa->setDsEndBairro($_POST['ds_End_Bairro']);
            $pessoa->setDsEndCep($_POST['ds_End_Cep']);
            $pessoa->setDsEndCidade($_POST['ds_End_Cidade']);
            $pessoa->setDsEndUf($_POST['ds_End_Uf']);
            $pessoa->setDsTelefone($_POST['ds_Telefone']);
            $pessoa->setDsCelular($_POST['ds_Celular']);
            $pessoa->setDsTelefoneTrabalho($_POST['ds_TelefoneTrabalho']);
            $pessoa->setDsLotacao($_POST['ds_Lotacao']);
            $pessoa->setDsNomeMae($_POST['ds_NomeMae']);
            $pessoa->setDsNomePai($_POST['ds_NomePai']);
            $pessoa->setIsPrimeiroAcesso(1);
            $pessoa->setIsAtivo(1);
            $pessoa->setIdSitPessoasFk(2);
            $pessoa->setLogALTERADOPOR("Site");


            //UPLOAD
            $get_imagem = $_FILES["file_Foto"];
            if ($get_imagem['name'] == "")
                $get_imagem = "";
            if (!empty($get_imagem)) {
                $get_imagem = enviarArquivo($get_imagem, 'Pessoas', NULL, NULL);
                $pessoa->setFileFoto($get_imagem);
            }


            if ($pessoa->Salvar()) {

                if (is_numeric($_POST['nu_Dependentes'])) {
                    for ($x = 1; $x <= $_POST['nu_Dependentes']; $x++) {
                        $dep = new Dependentes(NULL);
                        $dep->setDsNome($_POST['dep_ds_Nome_' . $x]);
                        $dep->setDtNascimento($_POST['dep_dt_Nascimento_' . $x]);
                        $dep->setDsParentesco($_POST['dep_ds_Parentesco_' . $x]);
                        $x++;
                        $dep->setIdPessoasFk($pessoa->getIdPessoas());
                        $dep->Salvar();
                    }
                }


                $msg     = array("success", "Mensagem enviada com sucesso. Obrigado por entrar em contato", "Formulário de Contato");
                $usuario = new Usuarios(NULL);
                $usuario->setDsNome($pessoa->getDsNome());
                $usuario->setDsUsuario($pessoa->getDsCpf());
                $usuario->setDsSenha(mycrypt_encode($_POST['ds_Senha']));
                $usuario->setIsAtivo(0);
                $usuario->setIdPerfisFk(99);
                $usuario->setIdPessoasFk($pessoa->getIdPessoas());
                $usuario->setLogALTERADOPOR("Site");
                $usuario->Salvar();
                Email::EnviaEmail($pessoa->getDsNome(), $pessoa->getDsEmail(), "Sua solicitação de fialiação foi enviada com sucesso, favor esperar alguns dias para que possamos validar.");

            } else {
                $msg = array("error", "Ouve um erro durante o envio da mensagem.", "Formulário de Contato");
            }
        } else {
            $msg = array("error", "Ouve um erro durante o envio da mensagem.", "Formulário de Contato");
        }

        $this->set("msg", $msg);
        $this->view->setTitulo($msg[2]);
        return "Mensagem";

    }

    function Contato()
    {
        $this->view->setTemplate("Site");
        $this->view->setTitulo("Contato");
        return "Contato";
    }

    function ContatoSalvar()
    {

        if (isset($_POST['nome'])) {
            $contato = new FaleConosco(NULL);
            $contato->setDsNome($_POST['nome']);
            $contato->setDsAssunto($_POST['assunto']);
            $contato->setDsEmail($_POST['email']);
            $contato->setTxTexto($_POST['mensagem']);
            $contato->setDhPostagem(DATETIME);
            $contato->setIsLido(0);
            if ($contato->Salvar()) {
                $msg = array("success", "Mensagem enviada com sucesso. Obrigado por entrar em contato", "Formulário de Contato");
                Email::ConfirmacaoContato($contato->getDsNome(), $contato->getDsEmail());
            } else {
                $msg = array("error", "Ouve um erro durante o envio da mensagem.", "Formulário de Contato");
            }
        } else {
            $msg = array("error", "Ouve um erro durante o envio da mensagem.", "Formulário de Contato");
        }
        $this->set("msg", $msg);
        $this->view->setTitulo($msg[2]);

        return "Mensagem";
    }


}