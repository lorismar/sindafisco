<?php
/**
 * User: Gustavo
 * Date: 12/04/13
 * Time: 12:58
 */
abstract class site_Controller
{
    protected $_controller;
    protected $_action;
    protected $_template;
    /** @var $View View */
    public $view;
    public $renderizar;

    function __construct($area, $controller, $acao)
    {
        $this->view       = new View();
        $this->_template  = new Template($area, $controller, $acao);
        $this->renderizar = 1;


        if ($controller == "Noticias" || $controller == "Eventos")
            $GLOBALS['gui_js'] .= "\n$(\"#" . strtolower($controller) . "\").addClass(\"atual\");";
        else
            $GLOBALS['gui_js'] .= "\n$(\"#" . strtolower($controller) . "_" . strtolower($acao) . "\").addClass(\"atual\");";



    }

    function set($variavel, $valor)
    {
        $this->_template->set($variavel, $valor);
    }

    function __destruct()
    {
        if ($this->renderizar) {
            $this->_template->renderizar();
        }
    }

}