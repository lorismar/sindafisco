<?php
/**
 * User: Iuri Gustavo
 * Date: 29/08/13
 * Time: 12:57
 * @property Conteudos tbl
 */

class PesquisaController extends site_Controller
{
    function beforeAction()
    {
        $this->tbl = new Conteudos(NULL);
    }

    function afterAction()
    {
        $this->set("tbl", $this->tbl);
    }


    function Procurar()
    {
        $this->view->setTemplate("Site");
        $this->view->setTitulo("Procurar");
        return "Procurar";
    }

    function Grid()
    {
        $aColunas        = array(array("id_Conteudos", '
        <div class="eight columns alpha">
                    <article class="recent-blog">
                        <section class="date">
                            <span class="day">{2}</span>
                            <span class="month">{3}</span>
                        </section>
                        <h4><a href="../../Pesquisa/{7}/">{4}</a></h4>
                        <p>{5}</p>
                        <a href="?categoria={8}"><span style="margin-left: 60px;" class="highlight color">{6}</span></a>
                    </article>
                </div>
        '), 'IFNULL(file_Capa,\'blank.png\')', 'DATE_FORMAT(dh_Cadastro, \'%d\')', 'DATE_FORMAT(dh_Cadastro, \'%m\')', 'ds_Titulo', 'tx_Resumo', 'ds_Descricao', 'ds_PermaLink', 'id_Categorias');
        $aColunasFiltros = array('ds_Titulo');
        $sColunaIndex    = "id_Conteudos";
        $sTabela         = "Conteudos LEFT JOIN Categorias ON Categorias.id_Categorias = Conteudos.id_Categorias_fk";
        $sCondicao       = "Where Conteudos.id_CategoriasPrincipal_fk in (1,4) and Conteudos.is_Ativo = TRUE and Conteudos.is_Publico = TRUE and (Conteudos.tx_Conteudo like '%{$_GET['Pesquisa']}%' or Conteudos.ds_Titulo like '%{$_GET['Pesquisa']}%')";
        $aOrdenacao = array("Conteudos.dh_Cadastro DESC");
        echo Grid::Gera($aColunas, $aColunasFiltros, $sColunaIndex, $sTabela, $sCondicao, $aOrdenacao, array());
    }


    function Escape()
    {
        $this->view->setTemplate("Site");
        $this->tbl->VisualizarPorPermalink($_GET['id']);
        $this->view->setTitulo($this->tbl->getDsTitulo());
        return "Visualizar";
    }

}