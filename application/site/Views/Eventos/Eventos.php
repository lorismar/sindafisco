<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var $tbl Conteudos
 */

?>


<!-- 960 Container id: <?= $tbl->getIdConteudos() ?> -->
<div class="container floated">

    <div class="sixteen floated page-title">

        <h2><?=$tbl->getDsTitulo()?></h2>



    </div>

</div>
<!-- 960 Container / End -->


<!-- 960 Container -->
<div class="container floated">

<!-- Page Content -->
<div class="sixteen floated">

    <!-- Post -->
    <article class="post">

        <section class="post-content">
            <?=$tbl->getTxConteudo()?>
        </section>

    </article>
    <!-- 960 Container -->
    <div class="container">

        <!-- Portfolio Content -->
        <div id="portfolio-wrapper">

            <?php /** @var $arq Arquivos */
            foreach ($listaArquivos as $arq): ?>
            <!-- 1/4 Column -->
            <div class="four columns isotope-item">



                <a href="../../public/upload/Galerias/<?= $arq->getIdConteudosFk() ?>/<?= $arq->getFileArquivo() ?>" rel="fancybox-gallery"  class="portfolio-item isotope">
                    <figure>
                        <div class="picture"><img src="../../public/upload/Galerias/<?= $arq->getIdConteudosFk() ?>/thumbs/<?= $arq->getFileArquivo() ?>" alt=""/><div class="image-overlay-link"></div></div>

                    </figure>
                </a>
            </div>
            <?php endforeach; ?>



        </div>
        <!-- Portfolio Content / End -->

    </div>
    <!-- 960 Container / End -->


</div>
<!-- Content / End -->



</div>
<!-- 960 Container / End -->
