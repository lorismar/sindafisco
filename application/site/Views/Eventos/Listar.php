<?php
/**
 * Created by PhpStorm.
 * User: user
 */

?>

<style type="text/css" title="currentStyle">
    @import "../../public/site/plugins/datatables/css/jquery.dataTables.css";
</style>
<script type="text/javascript" language="javascript" src="../../public/site/plugins/datatables/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../../public/site/plugins/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#tabela_listar').dataTable({
            "oLanguage": {
                "sLengthMenu": "_MENU_ registros por p�gina",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sEmptyTable": "Sem registros dispon&iacute;veis",
                "sLoadingRecords": "Carregando...",
                "sInfo": "Mostrando _START_ at� _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 at� 0 de 0 registros",
                "sInfoFiltered": "(_MAX_ no total)",
                "sInfoPostFix": "",
                "sInfoThousands": ",",
                "sSearch": "Procurar",
                "fnInfoCallback": null
            },

            "aoColumnDefs": [
                {
                    'bVisible': false,
                    'bSortable': true,
                    'aTargets': [1,2,3,4,5,6]
                }
            ],

            //Salva estado da grid
            "iDisplayLength": 10,
            "aaSorting": [
                [ 0, "desc" ]
            ], // Sort by first column descending
            // "bStateSave": true,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "../../Eventos/Grid<?= isset($_GET['categoria']) ? "?categoria=".$_GET['categoria'] : "" ?>"

        });

    });
</script>

<style>
    #tabela_listar tbody tr {
        display:inline;
        float:left;
        width: 480px;
        height: 220px;
    }
</style>
<table cellpadding="0" cellspacing="0" border="0" id="tabela_listar">
    <thead style="display: none;">
    <tr>
        <th>ID</th>
        <th>Imagem</th>
        <th>Titulo</th>
        <th>Data de Cadastro</th>
        <th>Resumo</th>
        <th>Categoria</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>