<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var $tbl Conteudos
 */

?>


<!-- 960 Container -->
<div class="container floated">

    <div class="sixteen floated page-title">

        <h2>Contato</h2>



    </div>

</div>
<!-- 960 Container / End -->

<!-- 960 Container -->
<div class="container floated">

    <!-- Sidebar -->
    <div class="four floated sidebar left">
        <aside class="sidebar padding-reset">

            <div class="widget">

                <p>Sindicato dos Auditores Fiscais de Tributos Estaduais de Rond�nia - SINDAFISCO.</p>
            </div>

            <div class="widget">
                <h4>Informa��es Gerais</h4>

                <ul class="contact-informations">
                    <li><span class="address">Rua Jos� Bonif�cio, 814 - Olaria</span></li>
                    <li><span class="address">Porto Velho - RO</span></li>
                </ul>

                <ul class="contact-informations second">
                    <li><i class="halflings phone"></i> <p>(69) 3223-3469</p></li>
                    <li><i class="halflings print"></i> <p>(69) 3224-4407</p></li>
                    <li><i class="halflings envelope"></i> <p>sindafisco_pvh@yahoo.com</p></li>
                </ul>

            </div>

            <div class="widget">
                <h4>Hor�rio de Funcionamento</h4>
                <ul class="contact-informations hours">
                    <li><i class="halflings time"></i>Segunda - Sexta <span class="hours">8h at� 18h</span></li>

                </ul>
            </div>

        </aside>
    </div>
    <!-- Sidebar / End -->

    <!-- Page Content -->
    <div class="eleven floated">
        <section class="page-content">

            <h3 class="margin-reset">Nossa Localiza��o</h3>

            <br />

            <!-- Google Maps -->
            <section class="google-map-container">

                <div id="googlemaps" class="google-map google-map-full" style="padding-bottom:40%"></div>

                <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
                <script src="<?= CONTENT_SITE ?>scripts/jquery.gmap.min.js"></script>

                <script type="text/javascript">
                    jQuery('#googlemaps').gMap({
                        maptype: 'ROADMAP',
                        scrollwheel: false,
                        zoom: 16,
                        markers: [
                            {
                                address: 'Rua Jos� Bonif�cio, 814 - Olaria, Porto Velho - RO', // Your Adress Here
                                html: '',
                                popup: false,
                            }
                        ],
                    });
                </script>
            </section>
            <!-- Google Maps / End -->


            <h3 class="margin">Formul�rio de Contato</h3>

            <!-- Contact Form -->
            <section id="contact">

                <!-- Success Message -->
                <mark id="message"></mark>

                <!-- Form -->
                <form method="post" action="../ContatoSalvar/" name="contactform" id="contactform">

                    <fieldset>

                        <div>
                            <label for="nome" accesskey="U">Nome:</label>
                            <input name="nome" type="text" id="nome" class="required" />
                        </div>

                        <div>
                            <label for="email" accesskey="E">Email: <span>*</span></label>
                            <input name="email" type="email" id="email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" />
                        </div>

                        <div>
                            <label for="assunto" accesskey="S">Assunto:</label>
                            <input name="assunto" type="text" id="assunto" class="required" />
                        </div>

                        <div>
                            <label for="mensagem" accesskey="C">Mensagem: <span>*</span></label>
                            <textarea name="mensagem" cols="40" rows="3" id="mensagem" spellcheck="true"></textarea>
                        </div>

                    </fieldset>

                    <input type="submit" class="submit" id="submit" value="Enviar Mensagem" />
                    <div class="clearfix"></div>

                </form>

            </section>
            <!-- Contact Form / End -->


        </section>
    </div>
    <!-- Page Content / End -->


</div>
<!-- 960 Container / End -->

