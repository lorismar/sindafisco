<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var $tbl Conteudos
 */
?>



<!-- 960 Container -->
<div class="container floated">

    <div class="sixteen floated page-title">

        <h2>Filia-se</h2>


    </div>

</div>
<!-- 960 Container / End -->

<!-- 960 Container -->
<div class="container floated">


    <!-- Page Content -->
    <div class="sixteen floated">
        <section class="page-content">


            <h3 class="margin">Formul�rio de Cadastro</h3>

            <!-- Fialia-se Form -->
            <section id="cadastro">

                <!-- Success Message -->
                <mark id="message"></mark>

                <!-- Form -->
                <form method="post" action="../CadastroSalvar/" name="cadastroform" id="cadastroform" autocomplete="off" enctype="multipart/form-data">

                    <div class="large-notice">
                        <h2 class="margin-reset">Dados Pessoais</h2>

                        <div class="sixteen columns">
                            <label for="ds_Cpf">CPF:</label>
                            <input name="ds_Cpf" type="text" id="ds_Cpf" class="required maskCpf" maxlength="14" size="30"/>
                        </div>
                        <div class="seven columns">
                            <label for="ds_Rg">RG:</label>
                            <input name="ds_Rg" type="text" id="ds_Rg" class="required" maxlength="20" size="30"/>
                        </div>

                        <div class="seven columns">
                            <label for="ds_OrgaoExpedidor">Org�o Expedidor:</label>
                            <input name="ds_OrgaoExpedidor" type="text" class="required" id="ds_OrgaoExpedidor" maxlength="10" size="30"/>
                        </div>

                        <div class="sixteen columns">
                            <label for="ds_Nome">Nome:</label>
                            <input name="ds_Nome" type="text" id="ds_Nome" class="required" maxlength="100" size="100"/>
                        </div>

                        <div class="sixteen columns">
                            <label for="ds_Email">Email:</label>
                            <input name="ds_Email" type="email" id="ds_Email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" size="100"/>
                        </div>

                        <div class="sixteen columns">
                            <label for="dt_Nascimento">Data de Nascimento:</label>
                            <input name="dt_Nascimento" type="text" id="dt_Nascimento" class="required maskDate" size="30"/>
                        </div>

                        <div class="sixteen columns">
                            <label for="ds_Sexo">Sexo:</label>
                            <select name="ds_Sexo" id="ds_Sexo">
                                <option value="M">Masculino</option>
                                <option value="F">Feminino</option>
                            </select>
                        </div>
                        <div class="sixteen columns">
                            <label for="ds_NomePai">Nome do Pai:</label>
                            <input name="ds_NomePai" type="text" id="ds_NomePai" maxlength="100" size="100"/>
                        </div>
                        <div class="sixteen columns">
                            <label for="ds_NomeMae">Nome da M�e:</label>
                            <input name="ds_NomeMae" type="text" id="ds_NomeMae" maxlength="100" size="100"/>
                        </div>

                        <div class="sixteen columns">
                            <label for="ds_EstadoCivil">Estado Civ�l:</label>
                            <select name="ds_EstadoCivil" id="ds_EstadoCivil">
                                <option value="Solteiro">Solteiro</option>
                                <option value="Casado">Casado</option>
                                <option value="Divorciado">Divorciado</option>
                                <option value="Outros">Outros</option>
                            </select>
                        </div>

                        <div class="sixteen columns">
                            <label for="ds_NomeConjuge">Nome do C�njuge:</label>
                            <input name="ds_NomeConjuge" type="text" id="ds_NomeConjuge" maxlength="100" size="100"/>
                        </div>

                        <h2 class="margin-reset">Dependentes</h2>

                        <div class="sixteen columns">
                            <label for="nu_Dependentes">N�mero de Dependentes:</label>
                            <input name="nu_Dependentes" type="text" id="nu_Dependentes" maxlength="2" size="5" onkeyup="AlteraNumeroDependentes()"/>
                        </div>

                        <div id="dependentes" class="sixteen columns">

                        </div>

                        <h2 class="">Dados Profissionais</h2>

                        <div class="sixteen columns">
                            <label for="ds_Formacao">Forma��o:</label>
                            <input name="ds_Formacao" type="text" id="ds_Formacao" maxlength="100" size="100"/>
                        </div>


                        <div class="sixteen columns">
                            <label for="ds_Matricula">Matr�cula:</label>
                            <input name="ds_Matricula" type="text" id="ds_Matricula" class="required" maxlength="20" size="30"/>
                        </div>

                        <div class="sixteen columns">
                            <label for="ds_Lotacao">Lota��o:</label>
                            <input name="ds_Lotacao" type="text" id="ds_Lotacao" maxlength="100" size="100"/>
                        </div>

                        <h2 class="margin-reset">Contato</h2>

                        <div class="five column">
                            <label for="ds_Telefone">Telefone:</label>
                            <input name="ds_Telefone" type="text" id="ds_Telefone" class="required maskTel" maxlength="14" size="20"/>
                        </div>

                        <div class="five column">
                            <label for="ds_Celular">Celular:</label>
                            <input name="ds_Celular" type="text" id="ds_Celular" class="maskCel" maxlength="15" size="20"/>
                        </div>

                        <div class="five column">
                            <label for="ds_TelefoneTrabalho">Telefone Trabalho:</label>
                            <input name="ds_TelefoneTrabalho" type="text" id="ds_TelefoneTrabalho" class="maskTel" maxlength="14" size="20"/>
                        </div>


                        <div class="sixteen columns">
                            <label for="ds_End_Completo">Endere�o:</label>
                            <input name="ds_End_Completo" type="text" id="ds_End_Completo" class="required" maxlength="200" size="100"/>
                        </div>

                        <div class="two-thirds columns">
                            <label for="ds_End_Bairro">Bairro:</label>
                            <input name="ds_End_Bairro" type="text" id="ds_End_Bairro" class="required" maxlength="100" size="100"/>
                        </div>

                        <div class="one-third columns">
                            <label for="ds_End_Cep">CEP:</label>
                            <input name="ds_End_Cep" type="text" id="ds_End_Cep" class="required maskCep" maxlength="9" size="30"/>
                        </div>

                        <div class="two-thirds columns">
                            <label for="ds_End_Cidade">Cidade:</label>
                            <input name="ds_End_Cidade" type="text" id="ds_End_Cidade" class="required" maxlength="100" size="100"/>
                        </div>

                        <div class="one-third columns">
                            <label for="ds_End_Uf">UF:</label>
                            <input name="ds_End_Uf" type="text" id="ds_End_Uf" class="required" maxlength="2" size="5"/>
                        </div>


                        <div class="sixteen columns">
                            <label for="ds_EmailAlternativo">Email Alternativo:</label>
                            <input name="ds_EmailAlternativo" type="email" id="ds_EmailAlternativo" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" size="100"/>
                        </div>

                        <div class="sixteen columns">
                            <label for="ds_Senha">Senha:</label>
                            <input name="ds_Senha" type="password" id="ds_Senha" maxlength="20" size="30"/>
                        </div>
                        <div class="sixteen columns">
                            <label for="file_Foto">Foto:</label>
                            <input name="file_Foto" type="file" id="file_Foto"/>
                        </div>
                        <div class="sixteen columns">
                            <input type="submit" class="submit" id="submit" value="Enviar Solicita��o"/>
                        </div>


                        <div class="clearfix"></div>
                    </div>
                </form>

            </section>
            <!-- Contact Form / End -->


        </section>
    </div>
    <!-- Page Content / End -->


</div>
<!-- 960 Container / End -->

