<?php
/**
 * User: Gustavo
 * @var Html $Html
 */
?>
<!-- 960 Container -->
<div class="container" style="margin-top:20px;">
    <div class="two-thirds column">
        <!-- FlexSlider  -->
        <section class="flexslider home">
            <ul class="slides">
                <?php /** @var $des Conteudos */
                foreach ($listaDestaque as $des) :?>
                    <li>
                        <a href="../../<?= $des->getIdCategoriasPrincipalFk() == 1 ? "Noticias" : "Informativos" ?>/<?= $des->getDsPermalink() ?>/">
                            <img src="../../public/upload/Conteudos/<?= $des->getFileCapa() ?>" alt="" style="width:620;height:274px;"/>
                        </a>
                        <article class="slide-caption">
                            <a href="../../<?= $des->getIdCategoriasPrincipalFk() == 1 ? "Noticias" : "Informativos" ?>/<?= $des->getDsPermalink() ?>/">
                            <p><?= $des->getDsTitulo() ?></p>
                            </a>
                        </article>
                    </li>
                <?php endforeach; ?>

            </ul>
        </section>
        <!-- FlexSlider / End -->
    </div>
    <!-- Icon Box Start -->
    <div class="one-third column">
        <div class="large-notice" id="login">

            <h3>Login <span>/ Acesse nossa área restrita</span></h3>

            <form method="post" action="../../admin/Login/Logar/" name="loginform" id="loginform" autocomplete="off">
                <fieldset>

                    <div>
                        <label for="cpf" accesskey="U">CPF:</label>
                        <input name="cpf" type="text" id="cpf" maxlength="11" placeholder="Digite seu CPF" >
                        <mark class="validate"></mark>
                    </div>

                    <div>
                        <label for="senha" accesskey="E">Senha: <span>*</span></label>
                        <input name="senha" type="password" id="senha" placeholder="*****">
                        <mark class="validate"></mark>
                    </div>
                    <a href="../RecuperarSenha/">Recuperar Senha</a>


                </fieldset>

                <input type="submit" class="submit" id="submit" value="Entrar">

                <div class="clearfix"></div>

            </form>

        </div>
    </div>
    <!-- Icon Box End -->

</div>


<!-- 960 Container -->
<div class="container floated" style="margin-top: 20px;">
    <div class="blank floated">
        <!-- Recent News -->
        <div class="eight columns alpha" style="border-right: 1px dashed #ccc;  padding-bottom:10px;">

            <h3 class="margin-1">Notícias <span>/ Mais Recentes</span></h3>
            <?php /** @var $des Conteudos */
            foreach ($listaNormal as $des) :?>
                <div class="eight columns alpha">
                    <article class="recent-blog">
                        <section class="date">
                            <span class="day"><?= substr($des->getDhCadastro(), 0, 2) ?></span>
                            <span class="month"><?= substr($des->getDhCadastro(), 3, 2) ?></span>
                        </section>
                        <h4><a href="../../<?= $des->getIdCategoriasPrincipalFk() == 1 ? "Noticias" : "Informativos" ?>/<?= $des->getDsPermalink() ?>/"><?= $des->getDsTitulo() ?></a></h4>

                        <p><?= $des->getTxResumo() ?></p>
                    </article>
                </div>
            <?php endforeach; ?>


            <a href="<?= $Html->ActionLink("Noticias", "Listar") ?>" class="button color">Ver Mais</a>


        </div>

        <!-- Informativos -->
        <div class="eight columns">

            <h3 class="margin-1">Informes <span>/ Mais Recentes</span></h3>

            <div class="eight columns ">
                <ul class="list">
                    <?php /** @var $des Conteudos */
                    foreach ($listaInformativos as $des) :?>
                        <li style="margin-bottom:2px; margin-top:2px;"><?= substr($des->getDhCadastro(), 0, 5) ?> - <a href="../../Informativos/<?= $des->getDsPermalink() ?>/"><?= $des->getDsTitulo() ?></a></li>
                    <?php endforeach; ?>

                </ul>
            </div>


        </div>

        <!-- Eventos -->
        <div class="four columns">

            <h3 class="margin-1">Eventos</h3>

            <?php /** @var $des Conteudos */
            foreach ($listaEventos as $des) :?>
                <a href="<?= $Html->ActionLink("Eventos", "Listar") ?>" class="portfolio-item isotope">
                    <figure>
                        <div class="picture"><img src="../../public/upload/Galerias/<?= $des->getFileCapa() ?>" alt="">

                            <div class="image-overlay-link"></div>
                        </div>
                        <figcaption class="item-description">
                            <h5><?= $des->getDsTitulo() ?></h5>
                        </figcaption>
                    </figure>
                </a>
            <?php endforeach; ?>


        </div>
        <!-- Videos -->
        <div class="four columns">

            <h3 class="margin-1">V�deos</h3>
            <figure>
                <?php /** @var $youtube Youtube */ ?>
                <div class="picture"> <iframe width="220" height="165" src="http://www.youtube.com/embed/<?= $youtube->getId() ?>?&rel=0&theme=light&hd=1&autohide=1" frameborder="0" allowfullscreen></iframe> </div>
                <figcaption class="item-description">
                    <h5><?= utf8_decode($youtube->getDsTitulo()) ?></h5>
                </figcaption>
            </figure>
            <a style="margin-left: 140px;" href="<?= $Html->ActionLink("Youtube", "Canal") ?>" class="button color">Ver Mais</a>
        </div>


    </div>


</div>

<div class="container">
    <!-- Parceiros -->
    <div class="sixteen columns">

        <h3 class="margin-1">Parceiros</h3>
        <?php /** @var $arq Arquivos */
        foreach ($listaParceiros as $arq) :?>
            <div class="four columns alpha">
                <a href="<?= $arq->getDsUrlLink() ?>" target="_blank" class="portfolio-item isotope">
                    <figure>
                        <div class="picture"><img src="../../public/upload/Parceiros/<?= $arq->getFileArquivo() ?>" alt="">

                            <div class="image-overlay-link"></div>
                        </div>

                    </figure>
                </a>
            </div>
        <?php endforeach; ?>


    </div>
</div>

