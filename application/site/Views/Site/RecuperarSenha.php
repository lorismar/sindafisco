<?php
/**
 * User: Gustavo
 * @var Html $Html
 */

?>


<!-- 960 Container -->
<div class="container floated">

    <div class="sixteen floated page-title">

        <h2>Recuperar Senha</h2>


    </div>

</div>
<!-- 960 Container / End -->


<!-- 960 Container -->
<div class="container floated">

    <!-- Page Content -->
    <div class="six floated centered">

        <!-- Post -->
        <article class="post">
            <div class="large-notice" id="login">


                <form method="post" action="../RecuperarSenha/" name="loginform" id="loginform" autocomplete="off">
                    <fieldset>

                        <div>
                            <label for="cpf" accesskey="U">CPF: <span>*</span></label>
                            <input name="cpf" type="text" id="cpf" class="maskCpf" placeholder="Digite seu CPF">
                            <mark class="validate"></mark>
                        </div>

                        <div>
                            <label for="email" accesskey="E">E-Mail: <span>*</span></label>
                            <input name="email" type="email" id="email" placeholder="Digite seu E-Mail" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" />

                            <mark class="validate"></mark>
                        </div>

                    </fieldset>

                    <input type="submit" class="submit" id="submit" value="Entrar">

                    <div class="clearfix"></div>

                </form>

            </div>
        </article>


    </div>
    <!-- Content / End -->


</div>
<!-- 960 Container / End -->
