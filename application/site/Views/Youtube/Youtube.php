<?php
/**
 * Created by PhpStorm.
 * User: user
 */

?>

<!-- 960 Container -->
<div class="container floated">

    <div class="sixteen floated page-title">

        <h2>Canal Imprensa Sindafisco</h2>


    </div>

</div>
<!-- 960 Container / End -->

<!-- Portfolio Content -->
<div id="portfolio-wrapper">


    <?php /** @var $v Youtube */
    foreach ($listaVideos as $v): ?>
        <!-- 1/4 Column -->
        <div class="one-third column isotope-item" style="margin-left: 40px; ">
                <figure>
                    <div class="picture"> <iframe width="300" height="218" src="http://www.youtube.com/embed/<?= $v->getId() ?>?&rel=0&theme=light&hd=1&autohide=1" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <figcaption class="item-description">
                        <h5><?= utf8_decode($v->getDsTitulo()) ?></h5>
                    </figcaption>
                </figure>
        </div>
    <?php endforeach; ?>


</div>
<div class="container floated">
    <a href="https://www.youtube.com/channel/UCxZVG0u-cVaFh6OQk-3HrCw" target="_blank" class="button color">Clique aqui e assista mais no nosso canal</a>
</div>
<br/> <br/>
<!-- Portfolio Content / End -->