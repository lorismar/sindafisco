<?php

/**
 * @var $Html Html
 */


?>
<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="pt-br"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="pt-br"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="ISO-8859-1">

<!--    <meta charset="utf-8"/>-->
    <title>SINDAFISCO | <?= $Html->Layout->getTitle() ?></title>

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="<?= CONTENT_SITE ?>css/base.css">
    <link rel="stylesheet" href="<?= CONTENT_SITE ?>css/responsive.css">
    <link rel="stylesheet" href="<?= CONTENT_SITE ?>css/icons.css">
    <link rel="stylesheet" href="<?= CONTENT_SITE ?>css/style.css">
    <link rel="stylesheet" href="<?= CONTENT_SITE ?>css/colors/blue.css" id="colors">

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Java Script
    ================================================== -->
    <script src="<?= CONTENT_SITE ?>scripts/jquery.min.js"></script>
    <script src="<?= CONTENT_SITE ?>scripts/jquery-ui.min.js"></script>
    <script src="<?= CONTENT_SITE ?>scripts/jquery.flexslider.js"></script>
    <script src="<?= CONTENT_SITE ?>scripts/jquery.selectnav.js"></script>
    <script src="<?= CONTENT_SITE ?>scripts/jquery.twitter.js"></script>
    <script src="<?= CONTENT_SITE ?>scripts/jquery.modernizr.js"></script>
    <script src="<?= CONTENT_SITE ?>scripts/jquery.easing.1.3.js"></script>
    <script src="<?= CONTENT_SITE ?>scripts/jquery.contact.js"></script>
    <script src="<?= CONTENT_SITE ?>scripts/jquery.cadastro.js"></script>
    <script src="<?= CONTENT_SITE ?>scripts/jquery.isotope.min.js"></script>
    <script src="<?= CONTENT_SITE ?>scripts/jquery.jcarousel.js"></script>
    <script src="<?= CONTENT_SITE ?>scripts/jquery.fancybox.min.js"></script>
    <script src="<?= CONTENT_SITE ?>scripts/jquery.layerslider.min.js"></script>
<!--    <script src="--><?//= CONTENT_SITE ?><!--scripts/jquery.maskedinput-1.1.4.pack.js"></script>-->
    <script type="text/javascript" src="../../public/admin/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="../../public/admin/custom/js/jquery.maskedinput.min.js"></script>
    <script src="<?= CONTENT_SITE ?>scripts/custom.js"></script>
</head>
<body>

<!-- Wrapper / Start -->
<div id="wrapper">

    <!-- Header
    ================================================== -->
    <div id="top-line"></div>

    <!-- 960 Container -->
    <div class="container">

        <!-- Header -->
        <header id="header">

            <!-- Logo -->
            <div class="ten columns">
                <div id="logo">
                    <a href="<?= $Html->ActionLink("Site", "Principal") ?>"><img src="<?= CONTENT_SITE ?>images/logo-sindafisco.png" alt="SINDAFISCO"/></a>

                    <div class="clearfix"></div>
                </div>
            </div>

            <!-- Social / Contact -->
            <div class="six columns">

                <!-- Social Icons -->
                <ul class="social-icons">
                    <li class="twitter"><a href="#">Twitter</a></li>
                    <li class="facebook"><a href="#">Facebook</a></li>
                    <li class="dribbble"><a href="#">Dribbble</a></li>
                    <li class="linkedin"><a href="#">LinkedIn</a></li>
                    <li class="rss"><a href="#">RSS</a></li>
                </ul>

                <div class="clearfix"></div>

                <!-- Contact Details -->

                <div class="contact-details">Telefone de Contato: (69) 3223-3469</div>

                <div class="clearfix"></div>

                <!-- Search -->
                <nav class="top-search no-print">
                    <form action="<?= $Html->ActionLink("Pesquisa", "Procurar") ?>" method="get">
                        <button class="search-btn"></button>
                        <input class="search-field" type="text" name="Pesquisa" onblur="if(this.value=='')this.value='Procurar';" onfocus="if(this.value=='Procurar')this.value='';" value="Procurar"/>
                    </form>
                </nav>

            </div>
        </header>
        <!-- Header / End -->

        <div class="clearfix"></div>

    </div>
    <!-- 960 Container / End -->


    <!-- Navigation
    ================================================== -->
    <nav id="navigation" class="style-1">

        <div class="left-corner no-print"></div>
        <div class="right-corner no-print"></div>

        <ul class="menu no-print" id="responsive">

            <li><a href="<?= $Html->ActionLink("Site", "Principal") ?>" id="site_principal"><i class="halflings white home"></i> Principal</a></li>
            <li><a href="<?= $Html->ActionLink("Pagina", "Sindafisco") ?>" id="pagina_sindafisco"> O SINDAFISCO</a></li>
            <li><a href="<?= $Html->ActionLink("Pagina", "Convenios") ?>" id="pagina_convenios"> Convênios</a></li>
            <li><a href="<?= $Html->ActionLink("Pagina", "Estatuto") ?>" id="pagina_estatuto"> Estatuto</a></li>
            <li><a href="<?= $Html->ActionLink("Pagina", "Legislacao") ?>" id="pagina_legislacao"> Legislação</a></li>
            <li><a href="<?= $Html->ActionLink("Noticias", "Listar") ?>" id="noticias"> Notícias</a></li>
            <li><a href="<?= $Html->ActionLink("Informativos", "Listar") ?>" id="informes"> Informes</a></li>
            <li><a href="<?= $Html->ActionLink("Eventos", "Listar") ?>" id="eventos"> Eventos</a></li>
<!--            <li><a href="#" id="pagina_servicos"> Servi�os</a></li>-->
            <li><a href="<?= $Html->ActionLink("Formulario", "Cadastro") ?>" id="formulario_cadastro"> Filia-se</a></li>
            <li><a href="<?= $Html->ActionLink("Formulario", "Contato") ?>" id="formulario_contato"> Contato</a></li>
        </ul>
    </nav>
    <div class="clearfix"></div>


    <!-- Content
    ================================================== -->
    <div id="content">
        <?= GUI::retornaErro() ?>
        <?= $Html->Layout->RenderBody() ?>


    </div>
    <!-- Content / End -->

</div>
<!-- Wrapper / End -->


<!-- Footer
================================================== -->

<!-- Footer / Start -->
<footer id="footer" class="no-print">
    <!-- 960 Container -->
    <div class="container">

        <!-- About -->
        <div class="four columns">
            <img src="<?= CONTENT_SITE ?>images/logo-sindafisco2.png" style="margin: 0 auto" alt=""/>

            <p>Sindicato dos Auditores Fiscais de Tributos Estaduais do Estado de Rondônia</p>

        </div>


        <!-- Contact Details -->
        <div class="eight columns">
            <h4>Detalhes de Contato</h4>
            <ul class="contact-details-alt">
                <li><i class="halflings white map-marker"></i>

                    <p><strong>Endereço:</strong> Rua José Bonifácio, 814 - Olaria, CEP 76801-230, Porto Velho - RO</p></li>
                <li><i class="halflings white user"></i>

                    <p><strong>Telefone:</strong> (69) 3223-3469</p></li>
                <li><i class="halflings white user"></i>

                    <p><strong>Fone e Fax:</strong> (69) 3224-4407</p></li>
                <li><i class="halflings white envelope"></i>

                    <p><strong>Email:</strong> <a href="#">sindafisco_pvh@yahoo.com</a></p></li>
            </ul>
        </div>

        <!-- Photo Stream -->
        <div class="four columns">

        </div>


    </div>
    <!-- 960 Container / End -->

</footer>
<!-- Footer / End -->


<!-- Footer Bottom / Start  -->
<footer id="footer-bottom" class="no-print">

    <!-- 960 Container -->
    <div class="container">

        <!-- Copyrights -->
        <div class="eight columns">
            <div class="copyright">
                 Copyright 2014 por SINDAFISCO. Todos direitos reservados.
            </div>
        </div>


        <!-- Menu -->
        <div class="eight columns">
            <nav id="sub-menu">
                <ul>
                    <li><a href="http://www.havit.com.br" target="_blank">HAVIT Soluções Tecnológicas</a></li>
                </ul>
            </nav>
        </div>

    </div>
    <!-- 960 Container / End -->

</footer>
<!-- Footer Bottom / End -->

<script>
    <?= GUI::retornaJS() ?>
</script>
</body>
</html>