<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var $tbl Conteudos
 */

?>


<!-- 960 Container -->
<div class="container floated">

    <div class="sixteen floated page-title">

        <h2><?= $tbl->getDsTitulo() ?></h2>


    </div>

</div>
<!-- 960 Container / End -->


<!-- 960 Container -->
<div class="container floated">

    <!-- Page Content -->
    <div class="sixteen floated">
        <header class="meta">
            <span><i class="halflings calendar"></i><?= substr($tbl->getDhCadastro(), 0, 10) ?></span>
            <span class="no-print "><i class="halflings print"></i><a href="#" onclick="window.print()">Imprimir</a></span>
        </header>
        <!-- Post -->
        <article class="post">

            <section class="post-content">
                <?= $tbl->getTxConteudo() ?>
            </section>
            <?php
            $listaArquivos = $tbl->Arquivos();
            if ($listaArquivos): ?>
                <section class="post-content">
                    <h3>Lista de Anexos:</h3>
                    <ul class="">
                        <?php /** @var $anexo Arquivos */
                        foreach ($listaArquivos as $anexo): ?>
                            <li><i class="icon-arrow-down"></i> <a href="<?= CONTENT ?>upload/Anexos/<?= $anexo->getFileArquivo() ?>" target="_blank">Fazer download de <?= $anexo->getDsTitulo() ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </section>
            <?php endif; ?>
        </article>


    </div>
    <!-- Content / End -->


</div>
<!-- 960 Container / End -->
