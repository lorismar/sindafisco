<?php
/**
 * User: Gustavo
 * @var Html $Html
 * @var $tbl Conteudos
 */
?>


<!-- 960 Container -->
<div class="container floated">

    <div class="sixteen floated page-title">

        <h2><?= $tbl->getDsTitulo() ?></h2>


    </div>

</div>
<!-- 960 Container / End -->


<!-- 960 Container -->
<div class="container floated">

    <?php if (count($listaFilhos) > 0): ?>
        <!-- Sidebar -->
        <div class="three floated sidebar left">
            <aside class="sidebar padding-reset">
                <div class="clearfix" style="margin: 6px 0 0 0;"></div>
                <a href="../<?= $pai->getDsPermalink() ?>/" class="button color medium" style="width: 100%"><?= $pai->getDsTitulo() ?></a>
                <?php /** @var $f Conteudos */
                foreach ($listaFilhos as $f):
                    ?>
                    <div class="clearfix" style="margin: 1px 0 0 0;"></div>
                    <a href="../<?=$f->getDsPermalink()?>/" class="button color medium" style="width: 100%"><?=$f->getDsTitulo() ?></a>
                <?php endforeach; ?>
            </aside>
        </div>
        <!-- Sidebar / End -->
    <?php endif; ?>

    <!-- Page Content -->
    <div class="<?= count($listaFilhos) > 0 ? "twelve" : "sixteen" ?> floated">

        <!-- Post -->
        <article class="post">

            <section class="post-content">
                <?= $tbl->getTxConteudo() ?>
            </section>

        </article>


    </div>
    <!-- Content / End -->


</div>
<!-- 960 Container / End -->
