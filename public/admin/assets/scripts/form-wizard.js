var FormWizard = function () {


    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }


            var form = $('#submit_form');


            var handleTitle = function (tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#form_wizard_1')).text('Passo ' + (index + 1) + ' de ' + total);
                // set done steps
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-submit').show();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                }
                App.scrollTo($('.page-title'));
            }

            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {

//                    handleTitle(tab, navigation, clickedIndex);
                    return false
                },
                onNext: function (tab, navigation, index) {
                    var current = index + 1, valor_com_desconto, forma_pagamento, cliente;

                    var senha2;
                    var senha;
                    if (current == 2) {
                        senha = document.getElementById('ds_Senha').value;
                        senha2 = document.getElementById('ds_Senha_conf').value;
                        if (senha != senha2) {
                            alert('A senha n�o confere com a confirma��o!');
                            return false;
                        }
                        if (senha == ""){
                            alert("A senha n�o pode ser em branco");
                            return false;
                        }

                    }
                    if (current == 3) {


                    }

                    if (current == 4) {


                    }


                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function () {

            }).hide();
        }

    };

}();