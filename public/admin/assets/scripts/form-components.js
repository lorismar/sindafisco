var FormComponents = function () {


    var handleWysihtml5 = function () {
        if (!jQuery().wysihtml5) {
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["assets/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }
    }


    var handleInputMasks = function () {
        $.extend($.inputmask.defaults, {
            'autounmask': true
        });
        $(".maskDate").inputmask("d/m/y", {
            "placeholder": "dd/mm/aaaa",
            autoUnmask: true
        }); //direct mask
        $(".maskDateTime").inputmask("d/m/y h:s", {
            "placeholder": "dd/mm/aaaa hh:mm",
            autoUnmask: true
        }); //direct mask
        $("#mask_phone").inputmask("mask", {
            "mask": "(999) 999-9999"
        }); //specifying fn & options
        $("#mask_tin").inputmask({
            "mask": "99-9999999"
        }); //specifying options only
        $("#mask_number").inputmask({
            "mask": "9",
            "repeat": 10,
            "greedy": false
        }); // ~ mask "9" or mask "99" or ... mask "9999999999"
        $("#mask_decimal").inputmask('decimal', {
            rightAlignNumerics: false
        }); //disables the right alignment of the decimal input
        $("#mask_currency").inputmask('€ 999.999.999,99', {
            numericInput: true
        }); //123456  =>  € ___.__1.234,56

        $("#mask_currency2").inputmask('€ 999,999,999.99', {
            numericInput: true,
            rightAlignNumerics: false,
            greedy: false
        }); //123456  =>  € ___.__1.234,56
        $("#mask_ssn").inputmask("999-99-9999", {
            placeholder: " ",
            clearMaskOnLostFocus: true
        }); //default

        //===== Masked input =====//

        $.mask.definitions['~'] = "[+-]";

        $(".maskTel").mask("(99) 9999-9999");
        $(".maskCel").mask("(99) 9999-9999?9").ready(function (event) {
            var target, phone, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;
            if (typeof target != 'undefined') {
                phone = target.value.replace(/\D/g, '');
                element = $(target);
                element.unmask();
                if (phone.length > 10) {
                    element.mask("(99) 99999-999?9");
                } else {
                    element.mask("(99) 9999-9999?9");
                }
            }

        });
        $(".maskChave").mask("********")
        $(".maskPct99").mask("99");
        $(".maskPct999").mask("199%");
        $(".maskCep").mask("99999-999");
        $(".maskCpf").mask("999.999.999-99");
        $(".maskCnpj").mask("99.999.999/9999-99");
        $(".maskMoney").maskMoney({thousands: '.', decimal: ','});
        $(".maskDinheiro").maskMoney({thousands: '.', decimal: ','});
        $(".maskDecimal").maskMoney({showSymbol: false});
        $(".maskCpfCnpj").change(function () {
            var cgc = $(".maskCpfCnpj").val();
            if (cgc.length == 11) {
                $(".maskCpfCnpj").mask("999.999.999-99");
                $(".maskCpfCnpj").unmask();
            } else if (cgc.length == 15) {
                $(".maskCpfCnpj").mask("99.999.999/9999-99");
                $(".maskCpfCnpj").unmask();
            } else if (cgc.length == 0) {
                $(".maskCpfCnpj").unmask();
            }
        });
        $(".maskCpfCnpj2").change(function () {
            var cgc = $(".maskCpfCnpj2").val();
            if (cgc.length == 11) {
                $(".maskCpfCnpj2").mask("999.999.999-99");
                $(".maskCpfCnpj2").unmask();
            } else if (cgc.length == 15) {
                $(".maskCpfCnpj2").mask("99.999.999/9999-99");
                $(".maskCpfCnpj2").unmask();
            } else if (cgc.length == 0) {
                $(".maskCpfCnpj2").unmask();
            }

        });
    }


    var handlePasswordStrengthChecker = function () {
        var initialized = false;
        var input = $("#password_strength");

        input.keydown(function () {
            if (initialized === false) {
                // set base options
                input.pwstrength({
                    raisePower: 1.4,
                    minChar: 8,
                    verdicts: ["Weak", "Normal", "Medium", "Strong", "Very Strong"],
                    scores: [17, 26, 40, 50, 60]
                });

                // add your own rule to calculate the password strength
                input.pwstrength("addRule", "demoRule", function (options, word, score) {
                    return word.match(/[a-z].[0-9]/) && score;
                }, 10, true);

                // set as initialized 
                initialized = true;
            }
        });
    }


    var handleMultiSelect = function () {


        $('#my_multi_select3').multiSelect({
            selectableOptgroup: true,
            selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='pesquisar...'>",
            selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='pesquisar...'>",
            afterInit: function (ms) {
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function (e) {
                        if (e.which === 40) {
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function (e) {
                        if (e.which == 40) {
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function () {
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function () {
                this.qs1.cache();
                this.qs2.cache();
            }
        });
    }


    return {
        //main function to initiate the module
        init: function () {
            handleWysihtml5();
            handleInputMasks();
            handlePasswordStrengthChecker();
            handleMultiSelect();
        }


    };

}();