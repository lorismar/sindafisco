var n = jQuery;
n(document).ready(function ($) {

    $("#validate").validationEngine();
//
//    //===== Masked input =====//
//
//    $.mask.definitions['~'] = "[+-]";
//
//    $(".maskTel").mask("(99) 9999-9999");
//    $(".maskCel").mask("(99) 9999-9999?9").ready(function (event) {
//        var target, phone, element;
//        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
//		 if (typeof target != 'undefined'){
//        phone = target.value.replace(/\D/g, '');
//		 element = $(target);
//        element.unmask();
//        if (phone.length > 10) {
//            element.mask("(99) 99999-999?9");
//        } else {
//            element.mask("(99) 9999-9999?9");
//        }
//		}
//
//    });
//    $(".maskChave").mask("********")
//    $(".maskPct99").mask("99");
//    $(".maskPct999").mask("199%");
//    $(".maskCep").mask("99999-999");
//    $(".maskCpf").mask("999.999.999-99");
//    $(".maskCnpj").mask("99.999.999/9999-99");
//    $(".maskMoney").maskMoney({thousands:'.', decimal:','});
//    $(".maskDinheiro").maskMoney({thousands:'.', decimal:','});
//    $(".maskDecimal").maskMoney({showSymbol:false});
//    $(".maskCpfCnpj").change(function () {
//        var cgc = $(".maskCpfCnpj").val();
//        if (cgc.length == 11) {
//            $(".maskCpfCnpj").mask("999.999.999-99");
//            $(".maskCpfCnpj").unmask();
//        } else if (cgc.length == 15) {
//            $(".maskCpfCnpj").mask("99.999.999/9999-99");
//            $(".maskCpfCnpj").unmask();
//        } else if (cgc.length == 0) {
//            $(".maskCpfCnpj").unmask();
//        }
//    });
//    $(".maskCpfCnpj2").change(function () {
//        var cgc = $(".maskCpfCnpj2").val();
//        if (cgc.length == 11) {
//            $(".maskCpfCnpj2").mask("999.999.999-99");
//            $(".maskCpfCnpj2").unmask();
//        } else if (cgc.length == 15) {
//            $(".maskCpfCnpj2").mask("99.999.999/9999-99");
//            $(".maskCpfCnpj2").unmask();
//        } else if (cgc.length == 0) {
//            $(".maskCpfCnpj2").unmask();
//        }
//
//    });

});

function go(url) {
    window.location = url;
}

function Remover(url) {
    var isOK = confirm("Tem certeza que deseja remover?");
    if (isOK) {
        go(url);
    }
}

function AlteraNumeroDependentes() {

    if (parseFloat(document.getElementById('nu_Dependentes').value) > 0) {
        var valor = parseFloat(document.getElementById('nu_Dependentes').value);
        var retorno = "";
        for (var x = 1; x <= valor; x++) {
            retorno += '<div class="col-md-5 col-lg-5"><div class="form-group"><label class="control-label" for="dep_ds_Nome_' + x + '">Nome do Dependente:</label><input type="text" name="dep_ds_Nome_' + x + '" id="dep_ds_Nome_' + x + '" class="form-control" maxlength="100" value=""/></div></div>' +
                '<div class="col-md-3 col-lg-3"><div class="form-group"><label class="control-label" for="dep_ds_Parentesco_' + x + '">Parentesco:</label><input type="text" name="dep_ds_Parentesco_' + x + '" id="dep_ds_Parentesco_' + x + '" class="form-control" maxlength="100" value=""/></div></div>' +
                '<div class="col-md-3 col-lg-3"><div class="form-group"><label class="control-label" for="dep_dt_Nascimento_' + x + '">Data de Nascimento:</label><input type="text" name="dep_dt_Nascimento_' + x + '" id="dep_dt_Nascimento_' + x + '" class="form-control maskDate" maxlength="100" value=""/></div></div>';
        }
        document.getElementById('dependentes').innerHTML = retorno;
        for (var x = 1; x <= valor; x++) {
            $("#dep_dt_Nascimento_" + x).inputmask("d/m/y", {
                "placeholder": "dd/mm/aaaa",
                autoUnmask: true
            }); //direct mask
        }
    } else {
        document.getElementById('dependentes').innerHTML = '';
    }

}