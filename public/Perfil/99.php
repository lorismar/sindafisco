<li id="informativos_filiados">
    <a href="<?= $Html->ActionLink("Informativos", "Filiados") ?>">
        <i class="fa fa-globe"></i>
        <span class="title">Informativos</span>
    </a>
</li>

<li id="contabil_informacoesfinanceiras">
    <a href="<?= $Html->ActionLink("Contabil", "InformacoesFinanceiras") ?>">
        <i class="fa fa-money"></i>
        <span class="title">Informativos Financeiros</span>
    </a>
</li>


<li id="processos_menu" class="">
    <a href="javascript:;" class="">
        <i class="fa fa-folder-open"></i>
        <span class="title">Jur�dico</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">

        <li id="processos_sindicato">
            <a class="" href="<?= $Html->ActionLink("Processos", "Sindicato") ?>">A��es Coletivas</a>
        </li>
        <li id="processos_meus">
            <a class="" href="<?= $Html->ActionLink("Processos", "Meus") ?>">A��es Individuais/Admin.</a>
        </li>
    </ul>
</li>
