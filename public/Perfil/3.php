<li class="">
    <a href="javascript:;" class="">
        <i class="fa fa-globe"></i>
        <span class="title">Conte�do</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li id="noticias_listar">
            <a class="" href="<?= $Html->ActionLink("Noticias", "Listar") ?>">Not�cias</a>
        </li>
        <li id="categorias_listar">
            <a class="" href="<?= $Html->ActionLink("Categorias", "Listar") ?>">Categorias</a>
        </li>
        <li id="galerias_listar">
            <a class="" href="<?= $Html->ActionLink("Galerias", "Listar") ?>">Galerias</a>
        </li>
        <li id="informativos_listar">
            <a class="" href="<?= $Html->ActionLink("Informativos", "Listar") ?>">Informativos</a>
        </li>
        <li id="paginas_listar">
            <a class="" href="<?= $Html->ActionLink("Paginas", "Listar") ?>">P�ginas</a>
        </li>
        <li id="parceiros_listar">
            <a class="" href="<?= $Html->ActionLink("Parceiros", "Listar") ?>">Parceiros</a>
        </li>
        <li id="faleconosco_listar">
            <a href="javascript:;">
                <i class="fa fa-envelope"></i>
                Fale Conosco
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li id="faleconosco_listarrespondidas"><a class="" href="<?= $Html->ActionLink("FaleConosco", "ListarRespondidas") ?>">Respondidas</a></li>
                <li id="faleconosco_listarnaorespondidas"><a class="" href="<?= $Html->ActionLink("FaleConosco", "ListarNaoRespondidas") ?>">N�o Respondidas</a></li>
            </ul>
        </li>

    </ul>
</li>