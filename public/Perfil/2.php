<li class="">
    <a href="javascript:;" class="">
        <i class="fa fa-globe"></i>
        <span class="title">Conte�do</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li id="noticias_listar">
            <a class="" href="<?= $Html->ActionLink("Noticias", "Listar") ?>">Not�cias</a>
        </li>
        <li id="categorias_listar">
            <a class="" href="<?= $Html->ActionLink("Categorias", "Listar") ?>">Categorias</a>
        </li>
        <li id="galerias_listar">
            <a class="" href="<?= $Html->ActionLink("Galerias", "Listar") ?>">Galerias</a>
        </li>
        <li id="informativos_listar">
            <a class="" href="<?= $Html->ActionLink("Informativos", "Listar") ?>">Informativos</a>
        </li>
        <li id="paginas_listar">
            <a class="" href="<?= $Html->ActionLink("Paginas", "Listar") ?>">P�ginas</a>
        </li>
        <li id="parceiros_listar">
            <a class="" href="<?= $Html->ActionLink("Parceiros", "Listar") ?>">Parceiros</a>
        </li>
        <li id="faleconosco_listar">
            <a href="javascript:;">
                <i class="fa fa-envelope"></i>
                Fale Conosco
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li id="faleconosco_listarrespondidas"><a class="" href="<?= $Html->ActionLink("FaleConosco", "ListarRespondidas") ?>">Respondidas</a></li>
                <li id="faleconosco_listarnaorespondidas"><a class="" href="<?= $Html->ActionLink("FaleConosco", "ListarNaoRespondidas") ?>">N�o Respondidas</a></li>
            </ul>
        </li>

    </ul>
</li>

<li class="">
    <a href="javascript:;" class="">
        <i class="fa fa-group"></i>
        <span class="title">Filiados</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li id="filiados_listar"><a class="" href="<?= $Html->ActionLink("Filiados", "Listar") ?>">Filiados</a></li>
        <!--                    <li id="filiados_listarnaoativos"><a class="" href="--><?//= $Html->ActionLink("Filiados", "ListarNaoAtivos") ?><!--">Filiados n�o Ativos</a></li>-->
        <li id="filiados_listarsolicitacoes"><a class="" href="<?= $Html->ActionLink("Filiados", "ListarSolicitacoes") ?>">Novas Solicita��es</a></li>
        <li id="funcionarios_listar"><a class="" href="<?= $Html->ActionLink("Funcionarios", "Listar") ?>">Funcionarios</a></li>
    </ul>
</li>


<li class="">
    <a href="javascript:;" class="">
        <i class="fa fa-money"></i>
        <span class="title">Cont�bil</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li id="contabil_informacoesfinanceiras"><a href="../../Contabil/InformacoesFinanceiras/">Informa��es Financeiras</a></li>
        <li id="contabil_manutencao">
            <a href="../../Contabil/Pasta/">
                <i class="fa fa-pencil"></i>
                Manuten��o
            </a>

        </li>


    </ul>
</li>
<li id="processos_menu" class="">
    <a href="javascript:;" class="">
        <i class="fa fa-folder-open"></i>
        <span class="title">Jur�dico</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li id="processos_todos">
            <a class="" href="<?= $Html->ActionLink("Processos", "Todos") ?>">Todos Processos</a>
        </li>
        <li id="processos_sindicato">
            <a class="" href="<?= $Html->ActionLink("Processos", "Sindicato") ?>">A��es Coletivas</a>
        </li>
        <li id="processos_meus">
            <a class="" href="<?= $Html->ActionLink("Processos", "Meus") ?>">A��es Individuais/Admin.</a>
        </li>
    </ul>
</li>
<li class="">
    <a href="javascript:;" class="">
        <i class="fa fa-gavel"></i>
        <span class="title">Elei��es</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li id="">
        </li>
    </ul>
</li>
<li class="">
    <a href="javascript:;" class="">
        <i class="fa fa-book"></i>
        <span class="title">Cursos</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li id="">
        </li>
    </ul>
</li>
<li id="parametros_listar">
    <a href="<?= $Html->ActionLink("Parametros", "Listar") ?>">
        <i class="fa fa-cogs"></i>
        <span class="title">Parametros</span>
    </a>
</li>
<li id="usuarios_listar">
    <a href="<?= $Html->ActionLink("Usuarios", "Listar") ?>">
        <i class="fa fa-group"></i>
        <span class="title">Usuarios</span>
    </a>
</li>