<?php
/**
 * Created by PhpStorm.
 * User: Gustavo
 */
echo "Gerando Data Sources...<br /><br />";
include "Classes/Schema.php";
include "Classes/Colunas.php";
include "Classes/Tabelas.php";
include "Classes/ChavesEstrangeiras.php";

$schema = new Schema(DB_NAME);
$schema->PreencheTabelas();

/** @var $Tabela Tabelas */
foreach ($schema->getTabelas() as $Tabela) {

    echo "-------Tabela: " . $Tabela->getNomeTabela() . "-------<br />";
    $tbl = "tbl" . $Tabela->getNomeTabela();
    $f = fopen("../application/admin/Views/" . $Tabela->getNomeTabela() . "/" . $Tabela->getNomeTabela() . ".php", "w");
    $arquivoTbl = "<?php
\n/**\n * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.\n */
\nabstract class " . $tbl . "
{\n";
    $TabelasCompostas = explode("_tem_", $Tabela->getNomeTabela());
    $Tabela->PreencheColunas();

    $arquivoTbl .= "\n\n}";
    fwrite($f, $arquivoTbl);
    fclose($f);
    echo "-------Tabela: " . $Tabela->getNomeTabela() . " Criada com Sucesso-------<br /><br />";

}
