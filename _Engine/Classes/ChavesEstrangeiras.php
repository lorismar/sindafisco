<?php
/**
 * Created by PhpStorm.
 * User: Gustavo
 */

class ChavesEstrangeiras
{
    private $_NomeSchema;
    private $_NomeTabela;
    private $_NomeColuna;
    private $_NomeTabelaReferenciada;
    private $_NomeColunaReferenciada;
    private $_DescricaoReferenciada;

    function __construct($_NomeSchema, $_NomeTabela, $_NomeColuna)
    {
        $this->_NomeSchema = $_NomeSchema;
        $this->_NomeTabela = $_NomeTabela;
        $this->_NomeColuna = $_NomeColuna;
    }


    public function PreencheFK()
    {
        $c = Conexao::getInstance();
        $sql = "SELECT u.COLUMN_NAME, u.REFERENCED_TABLE_NAME, u.REFERENCED_COLUMN_NAME,
                        (select COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA ='" . $this->getNomeSchema() . "' AND TABLE_NAME = u.REFERENCED_TABLE_NAME AND DATA_TYPE = 'VARCHAR' ORDER BY ORDINAL_POSITION ASC LIMIT 1)
                        as DESCRICAO FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE u
                        WHERE CONSTRAINT_NAME IN (   SELECT CONSTRAINT_NAME   FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS) and TABLE_SCHEMA = '" . $this->getNomeSchema() . "' and TABLE_NAME = '" . $this->getNomeTabela() . "' and COLUMN_NAME ='" . $this->getNomeColuna() . "' ORDER BY TABLE_NAME, ORDINAL_POSITION";

        $c->Consulta($sql);
        if ($c->Resultado()) {
            $this->setNomeColuna($c->linha['COLUMN_NAME']);
            $this->setNomeTabelaReferenciada($c->linha['REFERENCED_TABLE_NAME']);
            $this->setNomeColunaReferenciada($c->linha['REFERENCED_COLUMN_NAME']);
            $this->setDescricaoReferenciada($c->linha['DESCRICAO']);
            return true;
        }
        return false;
    }

    /**
     * @param mixed $DescricaoReferenciada
     */
    public function setDescricaoReferenciada($DescricaoReferenciada)
    {
        $this->_DescricaoReferenciada = $DescricaoReferenciada;
    }

    /**
     * @return mixed
     */
    public function getDescricaoReferenciada()
    {
        return $this->_DescricaoReferenciada;
    }

    /**
     * @param mixed $NomeColuna
     */
    public function setNomeColuna($NomeColuna)
    {
        $this->_NomeColuna = $NomeColuna;
    }

    /**
     * @return mixed
     */
    public function getNomeColuna()
    {
        return $this->_NomeColuna;
    }

    /**
     * @param mixed $NomeColunaReferenciada
     */
    public function setNomeColunaReferenciada($NomeColunaReferenciada)
    {
        $this->_NomeColunaReferenciada = $NomeColunaReferenciada;
    }

    /**
     * @return mixed
     */
    public function getNomeColunaReferenciada()
    {
        return $this->_NomeColunaReferenciada;
    }

    /**
     * @param mixed $NomeSchema
     */
    public function setNomeSchema($NomeSchema)
    {
        $this->_NomeSchema = $NomeSchema;
    }

    /**
     * @return mixed
     */
    public function getNomeSchema()
    {
        return $this->_NomeSchema;
    }

    /**
     * @param mixed $NomeTabela
     */
    public function setNomeTabela($NomeTabela)
    {
        $this->_NomeTabela = $NomeTabela;
    }

    /**
     * @return mixed
     */
    public function getNomeTabela()
    {
        return $this->_NomeTabela;
    }

    /**
     * @param mixed $NomeTabelaReferenciada
     */
    public function setNomeTabelaReferenciada($NomeTabelaReferenciada)
    {
        $this->_NomeTabelaReferenciada = $NomeTabelaReferenciada;
    }

    /**
     * @return mixed
     */
    public function getNomeTabelaReferenciada()
    {
        return $this->_NomeTabelaReferenciada;
    }


}