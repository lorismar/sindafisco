<?php
/**
 * Created by PhpStorm.
 * User: Gustavo
 */

class Tabelas
{
    private $_NomeSchema;
    private $_NomeTabela;
    private $_Colunas = array();

    function __construct($_NomeSchema, $_NomeTabela)
    {
        $this->_NomeSchema = $_NomeSchema;
        $this->_NomeTabela = $_NomeTabela;
    }

    public function PreencheColunas()
    {
        $c = Conexao::getInstance();
        $sql = "SELECT distinct `COLUMNS`.`TABLE_CATALOG`,
    `COLUMNS`.`TABLE_SCHEMA`,
    `COLUMNS`.`TABLE_NAME`,
    `COLUMNS`.`COLUMN_NAME`,
    `COLUMNS`.`ORDINAL_POSITION`,
    `COLUMNS`.`COLUMN_DEFAULT`,
    `COLUMNS`.`IS_NULLABLE`,
    `COLUMNS`.`DATA_TYPE`,
    `COLUMNS`.`CHARACTER_MAXIMUM_LENGTH`,
    `COLUMNS`.`CHARACTER_OCTET_LENGTH`,
    `COLUMNS`.`NUMERIC_PRECISION`,
    `COLUMNS`.`NUMERIC_SCALE`,
    `COLUMNS`.`CHARACTER_SET_NAME`,
    `COLUMNS`.`COLLATION_NAME`,
    `COLUMNS`.`COLUMN_TYPE`,
    `COLUMNS`.`COLUMN_KEY`,
    `COLUMNS`.`EXTRA`,
    `COLUMNS`.`PRIVILEGES`,
    `COLUMNS`.`COLUMN_COMMENT` FROM `information_schema`.`COLUMNS` WHERE TABLE_NAME = '" . $this->getNomeTabela() . "' and TABLE_SCHEMA = '" . $this->getNomeSchema() . "'";

        $c->Consulta($sql);
        $arrayList = array();
        while ($c->Resultado()) {
            $tbl = new Colunas(NULL);
            $tbl->setNomeSchema($this->getNomeSchema());
            $tbl->setNomeTabela($this->getNomeTabela());
            $tbl->setPrefixo(substr($c->linha['COLUMN_NAME'], 0, strpos($c->linha['COLUMN_NAME'], "_")));
            $tbl->setSufixo(strrchr($c->linha['COLUMN_NAME'], "_"));
            $tbl->setNomeColuna($c->linha['COLUMN_NAME']);
            $tbl->setOrdemOrdinal($c->linha['ORDINAL_POSITION']);
            $tbl->setValorPadrao($c->linha['COLUMN_DEFAULT']);
            $tbl->setPodeNulo($c->linha['IS_NULLABLE'] == "NO" ? false : true);
            $tbl->setChaveColuna($c->linha['COLUMN_KEY']);
            $tbl->setTipoDeDados($c->linha['DATA_TYPE']);
            $tbl->setTamanhoMaximoCaracteres($c->linha['CHARACTER_MAXIMUM_LENGTH']);
            $tbl->setPrecisaoNumerica($c->linha['NUMERIC_PRECISION']);
            $tbl->setEscalaNumerica($c->linha['NUMERIC_SCALE']);
            $tbl->setExtra($c->linha['EXTRA']);
            $tbl->setComentario($c->linha['COLUMN_COMMENT']);

            array_push($arrayList, $tbl);
        }
        $this->setColunas($arrayList);
    }


    /**
     * @param array $Colunas
     */
    public function setColunas($Colunas)
    {
        $this->_Colunas = $Colunas;
    }

    /**
     * @return Colunas
     */
    public function getColunas()
    {
        return $this->_Colunas;
    }

    /**
     * @param mixed $NomeTabela
     */
    public function setNomeTabela($NomeTabela)
    {
        $this->_NomeTabela = $NomeTabela;
    }

    /**
     * @param mixed $NomeSchema
     */
    public function setNomeSchema($NomeSchema)
    {
        $this->_NomeSchema = $NomeSchema;
    }

    /**
     * @return mixed
     */
    public function getNomeSchema()
    {
        return $this->_NomeSchema;
    }

    /**
     * @return mixed
     */
    public function getNomeTabela()
    {
        return $this->_NomeTabela;
    }

} 