<?php
/**
 * Created by PhpStorm.
 * User: Gustavo
 */

class Colunas
{
    private $_NomeSchema;
    private $_NomeTabela;
    private $_NomeColuna;
    private $_Prefixo;
    private $_Sufixo;
    private $_OrdemOrdinal;
    private $_ValorPadrao;
    private $_PodeNulo;
    private $_TipoDeDados;
    private $_TamanhoMaximoCaracteres;
    private $_PrecisaoNumerica;
    private $_EscalaNumerica;
    private $_ChaveColuna;
    private $_Extra;
    private $_Comentario;
    private $_ChaveEstrangeira;


    /**
     * @param mixed $ChaveColuna
     */
    public function setChaveColuna($ChaveColuna)
    {
        $this->_ChaveColuna = $ChaveColuna;
        if ($this->_ChaveColuna == "MUL"  || $this->_Sufixo == "_fk") {
            $this->_ChaveEstrangeira = new ChavesEstrangeiras($this->getNomeSchema(), $this->getNomeTabela(), $this->getNomeColuna());
        }

    }


    public function retornaParam()
    {
        $param = $this->getTipoDeDados();
        switch ($param) {
            case "varchar":
                $param = "string";
                break;
            case "tinyint":
                $param = "bool";
                break;
            case "datetime":
                $param = "string";
                break;
            case "text":
                $param = "string";
                break;
            case "mediumtext":
                $param = "string";
                break;
            case "char":
                $param = "string";
                break;
            case "decimal":
                $param = "float";
                break;
        }

        return $param . "|null";
    }

    /**
     * @param mixed $Prefixo
     */
    public function setPrefixo($Prefixo)
    {
        $this->_Prefixo = $Prefixo;
    }

    /**
     * @return mixed
     */
    public function getPrefixo()
    {
        return $this->_Prefixo;
    }


    /**
     * @param mixed $EscalaNumerica
     */
    public function setEscalaNumerica($EscalaNumerica)
    {
        $this->_EscalaNumerica = $EscalaNumerica;
    }

    /**
     * @return mixed
     */
    public function getEscalaNumerica()
    {
        return $this->_EscalaNumerica;
    }

    /**
     * @param mixed $Extra
     */
    public function setExtra($Extra)
    {
        $this->_Extra = $Extra;
    }

    /**
     * @return mixed
     */
    public function getExtra()
    {
        return $this->_Extra;
    }

    /**
     * @param mixed $OrdemOrdinal
     */
    public function setOrdemOrdinal($OrdemOrdinal)
    {
        $this->_OrdemOrdinal = $OrdemOrdinal;
    }

    /**
     * @return mixed
     */
    public function getOrdemOrdinal()
    {
        return $this->_OrdemOrdinal;
    }

    /**
     * @param mixed $PodeNulo
     */
    public function setPodeNulo($PodeNulo)
    {
        $this->_PodeNulo = $PodeNulo;
    }

    /**
     * @return mixed
     */
    public function getPodeNulo()
    {
        return $this->_PodeNulo;
    }

    /**
     * @param mixed $PrecisaoNumerica
     */
    public function setPrecisaoNumerica($PrecisaoNumerica)
    {
        $this->_PrecisaoNumerica = $PrecisaoNumerica;
    }

    /**
     * @return mixed
     */
    public function getPrecisaoNumerica()
    {
        return $this->_PrecisaoNumerica;
    }

    /**
     * @param mixed $TamanhoMaximoCaracteres
     */
    public function setTamanhoMaximoCaracteres($TamanhoMaximoCaracteres)
    {
        $this->_TamanhoMaximoCaracteres = $TamanhoMaximoCaracteres;
    }

    /**
     * @return mixed
     */
    public function getTamanhoMaximoCaracteres()
    {
        return $this->_TamanhoMaximoCaracteres;
    }

    /**
     * @param mixed $TipoDeDados
     */
    public function setTipoDeDados($TipoDeDados)
    {
        $this->_TipoDeDados = $TipoDeDados;
    }

    /**
     * @return mixed
     */
    public function getTipoDeDados()
    {
        return $this->_TipoDeDados;
    }


    /**
     * @param mixed $NomeSchema
     */
    public function setNomeSchema($NomeSchema)
    {
        $this->_NomeSchema = $NomeSchema;
    }

    /**
     * @return mixed
     */
    public function getNomeSchema()
    {
        return $this->_NomeSchema;
    }

    /**
     * @param mixed $NomeTabela
     */
    public function setNomeTabela($NomeTabela)
    {
        $this->_NomeTabela = $NomeTabela;
    }

    /**
     * @return mixed
     */
    public function getNomeTabela()
    {
        return $this->_NomeTabela;
    }


    /**
     * @return mixed
     */
    public function getChaveColuna()
    {
        return $this->_ChaveColuna;
    }

    /**
     * @param mixed $NomeColuna
     */
    public function setNomeColuna($NomeColuna)
    {
        $this->_NomeColuna = $NomeColuna;
    }

    /**
     * @return mixed
     */
    public function getNomeColuna()
    {
        return $this->_NomeColuna;
    }


    /**
     * @param mixed $Comentario
     */
    public function setComentario($Comentario)
    {
        $this->_Comentario = $Comentario;
    }

    /**
     * @return mixed
     */
    public function getComentario()
    {
        return $this->_Comentario;
    }

    /**
     * @param mixed $ChaveEstrangeira
     */
    public function setChaveEstrangeira($ChaveEstrangeira)
    {
        $this->_ChaveEstrangeira = $ChaveEstrangeira;
    }

    /**
     * @return ChavesEstrangeiras
     */
    public function getChaveEstrangeira()
    {
        return $this->_ChaveEstrangeira;
    }

    /**
     * @param mixed $Sufixo
     */
    public function setSufixo($Sufixo)
    {
        $this->_Sufixo = $Sufixo;
    }

    /**
     * @return mixed
     */
    public function getSufixo()
    {
        return $this->_Sufixo;
    }

    /**
     * @param mixed $ValorPadrao
     */
    public function setValorPadrao($ValorPadrao)
    {
        $this->_ValorPadrao = $ValorPadrao;
    }

    /**
     * @return mixed
     */
    public function getValorPadrao()
    {
        return $this->_ValorPadrao;
    }


}
