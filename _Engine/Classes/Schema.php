<?php
/**
 * Created by PhpStorm.
 * User: Gustavo
 */

class Schema
{
    private $_NomeSchema;
    private $_Tabelas = array();

    function __construct($_NomeSchema)
    {
        $this->_NomeSchema = $_NomeSchema;
    }


    public function PreencheTabelas()
    {
        $c = Conexao::getInstance();
        $sql = "SELECT TABLE_SCHEMA, TABLE_NAME FROM `information_schema`.`TABLES` WHERE TABLE_SCHEMA = '" . $this->getNomeSchema() . "' ";

        $c->Consulta($sql);
        $arrayList = array();
        while ($c->Resultado()) {
            $tbl = new Tabelas($c->linha['TABLE_SCHEMA'], $c->linha['TABLE_NAME']);
            array_push($arrayList, $tbl);
        }

        $this->setTabelas($arrayList);

    }

    /**
     * @param mixed $NomeSchema
     */
    public function setNomeSchema($NomeSchema)
    {
        $this->_NomeSchema = $NomeSchema;
    }

    /**
     * @return mixed
     */
    public function getNomeSchema()
    {
        return $this->_NomeSchema;
    }

    /**
     * @param array $Tabelas
     */
    public function setTabelas($Tabelas)
    {
        $this->_Tabelas = $Tabelas;
    }

    /**
     * @return Tabelas
     */
    public function getTabelas()
    {
        return $this->_Tabelas;
    }


}





