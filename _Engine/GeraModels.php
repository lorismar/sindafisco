<?php
/**
 * Created by PhpStorm.
 * User: Gustavo
 */
echo "Gerando Data Sources...<br /><br />";
include "Classes/Schema.php";
include "Classes/Colunas.php";
include "Classes/Tabelas.php";
include "Classes/ChavesEstrangeiras.php";

$schema = new Schema(DB_NAME);
$schema->PreencheTabelas();

/** @var $Tabela Tabelas */
foreach ($schema->getTabelas() as $Tabela) {

    echo "-------Tabela: " . $Tabela->getNomeTabela() . "-------<br />";
    $tbl = $Tabela->getNomeTabela();
    $nomearquivotbl = trim($Tabela->getNomeTabela() . ".model.php");
    $f = fopen("../models/" . $nomearquivotbl, "w");
    $arquivoTbl = "<?php
\n/**\n * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.\n */
\nclass " . $tbl . " extends tbl" . $Tabela->getNomeTabela() . "
{\n";

    $Tabela->PreencheColunas();
    $colunasTODAS = array();
    $colunasPRI = array();
    $colunasNORMAIS = array();
    $tabelaTEMLOG = false;
    $TabelasCompostas = explode("_tem_", $Tabela->getNomeTabela());

    /** @var $Coluna Colunas */
    foreach ($Tabela->getColunas() as $Coluna) {
        if ($Coluna->getChaveColuna() == "PRI") {
            array_push($colunasPRI, $Coluna->getNomeColuna());
            array_push($colunasNORMAIS, $Coluna->getNomeColuna());
            array_push($colunasTODAS, $Coluna->getNomeColuna());
        } else {
            if ($Coluna->getPrefixo() == "log") {
                $tabelaTEMLOG = true;
            }
            if ($Coluna->getPrefixo() == "dt") {
                array_push($colunasNORMAIS, $Coluna->getNomeColuna());
                array_push($colunasTODAS, "DATE_FORMAT(" . $Coluna->getNomeColuna() . ", '%d/%m/%Y') " . $Coluna->getNomeColuna());
            } elseif ($Coluna->getPrefixo() == "dh") {
                array_push($colunasNORMAIS, $Coluna->getNomeColuna());
                array_push($colunasTODAS, "DATE_FORMAT(" . $Coluna->getNomeColuna() . ", '%d/%m/%Y %H:%i') " . $Coluna->getNomeColuna());
            } elseif ($Coluna->getPrefixo() == "log") {
                array_push($colunasTODAS, $Coluna->getNomeColuna());
            } else {
                array_push($colunasNORMAIS, $Coluna->getNomeColuna());
                array_push($colunasTODAS, $Coluna->getNomeColuna());
            }
        }
    }

    $countPRI = 0;
    $where1 = "";
    $null1 = "";
    $salvar1 = "";
    foreach ($colunasPRI as $Coluna) {
        $countPRI++;
        if ($countPRI == 1) {
            $where1 .= $Coluna . " = ? ";
            $null1 .= "NULL";
            $salvar1 .= "\$this->get" . ucfirst(StringUtils::camelCase($Coluna)) . "() > 0";
        } else {
            $where1 .= "AND " . $Coluna . " = ? ";
            $null1 .= ", NULL";
            $salvar1 .= " && \$this->get" . ucfirst(StringUtils::camelCase($Coluna)) . "() > 0";
        }
    }

    // visualizar por id
    $arquivoTbl .= "\n    /**";
    $arquivoTbl .= "\n     * Busca atrav�s do ID da PK j� definido na classe";
    $arquivoTbl .= "\n     * @return bool - TRUE se achou registro e FALSE se n�o achou ";
    $arquivoTbl .= "\n     */";
    $arquivoTbl .= "\n    public function VisualizarPorId()";
    $arquivoTbl .= "\n    {";
    $arquivoTbl .= "\n        \$c   = Conexao::getInstance();";
    $arquivoTbl .= "\n        \$sql = \"SELECT " . implode(', ', array_values($colunasTODAS)) . " FROM " . $Tabela->getNomeTabela() . " WHERE " . $where1 . "  \";";
    $arquivoTbl .= "\n        \$c->preparaStatement(\$sql);";
    foreach ($Tabela->getColunas() as $Coluna) {
        if ($Coluna->getChaveColuna() == "PRI") {
            $arquivoTbl .= "\n        \$c->adicionaParametros(\$this->get" . ucfirst(StringUtils::camelCase($Coluna->getNomeColuna())) . "());";
        }
    }
    $arquivoTbl .= "\n        \$c->executaStatement();";
    $arquivoTbl .= "\n";
    $arquivoTbl .= "\n        if (\$c->Resultado()) {";
    foreach ($Tabela->getColunas() as $Coluna) {
        $arquivoTbl .= "\n            \$this->set" . ucfirst(StringUtils::camelCase($Coluna->getNomeColuna())) . "(\$c->linha['" . $Coluna->getNomeColuna() . "']);";
    }
    $arquivoTbl .= "\n";
    $arquivoTbl .= "\n            return TRUE;";
    $arquivoTbl .= "\n        }";
    $arquivoTbl .= "\n        return FALSE;";
    $arquivoTbl .= "\n    }";

    // listar todos
    $arquivoTbl .= "\n";
    $arquivoTbl .= "\n    /**";
    $arquivoTbl .= "\n     * Busca por todos os registros com ou sem cl�usula";
    $arquivoTbl .= "\n     * @param null \$where Cl�usula ";
    $arquivoTbl .= "\n     * @return array ";
    $arquivoTbl .= "\n     */";
    $arquivoTbl .= "\n    public static function ListarTodos(\$where = NULL)";
    $arquivoTbl .= "\n    {";
    $arquivoTbl .= "\n        \$c   = Conexao::getInstance();";
    $arquivoTbl .= "\n        \$sql = \"SELECT " . implode(', ', array_values($colunasTODAS)) . " FROM " . $Tabela->getNomeTabela() . " \$where \";";
    $arquivoTbl .= "\n        \$c->Consulta(\$sql);";
    $arquivoTbl .= "\n        \$arrayList = array();";
    $arquivoTbl .= "\n";
    $arquivoTbl .= "\n        while (\$c->Resultado()) {";
    $arquivoTbl .= "\n            \$tbl = new " . $Tabela->getNomeTabela() . "(" . $null1 . ");";
    foreach ($Tabela->getColunas() as $Coluna) {
        $arquivoTbl .= "\n            \$tbl->set" . ucfirst(StringUtils::camelCase($Coluna->getNomeColuna())) . "(\$c->linha['" . $Coluna->getNomeColuna() . "']);";
    }
    $arquivoTbl .= "\n            array_push(\$arrayList, \$tbl);";
    $arquivoTbl .= "\n        }";
    $arquivoTbl .= "\n        return \$arrayList;";
    $arquivoTbl .= "\n    }";


    if (count($TabelasCompostas) > 1) {
        // listar todos por pk
        $arquivoTbl .= "\n";
        $arquivoTbl .= "\n    /**";
        $arquivoTbl .= "\n     * Busca por todos os registros por " . $TabelasCompostas[0];
        $arquivoTbl .= "\n     * @param int \$id_" . $TabelasCompostas[0] . "_fk ID da PK da " . $TabelasCompostas[0]." ";
        $arquivoTbl .= "\n     * @return array ";
        $arquivoTbl .= "\n     */";
        $arquivoTbl .= "\n    public static function ListarTodosPor" . ucfirst($TabelasCompostas[0]) . "(\$id_" . $TabelasCompostas[0] . "_fk)";
        $arquivoTbl .= "\n    {";
        $arquivoTbl .= "\n        \$c   = Conexao::getInstance();";
        $arquivoTbl .= "\n        \$sql = \"SELECT " . implode(', ', array_values($colunasTODAS)) . " FROM " . $Tabela->getNomeTabela() . " WHERE id_" . $TabelasCompostas[0] . "_fk = ? \";";
        $arquivoTbl .= "\n        \$c->preparaStatement(\$sql);";
        $arquivoTbl .= "\n        \$c->adicionaParametros(\$id_" . $TabelasCompostas[0] . "_fk);";
        $arquivoTbl .= "\n        \$c->executaStatement();";
        $arquivoTbl .= "\n        \$arrayList = array();";
        $arquivoTbl .= "\n";
        $arquivoTbl .= "\n        while (\$c->Resultado()) {";
        $arquivoTbl .= "\n            \$tbl = new " . $Tabela->getNomeTabela() . "(" . $null1 . ");";
        foreach ($Tabela->getColunas() as $Coluna) {
            $arquivoTbl .= "\n            \$tbl->set" . ucfirst(StringUtils::camelCase($Coluna->getNomeColuna())) . "(\$c->linha['" . $Coluna->getNomeColuna() . "']);";
        }
        $arquivoTbl .= "\n            array_push(\$arrayList, \$tbl);";
        $arquivoTbl .= "\n        }";
        $arquivoTbl .= "\n        return \$arrayList;";
        $arquivoTbl .= "\n    }";
    }
    // fun��o SALVAR
    $arquivoTbl .= "\n";
    $arquivoTbl .= "\n    /**";
    $arquivoTbl .= "\n     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro";
    $arquivoTbl .= "\n     * @return bool ";
    $arquivoTbl .= "\n     */";
    $arquivoTbl .= "\n    public function Salvar()";
    $arquivoTbl .= "\n    {";
    $arquivoTbl .= "\n        if (" . $salvar1 . ") {";
    $arquivoTbl .= "\n            return \$this->Alterar();";
    $arquivoTbl .= "\n        } else {";
    $arquivoTbl .= "\n            return \$this->Criar();";
    $arquivoTbl .= "\n        }";
    $arquivoTbl .= "\n    }";


    // funcao CRIAR
    $insertLOG = "";
    $insertPARAM = "";

    $countCol = 0;
    foreach ($Tabela->getColunas() as $Coluna) {
        $countCol++;
        if ($Coluna->getPrefixo() != "log") {
            if ($countCol == 1) {
                if (($Coluna->getPrefixo() == "dt" && $Coluna->getValorPadrao() != NULL) || ($Coluna->getPrefixo() == "dh" && $Coluna->getValorPadrao() != NULL))
                    $insertPARAM .= "now()";
                else if ($Coluna->getPrefixo() == "dt" && $Coluna->getValorPadrao() == NULL)
                    $insertPARAM .= "STR_TO_DATE(?,'%d/%m/%Y')";
                else if ($Coluna->getPrefixo() == "dh" && $Coluna->getValorPadrao() == NULL)
                    $insertPARAM .= "STR_TO_DATE(?,'%d/%m/%Y %H:%i')";
                else
                    $insertPARAM .= "?";
            } else {
                if (($Coluna->getPrefixo() == "dt" && $Coluna->getValorPadrao() != NULL) || ($Coluna->getPrefixo() == "dh" && $Coluna->getValorPadrao() != NULL))
                    $insertPARAM .= ", now()";
                else if ($Coluna->getPrefixo() == "dt" && $Coluna->getValorPadrao() == NULL)
                    $insertPARAM .= ", STR_TO_DATE(?,'%d/%m/%Y')";
                else if ($Coluna->getPrefixo() == "dh" && $Coluna->getValorPadrao() == NULL)
                    $insertPARAM .= ", STR_TO_DATE(?,'%d/%m/%Y %H:%i')";
                else
                    $insertPARAM .= ", ?";
            }
        }
    }
    if ($tabelaTEMLOG) {
        $insertLOG = ", log_CRIADOPOR, log_ALTERADOPOR, log_DATA";
        $insertPARAM .= ", ?, ?, now()";
    }
    $arquivoTbl .= "\n";
    $arquivoTbl .= "\n    /**";
    $arquivoTbl .= "\n     * Adiciona um Registro na Tabela";
    $arquivoTbl .= "\n     * @return bool - TRUE se criou registro e FALSE se n�o criou ";
    $arquivoTbl .= "\n     */";
    $arquivoTbl .= "\n    private function Criar()";
    $arquivoTbl .= "\n    {";
    $arquivoTbl .= "\n        \$c   = Conexao::getInstance();";
    $arquivoTbl .= "\n        \$sql = \"INSERT INTO " . $Tabela->getNomeTabela() . " (" . implode(', ', array_values($colunasNORMAIS)) . "" . $insertLOG . " ) VALUES (" . $insertPARAM . ")  \";";
    $arquivoTbl .= "\n        \$c->preparaStatement(\$sql);";
    foreach ($Tabela->getColunas() as $Coluna) {
        if ($Coluna->getPrefixo() != "log") {
            if (($Coluna->getPrefixo() != "dt" && $Coluna->getValorPadrao() == NULL) || ($Coluna->getPrefixo() != "dh" && $Coluna->getValorPadrao() == NULL))
                $arquivoTbl .= "\n        \$c->adicionaParametros(\$this->get" . ucfirst(StringUtils::camelCase($Coluna->getNomeColuna())) . "());";
        }

    }
    if ($tabelaTEMLOG) {
        $arquivoTbl .= "\n        \$c->adicionaParametros(\$this->getLogALTERADOPOR());";
        $arquivoTbl .= "\n        \$c->adicionaParametros(\$this->getLogALTERADOPOR());";
    }
    $arquivoTbl .= "\n";
    $arquivoTbl .= "        if (\$c->executaStatement()) {";
    if ($countPRI == 1) {
        foreach ($Tabela->getColunas() as $Coluna) {
            if ($Coluna->getChaveColuna() == "PRI")
                $arquivoTbl .= "\n            \$this->set" . ucfirst(StringUtils::camelCase($Coluna->getNomeColuna())) . "(\$c->last_id);";
        }
    }
    $arquivoTbl .= "\n            return TRUE;";
    $arquivoTbl .= "\n        }";
    $arquivoTbl .= "\n        return FALSE;";
    $arquivoTbl .= "\n    }";


    // funcao Alterar
    $updateWHERE = array();
    $updateCOLUNAS = array();

    foreach ($Tabela->getColunas() as $Coluna) {
        if ($Coluna->getChaveColuna() == "PRI") {
            array_push($updateWHERE, $Coluna->getNomeColuna() . " = ?");
        } elseif ($Coluna->getPrefixo() == "log") {

        } else if ($Coluna->getPrefixo() == "dt")
            array_push($updateCOLUNAS, $Coluna->getNomeColuna() . " = STR_TO_DATE(?,'%d/%m/%Y')");
        else if ($Coluna->getPrefixo() == "dh")
            array_push($updateCOLUNAS, $Coluna->getNomeColuna() . " = STR_TO_DATE(?,'%d/%m/%Y %H:%i')");
        else {
            array_push($updateCOLUNAS, $Coluna->getNomeColuna() . " = ?");
        }
    }
    $updateLOG = "";
    if ($tabelaTEMLOG) {
        $updateLOG = ", log_ALTERADOPOR=?, log_DATA=now()";
    }
    $arquivoTbl .= "\n";
    $arquivoTbl .= "\n    /**";
    $arquivoTbl .= "\n     * Altera um Registro da Tabela";
    $arquivoTbl .= "\n     * @return bool - TRUE se criou registro e FALSE se n�o criou ";
    $arquivoTbl .= "\n     */";
    $arquivoTbl .= "\n    private function Alterar()";
    $arquivoTbl .= "\n    {";
    $arquivoTbl .= "\n        \$c   = Conexao::getInstance();";
    $arquivoTbl .= "\n        \$sql = \"UPDATE " . $Tabela->getNomeTabela() . " SET " . implode(', ', array_values($updateCOLUNAS)) . "" . $updateLOG . " WHERE " . implode(' AND ', array_values($updateWHERE)) . "  \";";
    $arquivoTbl .= "\n        \$c->preparaStatement(\$sql);";
    foreach ($Tabela->getColunas() as $Coluna) {
        if ($Coluna->getPrefixo() != "log" && $Coluna->getChaveColuna() != "PRI") {
            $arquivoTbl .= "\n        \$c->adicionaParametros(\$this->get" . ucfirst(StringUtils::camelCase($Coluna->getNomeColuna())) . "());";
        }
    }
    if ($tabelaTEMLOG) {
        $arquivoTbl .= "\n        \$c->adicionaParametros(\$this->getLogALTERADOPOR());";
    }
    $arquivoTbl .= "\n        // PK";
    foreach ($Tabela->getColunas() as $Coluna) {
        if ($Coluna->getChaveColuna() == "PRI") {
            $arquivoTbl .= "\n        \$c->adicionaParametros(\$this->get" . ucfirst(StringUtils::camelCase($Coluna->getNomeColuna())) . "());";
        }
    }
    $arquivoTbl .= "\n        if (\$c->executaStatement()) {";
    $arquivoTbl .= "\n            return TRUE;";
    $arquivoTbl .= "\n        }";
    $arquivoTbl .= "\n        return FALSE;";
    $arquivoTbl .= "\n    }";

    // FUNCAO Remover
    $deleteID = array();
    foreach ($Tabela->getColunas() as $Coluna) {
        if ($Coluna->getChaveColuna() == "PRI") {
            array_push($deleteID, "$" . $Coluna->getNomeColuna());
        }
    }

    $arquivoTbl .= "\n";
    $arquivoTbl .= "\n    /**";
    $arquivoTbl .= "\n     * Remove um Registro da Tabela";
    foreach ($Tabela->getColunas() as $Coluna) {
        if ($Coluna->getChaveColuna() == "PRI") {
            $arquivoTbl .= "\n     * @param \$" . $Coluna->getNomeColuna() . "";
        }
    }
    $arquivoTbl .= "\n     * @return bool - TRUE se removeu registro e FALSE se n�o removeu ";
    $arquivoTbl .= "\n     */";
    $arquivoTbl .= "\n    public static function Remover(" . implode(', ', array_values($deleteID)) . ")";
    $arquivoTbl .= "\n    {";
    $arquivoTbl .= "\n        \$c   = Conexao::getInstance();";
    $arquivoTbl .= "\n        \$sql = \"DELETE FROM " . $Tabela->getNomeTabela() . " WHERE " . implode(' AND ', array_values($updateWHERE)) . "  \";";
    $arquivoTbl .= "\n        \$c->preparaStatement(\$sql);";
    foreach ($Tabela->getColunas() as $Coluna) {
        if ($Coluna->getChaveColuna() == "PRI") {
            $arquivoTbl .= "\n        \$c->adicionaParametros(\$" . $Coluna->getNomeColuna() . ");";
        }
    }
    $arquivoTbl .= "\n        if (\$c->executaStatement()) {";
    $arquivoTbl .= "\n            return TRUE;";
    $arquivoTbl .= "\n        }";
    $arquivoTbl .= "\n        return FALSE;";
    $arquivoTbl .= "\n    }";

    $arquivoTbl .= "\n\n}";
    fwrite($f, $arquivoTbl);
    fclose($f);
    echo "-------Tabela: " . $Tabela->getNomeTabela() . " Criada com Sucesso-------<br /><br />";

}