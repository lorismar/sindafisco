<?php

/* ---------------------------------- */
// FUNCOES
/* ---------------------------------- */

date_default_timezone_set("Brazil/West");

// ####################################################
// FUNCAO PARA ESCREVER EM ARQUIVO E LER
// ####################################################
// $arquivo 	= arquivo.txt
// $conteudo 	= conteudo..
// ####################################################
// --> ESCREVE

function aplicarMascara($val, $mask)
{
    $maskared = '';
    $k = 0;
    for($i = 0; $i<=strlen($mask)-1; $i++)
    {
        if($mask[$i] == '#')
        {
            if(isset($val[$k]))
                $maskared .= $val[$k++];
        }
        else
        {
            if(isset($mask[$i]))
                $maskared .= $mask[$i];
        }
    }
    return $maskared;
}


function geraChave()
{
    $character_set_array   = array();
    $character_set_array[] = array('count' => 4, 'characters' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    $character_set_array[] = array('count' => 4, 'characters' => '0123456789');
    $temp_array            = array();
    foreach ($character_set_array as $character_set) {
        for ($i = 0; $i < $character_set['count']; $i++) {
            $temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
        }
    }
    shuffle($temp_array);
    return implode('', $temp_array);
}


function retornaIcone($arquivo)
{
    $icone = "";
    switch (substr(strtolower($arquivo), -3)) {
        case "pdf":
            $icone = "../../../admin/custom/img/dtree/pdf.png";
            break;
        case "xls":
            $icone = "../../../admin/custom/img/dtree/excel.png";
            break;
        case "lsx":
            $icone = "../../../admin/custom/img/dtree/excel.png";
            break;
        case "zip":
            $icone = "../../../admin/custom/img/dtree/zip.png";
            break;
        case "rar":
            $icone = "../../../admin/custom/img/dtree/zip.png";
            break;
    }
    return $icone;
}

function escreve_arquivo($arquivo, $conteudo)
{

    if (file_exists($arquivo)) {

        if (is_writable($arquivo)) {

            // abre..
            if (!$handle = fopen($arquivo, 'w')) {
                echo "Nao foi possivel abrir o arquivo ($arquivo)";
                exit;
            }

            // escreve $conteudo no arquivo aberto..
            if (fwrite($handle, $conteudo) === FALSE) {
                echo "Nao foi possivel escrever no arquivo ($arquivo)";
                exit;
            }

            //echo "Sucesso: Escrito ($conteudo) no arquivo ($arquivo)";
            // fecha arquivo..
            fclose($handle);
        } else {
            echo "O arquivo $arquivo não pode ser alterado! Verifique as permissoes!";
        }
    }
}

function ler_arquivo($arquivo)
{
    if ($arquivo) {
        $handle = fopen($arquivo, "r");
        return fread($handle, filesize($arquivo));
        fclose($handle);
    }
}

/*
 * Redirect Function .. tenta header, java e http.. :P
 */

function redirect($url)
{
    if (!headers_sent()) { //se saida nao tiver sido feita.. entao vai com php redirect
        header('Location: ' . $url);
        exit;
    } else { //se tiver saida... bora com javscript.. se java tiver desabilitado, vai no meta
        echo '<script type="text/javascript">';
        echo 'window.location.href="' . $url . '";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url=' . $url . '" />';
        echo '</noscript>';
        exit;
    }
}

/*
* Formatar tags de conteudo
*/

function BBcode($texto)
{
    $texto = htmlentities($texto);
    $a     = array("/\[i\](.*?)\[\/i\]/is", "/\[b\](.*?)\[\/b\]/is", "/\[u\](.*?)\[\/u\]/is", "/\[red\](.*?)\[\/red\]/is", "/\[blue\](.*?)\[\/blue\]/is", "/\[img\](.*?)\[\/img\]/is", "/\[url=(.*?)\](.*?)\[\/url\]/is");
    $b     = array("<i>$1</i>", "<b>$1</b>", "<u>$1</u>", "<font color='red'>$1</font>", "<font color='blue'>$1</font>", "<img src=\"$1\" />", "<a href=\"$1\" target=\"_blank\">$2</a>");
    $texto = preg_replace($a, $b, $texto);
    $texto = nl2br($texto);
    return $texto;
}

/*
 * Slug (URL amigavel)
 */

function cleanURI($str)
{
    $str = strtr(strtolower(html_entity_decode($str)), '??????????????????????????', 'AAAAEEIOOOUUCaaaaeeiooouuc');

    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", '-', $clean);

    return $clean;
}

function cleanURI2($str)
{
    $str = strtr(strtolower(html_entity_decode($str)), '??????????????????????????', 'AAAAEEIOOOUUCaaaaeeiooouuc');

    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", '+', $clean);

    return strtoupper($clean);
}


/**
 * Descriptografada com Base64 3 vezes
 *
 * @param string $str
 * @return string
 */
function mycrypt_encode($str)
{
    for ($i = 0; $i < 1; $i++) {
        $str = strrev(base64_encode($str));
    }
    return $str;
}

/**
 * Descriptografada com mycrypt_encode
 *
 * @param string $str
 * @return string
 */
function mycrypt_decode($str)
{
    for ($i = 0; $i < 1; $i++) {
        $str = base64_decode(strrev($str));
    }
    return $str;
}


/**
 * Manipulando dados Datetime do MySQL
 * Funcao para manipular as informacoes trazidas pelo campo "datetime" do MySQL
 */
function converte_datetime($datetime)
{
    if (preg_match("/^\d{4}(-\d{2}){2} (\d{2}:){2}\d{2}$/", $datetime)) {
        $array['dia']     = substr($datetime, 8, 2);
        $array['mes']     = substr($datetime, 5, 2);
        $array['ano']     = substr($datetime, 2, 2);
        $array['hora']    = substr($datetime, 11, 2);
        $array['minuto']  = substr($datetime, 14, 2);
        $array['segundo'] = substr($datetime, 17, 2);
        $array['data']    = $array['dia'] . '/' . $array['mes'] . '/' . $array['ano'];
        $array['horario'] = substr($datetime, 11, 8);
        return $array;
    } else {
        return FALSE;
    }
}

function diasNoMes($year, $month)
{
    return date("t", mktime(0, 0, 0, $month, 1, $year));
}

function getDiaDaSemana($ano, $mes, $dia)
{
    switch (date("D", mktime(0, 0, 0, $mes, $dia, $ano))) {

        case "Sun":
            return "DOM";
            break;

        case "Mon":
            return "SEG";
            break;

        case "Tue":
            return "TER";
            break;

        case "Wed":
            return "QUA";
            break;

        case "Thu":
            return "QUI";
            break;

        case "Fri":
            return "SEX";
            break;

        case "Sat":
            return "SAB";
            break;
    }
}

// Exemplo de uso:
//$datetime = "2004-07-23 09:15:20";
//$conv_datetime = converte_datetime($datetime);
//
//echo $conv_datetime['data'];      // Retornara? 23/07/2004
//echo $conv_datetime['horario'];   // Retornara? 09:15:20
//echo $conv_datetime['dia'];       // Retornara? 23
//echo $conv_datetime['mes'];       // Retornara? 07
//echo $conv_datetime['ano'];       // Retornara? 2004
//echo $conv_datetime['hora'];      // Retornara? 09
//echo $conv_datetime['minuto'];    // Retornara? 15
//echo $conv_datetime['segundo'];   // Retornara? 20

/**
 * Manipulando dados Datetime do MySQL
 * Funcao para retornar a data corrente
 */

function retorna_data()
{
    // leitura das datas
    $dia    = date('d');
    $mes    = date('m');
    $ano    = date('Y');
    $semana = date('w');

    // configuracao mes
    switch ($mes) {
        case 1:
            $mes = "janeiro";
            break;
        case 2:
            $mes = "fevereiro";
            break;
        case 3:
            $mes = "marco";
            break;
        case 4:
            $mes = "abril";
            break;
        case 5:
            $mes = "maio";
            break;
        case 6:
            $mes = "junho";
            break;
        case 7:
            $mes = "julho";
            break;
        case 8:
            $mes = "agosto";
            break;
        case 9:
            $mes = "setembro";
            break;
        case 10:
            $mes = "outubro";
            break;
        case 11:
            $mes = "novembro";
            break;
        case 12:
            $mes = "dezembro";
            break;
    }

    // configuracao semana

    switch ($semana) {
        case 0:
            $semana = "domingo";
            break;
        case 1:
            $semana = "segunda-feira";
            break;
        case 2:
            $semana = "terca-feira";
            break;
        case 3:
            $semana = "quarta-feira";
            break;
        case 4:
            $semana = "quinta-feira";
            break;
        case 5:
            $semana = "sexta-feira";
            break;
        case 6:
            $semana = "sabado";
            break;
    }
    //usar: print ("$semana, $dia de $mes de $ano");

    print ("Porto Velho, $dia de $mes de $ano");
}

/*
 * resizeMarkup
 * http://ennuidesign.com/blog/ITT+%237:+Dynamically+Change+Width+and+Height+in+HTML+Markup/
 */

function resizeMarkup($markup, $dimensions)
{
    $w = $dimensions['width'];
    $h = $dimensions['height'];

    $patterns     = array();
    $replacements = array();
    if (!empty($w)) {
        $patterns[] = '/width="([0-9]+)"/';
        $patterns[] = '/width:([0-9]+)/';

        $replacements[] = 'width="' . $w . '"';
        $replacements[] = 'width:' . $w;
    }

    if (!empty($h)) {
        $patterns[] = '/height="([0-9]+)"/';
        $patterns[] = '/height:([0-9]+)/';

        $replacements[] = 'height="' . $h . '"';
        $replacements[] = 'height:' . $h;
    }

    return preg_replace($patterns, $replacements, $markup);
}

function traduzMes($data)
{
    // str_replace("o que voc� procura", "pelo que voc� quer trocar", "onde voc� quer trocar");
    $months = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12");
    $meses  = array("Janeiro", "Fevereiro", "Mar�o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
    return str_replace($months, $meses, $data);
}

function embedVideo($url, $width, $height)
{

    if (preg_match("#http://(.*)\.youtube\.com/watch\?v=(.*)(&(.*))?#", $url, $matches)) {
        $vec = explode('&', $matches[2]);
        echo '
 <object width="' . $width . '" height="' . $height . '">
               <param name="movie" value="http://www.youtube.com/v/' . $matches[2] . '&hl=pt-br&fs=1"></param>
               <param name="allowFullScreen" value="true"></param>
               <param name="allowscriptaccess" value="always"></param>
               <embed src="http://www.youtube.com/v/' . $matches[2] . '&hl=pt-br&fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="' . $width . '" height="' . $height . '"></embed>
            </object>
        ';
    } else {
        return FALSE;
    }
}

function youtubeImage($url, $qualidade)
{

    if (preg_match("#http://(.*)\.youtube\.com/watch\?v=(.*)(&(.*))?#", $url, $matches)) {
        if (isset($matches[2]) && $matches[2] != '') {
            $vec = explode('&', $matches[2]);
            if ($qualidade == '0') {
                $img = 'http://img.youtube.com/vi/' . $vec[0] . '/default.jpg';
            } else {
                $img = 'http://img.youtube.com/vi/' . $vec[0] . '/0.jpg';
            }

            return $img;
            //return '';
        } else {
            return FALSE;
        }
    } else {
        return FALSE;
    }
}

function converteDataMysql($data)
{

    $data     = explode("-", $data);
    $novadata = $data[2] . "/" . $data[1] . "/" . $data[0]; // Formato PT-BR(DD/MM/YYYY)
    print($novadata);
}

?>