<?php
/**
 * User: Iuri Gustavo
 * Date: 27/07/13
 * Time: 17:27
 *
 */

class Html
{

    /** @var $Layout Layout */
    public $Layout;

    function __construct()
    {
        $this->Layout = new Layout();
    }


    function ActionLink($controller, $action)
    {

        $cur_url = str_replace(BASE_PATH, "", CURRENT_URL);
        $cur_url = str_replace(_AREA, "", $cur_url);
        $expld = explode("/", $cur_url);

        if ($controller != NULL) {
            if (count($expld) == 5) {
                return "../../" . $controller . "/" . $action . "/";
            } else {
                return "../../" . $controller . "/" . $action . "/";
            }
        } else {
            return "../" . $action . "/";
        }

    }



}

