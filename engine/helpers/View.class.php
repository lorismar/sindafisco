<?php
/**
 * User: Gustavo
 * Date: 01/11/13
 * Time: 18:29
 */

class View
{
    /**
     * @param $template
     */
    function setTemplate($template)
    {
        if (!isset($GLOBALS['_layout'])) {
            $GLOBALS['_layout'] = $template;
        }
    }

    /**
     * @param $titulo
     */
    function setTitulo($titulo)
    {
        if (!isset($GLOBALS['_titulo'])) {
            $GLOBALS['_titulo'] = $titulo;
        }
    }

    /**
     * @param $descricao
     */
    function setDescricao($descricao)
    {
        if (!isset($GLOBALS['_descricao'])) {
            $GLOBALS['_descricao'] = $descricao;
        }
    }

    /**
     * @param $idMenu
     */
    function setMenu($idMenu)
    {
        GUI::selecionarMenuAtivo(strtolower($idMenu), "active");
    }


    /**
 * @param $controllerNome      // Nome do Controller
 * @param $funcaoNome          // Nome da Fun��o
 * @param array $colunasEsconder // Mostrar a Coluna ID - TRUE|FALSE
 * @param string $idNome       // Id da DIV que ir� mostrar a tabela
 */
    function Grid($controllerNome, $funcaoNome, $colunasEsconder = array(), $idNome = "tabelaPrincipal")
    {

        $grid = "oTable = $('#" . $idNome . "').dataTable({
    \"sPaginationType\": \"bootstrap\",
    \"oLanguage\": {
      \"sLengthMenu\": \"_MENU_ registros por p�gina\",
            \"sProcessing\": \"Processando...\",
            \"sZeroRecords\": \"Nenhum registro encontrado\",
            \"sEmptyTable\": \"Sem registros dispon&iacute;veis\",
            \"sLoadingRecords\": \"Carregando...\",
            \"sInfo\": \"Mostrando _START_ at� _END_ de _TOTAL_ registros\",
            \"sInfoEmpty\": \"Mostrando 0 at� 0 de 0 registros\",
            \"sInfoFiltered\": \"(_MAX_ no total)\",
            \"sInfoPostFix\": \"\",
            \"sInfoThousands\": \",\",
            \"sSearch\": \"Procurar\",
            \"fnInfoCallback\": null
        },
    \"aoColumnDefs\": [
        {
            'bVisible': false,
            'bSortable': false,
            'aTargets': [" . implode(', ', array_values($colunasEsconder)) . "]
        }
    ],

    \"iDisplayLength\": 10,
    \"aaSorting\": [],
    \"bProcessing\": true,
    \"bServerSide\": true,
    \"sAjaxSource\": \"../../" . $controllerNome . "/" . $funcaoNome . "\"
});

jQuery('#tabelaPrincipal_wrapper .dataTables_filter input').addClass(\"m-wrap medium\"); // modify table search input
jQuery('#tabelaPrincipal_wrapper .dataTables_length select').addClass(\"m-wrap xsmall\"); // modify table per page dropdown
";
        GUI::adicionaJS($grid);
    }


    function GridEstatico($idNome = "tabelaPrincipal")
    {

        $grid = "oTable = $('#" . $idNome . "').dataTable({
    \"sPaginationType\": \"bootstrap\",
    \"oLanguage\": {
      \"sLengthMenu\": \"_MENU_ registros por p�gina\",
            \"sProcessing\": \"Processando...\",
            \"sZeroRecords\": \"Nenhum registro encontrado\",
            \"sEmptyTable\": \"Sem registros dispon&iacute;veis\",
            \"sLoadingRecords\": \"Carregando...\",
            \"sInfo\": \"Mostrando _START_ at� _END_ de _TOTAL_ registros\",
            \"sInfoEmpty\": \"Mostrando 0 at� 0 de 0 registros\",
            \"sInfoFiltered\": \"(_MAX_ no total)\",
            \"sInfoPostFix\": \"\",
            \"sInfoThousands\": \",\",
            \"sSearch\": \"Procurar\",
            \"fnInfoCallback\": null
        },

    //Salva estado da grid
    \"iDisplayLength\": 10,
    // \"bStateSave\": true,
    \"bProcessing\": true,
     \"aaSorting\": []

});

jQuery('#tabelaPrincipal_wrapper .dataTables_filter input').addClass(\"m-wrap medium\"); // modify table search input
jQuery('#tabelaPrincipal_wrapper .dataTables_length select').addClass(\"m-wrap xsmall\"); // modify table per page dropdown
";
        GUI::adicionaJS($grid);
    }

}