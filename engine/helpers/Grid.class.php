<?php
/**
 * Created by PhpStorm.
 * User: Gustavo
 */

class Grid
{

    static function Gera($vColunas, $aColunasFiltros, $sColunaIndex, $sTabela, $sCondicao = NULL, $aOrdenacao = NULL, $sBotaoExtra)
    {
        $aColunas = array();
        $lColunas = array();

        foreach ($vColunas as $v) {
            if (is_array($v)) {
                array_push($aColunas, array("campo" => $v[0], "tag" => $v[1]));
                array_push($lColunas, $v[0]);
            } else {
                array_push($aColunas, array("campo" => $v, "tag" => ""));
                array_push($lColunas, $v);
            }
        }

        $c = Conexao::getInstance();
        /*
         * Pagina��o
         */
        $sLimite = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimite = "LIMIT " . intval($_GET['iDisplayStart']) . ", " . intval($_GET['iDisplayLength']);
        }

        /*
         * Ordena��o
         */
        $sOrdenacao = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrdenacao = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if (is_array($aColunas[intval($_GET['iSortCol_' . $i])])) {

                }
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrdenacao .= "`" . $aColunas[intval($_GET['iSortCol_' . $i])]["campo"] . "` " . addslashes($_GET['sSortDir_' . $i]) . ", ";
                }
            }

            $sOrdenacao = substr_replace($sOrdenacao, "", -2);
            if ($sOrdenacao == "ORDER BY") {
                $sOrdenacao = "";
            }
        }

        if (isset($aOrdenacao)){
            $sOrdenacao = "ORDER BY  ";
            foreach ($aOrdenacao as $o) {
                $sOrdenacao .= "{$o}, ";
            }
            $sOrdenacao = substr_replace($sOrdenacao, "", -2);
            if ($sOrdenacao == "ORDER BY") {
                $sOrdenacao = "";
            }



        }

        /*
         * Filtrando
         */
        $sWhere = "";
        if (isset($sCondicao) && $sCondicao != "") {
            $sWhere = $sCondicao . " ";
        }

        if ((isset($_GET['sSearch']) && $_GET['sSearch'] != "")) {
            {
                if (isset($sCondicao) && $sCondicao != "") {
                    $sWhere .= " AND (";
                } else {
                    $sWhere = "WHERE (";
                }
                for ($i = 0; $i < count($aColunasFiltros); $i++) {
                    $sWhere .= "" . $aColunasFiltros[$i] . " LIKE '%" . addslashes($_GET['sSearch']) . "%' OR ";
                }
                $sWhere = substr_replace($sWhere, "", -3);
                $sWhere .= ')';
            }
        }

        /* Filtrando coluna individualmente */
        for ($i = 0; $i < count($aColunasFiltros); $i++) {
            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $aColunasFiltros[$i] . "` LIKE '%" . addslashes($_GET['sSearch_' . $i]) . "%' ";
            }
        }

        $sQuery = "SELECT $sColunaIndex FROM $sTabela $sCondicao";
        $c->Consulta($sQuery);
        $iTotal = $c->qtd;

        if ($sWhere != "") {
            $sQuery = "
            SELECT $sColunaIndex
            FROM   $sTabela
            $sWhere
        ";
            $c->Consulta($sQuery);

            $iFilteredTotal = $c->qtd;
        } else {
            $iFilteredTotal = $iTotal;
        }

        $sQuery = "
        SELECT " . str_replace(" , ", " ", implode(", ", $lColunas)) . "
        FROM  $sTabela
        $sWhere
        $sOrdenacao
        $sLimite
    ";
//   echo $sQuery;exit();
        $c->Consulta($sQuery);

        /*
         * Sa�da
         */
        $output = array("sEcho" => intval($_GET['sEcho']), "iTotalRecords" => $iTotal, "iTotalDisplayRecords" => $iFilteredTotal, "aaData" => array());
        while ($c->Resultado()) {
            $row = array();
            for ($i = 0; $i < count($aColunas); $i++) {
                if ($aColunas[$i]["tag"] != "") {
//                    $row[] = str_replace("{0}", mb_convert_encoding($c->linha[$i], "UTF-8"), $aColunas[$i]["tag"]);
                    $linhaAtual = $aColunas[$i]["tag"];
                    if (preg_match_all('/{+(.*?)}/', $aColunas[$i]["tag"], $matches)) {
                        foreach ($matches[1] as $m) {
                            $valor      = intval($m);
                            $linhaAtual = str_replace("{" . $m . "}", mb_convert_encoding($c->linha[$aColunas[$valor]["campo"]], "UTF-8"), $linhaAtual);
                        }
                    }
                    $row[] = $linhaAtual;
                } else
                    $row[] = mb_convert_encoding($c->linha[$i], "UTF-8");
            }
            $linhaBotoes = "";
            foreach ($sBotaoExtra as $botao) {
                if (preg_match_all('/{+(.*?)}/', $botao, $matches)) {
                    foreach ($matches[1] as $m) {
                        $valor = intval($m);
                        $linhaBotoes .= str_replace("{" . $m . "}", mb_convert_encoding($c->linha[$aColunas[$valor]["campo"]], "UTF-8"), $botao);
                    }
                }
            }

            $row[]              = $linhaBotoes;
            $output['aaData'][] = $row;
        }


        return json_encode($output);
    }

    static function Editar($texto = "Editar")
    {
        return "<a href='../Editar/?id={0}' class='btn default btn-xs purple'><i class='fa fa-edit'> " . $texto . "</i></a> ";
    }

    static function Visualizar($texto = "Visualizar")
    {
        return "<a href='../Visualizar/?id={0}' class='btn default btn-xs blue'><i class='fa fa-search'> " . $texto . "</i></a> ";
    }

    static function Remover($texto = "Remover")
    {
        return "<button onclick=\"javascript:Remover('../Deletar/?did={0}')\" class='btn default btn-xs red'><i class='fa fa-trash-o'> " . $texto . "</i></button> ";
    }

    /**
     * @param $link
     * @param $texto
     * @param $cor
     * @param $icone
     * @param $target
     * @return string
     */
    static function Botao($link, $texto, $cor, $icone, $target = "_self")
    {
        return "<a href='" . $link . "' class='btn default btn-xs " . $cor . "' target='" . $target . "'><i class='fa fa-" . $icone . "'> " . $texto . "</i></a>";
    }


}