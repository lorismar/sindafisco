<?php
    /**
     * User: Gustavo
     * Date: 02/11/13
     * Time: 01:01
     */

    class Layout
    {
        function RenderBody()
        {
            return $GLOBALS['_conteudo'];
        }

        function getTitle()
        {
            if(isset($GLOBALS['_titulo'])) {
                return $GLOBALS['_titulo'];
            } else {
                return "";
            }

        }

        function getDescription()
        {
            if(isset($GLOBALS['_descricao'])) {
                return $GLOBALS['_descricao'];
            } else {
                return "";
            }
        }

    }