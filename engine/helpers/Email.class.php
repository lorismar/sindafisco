<?php

    /**
     * Created by PhpStorm.
     * User: user
     */
    class Email
    {


        /**
         * @param $listaPessoas Pessoas[]
         * @param $msg
         *
         * @return string
         */
        public static function EnviaNewsletters($listaPessoas, $msg)
        {

            $to_send = new PHPMailer(TRUE);
            $to_send->IsSMTP();
            $to_send->Host     = EMAIL_HOST;
            $to_send->SMTPAuth = TRUE;
            $to_send->Username = EMAIL_USUARIO;
            $to_send->Password = EMAIL_SENHA;
            $to_send->From     = EMAIL_USUARIO;
            $to_send->Sender   = EMAIL_USUARIO;
            $to_send->FromName = EMAIL_NOME;
            foreach ($listaPessoas as $pessoa) {
                if ($pessoa->getDsEmail() != NULL) {
                    $to_send->AddBCC($pessoa->getDsEmail(), $pessoa->getDsNome());
                }
            }
            $to_send->AddAddress(EMAIL_USUARIO, EMAIL_NOME);
            $to_send->Subject = 'Contato atrav�s do Site';
            $to_send->IsHTML(TRUE);
            $to_send->Body = $msg;
            return $to_send->Send() ? "Enviado Com Sucesso" : "Erro :" . $to_send->ErrorInfo;
        }

        public static function EnviaEmail($nome, $email, $msg)
        {
            $auth = "Ol�, " . $nome . "!<br /><br />
							" . $msg . "
							<br />
							Atenciosamente, <br /> SINDAFISCO-RO.
							";

            $to_send = new PHPMailer(TRUE);
            $to_send->IsSMTP();
            $to_send->Host     = EMAIL_HOST;
            $to_send->SMTPAuth = TRUE;
            $to_send->Username = EMAIL_USUARIO;
            $to_send->Password = EMAIL_SENHA;
            $to_send->From     = EMAIL_USUARIO;
            $to_send->Sender   = EMAIL_USUARIO;
            $to_send->FromName = EMAIL_NOME;
            $to_send->AddAddress($email, $nome);
            $to_send->Subject = 'Contato atrav�s do Site';
            $to_send->IsHTML(TRUE);
            $to_send->Body = $auth;
            $to_send->Send();
        }

        public static function ConfirmacaoContato($nome, $email)
        {

            $auth = "Ol�, " . $nome . "!<br /><br />
							Recebemos sua mensagem, responderemos assim que poss�vel.<br><br>
							<br>a
							Atenciosamente, <br /> SINDAFISCO-RO.
							";

            $to_send = new PHPMailer(TRUE);
            $to_send->IsSMTP();
            $to_send->Host     = EMAIL_HOST;
            $to_send->SMTPAuth = TRUE;
            $to_send->Username = EMAIL_USUARIO;
            $to_send->Password = EMAIL_SENHA;
            $to_send->From     = EMAIL_USUARIO;
            $to_send->Sender   = EMAIL_USUARIO;
            $to_send->FromName = EMAIL_NOME;
            $to_send->AddAddress($email, $nome);
            $to_send->Subject = 'Contato atrav�s do Site';
            $to_send->IsHTML(TRUE);
            $to_send->Body = $auth;
            $to_send->Send();
        }

        public static function RespostaContato($nome, $email, $texto, $pergunta)
        {

            $auth = "Em resposta a mensagem: <br/>.$pergunta. <br />" . $texto . "<br>
							<br>
							Atenciosamente, <br /> SINDAFISCO-RO.
							";

            $to_send = new PHPMailer(TRUE);
            $to_send->IsSMTP();
            $to_send->Host     = EMAIL_HOST;
            $to_send->SMTPAuth = TRUE;
            $to_send->Username = EMAIL_USUARIO;
            $to_send->Password = EMAIL_SENHA;
            $to_send->From     = EMAIL_USUARIO;
            $to_send->Sender   = EMAIL_USUARIO;
            $to_send->FromName = EMAIL_NOME;
            $to_send->AddAddress($email, $nome);
            $to_send->Subject = 'Contato atrav�s do Site';
            $to_send->IsHTML(TRUE);
            $to_send->Body = $auth;
            $to_send->Send();
        }


    }