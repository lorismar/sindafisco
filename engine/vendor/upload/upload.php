<?php

@set_time_limit(0);

include("class.upload.php");

// arquivo, pasta, width, height
function enviarArquivo($arquivo, $pasta, $w = 180, $h = 150)
{
    $error = FALSE;
    // Confere se existe imagem declarada na variavel
    if (!empty($arquivo)) {
        // Instanciamos o objeto Upload
        $handle = new Upload($arquivo);
        // Ent�o verificamos se o arquivo foi carregado corretamente
        if ($handle->uploaded) {
            // Se for banner, verifica extensao..
            if (($pasta == 'banners') || ($pasta == 'videos/thumbs')) {
                // Permitimos array de mime-types, pordefault: check Init
                $handle->allowed = array("image/bmp", "image/gif", "image/jpeg", "image/pjpeg", "image/png", "image/x-png", "application/x-shockwave-flash");
            }

            // Se for colunista, classificado, noticias
            if (($pasta == 'empresas')) {
                // Permitimos array de mime-types, pordefault: check Init
                $handle->allowed = array("image/bmp", "image/gif", "image/jpeg", "image/pjpeg", "image/png", "image/x-png");
            }


            if ($pasta == 'Contabil') {
//                $handle->allowed = array("application/*", "text/*");
            }

            if ($pasta == 'Pessoas') {
                $handle->allowed       = array("image/bmp", "image/gif", "image/jpeg", "image/pjpeg", "image/png", "image/x-png");
                $handle->file_max_size = 800000; // 800kb..
            }
            if ($pasta == 'audios') {
                $handle->allowed       = array("audio/mpeg3", "audio/x-mpeg-3", " audio/mpeg", "audio/x-mpeg", "audio/mp3", "audio/x-mp3", "audio/mpeg3", "audio/x-mpeg3", "audio/mpg", "audio/x-mpg", "audio/x-mpegaudio");
                $handle->file_max_size = 20971520;
            }
            if ($pasta == 'pdf') {
                $handle->allowed = array("application/pdf");
            }

            if (is_numeric($pasta)) {
                // $handle->allowed       = array("image/bmp", "image/gif", "image/jpeg", "image/pjpeg", "image/png", "image/x-png");
                $handle->file_max_size = 5242880; // 1048576 (1mb) * 5
            }

            //if ($pasta != 'banners')  {
            // Definimos md5 para o arquivo
            //$md5filename				= md5(uniqid());
            //$handle->file_new_name_body = $md5filename;
            //}
            // Definimos as configura��es desejadas da imagem maior
            $handle->dir_chmod = 0777;

            // Qualidade
            $handle->jpeg_quality = 100;


            // REDIMENSIONA OU NAO A IMAGEM
            if (($pasta == "videos/thumbs") || ($pasta == 'imagensAgenda') || ($pasta == 'imagensUsuarios') || ($pasta == 'imagensAgendaAdmin')) {

                $handle->image_resize = TRUE;
                $handle->image_x      = $w;
                $handle->image_y      = $h;
            } else {
                $handle->image_resize  = FALSE;
                $handle->image_ratio_y = FALSE;
                $handle->image_ratio_x = FALSE;
            }

            $handle->mime_check     = TRUE;
            $handle->file_safe_name = TRUE;

            if (is_numeric($pasta)) {
                $pasta_destino = "../public/upload/Galerias/" . $pasta . "/";
            } else {
                $pasta_destino = "../public/upload/" . $pasta . "/";

            }


            $handle->Process($pasta_destino);
            // Em caso de sucesso no upload podemos fazer outras a��es como insert em um banco de cados
            if ($handle->processed) {
                // Exibimos a informa��o de sucesso ou qualquer outra a��o de sua escolha
            } else {
                // Em caso de erro listamos o erro abaixo
                $error .= $handle->error;
            }

            if (!$error) {
                // GERA THUMBNAIL
                if (($pasta != 'banners') && ($pasta != 'videos') && ($pasta != 'pdf') && ($pasta != 'imagensColunistas') && ($pasta != 'imagensUsuarios') && ($pasta != 'imagensAgenda') && ($pasta != 'imagensAgendaAdmin') && ($pasta != 'videos/thumbs')) {
                    // Aqui nos definimos nossas configura��es de imagem do thumbs

                    if (empty($w))
                        $w = 200;
                    if (empty($h))
                        $h = 130;

                    //$handle->file_new_name_body = $md5filename;
                    $handle->dir_chmod = 0777;

                    $handle->image_resize = TRUE;
                    $handle->image_x      = $w;
                    $handle->image_y      = $h;

                    $pasta_destinoTb = "../public/upload/" . $pasta . "/thumbs/";
                    if (is_numeric($pasta)) {
                        $pasta_destinoTb = "../public/upload/Galerias/" . $pasta . "/thumbs/";
                    }
                    $handle->Process($pasta_destinoTb);
                }
            }

            // Excluimos os arquivos temporarios
            $handle->Clean();
        } else {
            // Em caso de erro listamos o erro abaixo
            $error .= $handle->error;
        }
    }

    // Aqui somente recupero o nome da imagem caso queira fazer um insert em banco de dados
    $nome_da_imagem = $handle->file_dst_name;


    if ($error)
        return "erro";
    else
        return $nome_da_imagem;
}

?> 