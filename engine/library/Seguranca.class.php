<?php

/**
 * Description of seguranca
 *
 * @author Gustavo
 */
class Seguranca
{

    /**
     * Pega todos os par�metros de GET e POST
     */
    public static function QueryProtect()
    {
        foreach($_POST as $key => &$POST) {
            if(is_array($POST)) {
                foreach($POST as &$p) {
                    $p = self::StringProtect($p);
                }
            } else {
                if(!strstr($key, "tx")) {
                    $POST = self::StringProtect($POST);
                }
                if(strstr($key, "va_")) {
                    $POST = str_replace("R$ ", "", self::StringProtect($POST));
                    $POST = str_replace(".", "", self::StringProtect($POST));
                    $POST = str_replace(",", ".", self::StringProtect($POST));
                }
            }
        }

        foreach($_GET as &$GET) {
            if(is_array($GET)) {
                foreach($GET as &$g) {
                    $g = self::StringProtect($g);
                }
            } else {
                $GET = self::StringProtect($GET);
            }
        }
    }

    /**
     * Aplica seguran�a na STRING.
     *
     * @param $strProtect
     *
     * @return string
     */
    public static function StringProtect($strProtect)
    {

        $strProtect = ((get_magic_quotes_gpc()) ? $strProtect : addslashes($strProtect));

        /*
        http://www.php.net/manual/pt_BR/function.htmlentities.php  - Transforma por ex. � = &ecute  - Funciona em algumas vers�es do PHP.
        */
        //$strProtect = htmlentities($strProtect);

        /*
           http://www.php.net/manual/pt_BR/function.mysql-real-escape-string.php - Esta fun��o ir� escapar os caracteres especiais em unescaped_string, levando em conta o atual conjunto de caracteres da conex�o, assim � seguro coloca-la em mysql_query().
           Somente se For MySQL
        */
        //$strProtect = mysql_real_escape_string($strProtect);

        return $strProtect;
    }
}

?>
