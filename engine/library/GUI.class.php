<?php

/**
 * Description of GUI
 *
 * @author Gustavo
 */
class GUI
{

    function __construct()
    {

        if (!isset($_SERVER['REQUEST_URI'])) {
            $_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'];
            if ($_SERVER['QUERY_STRING']) {
                $_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING'];
            }
        }
        $current_url            = explode("/", $_SERVER['REQUEST_URI']);
        $array_keys             = array_keys($current_url);
        $GLOBALS['urlAtual']    = $current_url[end($array_keys)];
        $GLOBALS['paginaAtual'] = basename($_SERVER['PHP_SELF'], ".php");
        $GLOBALS['gui_erro']    = "";
    }

    public static function dialogoErro($msg)
    {
        return "<script>alert(\"" . $msg . "\"); history.back(1);</script>";
    }

    public static function retornaErro()
    {
        $erro                = isset($GLOBALS['gui_erro']) ? $GLOBALS['gui_erro'] : NULL;
        $GLOBALS['gui_erro'] = "";
        return $erro;
    }

    public static function adicionaJS($js)
    {
        if (isset($GLOBALS['gui_js'])) {
            $GLOBALS['gui_js'] .= $js;
        } else {
            $GLOBALS['gui_js'] = $js;
        }
    }

    public static function retornaJS()
    {
        $js = "";
        // Adicionar JS
        if (isset($GLOBALS['gui_js'])) {
            $js = $GLOBALS['gui_js'];
        }
        unset($GLOBALS['gui_js']);
        return $js;
    }

    public static function msgAlerta($msg)
    {
        $GLOBALS['gui_erro'] .= '<div class="alert alert-warning"> <button class="close" data-dismiss="alert"></button><strong>Aviso!</strong> ' . $msg . '</div>';
    }

    public static function msgInformacao($msg)
    {
        $GLOBALS['gui_erro'] .= '<div class="alert alert-info"> <button class="close" data-dismiss="alert"></button><strong>Informação!</strong> ' . $msg . '</div>';
    }

    public static function msgSucesso($msg)
    {
        $GLOBALS['gui_erro'] .= '<div class="alert alert-success"> <button class="close" data-dismiss="alert"></button><strong>Sucesso!</strong> ' . $msg . '</div>';
    }

    public static function msgErro($msg)
    {
        $GLOBALS['gui_erro'] .= '<div class="alert alert-error"> <button class="close" data-dismiss="alert"></button><strong>Erro!</strong> ' . $msg . '</div>';
    }

    public static function msgDialogoErro($msg)
    {
        $GLOBALS['gui_erro'] = "<script>alert(\"" . $msg . "\"); history.back(1);</script>";
    }

    public static function msgDialogoRedirect($msg, $url)
    {
        $GLOBALS['gui_erro'] = "<script>if (confirm('" . $msg . "')){window.location.href='" . $url . "';}</script>";
    }

    public static function msg($msg)
    {
        $GLOBALS['gui_erro'] = $msg;
    }

    public static function tabAtivo($nomeTab, $formTab)
    {

        $GLOBALS['gui_js'] .= "\n$(\"#{$nomeTab}\").addClass(\"active\");";
        $GLOBALS['gui_js'] .= "\n$(\"#{$formTab}\").addClass(\"tab-pane active\");";
    }

    public static function selecionarMenuAtivo($elementId, $css)
    {
        $GLOBALS['gui_js'] .= "\n$(\"#{$elementId}\").addClass(\"{$css}\");";
        $GLOBALS['gui_js'] .= "\n$(\"#{$elementId}\").parent().parent().addClass(\"{$css}\");";

    }
}

?>
