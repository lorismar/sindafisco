<?php
    /**
     * Created by PhpStorm.
     * User: user
     */
    $_now = new DateTime();
    $_now->setTimezone(new DateTimeZone("America/Porto_Velho"));
    $pageURL = 'http';
    if (isset($_SERVER["HTTPS"])) {
        if ($_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }

    define('DATAHORA', $_now->format('d/m/Y H:i'));
    define('DATAHORA2', $_now->format('d-m-Y-H-i'));
    define('DATA', $_now->format('d/m/Y'));
    define('DATE', $_now->format('d/m/Y'));
    define('DATETIME', $_now->format('d/m/Y H:i'));
    define('ANO', $_now->format('Y'));
    define('CURRENT_URL', $pageURL);