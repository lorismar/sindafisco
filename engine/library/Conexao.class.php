<?php



/**
 * Class Conexao
 */
class Conexao extends PDO
{

    // Classe de Conex�o
    /**
     * Cria uma inst�ncia
     * @var object Inst�cia
     */
    private static $instancia;
    /**
     * ResultaSet
     * @var object ResultSet
     */
    var $res;
    /**
     * Linha (row) de uma consulta
     * @var object Registro de 1 linha
     */
    var $linha;
    /**
     * Quantidade de registros retornados
     * @var int Quantidade de Registros
     */
    var $qtd;
    /**
     * Statment da Consulta
     * @var object Statement
     */
    var $stmt;
    /**
     * Quando utilizado o INSERT ele retorna o valor da PK cadastrada no banco.
     * @var int Retorna �ltima Pk cadastrada no banco.
     */
    var $last_id;
    /**
     * Lista de Parametros para o Prepared Statment
     * @var array
     */
    private $parametros = array();

    /**
     * Conex�o do Banco
     *
     * @param $conexao
     */
//    public function Conexao($dsn, $username = "", $passwd = "")
//    {
//        // O construtro abaixo � o do PDO
//        parent::__construct($dsn, $username, $passwd);
//    }

    public function __construct($dsn, $username = "", $passwd = "")
    {
        parent::__construct($dsn, $username, $passwd);
    }

    /**
     *
     */
    private function __clone()
    {
    }

    /**
     * Ao pegar a inst�ncia, ele retorna a conex�o
     * @return Conexao
     */
    public static function getInstance()
    {
        // Se o a instancia n�o existe eu fa�o uma
        if (!isset(self::$instancia)) {
            try {

                self::$instancia = new Conexao("mysql:host=" . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
                self::$instancia->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$instancia->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_BOTH);
            } catch (Exception $e) {
                echo 'Erro ao conectar';
                echo $e->getMessage();
                exit();
            }
        }
        // Se j� existe instancia na mem�ria eu retorno ela
        return self::$instancia;
    }

    /**
     * Adiciona parametros para a prepared statment, ir� substituir o ? em ordem.
     *
     * @param $parametro
     */
    public function adicionaParametros($parametro)
    {
        array_push($this->parametros, $parametro);
    }

    /**
     * Prepara o Statement
     *
     * @param string $sql
     */
    public function preparaStatement($sql = "")
    {
        if ($sql == "") { // Se nao foi passada nenhuma SQL,
            $this->res = 0; // Sem resultados
            $this->qtd = 0; // Sem linhas
        }
        $this->parametros = array();
        $this->stmt       = self::prepare($sql);
    }

    /**
     * Executa a Statment
     * @return bool
     */
    public function executaStatement()
    {
        try {
            $x = 0;
            foreach ($this->parametros as $p) {
                $x++;
                $this->stmt->bindValue($x, $p);
            }

            $this->stmt->execute();
            $result        = $this->stmt;
            $this->last_id = self::lastInsertId();

//                if(strpos(strtoupper($result->queryString), 'INSERT') !== FALSE || strpos(strtoupper($result->queryString), 'UPDATE') !== FALSE || strpos(strtoupper($result->queryString), 'DELETE') !== FALSE) {
//                    $query = strtoupper($result->queryString);
//                    $query = $this::interpolateQuery($query, $this->parametros);
//                    $query = ((get_magic_quotes_gpc()) ? $query : addslashes($query));
//                    //                                                    echo $query;
//                    //                                                    echo "<br />";
//                    //  $c     = Conexao::getInstance();
//                    //  $GLOBALS['usuario'] = unserialize($_SESSION['usuario']);
//                    //   $sql                = "INSERT INTO LOGS (tx_Consulta, dh_Consulta, ds_Nome) VALUES ('" . $query . "', now(),'" . $GLOBALS['usuario']->getDsNome() . "')";
//                    //$c->Manipula($sql);
//
//                }
            if ($result) {
                $this->res = $result;
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            GUI::msgSucesso($e);
//            echo $e;
        }
    }

    /**
     * Executa a consulta SQL
     *
     * @param string $sql
     */
    public function Consulta($sql = "")
    {
        if ($sql == "") { // Se nao foi passada nenhuma SQL,
            $this->res = 0; // Sem resultados
            $this->qtd = 0; // Sem linhas
        }
        $result    = self::query($sql);

        $this->qtd = $result->rowCount();
        if ($result) {
            $this->res = $result;
        } else {
            $this->res = FALSE;
        }
    }

    /**
     * Executa uma DDL ou DML
     *
     * @param string $sql
     *
     * @return bool
     */
    public function Manipula($sql = "")
    {
        try {
            //Executa uma query de DDL ou DML (manipulacao de dados)
            if (self::exec($sql)) {
                return TRUE; // Se OK, retorna TRUE.
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
//            echo $e->getMessage();
            GUI::msgSucesso($e);
            return FALSE;
        }
    }

    /**
     * Resultado de uma consulta
     * @return bool
     */
    public function Resultado()
    {
        // Busca os dados de uma linha do resultado e
        // posiciona o ponteiro na proxima.
        if ($this->res) {
            $this->linha = $this->res->fetch();
        }
        if (!$this->linha) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Replaces any parameter placeholders in a query with the value of that
     * parameter. Useful for debugging. Assumes anonymous parameters from
     * $params are are in the same order as specified in $query
     *
     * @param string $query The sql query with parameter placeholders
     * @param array $params The array of substitution parameters
     *
     * @return string The interpolated query
     */
    public function interpolateQuery($query, $params)
    {
        $keys   = array();
        $values = $params;

        # build a regular expression for each parameter
        foreach ($params as $key => $value) {
            if (is_string($key)) {
                $keys[] = '/:' . $key . '/';
            } else {
                $keys[] = '/[?]/';
            }

            if (is_array($value))
                $values[$key] = implode(',', $value);

            if (is_null($value))
                $values[$key] = 'NULL';
        }
        // Walk the array to see if we can add single-quotes to strings
        array_walk($values, create_function('&$v, $k', 'if (!is_numeric($v) && $v!="NULL") $v = "\'".$v."\'";'));

        $query = preg_replace($keys, $values, $query, 1, $count);

        return $query;
    }
}

?>