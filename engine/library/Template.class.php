<?php
    /**
     * User: Iuri Gustavo
     * Date: 27/07/13
     * Time: 09:08
     */

    class Template
    {
        protected $variaveis = array();
        protected $_controller;
        protected $_acao;
        protected $_area;
        protected $_pagina;

        function __construct($area, $controller, $acao)
        {
            $this->_area       = $area;
            $this->_controller = $controller;
            $this->_acao       = $acao;
        }

        function set($nome, $valor)
        {
            $this->variaveis[$nome] = $valor;
        }

        function renderizar()
        {
            if (isset($GLOBALS['_pagina'])) {
                $this->_pagina = $GLOBALS['_pagina'];
            } else {
                $this->_pagina = "Index";
            }

            extract($this->variaveis);
            $Html = new Html();
            if (file_exists(ROOT . DS . 'application' . DS . $this->_area . DS . 'Views' . DS . $this->_controller . DS . $this->_pagina . '.php')) {
                ob_start();
                include (ROOT . DS . 'application' . DS . $this->_area . DS . 'Views' . DS . $this->_controller . DS . $this->_pagina . '.php');
                $GLOBALS['_conteudo'] = ob_get_contents();
                echo $GLOBALS['_conteudo'];
                ob_end_clean();
                if (isset($GLOBALS['_layout'])) {
                    include (ROOT . DS . 'application' . DS . $this->_area . DS . 'Views' . DS . '_Layout' . DS . $GLOBALS['_layout'] . '.php');
                } else {
                    echo $GLOBALS['_conteudo'];
                }
            } else {

            }
        }
    }