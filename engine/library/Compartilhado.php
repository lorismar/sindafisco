<?php
/**
 * User: Iuri Gustavo
 * Date: 27/07/13
 * Time: 08:45
 */

session_start();

// session_save_path(dirname($_SERVER['DOCUMENT_ROOT']).'/public_html/tmp');


/** Checa se o ambiente � de desenvolvimento e mostra os erros **/
function setRelatoriosErrors()
{
    if (DEVELOPMENT_ENVIRONMENT == TRUE) {
        error_reporting(E_ALL);
        ini_set('display_errors', 'On');
    } else {
        error_reporting(E_ALL);
        ini_set('display_errors', 'Off');
        //  ini_set('log_errors', 'On');
        //  ini_set('error_log', ROOT . DS . 'tmp' . DS . 'logs' . DS . 'error.log');
    }
}

function stripSlashesDeep($value)
{
    $value = is_array($value) ? array_map('stripSlashesDeep', $value) : stripslashes($value);
    return $value;
}

function removeMagicQuotes()
{
    if (get_magic_quotes_gpc()) {
        $_GET    = stripSlashesDeep($_GET);
        $_POST   = stripSlashesDeep($_POST);
        $_COOKIE = stripSlashesDeep($_COOKIE);
    }
}

function unregisterGlobals()
{
    if (ini_get('register_globals')) {
        $array = array('_SESSION', '_POST', '_GET', '_COOKIE', '_REQUEST', '_SERVER', '_ENV', '_FILES');
        foreach ($array as $value) {
            foreach ($GLOBALS[$value] as $key => $var) {
                if ($var === $GLOBALS[$key]) {
                    unset($GLOBALS[$key]);
                }
            }
        }
    }
}


/** ONDE A M�GICA ACONTECE :D **/

function callHook()
{

    global $url;
    global $default;

    $queryString = array();

    if (!isset($url)) {
        header("location: Site/Principal/");
        echo("<script>location.href='http://sindafisco.org.br/antigo/';</script>");
        $area       = $default['area'];
        $controller = $default['controller'];
        $acao       = $default['acao'];
    } else {
        $urlArray   = explode("/", $url);
        $area       = $urlArray[0];
        $controller = $urlArray[1];
        array_shift($urlArray);
        if (isset($urlArray[2])) {
            $acao = $urlArray[2];
            array_shift($urlArray);
        } else {
            $acao = 'Index'; // Default Action
        }
        $queryString = $urlArray;
    }
    if ($acao == NULL) {
        $acao = "Index";
    }

    //AREA DE TESTES
    if ($area == "test" || $area == "_Engine") {
        include(ROOT . DS . $area . DS . $controller);
    } else {
        $explode = explode("/", $url);
        if ($area == "admin") {
            $nivel = NIVEL_SITE;
        } else {
            $nivel = NIVEL_SITE - 1;
            $area  = "site";
        }

        if ($explode[$nivel] != NULL) {
            $acao = $explode[$nivel];
        }

        if ($explode[$nivel - 1] != NULL) {
            $controller = $explode[$nivel - 1];
        }


        include(ROOT . DS . 'application' . DS . $area . DS . "Controllers" . DS . "_Abstract" . DS . $area . "_Controller.class.php");
        $controllerNome = ucfirst($controller) . 'Controller';
        foreach (glob(ROOT . DS . "application/" . $area . "/Controllers/*") as $filename) {
            foreach (glob($filename . "/*") as $control) {
                $expd = explode("/", $control);
                if ($controllerNome . ".php" == $expd[(count($expd) - 1)]) {
                    if (file_exists(ROOT . DS . 'application' . DS . $area . DS . 'Controllers' . DS . $expd[(count($expd) - 2)] . DS . $controllerNome . ".php")) {
                        include(ROOT . DS . 'application' . DS . $area . DS . 'Controllers' . DS . $expd[(count($expd) - 2)] . DS . $controllerNome . ".php");
                        $controller = $expd[(count($expd) - NIVEL_SITE)];
                    }
                }
            }
        }

        define('_AREA', $area);
        define('_CONTROLLER', $controller);
        define('_ACTION', $acao);
        $dispatch = new $controllerNome($area, $controller, basename($acao, ".php"));
        if ((int)method_exists($controllerNome, $acao)) {
            if (!strstr($acao, "Grid")) {
                if ((int)method_exists($controllerNome, "beforeAction"))
                    call_user_func(array($dispatch, "beforeAction"));
            }
            $GLOBALS['_pagina'] = call_user_func(array($dispatch, $acao));
            if (!strstr($acao, "Grid")) {
                if ((int)method_exists($controllerNome, "afterAction"))
                    call_user_func(array($dispatch, "afterAction"));
            }
        } else {
            if (count($explode) == $nivel + 2) {
                $_GET['id'] = $explode[$nivel];
            }
            if ((int)method_exists($controllerNome, "Escape")) {
                call_user_func(array($dispatch, "beforeAction"));
                $GLOBALS['_pagina'] = call_user_func(array($dispatch, "Escape"));
                call_user_func(array($dispatch, "afterAction"));
            } else {
                /* Erro 404*/
                echo "Conteudo n�o existente";
            }

        }
    }
}


include(ROOT . DS . 'engine' . DS . 'library' . DS . 'Geral.php');
setRelatoriosErrors();
removeMagicQuotes();
//unregisterGlobals();
callHook();