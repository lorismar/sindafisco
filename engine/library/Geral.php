<?php
/**
 * User: Gustavo
 * Date: 14/01/13
 * Time: 20:54
 */

error_reporting(E_ALL);
ini_set('display_errors', 'On');

// Importa Helpers
foreach (glob("../engine/helpers/*.php") as $filename) {
    include $filename;
}


include("../config/Parametros.php");
include("Routing.php");
include("Template.class.php");
include("GUI.class.php");
include("Seguranca.class.php");
include("Conexao.class.php");
include("Variaveis.php");
include("../engine/vendor/mail/class.phpmailer.php");

// Ativa Seguranša
Seguranca::QueryProtect();

// Componente de Interface
$GUI = new GUI();

//// Importa classes MODEL
//foreach (glob("../datasource/interfaces/*.php") as $filename) {
//    include $filename;
//}


// Importa datasource
foreach (glob("../datasource/dao/*.php") as $filename) {
    include $filename;
}

// Importa classes MODEL
foreach (glob("../datasource/models/*.php") as $filename) {
    include $filename;
}

// Importa classe de UPLOAD de Arquivos
include("../engine/vendor/upload/upload.php");
