<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblFaleConosco
{

    private $_id_FaleConosco;
    private $_ds_Assunto;
    private $_ds_Nome;
    private $_ds_Email;
    private $_tx_Texto;
    private $_tx_Resposta;
    private $_is_Lido;
    private $_dh_Postagem;
    private $_dh_Resposta;
    private $_id_Usuarios_fk;

    private $_Usuarios;

    /**
     * @param int|null $id_FaleConosco
     */
    function __construct($id_FaleConosco)
    {
        if (!empty($id_FaleConosco)) {
            $this->_id_FaleConosco = $id_FaleConosco;
        }
    }

    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_FaleConosco, ds_Assunto, ds_Nome, ds_Email, tx_Texto, tx_Resposta, is_Lido, DATE_FORMAT(dh_Postagem, '%d/%m/%Y %H:%i') dh_Postagem, DATE_FORMAT(dh_Resposta, '%d/%m/%Y %H:%i') dh_Resposta, id_Usuarios_fk FROM FaleConosco WHERE id_FaleConosco = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdFaleConosco());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdFaleConosco($c->linha['id_FaleConosco']);
            $this->setDsAssunto($c->linha['ds_Assunto']);
            $this->setDsNome($c->linha['ds_Nome']);
            $this->setDsEmail($c->linha['ds_Email']);
            $this->setTxTexto($c->linha['tx_Texto']);
            $this->setTxResposta($c->linha['tx_Resposta']);
            $this->setIsLido($c->linha['is_Lido']);
            $this->setDhPostagem($c->linha['dh_Postagem']);
            $this->setDhResposta($c->linha['dh_Resposta']);
            $this->setIdUsuariosFk($c->linha['id_Usuarios_fk']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula
     * @return array
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_FaleConosco, ds_Assunto, ds_Nome, ds_Email, tx_Texto, tx_Resposta, is_Lido, DATE_FORMAT(dh_Postagem, '%d/%m/%Y %H:%i') dh_Postagem, DATE_FORMAT(dh_Resposta, '%d/%m/%Y %H:%i') dh_Resposta, id_Usuarios_fk FROM FaleConosco $where ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new FaleConosco(NULL);
            $tbl->setIdFaleConosco($c->linha['id_FaleConosco']);
            $tbl->setDsAssunto($c->linha['ds_Assunto']);
            $tbl->setDsNome($c->linha['ds_Nome']);
            $tbl->setDsEmail($c->linha['ds_Email']);
            $tbl->setTxTexto($c->linha['tx_Texto']);
            $tbl->setTxResposta($c->linha['tx_Resposta']);
            $tbl->setIsLido($c->linha['is_Lido']);
            $tbl->setDhPostagem($c->linha['dh_Postagem']);
            $tbl->setDhResposta($c->linha['dh_Resposta']);
            $tbl->setIdUsuariosFk($c->linha['id_Usuarios_fk']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool
     */
    public function Salvar()
    {
        if ($this->getIdFaleConosco() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO FaleConosco (id_FaleConosco, ds_Assunto, ds_Nome, ds_Email, tx_Texto, tx_Resposta, is_Lido, dh_Postagem, dh_Resposta, id_Usuarios_fk ) VALUES (?, ?, ?, ?, ?, ?, ?, STR_TO_DATE(?,'%d/%m/%Y %H:%i'), STR_TO_DATE(?,'%d/%m/%Y %H:%i'), ?)  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdFaleConosco());
        $c->adicionaParametros($this->getDsAssunto());
        $c->adicionaParametros($this->getDsNome());
        $c->adicionaParametros($this->getDsEmail());
        $c->adicionaParametros($this->getTxTexto());
        $c->adicionaParametros($this->getTxResposta());
        $c->adicionaParametros($this->getIsLido());
        $c->adicionaParametros($this->getDhPostagem());
        $c->adicionaParametros($this->getDhResposta());
        $c->adicionaParametros($this->getIdUsuariosFk());
        if ($c->executaStatement()) {
            $this->setIdFaleConosco($c->last_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Alterar()
    {
        $c   = Conexao::getInstance();
        $sql = "UPDATE FaleConosco SET ds_Assunto = ?, ds_Nome = ?, ds_Email = ?, tx_Texto = ?, tx_Resposta = ?, is_Lido = ?, dh_Postagem = STR_TO_DATE(?,'%d/%m/%Y %H:%i'), dh_Resposta = STR_TO_DATE(?,'%d/%m/%Y %H:%i'), id_Usuarios_fk = ? WHERE id_FaleConosco = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsAssunto());
        $c->adicionaParametros($this->getDsNome());
        $c->adicionaParametros($this->getDsEmail());
        $c->adicionaParametros($this->getTxTexto());
        $c->adicionaParametros($this->getTxResposta());
        $c->adicionaParametros($this->getIsLido());
        $c->adicionaParametros($this->getDhPostagem());
        $c->adicionaParametros($this->getDhResposta());
        $c->adicionaParametros($this->getIdUsuariosFk());
        // PK
        $c->adicionaParametros($this->getIdFaleConosco());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_FaleConosco
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu
     */
    public static function Remover($id_FaleConosco)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM FaleConosco WHERE id_FaleConosco = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_FaleConosco);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }


    /**
     * @param int|null $id_FaleConosco
     */
    public function setIdFaleConosco($id_FaleConosco)
    {
        $this->_id_FaleConosco = $id_FaleConosco;
    }

    /**
     * @return int|null
     */
    public function getIdFaleConosco()
    {
        return $this->_id_FaleConosco;
    }

    /**
     * @param string|null $ds_Assunto
     */
    public function setDsAssunto($ds_Assunto)
    {
        $this->_ds_Assunto = $ds_Assunto;
    }

    /**
     * @return string|null
     */
    public function getDsAssunto()
    {
        return $this->_ds_Assunto;
    }

    /**
     * @param string|null $ds_Nome
     */
    public function setDsNome($ds_Nome)
    {
        $this->_ds_Nome = $ds_Nome;
    }

    /**
     * @return string|null
     */
    public function getDsNome()
    {
        return $this->_ds_Nome;
    }

    /**
     * @param string|null $ds_Email
     */
    public function setDsEmail($ds_Email)
    {
        $this->_ds_Email = $ds_Email;
    }

    /**
     * @return string|null
     */
    public function getDsEmail()
    {
        return $this->_ds_Email;
    }

    /**
     * @param string|null $tx_Texto
     */
    public function setTxTexto($tx_Texto)
    {
        $this->_tx_Texto = $tx_Texto;
    }

    /**
     * @return string|null
     */
    public function getTxTexto()
    {
        return $this->_tx_Texto;
    }

    /**
     * @param string|null $tx_Resposta
     */
    public function setTxResposta($tx_Resposta)
    {
        $this->_tx_Resposta = $tx_Resposta;
    }

    /**
     * @return string|null
     */
    public function getTxResposta()
    {
        return $this->_tx_Resposta;
    }

    /**
     * @param bool|null $is_Lido
     */
    public function setIsLido($is_Lido)
    {
        $this->_is_Lido = $is_Lido;
    }

    /**
     * @return bool|null
     */
    public function getIsLido()
    {
        return $this->_is_Lido;
    }

    /**
     * @param string|null $dh_Postagem
     */
    public function setDhPostagem($dh_Postagem)
    {
        $this->_dh_Postagem = $dh_Postagem;
    }

    /**
     * @return string|null
     */
    public function getDhPostagem()
    {
        return $this->_dh_Postagem;
    }

    /**
     * @param string|null $dh_Resposta
     */
    public function setDhResposta($dh_Resposta)
    {
        $this->_dh_Resposta = $dh_Resposta;
    }

    /**
     * @return string|null
     */
    public function getDhResposta()
    {
        return $this->_dh_Resposta;
    }

    /**
     * @param int|null $id_Usuarios_fk
     */
    public function setIdUsuariosFk($id_Usuarios_fk)
    {
        $this->_id_Usuarios_fk = $id_Usuarios_fk;
        $this->_Usuarios = new Usuarios($id_Usuarios_fk);
    }

    /**
     * @return int|null
     */
    public function getIdUsuariosFk()
    {
        return $this->_id_Usuarios_fk;
    }

    /**
     * @param Usuarios $Usuarios
     */
    public function setUsuarios($Usuarios)
    {
        $this->_Usuarios = $Usuarios;
    }

    /**
     * @return Usuarios
     */
    public function getUsuarios()
    {
        return $this->_Usuarios;
    }

}