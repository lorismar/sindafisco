<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblMenus
{

    private $_id_Menus;
    private $_ds_Titulo;
    private $_ds_Url;
    private $_nu_Posicao;
    private $_is_Ativo;
    private $_id_Conteudos_fk;
    private $_id_Menus_fk;

    private $_Conteudos;
    private $_Menus;

    /**
     * @param int|null $id_Menus
     */
    function __construct($id_Menus)
    {
        if (!empty($id_Menus)) {
            $this->_id_Menus = $id_Menus;
        }
    }

    /**
     * @param int|null $id_Menus
     */
    public function setIdMenus($id_Menus)
    {
        $this->_id_Menus = $id_Menus;
    }

    /**
     * @return int|null
     */
    public function getIdMenus()
    {
        return $this->_id_Menus;
    }

    /**
     * @param string|null $ds_Titulo
     */
    public function setDsTitulo($ds_Titulo)
    {
        $this->_ds_Titulo = $ds_Titulo;
    }

    /**
     * @return string|null
     */
    public function getDsTitulo()
    {
        return $this->_ds_Titulo;
    }

    /**
     * @param string|null $ds_Url
     */
    public function setDsUrl($ds_Url)
    {
        $this->_ds_Url = $ds_Url;
    }

    /**
     * @return string|null
     */
    public function getDsUrl()
    {
        return $this->_ds_Url;
    }

    /**
     * @param int|null $nu_Posicao
     */
    public function setNuPosicao($nu_Posicao)
    {
        $this->_nu_Posicao = $nu_Posicao;
    }

    /**
     * @return int|null
     */
    public function getNuPosicao()
    {
        return $this->_nu_Posicao;
    }

    /**
     * @param bool|null $is_Ativo
     */
    public function setIsAtivo($is_Ativo)
    {
        $this->_is_Ativo = $is_Ativo;
    }

    /**
     * @return bool|null
     */
    public function getIsAtivo()
    {
        return $this->_is_Ativo;
    }

    /**
     * @param int|null $id_Conteudos_fk
     */
    public function setIdConteudosFk($id_Conteudos_fk)
    {
        $this->_id_Conteudos_fk = $id_Conteudos_fk;
        $this->_Conteudos = new Conteudos($id_Conteudos_fk);
    }

    /**
     * @return int|null
     */
    public function getIdConteudosFk()
    {
        return $this->_id_Conteudos_fk;
    }

    /**
     * @param int|null $id_Menus_fk
     */
    public function setIdMenusFk($id_Menus_fk)
    {
        $this->_id_Menus_fk = $id_Menus_fk;
        $this->_Menus = new Menus($id_Menus_fk);
    }

    /**
     * @return int|null
     */
    public function getIdMenusFk()
    {
        return $this->_id_Menus_fk;
    }

    /**
     * @param Conteudos $Conteudos
     */
    public function setConteudos($Conteudos)
    {
        $this->_Conteudos = $Conteudos;
    }

    /**
     * @return Conteudos
     */
    public function getConteudos()
    {
        return $this->_Conteudos;
    }

    /**
     * @param Menus $Menus
     */
    public function setMenus($Menus)
    {
        $this->_Menus = $Menus;
    }

    /**
     * @return Menus
     */
    public function getMenus()
    {
        return $this->_Menus;
    }

}