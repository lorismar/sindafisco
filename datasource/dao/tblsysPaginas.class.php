<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblsysPaginas
{

    private $_id_sysPaginas;
    private $_ds_Controller;
    private $_ds_Descricao;
    private $_ds_Icone;
    private $_id_sysPaginas_fk;
    private $_id_Modulos_fk;

    private $_sysPaginas;
    private $_Modulos;

    /**
     * @param int|null $id_sysPaginas
     */
    function __construct($id_sysPaginas)
    {
        if (!empty($id_sysPaginas)) {
            $this->_id_sysPaginas = $id_sysPaginas;
        }
    }

    /**
     * @param int|null $id_sysPaginas
     */
    public function setIdSysPaginas($id_sysPaginas)
    {
        $this->_id_sysPaginas = $id_sysPaginas;
    }

    /**
     * @return int|null
     */
    public function getIdSysPaginas()
    {
        return $this->_id_sysPaginas;
    }

    /**
     * @param string|null $ds_Controller
     */
    public function setDsController($ds_Controller)
    {
        $this->_ds_Controller = $ds_Controller;
    }

    /**
     * @return string|null
     */
    public function getDsController()
    {
        return $this->_ds_Controller;
    }

    /**
     * @param string|null $ds_Descricao
     */
    public function setDsDescricao($ds_Descricao)
    {
        $this->_ds_Descricao = $ds_Descricao;
    }

    /**
     * @return string|null
     */
    public function getDsDescricao()
    {
        return $this->_ds_Descricao;
    }

    /**
     * @param string|null $ds_Icone
     */
    public function setDsIcone($ds_Icone)
    {
        $this->_ds_Icone = $ds_Icone;
    }

    /**
     * @return string|null
     */
    public function getDsIcone()
    {
        return $this->_ds_Icone;
    }

    /**
     * @param int|null $id_sysPaginas_fk
     */
    public function setIdSysPaginasFk($id_sysPaginas_fk)
    {
        $this->_id_sysPaginas_fk = $id_sysPaginas_fk;
        $this->_sysPaginas = new sysPaginas($id_sysPaginas_fk);
    }

    /**
     * @return int|null
     */
    public function getIdSysPaginasFk()
    {
        return $this->_id_sysPaginas_fk;
    }

    /**
     * @param int|null $id_Modulos_fk
     */
    public function setIdModulosFk($id_Modulos_fk)
    {
        $this->_id_Modulos_fk = $id_Modulos_fk;
        $this->_Modulos = new Modulos($id_Modulos_fk);
    }

    /**
     * @return int|null
     */
    public function getIdModulosFk()
    {
        return $this->_id_Modulos_fk;
    }

    /**
     * @param sysPaginas $sysPaginas
     */
    public function setSysPaginas($sysPaginas)
    {
        $this->_sysPaginas = $sysPaginas;
    }

    /**
     * @return sysPaginas
     */
    public function getSysPaginas()
    {
        return $this->_sysPaginas;
    }

    /**
     * @param Modulos $Modulos
     */
    public function setModulos($Modulos)
    {
        $this->_Modulos = $Modulos;
    }

    /**
     * @return Modulos
     */
    public function getModulos()
    {
        return $this->_Modulos;
    }

}