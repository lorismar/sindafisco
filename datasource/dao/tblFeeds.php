<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblFeeds
{

    private $_id_Feeds;
    private $_ds_Titulo;
    private $_ds_Icone;
    private $_ds_UrlLink;
    private $_dh_Cadastro;
    private $_id_Usuarios_fk;

    private $_Usuarios;

    /**
     * @param int|null $id_Feeds
     */
    function __construct($id_Feeds)
    {
        if (!empty($id_Feeds)) {
            $this->_id_Feeds = $id_Feeds;
        }
    }

    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Feeds, ds_Titulo, ds_Icone, ds_UrlLink, id_Usuarios_fk FROM Feeds WHERE id_Feeds = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdFeeds());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdFeeds($c->linha['id_Feeds']);
            $this->setDsTitulo($c->linha['ds_Titulo']);
            $this->setDsIcone($c->linha['ds_Icone']);
            $this->setDsUrlLink($c->linha['ds_UrlLink']);
            $this->setDhCadastro($c->linha['dh_Cadastro']);
            $this->setIdUsuariosFk($c->linha['id_Usuarios_fk']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula
     * @return array
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Feeds, ds_Titulo, ds_Icone, ds_UrlLink,DATE_FORMAT(dh_Cadastro, '%d/%m/%Y %H:%i') dh_Cadastro, id_Usuarios_fk FROM Feeds $where ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Feeds(NULL);
            $tbl->setIdFeeds($c->linha['id_Feeds']);
            $tbl->setDsTitulo($c->linha['ds_Titulo']);
            $tbl->setDsIcone($c->linha['ds_Icone']);
            $tbl->setDsUrlLink($c->linha['ds_UrlLink']);
            $tbl->setDhCadastro($c->linha['dh_Cadastro']);
            $tbl->setIdUsuariosFk($c->linha['id_Usuarios_fk']);

            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool
     */
    public function Salvar()
    {
        if ($this->getIdFeeds() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO Feeds (ds_Titulo, ds_Icone, ds_UrlLink, dh_Cadastro, id_Usuarios_fk ) VALUES (?, ?, ?,now(), ?)  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsTitulo());
        $c->adicionaParametros($this->getDsIcone());
        $c->adicionaParametros($this->getDsUrlLink());
        $c->adicionaParametros($this->getIdUsuariosFk());
        if ($c->executaStatement()) {
            $this->setIdFeeds($c->last_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Alterar()
    {
        $c   = Conexao::getInstance();
        $sql = "UPDATE Feeds SET ds_Titulo =?, ds_Icone =?, ds_UrlLink =?, dh_Cadastro=now(), id_Usuarios_fk = ? WHERE id_Feeds = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsTitulo());
        $c->adicionaParametros($this->getDsIcone());
        $c->adicionaParametros($this->getDsUrlLink());
        $c->adicionaParametros($this->getIdUsuariosFk());
        // PK
        $c->adicionaParametros($this->getIdFeeds());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Feeds
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu
     */
    public static function Remover($id_Feeds)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Feeds WHERE id_Feeds = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Feeds);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @param mixed $ds_Icone
     */
    public function setDsIcone($ds_Icone)
    {
        $this->_ds_Icone = $ds_Icone;
    }

    /**
     * @return mixed
     */
    public function getDsIcone()
    {
        return $this->_ds_Icone;
    }

    /**
     * @param mixed $ds_Titulo
     */
    public function setDsTitulo($ds_Titulo)
    {
        $this->_ds_Titulo = $ds_Titulo;
    }

    /**
     * @return mixed
     */
    public function getDsTitulo()
    {
        return $this->_ds_Titulo;
    }

    /**
     * @param mixed $ds_UrlLink
     */
    public function setDsUrlLink($ds_UrlLink)
    {
        $this->_ds_UrlLink = $ds_UrlLink;
    }

    /**
     * @return mixed
     */
    public function getDsUrlLink()
    {
        return $this->_ds_UrlLink;
    }

    /**
     * @param int|null $id_Feeds
     */
    public function setIdFeeds($id_Feeds)
    {
        $this->_id_Feeds = $id_Feeds;
    }

    /**
     * @return int|null
     */
    public function getIdFeeds()
    {
        return $this->_id_Feeds;
    }

    /**
     * @param mixed $id_Usuarios_fk
     */
    public function setIdUsuariosFk($id_Usuarios_fk)
    {
        $this->_id_Usuarios_fk = $id_Usuarios_fk;
        $this->_Usuarios       = new Usuarios($id_Usuarios_fk);
    }

    /**
     * @return mixed
     */
    public function getIdUsuariosFk()
    {
        return $this->_id_Usuarios_fk;
    }

    /**
     * @param mixed $Usuarios
     */
    public function setUsuarios($Usuarios)
    {
        $this->_Usuarios = $Usuarios;
    }

    /**
     * @return Usuarios
     */
    public function getUsuarios()
    {
        return $this->_Usuarios;
    }

    /**
     * @param mixed $dh_Cadastro
     */
    public function setDhCadastro($dh_Cadastro)
    {
        $this->_dh_Cadastro = $dh_Cadastro;
    }

    /**
     * @return mixed
     */
    public function getDhCadastro()
    {
        return $this->_dh_Cadastro;
    }


}