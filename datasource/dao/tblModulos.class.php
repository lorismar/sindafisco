<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblModulos
{

    private $_id_Modulos;
    private $_ds_Descricao;


    /**
     * @param int|null $id_Modulos
     */
    function __construct($id_Modulos)
    {
        if (!empty($id_Modulos)) {
            $this->_id_Modulos = $id_Modulos;
        }
    }

    /**
     * @param int|null $id_Modulos
     */
    public function setIdModulos($id_Modulos)
    {
        $this->_id_Modulos = $id_Modulos;
    }

    /**
     * @return int|null
     */
    public function getIdModulos()
    {
        return $this->_id_Modulos;
    }

    /**
     * @param string|null $ds_Descricao
     */
    public function setDsDescricao($ds_Descricao)
    {
        $this->_ds_Descricao = $ds_Descricao;
    }

    /**
     * @return string|null
     */
    public function getDsDescricao()
    {
        return $this->_ds_Descricao;
    }

}