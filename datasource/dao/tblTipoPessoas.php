<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblTipoPessoas
{

    private $_id_TipoPessoas;
    private $_ds_Descricao;

    /**
     * @param int|null $_id_TipoPessoas
     */
    function __construct($_id_TipoPessoas)
    {
        if (!empty($_id_SitPessoas)) {
            $this->_id_TipoPessoas = $_id_TipoPessoas;
        }
    }



    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_TipoPessoas, ds_Descricao FROM TipoPessoas WHERE id_TipoPessoas = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdTipoPessoas());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdTipoPessoas($c->linha['id_TipoPessoas']);
            $this->setDsDescricao($c->linha['ds_Descricao']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula
     * @return array
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_TipoPessoas, ds_Descricao FROM TipoPessoas $where ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new TipoPessoas(NULL);
            $tbl->setIdTipoPessoas($c->linha['id_TipoPessoas']);
            $tbl->setDsDescricao($c->linha['ds_Descricao']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool
     */
    public function Salvar()
    {
        if ($this->getIdTipoPessoas() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO TipoPessoas (id_TipoPessoas, ds_Descricao) VALUES (?, ?)  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdTipoPessoas());
        $c->adicionaParametros($this->getDsDescricao());
        if ($c->executaStatement()) {
            $this->setIdTipoPessoas($c->last_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Alterar()
    {
        $c   = Conexao::getInstance();
        $sql = "UPDATE TipoPessoas SET ds_Descricao = ? WHERE id_TipoPessoas = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsDescricao());
        // PK
        $c->adicionaParametros($this->getIdTipoPessoas());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }



    /**
     * @param string|null $ds_Descricao
     */
    public function setDsDescricao($ds_Descricao)
    {
        $this->_ds_Descricao = $ds_Descricao;
    }

    /**
     * @return string|null
     */
    public function getDsDescricao()
    {
        return $this->_ds_Descricao;
    }

    /**
     * @param mixed $id_TipoPessoas
     */
    public function setIdTipoPessoas($id_TipoPessoas)
    {
        $this->_id_TipoPessoas = $id_TipoPessoas;
    }

    /**
     * @return mixed
     */
    public function getIdTipoPessoas()
    {
        return $this->_id_TipoPessoas;
    }




}