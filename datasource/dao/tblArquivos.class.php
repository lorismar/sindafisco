<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblArquivos
{

    private $_id_Arquivos;
    private $_ds_Titulo;
    private $_ds_Descricao;
    private $_file_Arquivo;
    private $_ds_UrlLink;
    private $_dt_Cadastro;
    private $_is_Ativo;
    private $_id_Arquivos_fk;
    private $_id_Conteudos_fk;
    private $_id_Categorias_fk;
    private $_id_CategoriasPrincipal_fk;
    private $_id_Usuarios_fk;
    private $_log_CRIADOPOR;
    private $_log_ALTERADOPOR;
    private $_log_DATA;

    private $_Conteudos;
    private $_Categorias;
    private $_Usuarios;
    private $_ArquivosPai;

    /**
     * @param int|null $id_Arquivos
     */
    function __construct($id_Arquivos)
    {
        if (!empty($id_Arquivos)) {
            $this->_id_Arquivos = $id_Arquivos;
        }

    }

    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Arquivos, ds_Titulo, ds_Descricao, file_Arquivo, ds_UrlLink, DATE_FORMAT(dt_Cadastro, '%d/%m/%Y') dt_Cadastro,is_Ativo, id_Arquivos_fk, id_Conteudos_fk, id_Categorias_fk, id_CategoriasPrincipal_fk, id_Usuarios_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA FROM Arquivos WHERE id_Arquivos = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdArquivos());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdArquivos($c->linha['id_Arquivos']);
            $this->setDsTitulo($c->linha['ds_Titulo']);
            $this->setDsDescricao($c->linha['ds_Descricao']);
            $this->setFileArquivo($c->linha['file_Arquivo']);
            $this->setDsUrlLink($c->linha['ds_UrlLink']);
            $this->setDtCadastro($c->linha['dt_Cadastro']);
            $this->setIsAtivo($c->linha['is_Ativo']);
            $this->setIdArquivosFk($c->linha['id_Arquivos_fk']);
            $this->setIdConteudosFk($c->linha['id_Conteudos_fk']);
            $this->setIdCategoriasFk($c->linha['id_Categorias_fk']);
            $this->setIdCategoriasPrincipalFk($c->linha['id_CategoriasPrincipal_fk']);
            $this->setIdUsuariosFk($c->linha['id_Usuarios_fk']);
            $this->setLogCRIADOPOR($c->linha['log_CRIADOPOR']);
            $this->setLogALTERADOPOR($c->linha['log_ALTERADOPOR']);
            $this->setLogDATA($c->linha['log_DATA']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula
     * @return array
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Arquivos, ds_Titulo, ds_Descricao, file_Arquivo, ds_UrlLink, DATE_FORMAT(dt_Cadastro, '%d/%m/%Y') dt_Cadastro, is_Ativo, id_Arquivos_fk, id_Conteudos_fk, id_Categorias_fk,id_CategoriasPrincipal_fk, id_Usuarios_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA FROM Arquivos $where ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Arquivos(NULL);
            $tbl->setIdArquivos($c->linha['id_Arquivos']);
            $tbl->setDsTitulo($c->linha['ds_Titulo']);
            $tbl->setDsDescricao($c->linha['ds_Descricao']);
            $tbl->setFileArquivo($c->linha['file_Arquivo']);
            $tbl->setDsUrlLink($c->linha['ds_UrlLink']);
            $tbl->setDtCadastro($c->linha['dt_Cadastro']);
            $tbl->setIsAtivo($c->linha['is_Ativo']);
            $tbl->setIdArquivosFk($c->linha['id_Arquivos_fk']);
            $tbl->setIdConteudosFk($c->linha['id_Conteudos_fk']);
            $tbl->setIdCategoriasFk($c->linha['id_Categorias_fk']);
            $tbl->getIdCategoriasPrincipalFk($c->linha['id_CategoriasPrincipal_fk']);
            $tbl->setIdUsuariosFk($c->linha['id_Usuarios_fk']);
            $tbl->setLogCRIADOPOR($c->linha['log_CRIADOPOR']);
            $tbl->setLogALTERADOPOR($c->linha['log_ALTERADOPOR']);
            $tbl->setLogDATA($c->linha['log_DATA']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool
     */
    public function Salvar()
    {
        if ($this->getIdArquivos() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO Arquivos (id_Arquivos, ds_Titulo, ds_Descricao, file_Arquivo, ds_UrlLink, dt_Cadastro, is_Ativo,id_Arquivos_fk, id_Conteudos_fk, id_Categorias_fk,id_CategoriasPrincipal_fk, id_Usuarios_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA ) VALUES (?, ?, ?, ?, ?, STR_TO_DATE(?,'%d/%m/%Y'),?, ?, ?, ?, ?, ?, ?, ?, now())  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdArquivos());
        $c->adicionaParametros($this->getDsTitulo());
        $c->adicionaParametros($this->getDsDescricao());
        $c->adicionaParametros($this->getFileArquivo());
        $c->adicionaParametros($this->getDsUrlLink());
        $c->adicionaParametros($this->getDtCadastro());
        $c->adicionaParametros($this->getIsAtivo());
        $c->adicionaParametros($this->getIdArquivosFk());
        $c->adicionaParametros($this->getIdConteudosFk());
        $c->adicionaParametros($this->getIdCategoriasFk());
        $c->adicionaParametros($this->getIdCategoriasPrincipalFk());
        $c->adicionaParametros($this->getIdUsuariosFk());
        $c->adicionaParametros($this->getLogALTERADOPOR());
        $c->adicionaParametros($this->getLogALTERADOPOR());
        if ($c->executaStatement()) {
            $this->setIdArquivos($c->last_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Alterar()
    {
        $c   = Conexao::getInstance();
        $sql = "UPDATE Arquivos SET ds_Titulo = ?, ds_Descricao = ?, file_Arquivo = ?, ds_UrlLink = ?, dt_Cadastro = STR_TO_DATE(?,'%d/%m/%Y'), is_Ativo=?, id_Arquivos_fk=?, id_Conteudos_fk = ?, id_Categorias_fk = ?, id_CategoriasPrincipal_fk=?, id_Usuarios_fk = ?, log_ALTERADOPOR=?, log_DATA=now() WHERE id_Arquivos = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsTitulo());
        $c->adicionaParametros($this->getDsDescricao());
        $c->adicionaParametros($this->getFileArquivo());
        $c->adicionaParametros($this->getDsUrlLink());
        $c->adicionaParametros($this->getDtCadastro());
        $c->adicionaParametros($this->getIsAtivo());
        $c->adicionaParametros($this->getIdArquivosFk());
        $c->adicionaParametros($this->getIdConteudosFk());
        $c->adicionaParametros($this->getIdCategoriasFk());
        $c->adicionaParametros($this->getIdCategoriasPrincipalFk());
        $c->adicionaParametros($this->getIdUsuariosFk());
        $c->adicionaParametros($this->getLogALTERADOPOR());
        // PK
        $c->adicionaParametros($this->getIdArquivos());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Arquivos
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu
     */
    public static function Remover($id_Arquivos)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Arquivos WHERE id_Arquivos = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Arquivos);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @param int|null $id_Arquivos
     */
    public function setIdArquivos($id_Arquivos)
    {
        $this->_id_Arquivos = $id_Arquivos;
    }

    /**
     * @return int|null
     */
    public function getIdArquivos()
    {
        return $this->_id_Arquivos;
    }

    /**
     * @param string|null $ds_Titulo
     */
    public function setDsTitulo($ds_Titulo)
    {
        $this->_ds_Titulo = $ds_Titulo;
    }

    /**
     * @return string|null
     */
    public function getDsTitulo()
    {
        return $this->_ds_Titulo;
    }

    /**
     * @param string|null $ds_Descricao
     */
    public function setDsDescricao($ds_Descricao)
    {
        $this->_ds_Descricao = $ds_Descricao;
    }

    /**
     * @return string|null
     */
    public function getDsDescricao()
    {
        return $this->_ds_Descricao;
    }

    /**
     * @param string|null $file_Arquivo
     */
    public function setFileArquivo($file_Arquivo)
    {
        $this->_file_Arquivo = $file_Arquivo;
    }

    /**
     * @return string|null
     */
    public function getFileArquivo()
    {
        return $this->_file_Arquivo;
    }

    /**
     * @param string|null $ds_UrlLink
     */
    public function setDsUrlLink($ds_UrlLink)
    {
        $this->_ds_UrlLink = $ds_UrlLink;
    }

    /**
     * @return string|null
     */
    public function getDsUrlLink()
    {
        return $this->_ds_UrlLink;
    }

    /**
     * @param date|null $dt_Cadastro
     */
    public function setDtCadastro($dt_Cadastro)
    {
        $this->_dt_Cadastro = $dt_Cadastro;
    }

    /**
     * @return date|null
     */
    public function getDtCadastro()
    {
        return $this->_dt_Cadastro;
    }

    /**
     * @param int|null $id_Conteudos_fk
     */
    public function setIdConteudosFk($id_Conteudos_fk)
    {
        $this->_id_Conteudos_fk = $id_Conteudos_fk;
        $this->_Conteudos = new Conteudos($id_Conteudos_fk);
    }

    /**
     * @return int|null
     */
    public function getIdConteudosFk()
    {
        return $this->_id_Conteudos_fk;
    }

    /**
     * @param int|null $id_Categorias_fk
     */
    public function setIdCategoriasFk($id_Categorias_fk)
    {
        $this->_id_Categorias_fk = $id_Categorias_fk;
        $this->_Categorias = new Categorias($id_Categorias_fk);
    }

    /**
     * @return int|null
     */
    public function getIdCategoriasFk()
    {
        return $this->_id_Categorias_fk;
    }

    /**
     * @param int|null $id_Usuarios_fk
     */
    public function setIdUsuariosFk($id_Usuarios_fk)
    {
        $this->_id_Usuarios_fk = $id_Usuarios_fk;
        $this->_Usuarios = new Usuarios($id_Usuarios_fk);
    }

    /**
     * @return int|null
     */
    public function getIdUsuariosFk()
    {
        return $this->_id_Usuarios_fk;
    }

    /**
     * @param string|null $log_CRIADOPOR
     */
    public function setLogCRIADOPOR($log_CRIADOPOR)
    {
        $this->_log_CRIADOPOR = $log_CRIADOPOR;
    }

    /**
     * @return string|null
     */
    public function getLogCRIADOPOR()
    {
        return $this->_log_CRIADOPOR;
    }

    /**
     * @param string|null $log_ALTERADOPOR
     */
    public function setLogALTERADOPOR($log_ALTERADOPOR)
    {
        $this->_log_ALTERADOPOR = $log_ALTERADOPOR;
    }

    /**
     * @return string|null
     */
    public function getLogALTERADOPOR()
    {
        return $this->_log_ALTERADOPOR;
    }

    /**
     * @param string|null $log_DATA
     */
    public function setLogDATA($log_DATA)
    {
        $this->_log_DATA = $log_DATA;
    }

    /**
     * @return string|null
     */
    public function getLogDATA()
    {
        return $this->_log_DATA;
    }

    /**
     * @param Conteudos $Conteudos
     */
    public function setConteudos($Conteudos)
    {
        $this->_Conteudos = $Conteudos;
    }

    /**
     * @return Conteudos
     */
    public function getConteudos()
    {
        return $this->_Conteudos;
    }

    /**
     * @param Categorias $Categorias
     */
    public function setCategorias($Categorias)
    {
        $this->_Categorias = $Categorias;
    }

    /**
     * @return Categorias
     */
    public function getCategorias()
    {
        return $this->_Categorias;
    }

    /**
     * @param Usuarios $Usuarios
     */
    public function setUsuarios($Usuarios)
    {
        $this->_Usuarios = $Usuarios;
    }

    /**
     * @return Usuarios
     */
    public function getUsuarios()
    {
        return $this->_Usuarios;
    }

    /**
     * @param mixed $id_CategoriasPrincipal_fk
     */
    public function setIdCategoriasPrincipalFk($id_CategoriasPrincipal_fk)
    {
        $this->_id_CategoriasPrincipal_fk = $id_CategoriasPrincipal_fk;
    }

    /**
     * @return mixed
     */
    public function getIdCategoriasPrincipalFk()
    {
        return $this->_id_CategoriasPrincipal_fk;
    }

    /**
     * @param mixed $is_Ativo
     */
    public function setIsAtivo($is_Ativo)
    {
        $this->_is_Ativo = $is_Ativo;
    }

    /**
     * @return mixed
     */
    public function getIsAtivo()
    {
        return $this->_is_Ativo;
    }

    /**
     * @param int $id_Arquivos_fk
     */
    public function setIdArquivosFk($id_Arquivos_fk)
    {
        $this->_id_Arquivos_fk = $id_Arquivos_fk;
        $this->_ArquivosPai = new Arquivos($id_Arquivos_fk);
    }

    /**
     * @return int
     */
    public function getIdArquivosFk()
    {
        return $this->_id_Arquivos_fk;
    }

    /**
     * @param Arquivos $ArquivosPai
     */
    public function setArquivosPai($ArquivosPai)
    {
        $this->_ArquivosPai = $ArquivosPai;
    }

    /**
     * @return Arquivos
     */
    public function getArquivosPai()
    {
        return $this->_ArquivosPai;
    }



}