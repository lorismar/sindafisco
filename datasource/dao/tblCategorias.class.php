<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblCategorias
{

    private $_id_Categorias;
    private $_ds_Descricao;
    private $_id_Categorias_fk;

    private $_Categorias;

    /**
     * @param int|null $id_Categorias
     */
    function __construct($id_Categorias)
    {
        if (!empty($id_Categorias)) {
            $this->_id_Categorias = $id_Categorias;
        }
    }

    /**
     * @param int|null $id_Categorias
     */
    public function setIdCategorias($id_Categorias)
    {
        $this->_id_Categorias = $id_Categorias;
    }

    /**
     * @return int|null
     */
    public function getIdCategorias()
    {
        return $this->_id_Categorias;
    }

    /**
     * @param string|null $ds_Descricao
     */
    public function setDsDescricao($ds_Descricao)
    {
        $this->_ds_Descricao = $ds_Descricao;
    }

    /**
     * @return string|null
     */
    public function getDsDescricao()
    {
        return $this->_ds_Descricao;
    }

    /**
     * @param int|null $id_Categorias_fk
     */
    public function setIdCategoriasFk($id_Categorias_fk)
    {
        $this->_id_Categorias_fk = $id_Categorias_fk;
        $this->_Categorias = new Categorias($id_Categorias_fk);
    }

    /**
     * @return int|null
     */
    public function getIdCategoriasFk()
    {
        return $this->_id_Categorias_fk;
    }

    /**
     * @param Categorias $Categorias
     */
    public function setCategorias($Categorias)
    {
        $this->_Categorias = $Categorias;
    }

    /**
     * @return Categorias
     */
    public function getCategorias()
    {
        return $this->_Categorias;
    }

}