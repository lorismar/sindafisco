<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblPessoas
{

    private $_id_Pessoas;
    private $_ds_Nome;
    private $_ds_Cpf;
    private $_ds_Rg;
    private $_ds_OrgaoExpedidor;
    private $_ds_Matricula;
    private $_dt_Nascimento;
    private $_ds_Sexo;
    private $_ds_Formacao;
    private $_ds_EstadoCivil;
    private $_ds_NomeConjuge;
    private $_nu_Dependentes;
    private $_ds_Email;
    private $_ds_EmailAlternativo;
    private $_ds_End_Completo;
    private $_ds_End_Bairro;
    private $_ds_End_Cep;
    private $_ds_End_Cidade;
    private $_ds_End_Uf;
    private $_ds_Telefone;
    private $_ds_Celular;
    private $_ds_TelefoneTrabalho;
    private $_ds_Lotacao;
    private $_file_Foto;
    private $_is_PrimeiroAcesso;
    private $_is_Ativo;
    private $_ds_NomePai;
    private $_ds_NomeMae;
    private $_ds_Outros;
    private $_log_CRIADOPOR;
    private $_log_ALTERADOPOR;
    private $_log_DATA;
    private $_id_SitPessoas_fk;
    private $_id_TipoPessoas_fk;

    private $_Usuarios;
    private $_SitPessoas;


    /**
     * @param int|null $id_Pessoas
     */
    function __construct($id_Pessoas)
    {
        if (!empty($id_Pessoas)) {
            $this->_id_Pessoas = $id_Pessoas;
        }
    }

    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Pessoas, ds_Nome, ds_Cpf, ds_Rg, ds_OrgaoExpedidor, ds_Matricula, DATE_FORMAT(dt_Nascimento, '%d/%m/%Y') dt_Nascimento, ds_Sexo, ds_Formacao, ds_EstadoCivil, ds_NomeConjuge, nu_Dependentes, ds_Email, ds_EmailAlternativo, ds_End_Completo, ds_End_Bairro, ds_End_Cep, ds_End_Cidade, ds_End_Uf, ds_Telefone, ds_Celular, ds_TelefoneTrabalho, ds_Lotacao, file_Foto, is_PrimeiroAcesso, is_Ativo, ds_NomePai, ds_NomeMae, ds_Outros, log_CRIADOPOR, log_ALTERADOPOR, log_DATA, id_SitPessoas_fk, id_TipoPessoas_fk FROM Pessoas WHERE id_Pessoas = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdPessoas());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdPessoas($c->linha['id_Pessoas']);
            $this->setDsNome($c->linha['ds_Nome']);
            $this->setDsCpf($c->linha['ds_Cpf']);
            $this->setDsRg($c->linha['ds_Rg']);
            $this->setDsOrgaoExpedidor($c->linha['ds_OrgaoExpedidor']);
            $this->setDsMatricula($c->linha['ds_Matricula']);
            $this->setDtNascimento($c->linha['dt_Nascimento']);
            $this->setDsSexo($c->linha['ds_Sexo']);
            $this->setDsFormacao($c->linha['ds_Formacao']);
            $this->setDsEstadoCivil($c->linha['ds_EstadoCivil']);
            $this->setDsNomeConjuge($c->linha['ds_NomeConjuge']);
            $this->setNuDependentes($c->linha['nu_Dependentes']);
            $this->setDsEmail($c->linha['ds_Email']);
            $this->setDsEmailAlternativo($c->linha['ds_EmailAlternativo']);
            $this->setDsEndCompleto($c->linha['ds_End_Completo']);
            $this->setDsEndBairro($c->linha['ds_End_Bairro']);
            $this->setDsEndCep($c->linha['ds_End_Cep']);
            $this->setDsEndCidade($c->linha['ds_End_Cidade']);
            $this->setDsEndUf($c->linha['ds_End_Uf']);
            $this->setDsTelefone($c->linha['ds_Telefone']);
            $this->setDsCelular($c->linha['ds_Celular']);
            $this->setDsTelefoneTrabalho($c->linha['ds_TelefoneTrabalho']);
            $this->setDsLotacao($c->linha['ds_Lotacao']);
            $this->setFileFoto($c->linha['file_Foto']);
            $this->setIsPrimeiroAcesso($c->linha['is_PrimeiroAcesso']);
            $this->setIsAtivo($c->linha['is_Ativo']);
            $this->setDsNomePai($c->linha['ds_NomePai']);
            $this->setDsNomeMae($c->linha['ds_NomeMae']);
            $this->setDsOutros($c->linha['ds_Outros']);
            $this->setLogCRIADOPOR($c->linha['log_CRIADOPOR']);
            $this->setLogALTERADOPOR($c->linha['log_ALTERADOPOR']);
            $this->setLogDATA($c->linha['log_DATA']);
            $this->setIdSitPessoasFk($c->linha['id_SitPessoas_fk']);
            $this->setIdTipoPessoasFk($c->linha['id_TipoPessoas_fk']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula
     * @return Pessoas[]
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Pessoas, ds_Nome, ds_Cpf, ds_Rg, ds_OrgaoExpedidor, ds_Matricula, DATE_FORMAT(dt_Nascimento, '%d/%m/%Y') dt_Nascimento, ds_Sexo, ds_Formacao, ds_EstadoCivil, ds_NomeConjuge, nu_Dependentes, ds_Email, ds_EmailAlternativo, ds_End_Completo, ds_End_Bairro, ds_End_Cep, ds_End_Cidade, ds_End_Uf, ds_Telefone, ds_Celular, ds_TelefoneTrabalho, ds_Lotacao, file_Foto, is_PrimeiroAcesso, is_Ativo, ds_NomePai, ds_NomeMae, ds_Outros, log_CRIADOPOR, log_ALTERADOPOR, log_DATA, id_SitPessoas_fk, id_TipoPessoas_fk FROM Pessoas $where ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Pessoas(NULL);
            $tbl->setIdPessoas($c->linha['id_Pessoas']);
            $tbl->setDsNome($c->linha['ds_Nome']);
            $tbl->setDsCpf($c->linha['ds_Cpf']);
            $tbl->setDsRg($c->linha['ds_Rg']);
            $tbl->setDsOrgaoExpedidor($c->linha['ds_OrgaoExpedidor']);
            $tbl->setDsMatricula($c->linha['ds_Matricula']);
            $tbl->setDtNascimento($c->linha['dt_Nascimento']);
            $tbl->setDsSexo($c->linha['ds_Sexo']);
            $tbl->setDsFormacao($c->linha['ds_Formacao']);
            $tbl->setDsEstadoCivil($c->linha['ds_EstadoCivil']);
            $tbl->setDsNomeConjuge($c->linha['ds_NomeConjuge']);
            $tbl->setNuDependentes($c->linha['nu_Dependentes']);
            $tbl->setDsEmail($c->linha['ds_Email']);
            $tbl->setDsEmailAlternativo($c->linha['ds_EmailAlternativo']);
            $tbl->setDsEndCompleto($c->linha['ds_End_Completo']);
            $tbl->setDsEndBairro($c->linha['ds_End_Bairro']);
            $tbl->setDsEndCep($c->linha['ds_End_Cep']);
            $tbl->setDsEndCidade($c->linha['ds_End_Cidade']);
            $tbl->setDsEndUf($c->linha['ds_End_Uf']);
            $tbl->setDsTelefone($c->linha['ds_Telefone']);
            $tbl->setDsCelular($c->linha['ds_Celular']);
            $tbl->setDsTelefoneTrabalho($c->linha['ds_TelefoneTrabalho']);
            $tbl->setDsLotacao($c->linha['ds_Lotacao']);
            $tbl->setFileFoto($c->linha['file_Foto']);
            $tbl->setIsPrimeiroAcesso($c->linha['is_PrimeiroAcesso']);
            $tbl->setIsAtivo($c->linha['is_Ativo']);
            $tbl->setDsNomePai($c->linha['ds_NomePai']);
            $tbl->setDsNomeMae($c->linha['ds_NomeMae']);
            $tbl->setDsOutros($c->linha['ds_Outros']);
            $tbl->setLogCRIADOPOR($c->linha['log_CRIADOPOR']);
            $tbl->setLogALTERADOPOR($c->linha['log_ALTERADOPOR']);
            $tbl->setLogDATA($c->linha['log_DATA']);
            $tbl->setIdSitPessoasFk($c->linha['id_SitPessoas_fk']);
            $tbl->setIdTipoPessoasFk($c->linha['id_TipoPessoas_fk']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool
     */
    public function Salvar()
    {
        if ($this->getIdPessoas() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO Pessoas (id_Pessoas, ds_Nome, ds_Cpf, ds_Rg, ds_OrgaoExpedidor, ds_Matricula, dt_Nascimento, ds_Sexo, ds_Formacao, ds_EstadoCivil, ds_NomeConjuge, nu_Dependentes, ds_Email, ds_EmailAlternativo, ds_End_Completo, ds_End_Bairro, ds_End_Cep, ds_End_Cidade, ds_End_Uf, ds_Telefone, ds_Celular, ds_TelefoneTrabalho, ds_Lotacao, file_Foto, is_PrimeiroAcesso, is_Ativo, ds_NomePai, ds_NomeMae, ds_Outros, log_CRIADOPOR, log_ALTERADOPOR, log_DATA,id_SitPessoas_fk, id_TipoPessoas_fk ) VALUES (?, ?, ?, ?, ?, ?, STR_TO_DATE(?,'%d/%m/%Y'), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), ?, ?)  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdPessoas());
        $c->adicionaParametros($this->getDsNome());
        $c->adicionaParametros($this->getDsCpf());
        $c->adicionaParametros($this->getDsRg());
        $c->adicionaParametros($this->getDsOrgaoExpedidor());
        $c->adicionaParametros($this->getDsMatricula());
        $c->adicionaParametros($this->getDtNascimento());
        $c->adicionaParametros($this->getDsSexo());
        $c->adicionaParametros($this->getDsFormacao());
        $c->adicionaParametros($this->getDsEstadoCivil());
        $c->adicionaParametros($this->getDsNomeConjuge());
        $c->adicionaParametros($this->getNuDependentes());
        $c->adicionaParametros($this->getDsEmail());
        $c->adicionaParametros($this->getDsEmailAlternativo());
        $c->adicionaParametros($this->getDsEndCompleto());
        $c->adicionaParametros($this->getDsEndBairro());
        $c->adicionaParametros($this->getDsEndCep());
        $c->adicionaParametros($this->getDsEndCidade());
        $c->adicionaParametros($this->getDsEndUf());
        $c->adicionaParametros($this->getDsTelefone());
        $c->adicionaParametros($this->getDsCelular());
        $c->adicionaParametros($this->getDsTelefoneTrabalho());
        $c->adicionaParametros($this->getDsLotacao());
        $c->adicionaParametros($this->getFileFoto());
        $c->adicionaParametros($this->getIsPrimeiroAcesso());
        $c->adicionaParametros($this->getIsAtivo());
        $c->adicionaParametros($this->getDsNomePai());
        $c->adicionaParametros($this->getDsNomeMae());
        $c->adicionaParametros($this->getDsOutros());
        $c->adicionaParametros($this->getLogALTERADOPOR());
        $c->adicionaParametros($this->getLogALTERADOPOR());
        $c->adicionaParametros($this->getIdSitPessoasFk());
        $c->adicionaParametros($this->getIdTipoPessoasFk());
        if ($c->executaStatement()) {
            $this->setIdPessoas($c->last_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Alterar()
    {
        $c   = Conexao::getInstance();
        $sql = "UPDATE Pessoas SET ds_Nome = ?, ds_Cpf = ?, ds_Rg = ?, ds_OrgaoExpedidor = ?, ds_Matricula = ?, dt_Nascimento = STR_TO_DATE(?,'%d/%m/%Y'), ds_Sexo = ?, ds_Formacao = ?, ds_EstadoCivil = ?, ds_NomeConjuge = ?, nu_Dependentes = ?, ds_Email = ?, ds_EmailAlternativo = ?, ds_End_Completo = ?, ds_End_Bairro = ?, ds_End_Cep = ?, ds_End_Cidade = ?, ds_End_Uf = ?, ds_Telefone = ?, ds_Celular = ?, ds_TelefoneTrabalho = ?, ds_Lotacao = ?, file_Foto = ?, is_PrimeiroAcesso = ?, is_Ativo = ?,ds_NomePai=?, ds_NomeMae=?, ds_Outros=?, log_ALTERADOPOR=?, log_DATA=now(),id_SitPessoas_fk=?, id_TipoPessoas_fk=? WHERE id_Pessoas = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsNome());
        $c->adicionaParametros($this->getDsCpf());
        $c->adicionaParametros($this->getDsRg());
        $c->adicionaParametros($this->getDsOrgaoExpedidor());
        $c->adicionaParametros($this->getDsMatricula());
        $c->adicionaParametros($this->getDtNascimento());
        $c->adicionaParametros($this->getDsSexo());
        $c->adicionaParametros($this->getDsFormacao());
        $c->adicionaParametros($this->getDsEstadoCivil());
        $c->adicionaParametros($this->getDsNomeConjuge());
        $c->adicionaParametros($this->getNuDependentes());
        $c->adicionaParametros($this->getDsEmail());
        $c->adicionaParametros($this->getDsEmailAlternativo());
        $c->adicionaParametros($this->getDsEndCompleto());
        $c->adicionaParametros($this->getDsEndBairro());
        $c->adicionaParametros($this->getDsEndCep());
        $c->adicionaParametros($this->getDsEndCidade());
        $c->adicionaParametros($this->getDsEndUf());
        $c->adicionaParametros($this->getDsTelefone());
        $c->adicionaParametros($this->getDsCelular());
        $c->adicionaParametros($this->getDsTelefoneTrabalho());
        $c->adicionaParametros($this->getDsLotacao());
        $c->adicionaParametros($this->getFileFoto());
        $c->adicionaParametros($this->getIsPrimeiroAcesso());
        $c->adicionaParametros($this->getIsAtivo());
        $c->adicionaParametros($this->getDsNomePai());
        $c->adicionaParametros($this->getDsNomeMae());
        $c->adicionaParametros($this->getDsOutros());
        $c->adicionaParametros($this->getLogALTERADOPOR());
        $c->adicionaParametros($this->getIdSitPessoasFk());
        $c->adicionaParametros($this->getIdTipoPessoasFk());
        // PK
        $c->adicionaParametros($this->getIdPessoas());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Pessoas
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu
     */
    public static function Remover($id_Pessoas)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Pessoas WHERE id_Pessoas = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Pessoas);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @param int|null $id_Pessoas
     */
    public function setIdPessoas($id_Pessoas)
    {
        $this->_id_Pessoas = $id_Pessoas;
        $this->_Usuarios   = new Usuarios(NULL);
        $this->_Usuarios->setIdPessoasFk($id_Pessoas);
    }

    /**
     * @return int|null
     */
    public function getIdPessoas()
    {
        return $this->_id_Pessoas;
    }

    /**
     * @param string|null $ds_Nome
     */
    public function setDsNome($ds_Nome)
    {
        $this->_ds_Nome = $ds_Nome;
    }

    /**
     * @return string|null
     */
    public function getDsNome()
    {
        return $this->_ds_Nome;
    }

    /**
     * @param string|null $ds_Cpf
     */
    public function setDsCpf($ds_Cpf)
    {
        $this->_ds_Cpf = $ds_Cpf;
    }

    /**
     * @return string|null
     */
    public function getDsCpf()
    {
        return $this->_ds_Cpf;
    }

    /**
     * @param string|null $ds_Rg
     */
    public function setDsRg($ds_Rg)
    {
        $this->_ds_Rg = $ds_Rg;
    }

    /**
     * @return string|null
     */
    public function getDsRg()
    {
        return $this->_ds_Rg;
    }

    /**
     * @param string|null $ds_OrgaoExpedidor
     */
    public function setDsOrgaoExpedidor($ds_OrgaoExpedidor)
    {
        $this->_ds_OrgaoExpedidor = $ds_OrgaoExpedidor;
    }

    /**
     * @return string|null
     */
    public function getDsOrgaoExpedidor()
    {
        return $this->_ds_OrgaoExpedidor;
    }

    /**
     * @param string|null $ds_Matricula
     */
    public function setDsMatricula($ds_Matricula)
    {
        $this->_ds_Matricula = $ds_Matricula;
    }

    /**
     * @return string|null
     */
    public function getDsMatricula()
    {
        return $this->_ds_Matricula;
    }

    /**
     * @param date|null $dt_Nascimento
     */
    public function setDtNascimento($dt_Nascimento)
    {
        $this->_dt_Nascimento = $dt_Nascimento;
    }

    /**
     * @return date|null
     */
    public function getDtNascimento()
    {
        return $this->_dt_Nascimento;
    }

    /**
     * @param string|null $ds_Sexo
     */
    public function setDsSexo($ds_Sexo)
    {
        $this->_ds_Sexo = $ds_Sexo;
    }

    /**
     * @return string|null
     */
    public function getDsSexo()
    {
        return $this->_ds_Sexo;
    }

    /**
     * @param string|null $ds_Formacao
     */
    public function setDsFormacao($ds_Formacao)
    {
        $this->_ds_Formacao = $ds_Formacao;
    }

    /**
     * @return string|null
     */
    public function getDsFormacao()
    {
        return $this->_ds_Formacao;
    }

    /**
     * @param string|null $ds_EstadoCivil
     */
    public function setDsEstadoCivil($ds_EstadoCivil)
    {
        $this->_ds_EstadoCivil = $ds_EstadoCivil;
    }

    /**
     * @return string|null
     */
    public function getDsEstadoCivil()
    {
        return $this->_ds_EstadoCivil;
    }

    /**
     * @param string|null $ds_NomeConjuge
     */
    public function setDsNomeConjuge($ds_NomeConjuge)
    {
        $this->_ds_NomeConjuge = $ds_NomeConjuge;
    }

    /**
     * @return string|null
     */
    public function getDsNomeConjuge()
    {
        return $this->_ds_NomeConjuge;
    }

    /**
     * @param int|null $nu_Dependentes
     */
    public function setNuDependentes($nu_Dependentes)
    {
        $this->_nu_Dependentes = $nu_Dependentes;
    }

    /**
     * @return int|null
     */
    public function getNuDependentes()
    {
        return $this->_nu_Dependentes;
    }

    /**
     * @param string|null $ds_Email
     */
    public function setDsEmail($ds_Email)
    {
        $this->_ds_Email = trim($ds_Email);
    }

    /**
     * @return string|null
     */
    public function getDsEmail()
    {
        return $this->_ds_Email;
    }

    /**
     * @param string|null $ds_EmailAlternativo
     */
    public function setDsEmailAlternativo($ds_EmailAlternativo)
    {
        $this->_ds_EmailAlternativo = $ds_EmailAlternativo;
    }

    /**
     * @return string|null
     */
    public function getDsEmailAlternativo()
    {
        return $this->_ds_EmailAlternativo;
    }

    /**
     * @param string|null $ds_End_Completo
     */
    public function setDsEndCompleto($ds_End_Completo)
    {
        $this->_ds_End_Completo = $ds_End_Completo;
    }

    /**
     * @return string|null
     */
    public function getDsEndCompleto()
    {
        return $this->_ds_End_Completo;
    }

    /**
     * @param string|null $ds_End_Bairro
     */
    public function setDsEndBairro($ds_End_Bairro)
    {
        $this->_ds_End_Bairro = $ds_End_Bairro;
    }

    /**
     * @return string|null
     */
    public function getDsEndBairro()
    {
        return $this->_ds_End_Bairro;
    }

    /**
     * @param string|null $ds_End_Cep
     */
    public function setDsEndCep($ds_End_Cep)
    {
        $this->_ds_End_Cep = $ds_End_Cep;
    }

    /**
     * @return string|null
     */
    public function getDsEndCep()
    {
        return $this->_ds_End_Cep;
    }

    /**
     * @param string|null $ds_End_Cidade
     */
    public function setDsEndCidade($ds_End_Cidade)
    {
        $this->_ds_End_Cidade = $ds_End_Cidade;
    }

    /**
     * @return string|null
     */
    public function getDsEndCidade()
    {
        return $this->_ds_End_Cidade;
    }

    /**
     * @param string|null $ds_End_Uf
     */
    public function setDsEndUf($ds_End_Uf)
    {
        $this->_ds_End_Uf = $ds_End_Uf;
    }

    /**
     * @return string|null
     */
    public function getDsEndUf()
    {
        return $this->_ds_End_Uf;
    }

    /**
     * @param string|null $ds_Telefone
     */
    public function setDsTelefone($ds_Telefone)
    {
        $this->_ds_Telefone = $ds_Telefone;
    }

    /**
     * @return string|null
     */
    public function getDsTelefone()
    {
        return $this->_ds_Telefone;
    }

    /**
     * @param string|null $ds_Celular
     */
    public function setDsCelular($ds_Celular)
    {
        $this->_ds_Celular = $ds_Celular;
    }

    /**
     * @return string|null
     */
    public function getDsCelular()
    {
        return $this->_ds_Celular;
    }

    /**
     * @param string|null $ds_TelefoneTrabalho
     */
    public function setDsTelefoneTrabalho($ds_TelefoneTrabalho)
    {
        $this->_ds_TelefoneTrabalho = $ds_TelefoneTrabalho;
    }

    /**
     * @return string|null
     */
    public function getDsTelefoneTrabalho()
    {
        return $this->_ds_TelefoneTrabalho;
    }

    /**
     * @param string|null $ds_Lotacao
     */
    public function setDsLotacao($ds_Lotacao)
    {
        $this->_ds_Lotacao = $ds_Lotacao;
    }

    /**
     * @return string|null
     */
    public function getDsLotacao()
    {
        return $this->_ds_Lotacao;
    }

    /**
     * @param string|null $file_Foto
     */
    public function setFileFoto($file_Foto)
    {
        $this->_file_Foto = $file_Foto;
    }

    /**
     * @return string|null
     */
    public function getFileFoto()
    {
        return $this->_file_Foto;
    }

    /**
     * @param bool|null $is_PrimeiroAcesso
     */
    public function setIsPrimeiroAcesso($is_PrimeiroAcesso)
    {
        $this->_is_PrimeiroAcesso = $is_PrimeiroAcesso;
    }

    /**
     * @return bool|null
     */
    public function getIsPrimeiroAcesso()
    {
        return $this->_is_PrimeiroAcesso;
    }

    /**
     * @param bool|null $is_Ativo
     */
    public function setIsAtivo($is_Ativo)
    {
        $this->_is_Ativo = $is_Ativo;
    }

    /**
     * @return bool|null
     */
    public function getIsAtivo()
    {
        return $this->_is_Ativo;
    }

    /**
     * @param string|null $log_CRIADOPOR
     */
    public function setLogCRIADOPOR($log_CRIADOPOR)
    {
        $this->_log_CRIADOPOR = $log_CRIADOPOR;
    }

    /**
     * @return string|null
     */
    public function getLogCRIADOPOR()
    {
        return $this->_log_CRIADOPOR;
    }

    /**
     * @param string|null $log_ALTERADOPOR
     */
    public function setLogALTERADOPOR($log_ALTERADOPOR)
    {
        $this->_log_ALTERADOPOR = $log_ALTERADOPOR;
    }

    /**
     * @return string|null
     */
    public function getLogALTERADOPOR()
    {
        return $this->_log_ALTERADOPOR;
    }

    /**
     * @param string|null $log_DATA
     */
    public function setLogDATA($log_DATA)
    {
        $this->_log_DATA = $log_DATA;
    }

    /**
     * @return string|null
     */
    public function getLogDATA()
    {
        return $this->_log_DATA;
    }

    /**
     * @param mixed $id_SitPessoas_fk
     */
    public function setIdSitPessoasFk($id_SitPessoas_fk)
    {
        $this->_id_SitPessoas_fk = $id_SitPessoas_fk;
        $this->_SitPessoas = new SitPessoas($id_SitPessoas_fk);
    }

    /**
     * @return mixed
     */
    public function getIdSitPessoasFk()
    {
        return $this->_id_SitPessoas_fk;
    }

    /**
     * @param Usuarios $Usuarios
     */
    public function setUsuarios($Usuarios)
    {
        $this->_Usuarios = $Usuarios;
    }

    /**
     * @return Usuarios
     */
    public function getUsuarios()
    {
        return $this->_Usuarios;
    }

    /**
     * @param mixed $ds_Outros
     */
    public function setDsOutros($ds_Outros)
    {
        $this->_ds_Outros = $ds_Outros;
    }

    /**
     * @return mixed
     */
    public function getDsOutros()
    {
        return $this->_ds_Outros;
    }

    /**
     * @param mixed $ds_NomeMae
     */
    public function setDsNomeMae($ds_NomeMae)
    {
        $this->_ds_NomeMae = $ds_NomeMae;
    }

    /**
     * @return mixed
     */
    public function getDsNomeMae()
    {
        return $this->_ds_NomeMae;
    }

    /**
     * @param mixed $ds_NomePai
     */
    public function setDsNomePai($ds_NomePai)
    {
        $this->_ds_NomePai = $ds_NomePai;
    }

    /**
     * @return mixed
     */
    public function getDsNomePai()
    {
        return $this->_ds_NomePai;
    }

    /**
     * @param mixed $id_TipoPessoas_fk
     */
    public function setIdTipoPessoasFk($id_TipoPessoas_fk)
    {
        $this->_id_TipoPessoas_fk = $id_TipoPessoas_fk;
    }

    /**
     * @return mixed
     */
    public function getIdTipoPessoasFk()
    {
        return $this->_id_TipoPessoas_fk;
    }

    /**
     * @param SitPessoas $SitPessoas
     */
    public function setSitPessoas($SitPessoas)
    {
        $this->_SitPessoas = $SitPessoas;
    }

    /**
     * @return SitPessoas
     */
    public function getSitPessoas()
    {
        return $this->_SitPessoas;
    }



}