<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblPerfis_tem_sysPaginas
{

    private $_id_Perfis_fk;
    private $_id_sysPaginas_fk;
    private $_is_Visivel;
    private $_is_Leitura;
    private $_is_Gravacao;
    private $_is_Remocao;

    private $_Perfis;
    private $_sysPaginas;

    /**
     * @param int|null $id_Perfis_fk
     * @param int|null $id_sysPaginas_fk
     */
    function __construct($id_Perfis_fk,$id_sysPaginas_fk)
    {
        if (!empty($_id_Perfis_fk)) {
            $this->_id_Perfis_fk = $id_Perfis_fk;
        }
        if (!empty($_id_sysPaginas_fk)) {
            $this->_id_sysPaginas_fk = $id_sysPaginas_fk;
        }
    }

    /**
     * @param int|null $id_Perfis_fk
     */
    public function setIdPerfisFk($id_Perfis_fk)
    {
        $this->_id_Perfis_fk = $id_Perfis_fk;
        $this->_Perfis = new Perfis($id_Perfis_fk);
    }

    /**
     * @return int|null
     */
    public function getIdPerfisFk()
    {
        return $this->_id_Perfis_fk;
    }

    /**
     * @param int|null $id_sysPaginas_fk
     */
    public function setIdSysPaginasFk($id_sysPaginas_fk)
    {
        $this->_id_sysPaginas_fk = $id_sysPaginas_fk;
        $this->_sysPaginas = new sysPaginas($id_sysPaginas_fk);
    }

    /**
     * @return int|null
     */
    public function getIdSysPaginasFk()
    {
        return $this->_id_sysPaginas_fk;
    }

    /**
     * @param bool|null $is_Visivel
     */
    public function setIsVisivel($is_Visivel)
    {
        $this->_is_Visivel = $is_Visivel;
    }

    /**
     * @return bool|null
     */
    public function getIsVisivel()
    {
        return $this->_is_Visivel;
    }

    /**
     * @param bool|null $is_Leitura
     */
    public function setIsLeitura($is_Leitura)
    {
        $this->_is_Leitura = $is_Leitura;
    }

    /**
     * @return bool|null
     */
    public function getIsLeitura()
    {
        return $this->_is_Leitura;
    }

    /**
     * @param bool|null $is_Gravacao
     */
    public function setIsGravacao($is_Gravacao)
    {
        $this->_is_Gravacao = $is_Gravacao;
    }

    /**
     * @return bool|null
     */
    public function getIsGravacao()
    {
        return $this->_is_Gravacao;
    }

    /**
     * @param bool|null $is_Remocao
     */
    public function setIsRemocao($is_Remocao)
    {
        $this->_is_Remocao = $is_Remocao;
    }

    /**
     * @return bool|null
     */
    public function getIsRemocao()
    {
        return $this->_is_Remocao;
    }

    /**
     * @param Perfis $Perfis
     */
    public function setPerfis($Perfis)
    {
        $this->_Perfis = $Perfis;
    }

    /**
     * @return Perfis
     */
    public function getPerfis()
    {
        return $this->_Perfis;
    }

    /**
     * @param sysPaginas $sysPaginas
     */
    public function setSysPaginas($sysPaginas)
    {
        $this->_sysPaginas = $sysPaginas;
    }

    /**
     * @return sysPaginas
     */
    public function getSysPaginas()
    {
        return $this->_sysPaginas;
    }

}