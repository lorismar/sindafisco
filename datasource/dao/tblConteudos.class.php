<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblConteudos
{

    private $_id_Conteudos;
    private $_ds_Titulo;
    private $_file_Capa;
    private $_tx_Resumo;
    private $_tx_Conteudo;
    private $_dh_Cadastro;
    private $_is_Ativo;
    private $_is_Destaque;
    private $_is_LiberarComentarios;
    private $_is_Publico;
    private $_is_Enviado;
    private $_ds_Permalink;
    private $_id_Conteudos_fk;
    private $_id_CategoriasPrincipal_fk;
    private $_id_Categorias_fk;
    private $_id_Usuarios_fk;
    private $_log_CRIADOPOR;
    private $_log_ALTERADOPOR;
    private $_log_DATA;

    private $_Conteudos;
    private $_CategoriasPrincipal;
    private $_Categorias;
    private $_Usuarios;

    /**
     * @param int|null $id_Conteudos
     */
    function __construct($id_Conteudos)
    {
        if (!empty($id_Conteudos)) {
            $this->_id_Conteudos = $id_Conteudos;
        }
    }


    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Conteudos, ds_Titulo, file_Capa, tx_Resumo, tx_Conteudo, DATE_FORMAT(dh_Cadastro, '%d/%m/%Y %H:%i') dh_Cadastro, is_Ativo, is_Destaque, is_LiberarComentarios, is_Publico, is_Enviado, ds_Permalink, id_Conteudos_fk, id_CategoriasPrincipal_fk, id_Categorias_fk, id_Usuarios_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA FROM Conteudos WHERE id_Conteudos = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdConteudos());
        $c->executaStatement();
        if ($c->Resultado()) {
            $this->setIdConteudos($c->linha['id_Conteudos']);
            $this->setDsTitulo($c->linha['ds_Titulo']);
            $this->setFileCapa($c->linha['file_Capa']);
            $this->setTxResumo($c->linha['tx_Resumo']);
            $this->setTxConteudo($c->linha['tx_Conteudo']);
            $this->setDhCadastro($c->linha['dh_Cadastro']);
            $this->setIsAtivo($c->linha['is_Ativo']);
            $this->setIsDestaque($c->linha['is_Destaque']);
            $this->setIsLiberarComentarios($c->linha['is_LiberarComentarios']);
            $this->setIsPublico($c->linha['is_Publico']);
            $this->setIsEnviado($c->linha['is_Enviado']);
            $this->setDsPermalink($c->linha['ds_Permalink']);
            $this->setIdConteudosFk($c->linha['id_Conteudos_fk']);
            $this->setIdCategoriasPrincipalFk($c->linha['id_CategoriasPrincipal_fk']);
            $this->setIdCategoriasFk($c->linha['id_Categorias_fk']);
            $this->setIdUsuariosFk($c->linha['id_Usuarios_fk']);
            $this->setLogCRIADOPOR($c->linha['log_CRIADOPOR']);
            $this->setLogALTERADOPOR($c->linha['log_ALTERADOPOR']);
            $this->setLogDATA($c->linha['log_DATA']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula
     * @return array
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Conteudos, ds_Titulo, file_Capa, tx_Resumo, tx_Conteudo, DATE_FORMAT(dh_Cadastro, '%d/%m/%Y %H:%i') dh_Cadastro, is_Ativo, is_Destaque, is_LiberarComentarios, is_Publico, is_Enviado, ds_Permalink, id_Conteudos_fk, id_CategoriasPrincipal_fk, id_Categorias_fk, id_Usuarios_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA FROM Conteudos $where ";
        $c->Consulta($sql);

        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Conteudos(NULL);
            $tbl->setIdConteudos($c->linha['id_Conteudos']);
            $tbl->setDsTitulo($c->linha['ds_Titulo']);
            $tbl->setFileCapa($c->linha['file_Capa']);
            $tbl->setTxResumo($c->linha['tx_Resumo']);
            $tbl->setTxConteudo($c->linha['tx_Conteudo']);
            $tbl->setDhCadastro($c->linha['dh_Cadastro']);
            $tbl->setIsAtivo($c->linha['is_Ativo']);
            $tbl->setIsDestaque($c->linha['is_Destaque']);
            $tbl->setIsLiberarComentarios($c->linha['is_LiberarComentarios']);
            $tbl->setIsPublico($c->linha['is_Publico']);
            $tbl->setIsEnviado($c->linha['is_Enviado']);
            $tbl->setDsPermalink($c->linha['ds_Permalink']);
            $tbl->setIdConteudosFk($c->linha['id_Conteudos_fk']);
            $tbl->setIdCategoriasPrincipalFk($c->linha['id_CategoriasPrincipal_fk']);
            $tbl->setIdCategoriasFk($c->linha['id_Categorias_fk']);
            $tbl->setIdUsuariosFk($c->linha['id_Usuarios_fk']);
            $tbl->setLogCRIADOPOR($c->linha['log_CRIADOPOR']);
            $tbl->setLogALTERADOPOR($c->linha['log_ALTERADOPOR']);
            $tbl->setLogDATA($c->linha['log_DATA']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool
     */
    public function Salvar()
    {
        if ($this->getIdConteudos() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();

        $sql = "INSERT INTO Conteudos (ds_Titulo, file_Capa, tx_Resumo, tx_Conteudo, dh_Cadastro, is_Ativo, is_Destaque, is_LiberarComentarios, is_Publico, is_Enviado, ds_Permalink, id_Conteudos_fk, id_CategoriasPrincipal_fk, id_Categorias_fk, id_Usuarios_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA ) VALUES (?, ?, ?, ?, STR_TO_DATE(?,'%d/%m/%Y %H:%i'), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now())  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsTitulo());
        $c->adicionaParametros($this->getFileCapa());
        $c->adicionaParametros($this->getTxResumo());
        $c->adicionaParametros($this->getTxConteudo());
        $c->adicionaParametros($this->getDhCadastro());
        $c->adicionaParametros($this->getIsAtivo());
        $c->adicionaParametros($this->getIsDestaque());
        $c->adicionaParametros($this->getIsLiberarComentarios());
        $c->adicionaParametros($this->getIsPublico());
        $c->adicionaParametros($this->getIsEnviado());
        $c->adicionaParametros($this->getDsPermalink());
        $c->adicionaParametros($this->getIdConteudosFk());
        $c->adicionaParametros($this->getIdCategoriasPrincipalFk());
        $c->adicionaParametros($this->getIdCategoriasFk());
        $c->adicionaParametros($this->getIdUsuariosFk());
        $c->adicionaParametros($this->getLogALTERADOPOR());
        $c->adicionaParametros($this->getLogALTERADOPOR());
        if ($c->executaStatement()) {
            $this->setIdConteudos($c->last_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Alterar()
    {

        $c   = Conexao::getInstance();
        $sql = "UPDATE Conteudos SET ds_Titulo = ?, file_Capa = ?, tx_Resumo = ?, tx_Conteudo = ?, dh_Cadastro = STR_TO_DATE(?,'%d/%m/%Y %H:%i'), is_Ativo = ?, is_Destaque = ?, is_LiberarComentarios = ?, is_Publico=?, is_Enviado = ?, ds_Permalink = ?, id_Conteudos_fk = ?, id_CategoriasPrincipal_fk=?, id_Categorias_fk = ?, id_Usuarios_fk = ?, log_ALTERADOPOR=?, log_DATA=now() WHERE id_Conteudos = ?  ";
        $c->preparaStatement($sql);

        $c->adicionaParametros($this->getDsTitulo());
        $c->adicionaParametros($this->getFileCapa());
        $c->adicionaParametros($this->getTxResumo());
        $c->adicionaParametros($this->getTxConteudo());
        $c->adicionaParametros($this->getDhCadastro());
        $c->adicionaParametros($this->getIsAtivo());
        $c->adicionaParametros($this->getIsDestaque());
        $c->adicionaParametros($this->getIsLiberarComentarios());
        $c->adicionaParametros($this->getIsPublico());
        $c->adicionaParametros($this->getIsEnviado());
        $c->adicionaParametros($this->getDsPermalink());
        $c->adicionaParametros($this->getIdConteudosFk());
        $c->adicionaParametros($this->getIdCategoriasPrincipalFk());
        $c->adicionaParametros($this->getIdCategoriasFk());
        $c->adicionaParametros($this->getIdUsuariosFk());
        $c->adicionaParametros($this->getLogALTERADOPOR());
        // PK
        $c->adicionaParametros($this->getIdConteudos());

        if ($c->executaStatement()) {
            return TRUE;
        }

        return FALSE;

    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Conteudos
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu
     */
    public static function Remover($id_Conteudos)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Conteudos WHERE id_Conteudos = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Conteudos);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @param int|null $id_Conteudos
     */
    public function setIdConteudos($id_Conteudos)
    {
        $this->_id_Conteudos = $id_Conteudos;
    }

    /**
     * @return int|null
     */
    public function getIdConteudos()
    {
        return $this->_id_Conteudos;
    }

    /**
     * @param string|null $ds_Titulo
     */
    public function setDsTitulo($ds_Titulo)
    {
        $this->_ds_Titulo = $ds_Titulo;
    }

    /**
     * @return string|null
     */
    public function getDsTitulo()
    {
        return $this->_ds_Titulo;
    }

    /**
     * @param string|null $file_Capa
     */
    public function setFileCapa($file_Capa)
    {
        $this->_file_Capa = $file_Capa;
    }

    /**
     * @return string|null
     */
    public function getFileCapa()
    {
        return $this->_file_Capa;
    }

    /**
     * @param string|null $tx_Resumo
     */
    public function setTxResumo($tx_Resumo)
    {
        $this->_tx_Resumo = $tx_Resumo;
    }

    /**
     * @return string|null
     */
    public function getTxResumo()
    {
        return $this->_tx_Resumo;
    }

    /**
     * @param string|null $tx_Conteudo
     */
    public function setTxConteudo($tx_Conteudo)
    {
        $this->_tx_Conteudo = $tx_Conteudo;
    }

    /**
     * @return string|null
     */
    public function getTxConteudo()
    {
        return $this->_tx_Conteudo;
    }

    /**
     * @param string|null $dh_Cadastro
     */
    public function setDhCadastro($dh_Cadastro)
    {
        $this->_dh_Cadastro = $dh_Cadastro;
    }

    /**
     * @return string|null
     */
    public function getDhCadastro()
    {
        return $this->_dh_Cadastro;
    }

    /**
     * @param bool|null $is_Ativo
     */
    public function setIsAtivo($is_Ativo)
    {
        $this->_is_Ativo = $is_Ativo;
    }

    /**
     * @return bool|null
     */
    public function getIsAtivo()
    {
        return $this->_is_Ativo;
    }

    /**
     * @param bool|null $is_Destaque
     */
    public function setIsDestaque($is_Destaque)
    {
        $this->_is_Destaque = $is_Destaque;
    }

    /**
     * @return bool|null
     */
    public function getIsDestaque()
    {
        return $this->_is_Destaque;
    }

    /**
     * @param bool|null $is_LiberarComentarios
     */
    public function setIsLiberarComentarios($is_LiberarComentarios)
    {
        $this->_is_LiberarComentarios = $is_LiberarComentarios;
    }

    /**
     * @return bool|null
     */
    public function getIsLiberarComentarios()
    {
        return $this->_is_LiberarComentarios;
    }

    /**
     * @param string|null $ds_Permalink
     */
    public function setDsPermalink($ds_Permalink)
    {
        $this->_ds_Permalink = $ds_Permalink;
    }

    /**
     * @return string|null
     */
    public function getDsPermalink()
    {
        return $this->_ds_Permalink;
    }

    /**
     * @param int|null $id_Conteudos_fk
     */
    public function setIdConteudosFk($id_Conteudos_fk)
    {
        $this->_id_Conteudos_fk = $id_Conteudos_fk;
        $this->_Conteudos = new Conteudos($id_Conteudos_fk);
    }

    /**
     * @return int|null
     */
    public function getIdConteudosFk()
    {
        return $this->_id_Conteudos_fk;
    }

    /**
     * @param int|null $id_Categorias_fk
     */
    public function setIdCategoriasFk($id_Categorias_fk)
    {
        $this->_id_Categorias_fk = $id_Categorias_fk;
        $this->_Categorias = new Categorias($id_Categorias_fk);
    }

    /**
     * @return int|null
     */
    public function getIdCategoriasFk()
    {
        return $this->_id_Categorias_fk;
    }

    /**
     * @param int|null $id_Usuarios_fk
     */
    public function setIdUsuariosFk($id_Usuarios_fk)
    {
        $this->_id_Usuarios_fk = $id_Usuarios_fk;
        $this->_Usuarios = new Usuarios($id_Usuarios_fk);
    }

    /**
     * @return int|null
     */
    public function getIdUsuariosFk()
    {
        return $this->_id_Usuarios_fk;
    }

    /**
     * @param string|null $log_CRIADOPOR
     */
    public function setLogCRIADOPOR($log_CRIADOPOR)
    {
        $this->_log_CRIADOPOR = $log_CRIADOPOR;
    }

    /**
     * @return string|null
     */
    public function getLogCRIADOPOR()
    {
        return $this->_log_CRIADOPOR;
    }

    /**
     * @param string|null $log_ALTERADOPOR
     */
    public function setLogALTERADOPOR($log_ALTERADOPOR)
    {
        $this->_log_ALTERADOPOR = $log_ALTERADOPOR;
    }

    /**
     * @return string|null
     */
    public function getLogALTERADOPOR()
    {
        return $this->_log_ALTERADOPOR;
    }

    /**
     * @param string|null $log_DATA
     */
    public function setLogDATA($log_DATA)
    {
        $this->_log_DATA = $log_DATA;
    }

    /**
     * @return string|null
     */
    public function getLogDATA()
    {
        return $this->_log_DATA;
    }

    /**
     * @param Conteudos $Conteudos
     */
    public function setConteudos($Conteudos)
    {
        $this->_Conteudos = $Conteudos;
    }

    /**
     * @return Conteudos
     */
    public function getConteudos()
    {
        return $this->_Conteudos;
    }

    /**
     * @param Categorias $Categorias
     */
    public function setCategorias($Categorias)
    {
        $this->_Categorias = $Categorias;
    }

    /**
     * @return Categorias
     */
    public function getCategorias()
    {
        return $this->_Categorias;
    }

    /**
     * @param Usuarios $Usuarios
     */
    public function setUsuarios($Usuarios)
    {
        $this->_Usuarios = $Usuarios;
    }

    /**
     * @return Usuarios
     */
    public function getUsuarios()
    {
        return $this->_Usuarios;
    }

    /**
     * @param mixed $CategoriasPrincipal
     */
    public function setCategoriasPrincipal($CategoriasPrincipal)
    {
        $this->_CategoriasPrincipal = $CategoriasPrincipal;
    }

    /**
     * @return Categorias
     */
    public function getCategoriasPrincipal()
    {
        return $this->_CategoriasPrincipal;
    }

    /**
     * @param mixed $id_CategoriasPrincipal_fk
     */
    public function setIdCategoriasPrincipalFk($id_CategoriasPrincipal_fk)
    {
        $this->_id_CategoriasPrincipal_fk = $id_CategoriasPrincipal_fk;
        $this->_CategoriasPrincipal = new Categorias($id_CategoriasPrincipal_fk);
    }

    /**
     * @return mixed
     */
    public function getIdCategoriasPrincipalFk()
    {
        return $this->_id_CategoriasPrincipal_fk;
    }

    /**
     * @param bool $is_Enviado
     */
    public function setIsEnviado($is_Enviado)
    {
        $this->_is_Enviado = $is_Enviado;
    }

    /**
     * @return bool
     */
    public function getIsEnviado()
    {
        return $this->_is_Enviado;
    }

    /**
     * @param bool $is_Publico
     */
    public function setIsPublico($is_Publico)
    {
        $this->_is_Publico = $is_Publico;
    }

    /**
     * @return bool
     */
    public function getIsPublico()
    {
        return $this->_is_Publico;
    }



}