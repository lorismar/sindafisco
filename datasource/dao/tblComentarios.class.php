<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblComentarios
{

    private $_id_Comentarios;
    private $_tx_Comentario;
    private $_dh_Comentario;
    private $_id_Conteudos_fk;
    private $_id_Usuarios_fk;
    private $_id_Comentarios_fk;

    private $_Conteudos;
    private $_Usuarios;


    /**
     * @param int|null $id_Comentarios
     */
    function __construct($id_Comentarios)
    {
        if (!empty($id_Comentarios)) {
            $this->_id_Comentarios = $id_Comentarios;
        }
    }

    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Comentarios, tx_Comentario, DATE_FORMAT(dh_Comentario, '%d/%m/%Y %H:%i') dh_Comentario, id_Conteudos_fk, id_Usuarios_fk, id_Comentarios_fk FROM Comentarios WHERE id_Comentarios = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdComentarios());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdComentarios($c->linha['id_Comentarios']);
            $this->setTxComentario($c->linha['tx_Comentario']);
            $this->setDhComentario($c->linha['dh_Comentario']);
            $this->setIdConteudosFk($c->linha['id_Conteudos_fk']);
            $this->setIdUsuariosFk($c->linha['id_Usuarios_fk']);
            $this->setIdComentariosFk($c->linha['id_Comentarios_fk']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula
     * @return array
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Comentarios, tx_Comentario, DATE_FORMAT(dh_Comentario, '%d/%m/%Y %H:%i') dh_Comentario, id_Conteudos_fk, id_Usuarios_fk, id_Comentarios_fk FROM Comentarios $where ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Comentarios(NULL);
            $tbl->setIdComentarios($c->linha['id_Comentarios']);
            $tbl->setTxComentario($c->linha['tx_Comentario']);
            $tbl->setDhComentario($c->linha['dh_Comentario']);
            $tbl->setIdConteudosFk($c->linha['id_Conteudos_fk']);
            $tbl->setIdUsuariosFk($c->linha['id_Usuarios_fk']);
            $tbl->setIdComentariosFk($c->linha['id_Comentarios_fk']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool
     */
    public function Salvar()
    {
        if ($this->getIdComentarios() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO Comentarios (tx_Comentario,dh_Comentario,id_Conteudos_fk,id_Usuarios_fk,id_Comentarios_fk) VALUES (?, STR_TO_DATE(?,'%d/%m/%Y %H:%i'), ?,?,?)  ";
        $c->preparaStatement($sql);;
        $c->adicionaParametros($this->getTxComentario());
        $c->adicionaParametros($this->getDhComentario());
        $c->adicionaParametros($this->getIdConteudosFk());
        $c->adicionaParametros($this->getIdUsuariosFk());
        $c->adicionaParametros($this->getIdComentariosFk());
        if ($c->executaStatement()) {
            $this->setIdComentarios($c->last_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Alterar()
    {
        $c   = Conexao::getInstance();
        $sql = "UPDATE Comentarios SET tx_Comentario = ?, dh_Comentario=STR_TO_DATE(?,'%d/%m/%Y %H:%i'), id_Conteudos_fk=?, id_Usuarios_fk =?, id_Comentarios_Fk=? WHERE id_Comentarios = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getTxComentario());
        $c->adicionaParametros($this->getDhComentario());
        $c->adicionaParametros($this->getIdConteudosFk());
        $c->adicionaParametros($this->getIdUsuariosFk());
        $c->adicionaParametros($this->getIdComentariosFk());
        // PK
        $c->adicionaParametros($this->getIdComentarios());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Comentarios
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu
     */
    public static function Remover($id_Comentarios)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Comentarios WHERE id_Comentarios = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Comentarios);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Conteudos_fk
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu
     */
    public static function RemoverPorConteudo($id_Conteudos_fk)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Comentarios WHERE id_Conteudos_fk = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Conteudos_fk);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @param Conteudos $Conteudos
     */
    public function setConteudos($Conteudos)
    {
        $this->_Conteudos = $Conteudos;
    }

    /**
     * @return Conteudos
     */
    public function getConteudos()
    {
        return $this->_Conteudos;
    }

    /**
     * @param Usuarios $Usuarios
     */
    public function setUsuarios($Usuarios)
    {
        $this->_Usuarios = $Usuarios;
    }

    /**
     * @return Usuarios
     */
    public function getUsuarios()
    {
        return $this->_Usuarios;
    }

    /**
     * @param mixed $dh_Comentario
     */
    public function setDhComentario($dh_Comentario)
    {
        $this->_dh_Comentario = $dh_Comentario;
    }

    /**
     * @return mixed
     */
    public function getDhComentario()
    {
        return $this->_dh_Comentario;
    }

    /**
     * @param int|null $id_Comentarios
     */
    public function setIdComentarios($id_Comentarios)
    {
        $this->_id_Comentarios = $id_Comentarios;
    }

    /**
     * @return int|null
     */
    public function getIdComentarios()
    {
        return $this->_id_Comentarios;
    }

    /**
     * @param mixed $id_Comentarios_fk
     */
    public function setIdComentariosFk($id_Comentarios_fk)
    {
        $this->_id_Comentarios_fk = $id_Comentarios_fk;
    }

    /**
     * @return mixed
     */
    public function getIdComentariosFk()
    {
        return $this->_id_Comentarios_fk;
    }

    /**
     * @param mixed $id_Conteudos_fk
     */
    public function setIdConteudosFk($id_Conteudos_fk)
    {
        $this->_id_Conteudos_fk = $id_Conteudos_fk;
        $this->_Conteudos = new Conteudos($id_Conteudos_fk);
    }

    /**
     * @return mixed
     */
    public function getIdConteudosFk()
    {
        return $this->_id_Conteudos_fk;
    }

    /**
     * @param mixed $id_Usuarios_fk
     */
    public function setIdUsuariosFk($id_Usuarios_fk)
    {
        $this->_id_Usuarios_fk = $id_Usuarios_fk;
        $this->_Usuarios = new Usuarios($id_Usuarios_fk);
    }

    /**
     * @return mixed
     */
    public function getIdUsuariosFk()
    {
        return $this->_id_Usuarios_fk;
    }

    /**
     * @param mixed $tx_Comentario
     */
    public function setTxComentario($tx_Comentario)
    {
        $this->_tx_Comentario = $tx_Comentario;
    }

    /**
     * @return mixed
     */
    public function getTxComentario()
    {
        return $this->_tx_Comentario;
    }







}