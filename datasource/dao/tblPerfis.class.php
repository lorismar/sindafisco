<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblPerfis
{

    private $_id_Perfis;
    private $_ds_Descricao;


    /**
     * @param int|null $id_Perfis
     */
    function __construct($id_Perfis)
    {
        if (!empty($id_Perfis)) {
            $this->_id_Perfis = $id_Perfis;
        }
    }

    /**
     * @param int|null $id_Perfis
     */
    public function setIdPerfis($id_Perfis)
    {
        $this->_id_Perfis = $id_Perfis;
    }

    /**
     * @return int|null
     */
    public function getIdPerfis()
    {
        return $this->_id_Perfis;
    }

    /**
     * @param string|null $ds_Descricao
     */
    public function setDsDescricao($ds_Descricao)
    {
        $this->_ds_Descricao = $ds_Descricao;
    }

    /**
     * @return string|null
     */
    public function getDsDescricao()
    {
        return $this->_ds_Descricao;
    }

}