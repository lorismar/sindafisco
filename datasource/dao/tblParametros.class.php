<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblParametros
{

    private $_id_Parametros;
    private $_ds_Descricao;
    private $_ds_Valor;


    /**
     * @param int|null $id_Parametros
     */
    function __construct($id_Parametros)
    {
        if (!empty($id_Parametros)) {
            $this->_id_Parametros = $id_Parametros;
        }
    }

    /**
     * @param int|null $id_Parametros
     */
    public function setIdParametros($id_Parametros)
    {
        $this->_id_Parametros = $id_Parametros;
    }

    /**
     * @return int|null
     */
    public function getIdParametros()
    {
        return $this->_id_Parametros;
    }

    /**
     * @param string|null $ds_Descricao
     */
    public function setDsDescricao($ds_Descricao)
    {
        $this->_ds_Descricao = $ds_Descricao;
    }

    /**
     * @return string|null
     */
    public function getDsDescricao()
    {
        return $this->_ds_Descricao;
    }

    /**
     * @param string|null $ds_Valor
     */
    public function setDsValor($ds_Valor)
    {
        $this->_ds_Valor = $ds_Valor;
    }

    /**
     * @return string|null
     */
    public function getDsValor()
    {
        return $this->_ds_Valor;
    }

}