<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblUsuarios
{

    private $_id_Usuarios;
    private $_ds_Nome;
    private $_ds_Usuario;
    private $_ds_Senha;
    private $_nu_TentativaAcesso;
    private $_is_AlterarSenha;
    private $_is_Ativo;
    private $_id_Perfis_fk;
    private $_id_Pessoas_fk;
    private $_log_CRIADOPOR;
    private $_log_ALTERADOPOR;
    private $_log_DATA;

    private $_Perfis;
    private $_Pessoas;

    /**
     * @param int|null $id_Usuarios
     */
    function __construct($id_Usuarios)
    {
        if (!empty($id_Usuarios)) {
            $this->_id_Usuarios = $id_Usuarios;
        }
    }

    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Usuarios, ds_Nome, ds_Usuario, ds_Senha, nu_TentativaAcesso, is_AlterarSenha, is_Ativo, id_Perfis_fk, id_Pessoas_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA FROM Usuarios WHERE id_Usuarios = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdUsuarios());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdUsuarios($c->linha['id_Usuarios']);
            $this->setDsNome($c->linha['ds_Nome']);
            $this->setDsUsuario($c->linha['ds_Usuario']);
            $this->setDsSenha($c->linha['ds_Senha']);
            $this->setNuTentativaAcesso($c->linha['nu_TentativaAcesso']);
            $this->setIsAlterarSenha($c->linha['is_AlterarSenha']);
            $this->setIsAtivo($c->linha['is_Ativo']);
            $this->setIdPerfisFk($c->linha['id_Perfis_fk']);
            $this->setIdPessoasFk($c->linha['id_Pessoas_fk']);
            $this->setLogCRIADOPOR($c->linha['log_CRIADOPOR']);
            $this->setLogALTERADOPOR($c->linha['log_ALTERADOPOR']);
            $this->setLogDATA($c->linha['log_DATA']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula
     * @return array
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Usuarios, ds_Nome, ds_Usuario, ds_Senha, nu_TentativaAcesso, is_AlterarSenha, is_Ativo, id_Perfis_fk, id_Pessoas_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA FROM Usuarios $where ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Usuarios(NULL);
            $tbl->setIdUsuarios($c->linha['id_Usuarios']);
            $tbl->setDsNome($c->linha['ds_Nome']);
            $tbl->setDsUsuario($c->linha['ds_Usuario']);
            $tbl->setDsSenha($c->linha['ds_Senha']);
            $tbl->setNuTentativaAcesso($c->linha['nu_TentativaAcesso']);
            $tbl->setIsAlterarSenha($c->linha['is_AlterarSenha']);
            $tbl->setIsAtivo($c->linha['is_Ativo']);
            $tbl->setIdPerfisFk($c->linha['id_Perfis_fk']);
            $tbl->setIdPessoasFk($c->linha['id_Pessoas_fk']);
            $tbl->setLogCRIADOPOR($c->linha['log_CRIADOPOR']);
            $tbl->setLogALTERADOPOR($c->linha['log_ALTERADOPOR']);
            $tbl->setLogDATA($c->linha['log_DATA']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool
     */
    public function Salvar()
    {
        if ($this->getIdUsuarios() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO Usuarios (id_Usuarios, ds_Nome, ds_Usuario, ds_Senha, nu_TentativaAcesso, is_AlterarSenha, is_Ativo, id_Perfis_fk, id_Pessoas_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now())  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdUsuarios());
        $c->adicionaParametros($this->getDsNome());
        $c->adicionaParametros($this->getDsUsuario());
        $c->adicionaParametros($this->getDsSenha());
        $c->adicionaParametros($this->getNuTentativaAcesso());
        $c->adicionaParametros($this->getIsAlterarSenha());
        $c->adicionaParametros($this->getIsAtivo());
        $c->adicionaParametros($this->getIdPerfisFk());
        $c->adicionaParametros($this->getIdPessoasFk());
        $c->adicionaParametros($this->getLogALTERADOPOR());
        $c->adicionaParametros($this->getLogALTERADOPOR());
        if ($c->executaStatement()) {
            $this->setIdUsuarios($c->last_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Alterar()
    {
        $c   = Conexao::getInstance();
        $sql = "UPDATE Usuarios SET ds_Nome = ?, ds_Usuario = ?, ds_Senha = ?, nu_TentativaAcesso = ?, is_AlterarSenha = ?, is_Ativo = ?, id_Perfis_fk = ?, id_Pessoas_fk = ?, log_ALTERADOPOR=?, log_DATA=now() WHERE id_Usuarios = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsNome());
        $c->adicionaParametros($this->getDsUsuario());
        $c->adicionaParametros($this->getDsSenha());
        $c->adicionaParametros($this->getNuTentativaAcesso());
        $c->adicionaParametros($this->getIsAlterarSenha());
        $c->adicionaParametros($this->getIsAtivo());
        $c->adicionaParametros($this->getIdPerfisFk());
        $c->adicionaParametros($this->getIdPessoasFk());
        $c->adicionaParametros($this->getLogALTERADOPOR());
        // PK
        $c->adicionaParametros($this->getIdUsuarios());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Usuarios
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu
     */
    public static function Remover($id_Usuarios)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Usuarios WHERE id_Usuarios = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Usuarios);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Pessoas_fk
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu
     */
    public static function RemoverPorIdPessoas($id_Pessoas_fk)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Usuarios WHERE id_Pessoas_fk = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Pessoas_fk);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @param int|null $id_Usuarios
     */
    public function setIdUsuarios($id_Usuarios)
    {
        $this->_id_Usuarios = $id_Usuarios;
    }

    /**
     * @return int|null
     */
    public function getIdUsuarios()
    {
        return $this->_id_Usuarios;
    }

    /**
     * @param string|null $ds_Nome
     */
    public function setDsNome($ds_Nome)
    {
        $this->_ds_Nome = $ds_Nome;
    }

    /**
     * @return string|null
     */
    public function getDsNome()
    {
        return $this->_ds_Nome;
    }

    /**
     * @param string|null $ds_Usuario
     */
    public function setDsUsuario($ds_Usuario)
    {
        $this->_ds_Usuario = $ds_Usuario;
    }

    /**
     * @return string|null
     */
    public function getDsUsuario()
    {
        return $this->_ds_Usuario;
    }

    /**
     * @param string|null $ds_Senha
     */
    public function setDsSenha($ds_Senha)
    {
        $this->_ds_Senha = $ds_Senha;
    }

    /**
     * @return string|null
     */
    public function getDsSenha()
    {
        return $this->_ds_Senha;
    }

    /**
     * @param int|null $nu_TentativaAcesso
     */
    public function setNuTentativaAcesso($nu_TentativaAcesso)
    {
        $this->_nu_TentativaAcesso = $nu_TentativaAcesso;
    }

    /**
     * @return int|null
     */
    public function getNuTentativaAcesso()
    {
        return $this->_nu_TentativaAcesso;
    }

    /**
     * @param bool|null $is_AlterarSenha
     */
    public function setIsAlterarSenha($is_AlterarSenha)
    {
        $this->_is_AlterarSenha = $is_AlterarSenha;
    }

    /**
     * @return bool|null
     */
    public function getIsAlterarSenha()
    {
        return $this->_is_AlterarSenha;
    }

    /**
     * @param bool|null $is_Ativo
     */
    public function setIsAtivo($is_Ativo)
    {
        $this->_is_Ativo = $is_Ativo;
    }

    /**
     * @return bool|null
     */
    public function getIsAtivo()
    {
        return $this->_is_Ativo;
    }

    /**
     * @param int|null $id_Perfis_fk
     */
    public function setIdPerfisFk($id_Perfis_fk)
    {
        $this->_id_Perfis_fk = $id_Perfis_fk;
        $this->_Perfis = new Perfis($id_Perfis_fk);
    }

    /**
     * @return int|null
     */
    public function getIdPerfisFk()
    {
        return $this->_id_Perfis_fk;
    }

    /**
     * @param int|null $id_Pessoas_fk
     */
    public function setIdPessoasFk($id_Pessoas_fk)
    {
        $this->_id_Pessoas_fk = $id_Pessoas_fk;
        $this->_Pessoas = new Pessoas($id_Pessoas_fk);
    }

    /**
     * @return int|null
     */
    public function getIdPessoasFk()
    {
        return $this->_id_Pessoas_fk;
    }

    /**
     * @param string|null $log_CRIADOPOR
     */
    public function setLogCRIADOPOR($log_CRIADOPOR)
    {
        $this->_log_CRIADOPOR = $log_CRIADOPOR;
    }

    /**
     * @return string|null
     */
    public function getLogCRIADOPOR()
    {
        return $this->_log_CRIADOPOR;
    }

    /**
     * @param string|null $log_ALTERADOPOR
     */
    public function setLogALTERADOPOR($log_ALTERADOPOR)
    {
        $this->_log_ALTERADOPOR = $log_ALTERADOPOR;
    }

    /**
     * @return string|null
     */
    public function getLogALTERADOPOR()
    {
        return $this->_log_ALTERADOPOR;
    }

    /**
     * @param string|null $log_DATA
     */
    public function setLogDATA($log_DATA)
    {
        $this->_log_DATA = $log_DATA;
    }

    /**
     * @return string|null
     */
    public function getLogDATA()
    {
        return $this->_log_DATA;
    }

    /**
     * @param Perfis $Perfis
     */
    public function setPerfis($Perfis)
    {
        $this->_Perfis = $Perfis;
    }

    /**
     * @return Perfis
     */
    public function getPerfis()
    {
        return $this->_Perfis;
    }

    /**
     * @param Pessoas $Pessoas
     */
    public function setPessoas($Pessoas)
    {
        $this->_Pessoas = $Pessoas;
    }

    /**
     * @return Pessoas
     */
    public function getPessoas()
    {
        return $this->_Pessoas;
    }

}