<?php
/**
 * Created by PhpStorm.
 * User: user
 */

abstract class tblDependentes
{
    private $_id_Dependentes;
    private $_ds_Nome;
    private $_ds_Parentesco;
    private $_dt_Nascimento;
    private $_id_Pessoas_fk;

    /**
     * @param int|null $_id_Dependentes
     */
    function __construct($_id_Dependentes)
    {
        if (!empty($_id_Dependentes)) {
            $this->_id_Dependentes = $_id_Dependentes;
        }
    }

    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Dependentes, ds_Nome, ds_Parentesco, DATE_FORMAT(dt_Nascimento, '%d/%m/%Y') dt_Nascimento, id_Pessoas_fk FROM Dependentes WHERE id_Dependentes = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdDependentes());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdDependentes($c->linha['id_Dependentes']);
            $this->setDsNome($c->linha['ds_Nome']);
            $this->setDsParentesco($c->linha['ds_Parentesco']);
            $this->setDtNascimento($c->linha['dt_Nascimento']);
            $this->setIdPessoasFk($c->linha['id_Pessoas_fk']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula
     * @return array
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Dependentes, ds_Nome, ds_Parentesco, DATE_FORMAT(dt_Nascimento, '%d/%m/%Y') dt_Nascimento, id_Pessoas_fk FROM Dependentes $where ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Dependentes(NULL);
            $tbl->setIdDependentes($c->linha['id_Dependentes']);
            $tbl->setDsNome($c->linha['ds_Nome']);
            $tbl->setDsParentesco($c->linha['ds_Parentesco']);
            $tbl->setDtNascimento($c->linha['dt_Nascimento']);
            $tbl->setIdPessoasFk($c->linha['id_Pessoas_fk']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool
     */
    public function Salvar()
    {
        if ($this->getIdDependentes() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO Dependentes (ds_Nome, ds_Parentesco, dt_Nascimento, id_Pessoas_fk) VALUES (?,?,STR_TO_DATE(?,'%d/%m/%Y'), ?)  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsNome());
        $c->adicionaParametros($this->getDsParentesco());
        $c->adicionaParametros($this->getDtNascimento());
        $c->adicionaParametros($this->getIdPessoasFk());
        if ($c->executaStatement()) {
            $this->setIdDependentes($c->last_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Alterar()
    {
        $c   = Conexao::getInstance();
        $sql = "UPDATE Dependentes SET ds_Nome = ?, ds_Parentesco = ?, dt_Nascimento = STR_TO_DATE(?,'%d/%m/%Y'), id_Pessoas_fk = ? WHERE id_Dependentes = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsNome());
        $c->adicionaParametros($this->getDsParentesco());
        $c->adicionaParametros($this->getDtNascimento());
        $c->adicionaParametros($this->getIdPessoasFk());
        // PK
        $c->adicionaParametros($this->getIdDependentes());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Dependentes
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu
     */
    public static function Remover($id_Dependentes)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Dependentes WHERE id_Dependentes = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Dependentes);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Pessoas
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu
     */
    public static function RemoverPorTitular($id_Pessoas)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Dependentes WHERE id_Pessoas_fk = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Pessoas);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @param mixed $ds_Nome
     */
    public function setDsNome($ds_Nome)
    {
        $this->_ds_Nome = $ds_Nome;
    }

    /**
     * @return mixed
     */
    public function getDsNome()
    {
        return $this->_ds_Nome;
    }

    /**
     * @param mixed $ds_Parentesco
     */
    public function setDsParentesco($ds_Parentesco)
    {
        $this->_ds_Parentesco = $ds_Parentesco;
    }

    /**
     * @return mixed
     */
    public function getDsParentesco()
    {
        return $this->_ds_Parentesco;
    }

    /**
     * @param mixed $dt_Nascimento
     */
    public function setDtNascimento($dt_Nascimento)
    {
        $this->_dt_Nascimento = $dt_Nascimento;
    }

    /**
     * @return mixed
     */
    public function getDtNascimento()
    {
        return $this->_dt_Nascimento;
    }

    /**
     * @param mixed $id_Dependentes
     */
    public function setIdDependentes($id_Dependentes)
    {
        $this->_id_Dependentes = $id_Dependentes;
    }

    /**
     * @return mixed
     */
    public function getIdDependentes()
    {
        return $this->_id_Dependentes;
    }

    /**
     * @param mixed $id_Pessoas_fk
     */
    public function setIdPessoasFk($id_Pessoas_fk)
    {
        $this->_id_Pessoas_fk = $id_Pessoas_fk;
    }

    /**
     * @return mixed
     */
    public function getIdPessoasFk()
    {
        return $this->_id_Pessoas_fk;
    }




} 