<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblConteudos_tem_Pessoas
{

    private $_id_Conteudos_fk;
    private $_id_Pessoas_fk;

    private $_Conteudos;
    private $_Pessoas;

    /**
     * @param int|null $_id_Conteudos_fk
     * @param int|null $id_Pessoas_fk
     */
    function __construct($_id_Conteudos_fk,$id_Pessoas_fk)
    {
        if (!empty($_id_Conteudos_fk)) {
            $this->_id_Conteudos_fk = $_id_Conteudos_fk;
        }
        if (!empty($_id_Pessoas_fk)) {
            $this->_id_Pessoas_fk = $id_Pessoas_fk;
        }
    }

    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Conteudos_fk, id_Pessoas_fk FROM Conteudos_tem_Pessoas WHERE id_Conteudos_fk = ? AND id_Pessoas_fk = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdConteudosFk());
        $c->adicionaParametros($this->getIdPessoasFk());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdConteudosFk($c->linha['id_Conteudos_fk']);
            $this->setIdPessoasFk($c->linha['id_Pessoas_fk']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula
     * @return array
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Conteudos_fk, id_Pessoas_fk FROM Conteudos_tem_Pessoas {$where} ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Conteudos_tem_Pessoas(NULL, NULL);
            $tbl->setIdConteudosFk($c->linha['id_Conteudos_fk']);
            $tbl->setIdPessoasFk($c->linha['id_Pessoas_fk']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Busca por todos os registros por Processos
     * @param int $id_Conteudos_fk ID da PK da Processos
     * @return array
     */
    public static function ListarTodosPorConteudo($id_Conteudos_fk)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Conteudos_fk, id_Pessoas_fk FROM Conteudos_tem_Pessoas WHERE id_Conteudos_fk = ? ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Conteudos_fk);
        $c->executaStatement();
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Conteudos_tem_Pessoas(NULL, NULL);
            $tbl->setIdConteudosFk($c->linha['id_Conteudos_fk']);
            $tbl->setIdPessoasFk($c->linha['id_Pessoas_fk']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool
     */
    public function Salvar()
    {

            return $this->Criar();

    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO Conteudos_tem_Pessoas (id_Conteudos_fk, id_Pessoas_fk ) VALUES (?, ?)  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdConteudosFk());
        $c->adicionaParametros($this->getIdPessoasFk());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }



    /**
     * Remove um Registro da Tabela
     * @param $id_Conteudos_fk
     * @param $id_Pessoas_fk
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu
     */
    public static function Remover($id_Conteudos_fk, $id_Pessoas_fk)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Conteudos_tem_Pessoas WHERE id_Conteudos_fk = ? AND id_Pessoas_fk = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Conteudos_fk);
        $c->adicionaParametros($id_Pessoas_fk);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Conteudos_fk
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu
     */
    public static function RemoverPorConteudo($id_Conteudos_fk)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Conteudos_tem_Pessoas WHERE id_Conteudos_fk = ? ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Conteudos_fk);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }


    /**
     * @param int|null $id_Pessoas_fk
     */
    public function setIdPessoasFk($id_Pessoas_fk)
    {
        $this->_id_Pessoas_fk = $id_Pessoas_fk;
        $this->_Pessoas = new Pessoas($id_Pessoas_fk);
    }

    /**
     * @return int|null
     */
    public function getIdPessoasFk()
    {
        return $this->_id_Pessoas_fk;
    }


    /**
     * @param Pessoas $Pessoas
     */
    public function setPessoas($Pessoas)
    {
        $this->_Pessoas = $Pessoas;
    }

    /**
     * @return Pessoas
     */
    public function getPessoas()
    {
        return $this->_Pessoas;
    }

    /**
     * @param Conteudos $Conteudos
     */
    public function setConteudos($Conteudos)
    {
        $this->_Conteudos = $Conteudos;
    }

    /**
     * @return Conteudos
     */
    public function getConteudos()
    {
        return $this->_Conteudos;
    }

    /**
     * @param int|null $id_Conteudos_fk
     */
    public function setIdConteudosFk($id_Conteudos_fk)
    {
        $this->_id_Conteudos_fk = $id_Conteudos_fk;
        $this->_Conteudos = new Conteudos($id_Conteudos_fk);
    }

    /**
     * @return int|null
     */
    public function getIdConteudosFk()
    {
        return $this->_id_Conteudos_fk;
    }




}