<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

abstract class tblSitPessoas
{

    private $_id_SitPessoas;
    private $_ds_Descricao;

    /**
     * @param int|null $_id_SitPessoas
     */
    function __construct($_id_SitPessoas)
    {
        if (!empty($_id_SitPessoas)) {
            $this->_id_SitPessoas = $_id_SitPessoas;
        }
    }



    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_SitPessoas, ds_Descricao FROM SitPessoas WHERE id_SitPessoas = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdSitPessoas());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdSitPessoas($c->linha['id_SitPessoas']);
            $this->setDsDescricao($c->linha['ds_Descricao']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula
     * @return array
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_SitPessoas, ds_Descricao FROM SitPessoas $where ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new SitPessoas(NULL);
            $tbl->setIdSitPessoas($c->linha['id_SitPessoas']);
            $tbl->setDsDescricao($c->linha['ds_Descricao']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool
     */
    public function Salvar()
    {
        if ($this->getIdSitPessoas() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO SitPessoas (id_SitPessoas, ds_Descricao) VALUES (?, ?)  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdSitPessoas());
        $c->adicionaParametros($this->getDsDescricao());
        if ($c->executaStatement()) {
            $this->setIdSitPessoas($c->last_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Alterar()
    {
        $c   = Conexao::getInstance();
        $sql = "UPDATE SitPessoas SET ds_Descricao = ? WHERE id_SitPessoas = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsDescricao());
        // PK
        $c->adicionaParametros($this->getIdSitPessoas());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }


    /**
     * @param mixed $id_SitPessoas
     */
    public function setIdSitPessoas($id_SitPessoas)
    {
        $this->_id_SitPessoas = $id_SitPessoas;
    }

    /**
     * @return mixed
     */
    public function getIdSitPessoas()
    {
        return $this->_id_SitPessoas;
    }


    /**
     * @param string|null $ds_Descricao
     */
    public function setDsDescricao($ds_Descricao)
    {
        $this->_ds_Descricao = $ds_Descricao;
    }

    /**
     * @return string|null
     */
    public function getDsDescricao()
    {
        return $this->_ds_Descricao;
    }


}