<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

class sysPaginas extends tblsysPaginas
{

    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou 
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_sysPaginas, ds_Controller, ds_Descricao, ds_Icone, id_sysPaginas_fk, id_Modulos_fk FROM sysPaginas WHERE id_sysPaginas = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdSysPaginas());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdSysPaginas($c->linha['id_sysPaginas']);
            $this->setDsController($c->linha['ds_Controller']);
            $this->setDsDescricao($c->linha['ds_Descricao']);
            $this->setDsIcone($c->linha['ds_Icone']);
            $this->setIdSysPaginasFk($c->linha['id_sysPaginas_fk']);
            $this->setIdModulosFk($c->linha['id_Modulos_fk']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula 
     * @return array 
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_sysPaginas, ds_Controller, ds_Descricao, ds_Icone, id_sysPaginas_fk, id_Modulos_fk FROM sysPaginas $where ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new sysPaginas(NULL);
            $tbl->setIdSysPaginas($c->linha['id_sysPaginas']);
            $tbl->setDsController($c->linha['ds_Controller']);
            $tbl->setDsDescricao($c->linha['ds_Descricao']);
            $tbl->setDsIcone($c->linha['ds_Icone']);
            $tbl->setIdSysPaginasFk($c->linha['id_sysPaginas_fk']);
            $tbl->setIdModulosFk($c->linha['id_Modulos_fk']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool 
     */
    public function Salvar()
    {
        if ($this->getIdSysPaginas() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou 
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO sysPaginas (id_sysPaginas, ds_Controller, ds_Descricao, ds_Icone, id_sysPaginas_fk, id_Modulos_fk ) VALUES (?, ?, ?, ?, ?, ?)  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdSysPaginas());
        $c->adicionaParametros($this->getDsController());
        $c->adicionaParametros($this->getDsDescricao());
        $c->adicionaParametros($this->getDsIcone());
        $c->adicionaParametros($this->getIdSysPaginasFk());
        $c->adicionaParametros($this->getIdModulosFk());
        if ($c->executaStatement()) {
            $this->setIdSysPaginas($c->last_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou 
     */
    private function Alterar()
    {
        $c   = Conexao::getInstance();
        $sql = "UPDATE sysPaginas SET ds_Controller = ?, ds_Descricao = ?, ds_Icone = ?, id_sysPaginas_fk = ?, id_Modulos_fk = ? WHERE id_sysPaginas = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsController());
        $c->adicionaParametros($this->getDsDescricao());
        $c->adicionaParametros($this->getDsIcone());
        $c->adicionaParametros($this->getIdSysPaginasFk());
        $c->adicionaParametros($this->getIdModulosFk());
        // PK
        $c->adicionaParametros($this->getIdSysPaginas());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_sysPaginas
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu 
     */
    public static function Remover($id_sysPaginas)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM sysPaginas WHERE id_sysPaginas = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_sysPaginas);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

}