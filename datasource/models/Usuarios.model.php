<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

class Usuarios extends tblUsuarios
{

    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorIdPessoa()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Usuarios, ds_Nome, ds_Usuario, ds_Senha, nu_TentativaAcesso, is_AlterarSenha, is_Ativo, id_Perfis_fk, id_Pessoas_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA FROM Usuarios WHERE id_Pessoas_fk = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdPessoasFk());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdUsuarios($c->linha['id_Usuarios']);
            $this->setDsNome($c->linha['ds_Nome']);
            $this->setDsUsuario($c->linha['ds_Usuario']);
            $this->setDsSenha($c->linha['ds_Senha']);
            $this->setNuTentativaAcesso($c->linha['nu_TentativaAcesso']);
            $this->setIsAlterarSenha($c->linha['is_AlterarSenha']);
            $this->setIsAtivo($c->linha['is_Ativo']);
            $this->setIdPerfisFk($c->linha['id_Perfis_fk']);
            $this->setIdPessoasFk($c->linha['id_Pessoas_fk']);
            $this->setLogCRIADOPOR($c->linha['log_CRIADOPOR']);
            $this->setLogALTERADOPOR($c->linha['log_ALTERADOPOR']);
            $this->setLogDATA($c->linha['log_DATA']);

            return TRUE;
        }
        return FALSE;
    }


    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function Logar()
    {

        $c   = Conexao::getInstance();
        $sql = "SELECT id_Usuarios, ds_Nome, ds_Usuario, ds_Senha, nu_TentativaAcesso, is_AlterarSenha, is_Ativo, id_Perfis_fk, id_Pessoas_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA FROM Usuarios WHERE ds_Usuario = ? AND ds_Senha = ? AND is_Ativo = TRUE  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsUsuario());
        $c->adicionaParametros($this->getDsSenha());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdUsuarios($c->linha['id_Usuarios']);
            $this->setDsNome($c->linha['ds_Nome']);
            $this->setDsUsuario($c->linha['ds_Usuario']);
            $this->setDsSenha($c->linha['ds_Senha']);
            $this->setNuTentativaAcesso($c->linha['nu_TentativaAcesso']);
            $this->setIsAlterarSenha($c->linha['is_AlterarSenha']);
            $this->setIsAtivo($c->linha['is_Ativo']);
            $this->setIdPerfisFk($c->linha['id_Perfis_fk']);
            $this->setIdPessoasFk($c->linha['id_Pessoas_fk']);
            $this->setLogCRIADOPOR($c->linha['log_CRIADOPOR']);
            $this->setLogALTERADOPOR($c->linha['log_ALTERADOPOR']);
            $this->setLogDATA($c->linha['log_DATA']);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorUsuario()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Usuarios, ds_Nome, ds_Usuario, ds_Senha, nu_TentativaAcesso, is_AlterarSenha, is_Ativo, id_Perfis_fk, id_Pessoas_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA FROM Usuarios WHERE ds_Usuario = ? ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsUsuario());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdUsuarios($c->linha['id_Usuarios']);
            $this->setDsNome($c->linha['ds_Nome']);
            $this->setDsUsuario($c->linha['ds_Usuario']);
            $this->setDsSenha($c->linha['ds_Senha']);
            $this->setNuTentativaAcesso($c->linha['nu_TentativaAcesso']);
            $this->setIsAlterarSenha($c->linha['is_AlterarSenha']);
            $this->setIsAtivo($c->linha['is_Ativo']);
            $this->setIdPerfisFk($c->linha['id_Perfis_fk']);
            $this->setIdPessoasFk($c->linha['id_Pessoas_fk']);
            $this->setLogCRIADOPOR($c->linha['log_CRIADOPOR']);
            $this->setLogALTERADOPOR($c->linha['log_ALTERADOPOR']);
            $this->setLogDATA($c->linha['log_DATA']);
            return TRUE;
        }
        return FALSE;
    }


}