<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

class Modulos extends tblModulos
{

    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou 
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Modulos, ds_Descricao FROM Modulos WHERE id_Modulos = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdModulos());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdModulos($c->linha['id_Modulos']);
            $this->setDsDescricao($c->linha['ds_Descricao']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula 
     * @return array 
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Modulos, ds_Descricao FROM Modulos $where ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Modulos(NULL);
            $tbl->setIdModulos($c->linha['id_Modulos']);
            $tbl->setDsDescricao($c->linha['ds_Descricao']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool 
     */
    public function Salvar()
    {
        if ($this->getIdModulos() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou 
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO Modulos (id_Modulos, ds_Descricao ) VALUES (?, ?)  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdModulos());
        $c->adicionaParametros($this->getDsDescricao());
        if ($c->executaStatement()) {
            $this->setIdModulos($c->last_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou 
     */
    private function Alterar()
    {
        $c   = Conexao::getInstance();
        $sql = "UPDATE Modulos SET ds_Descricao = ? WHERE id_Modulos = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsDescricao());
        // PK
        $c->adicionaParametros($this->getIdModulos());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Modulos
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu 
     */
    public static function Remover($id_Modulos)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Modulos WHERE id_Modulos = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Modulos);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

}