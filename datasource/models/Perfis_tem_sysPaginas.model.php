<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

class Perfis_tem_sysPaginas extends tblPerfis_tem_sysPaginas
{

    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou 
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Perfis_fk, id_sysPaginas_fk, is_Visivel, is_Leitura, is_Gravacao, is_Remocao FROM Perfis_tem_sysPaginas WHERE id_Perfis_fk = ? AND id_sysPaginas_fk = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdPerfisFk());
        $c->adicionaParametros($this->getIdSysPaginasFk());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdPerfisFk($c->linha['id_Perfis_fk']);
            $this->setIdSysPaginasFk($c->linha['id_sysPaginas_fk']);
            $this->setIsVisivel($c->linha['is_Visivel']);
            $this->setIsLeitura($c->linha['is_Leitura']);
            $this->setIsGravacao($c->linha['is_Gravacao']);
            $this->setIsRemocao($c->linha['is_Remocao']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula 
     * @return array 
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Perfis_fk, id_sysPaginas_fk, is_Visivel, is_Leitura, is_Gravacao, is_Remocao FROM Perfis_tem_sysPaginas $where ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Perfis_tem_sysPaginas(NULL, NULL);
            $tbl->setIdPerfisFk($c->linha['id_Perfis_fk']);
            $tbl->setIdSysPaginasFk($c->linha['id_sysPaginas_fk']);
            $tbl->setIsVisivel($c->linha['is_Visivel']);
            $tbl->setIsLeitura($c->linha['is_Leitura']);
            $tbl->setIsGravacao($c->linha['is_Gravacao']);
            $tbl->setIsRemocao($c->linha['is_Remocao']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Busca por todos os registros por Perfis
     * @param int $id_Perfis_fk ID da PK da Perfis 
     * @return array 
     */
    public static function ListarTodosPorPerfis($id_Perfis_fk)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Perfis_fk, id_sysPaginas_fk, is_Visivel, is_Leitura, is_Gravacao, is_Remocao FROM Perfis_tem_sysPaginas WHERE id_Perfis_fk = ? ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Perfis_fk);
        $c->executaStatement();
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Perfis_tem_sysPaginas(NULL, NULL);
            $tbl->setIdPerfisFk($c->linha['id_Perfis_fk']);
            $tbl->setIdSysPaginasFk($c->linha['id_sysPaginas_fk']);
            $tbl->setIsVisivel($c->linha['is_Visivel']);
            $tbl->setIsLeitura($c->linha['is_Leitura']);
            $tbl->setIsGravacao($c->linha['is_Gravacao']);
            $tbl->setIsRemocao($c->linha['is_Remocao']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool 
     */
    public function Salvar()
    {
        if ($this->getIdPerfisFk() > 0 && $this->getIdSysPaginasFk() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou 
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO Perfis_tem_sysPaginas (id_Perfis_fk, id_sysPaginas_fk, is_Visivel, is_Leitura, is_Gravacao, is_Remocao ) VALUES (?, ?, ?, ?, ?, ?)  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdPerfisFk());
        $c->adicionaParametros($this->getIdSysPaginasFk());
        $c->adicionaParametros($this->getIsVisivel());
        $c->adicionaParametros($this->getIsLeitura());
        $c->adicionaParametros($this->getIsGravacao());
        $c->adicionaParametros($this->getIsRemocao());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou 
     */
    private function Alterar()
    {
        $c   = Conexao::getInstance();
        $sql = "UPDATE Perfis_tem_sysPaginas SET is_Visivel = ?, is_Leitura = ?, is_Gravacao = ?, is_Remocao = ? WHERE id_Perfis_fk = ? AND id_sysPaginas_fk = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIsVisivel());
        $c->adicionaParametros($this->getIsLeitura());
        $c->adicionaParametros($this->getIsGravacao());
        $c->adicionaParametros($this->getIsRemocao());
        // PK
        $c->adicionaParametros($this->getIdPerfisFk());
        $c->adicionaParametros($this->getIdSysPaginasFk());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Perfis_fk
     * @param $id_sysPaginas_fk
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu 
     */
    public static function Remover($id_Perfis_fk, $id_sysPaginas_fk)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Perfis_tem_sysPaginas WHERE id_Perfis_fk = ? AND id_sysPaginas_fk = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Perfis_fk);
        $c->adicionaParametros($id_sysPaginas_fk);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

}