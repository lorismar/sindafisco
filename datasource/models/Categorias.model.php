<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

class Categorias extends tblCategorias
{

    public static function ListarTodosPorCategoria($id_PRO_Categorias_fk)
    {

        $c = Conexao::getInstance();
        $sql = "SELECT id_Categorias, ds_Descricao, id_Categorias_fk  FROM Categorias WHERE id_Categorias_fk = ?";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_PRO_Categorias_fk);
        $c->executaStatement();
        $arrayList = array();
        while ($c->Resultado()) {
            $tbl = new Categorias(NULL);
            $tbl->setIdCategorias($c->linha['id_Categorias']);
            $tbl->setDsDescricao($c->linha['ds_Descricao']);
            $tbl->setIdCategoriasFk($c->linha['id_Categorias_fk']);
            array_push($arrayList, $tbl);
        }

        return $arrayList;
    }


    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou 
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Categorias, ds_Descricao, id_Categorias_fk FROM Categorias WHERE id_Categorias = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdCategorias());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdCategorias($c->linha['id_Categorias']);
            $this->setDsDescricao($c->linha['ds_Descricao']);
            $this->setIdCategoriasFk($c->linha['id_Categorias_fk']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula 
     * @return array 
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Categorias, ds_Descricao, id_Categorias_fk FROM Categorias $where ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Categorias(NULL);
            $tbl->setIdCategorias($c->linha['id_Categorias']);
            $tbl->setDsDescricao($c->linha['ds_Descricao']);
            $tbl->setIdCategoriasFk($c->linha['id_Categorias_fk']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool 
     */
    public function Salvar()
    {
        if ($this->getIdCategorias() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou 
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO Categorias (id_Categorias, ds_Descricao, id_Categorias_fk ) VALUES (?, ?, ?)  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdCategorias());
        $c->adicionaParametros($this->getDsDescricao());
        $c->adicionaParametros($this->getIdCategoriasFk());
        if ($c->executaStatement()) {
            $this->setIdCategorias($c->last_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou 
     */
    private function Alterar()
    {
        $c   = Conexao::getInstance();
        $sql = "UPDATE Categorias SET ds_Descricao = ?, id_Categorias_fk = ? WHERE id_Categorias = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsDescricao());
        $c->adicionaParametros($this->getIdCategoriasFk());
        // PK
        $c->adicionaParametros($this->getIdCategorias());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Categorias
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu 
     */
    public static function Remover($id_Categorias)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Categorias WHERE id_Categorias = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Categorias);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

}