<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

class Arquivos extends tblArquivos
{

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula
     * @return array
     */
    public static function ListarTodosIdPai($id_CategoriasPrincipal_fk,$id_Arquivos_fk)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Arquivos, ds_Titulo, ds_Descricao, file_Arquivo, ds_UrlLink, DATE_FORMAT(dt_Cadastro, '%d/%m/%Y') dt_Cadastro, is_Ativo, id_Arquivos_fk, id_Conteudos_fk, id_Categorias_fk,id_CategoriasPrincipal_fk, id_Usuarios_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA FROM Arquivos WHERE id_CategoriasPrincipal_fk = {$id_CategoriasPrincipal_fk} and id_Arquivos_fk = {$id_Arquivos_fk} ORDER BY id_Arquivos ASC ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Arquivos(NULL);
            $tbl->setIdArquivos($c->linha['id_Arquivos']);
            $tbl->setDsTitulo($c->linha['ds_Titulo']);
            $tbl->setDsDescricao($c->linha['ds_Descricao']);
            $tbl->setFileArquivo($c->linha['file_Arquivo']);
            $tbl->setDsUrlLink($c->linha['ds_UrlLink']);
            $tbl->setDtCadastro($c->linha['dt_Cadastro']);
            $tbl->setIsAtivo($c->linha['is_Ativo']);
            $tbl->setIdArquivosFk($c->linha['id_Arquivos_fk']);
            $tbl->setIdConteudosFk($c->linha['id_Conteudos_fk']);
            $tbl->setIdCategoriasFk($c->linha['id_Categorias_fk']);
            $tbl->getIdCategoriasPrincipalFk($c->linha['id_CategoriasPrincipal_fk']);
            $tbl->setIdUsuariosFk($c->linha['id_Usuarios_fk']);
            $tbl->setLogCRIADOPOR($c->linha['log_CRIADOPOR']);
            $tbl->setLogALTERADOPOR($c->linha['log_ALTERADOPOR']);
            $tbl->setLogDATA($c->linha['log_DATA']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

}