<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

class Conteudos extends tblConteudos
{

    /**
     * @param $permalink string
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorPermalink($permalink)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Conteudos, ds_Titulo, file_Capa, tx_Resumo, tx_Conteudo, DATE_FORMAT(dh_Cadastro, '%d/%m/%Y %H:%i') dh_Cadastro, is_Ativo, is_Destaque, is_LiberarComentarios, is_Publico ,is_Enviado, ds_Permalink, id_Conteudos_fk, id_CategoriasPrincipal_fk, id_Categorias_fk, id_Usuarios_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA FROM Conteudos WHERE ds_Permalink = ? and is_Ativo = TRUE and is_Publico = TRUE  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($permalink);
        $c->executaStatement();
        if ($c->Resultado()) {
            $this->setIdConteudos($c->linha['id_Conteudos']);
            $this->setDsTitulo($c->linha['ds_Titulo']);
            $this->setFileCapa($c->linha['file_Capa']);
            $this->setTxResumo($c->linha['tx_Resumo']);
            $this->setTxConteudo($c->linha['tx_Conteudo']);
            $this->setDhCadastro($c->linha['dh_Cadastro']);
            $this->setIsAtivo($c->linha['is_Ativo']);
            $this->setIsDestaque($c->linha['is_Destaque']);
            $this->setIsLiberarComentarios($c->linha['is_LiberarComentarios']);
            $this->setIsPublico($c->linha['is_Publico']);
            $this->setIsEnviado($c->linha['is_Enviado']);
            $this->setDsPermalink($c->linha['ds_Permalink']);
            $this->setIdConteudosFk($c->linha['id_Conteudos_fk']);
            $this->setIdCategoriasPrincipalFk($c->linha['id_CategoriasPrincipal_fk']);
            $this->setIdCategoriasFk($c->linha['id_Categorias_fk']);
            $this->setIdUsuariosFk($c->linha['id_Usuarios_fk']);
            $this->setLogCRIADOPOR($c->linha['log_CRIADOPOR']);
            $this->setLogALTERADOPOR($c->linha['log_ALTERADOPOR']);
            $this->setLogDATA($c->linha['log_DATA']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * @param $permalink string
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorPermalinkCategoria($permalink,$categoria)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Conteudos, ds_Titulo, file_Capa, tx_Resumo, tx_Conteudo, DATE_FORMAT(dh_Cadastro, '%d/%m/%Y %H:%i') dh_Cadastro, is_Ativo, is_Destaque, is_LiberarComentarios, is_Publico ,is_Enviado, ds_Permalink, id_Conteudos_fk, id_CategoriasPrincipal_fk, id_Categorias_fk, id_Usuarios_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA FROM Conteudos WHERE ds_Permalink = ? and id_CategoriasPrincipal_fk = ? and is_Ativo = TRUE and is_Publico = TRUE  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($permalink);
        $c->adicionaParametros($categoria);
        $c->executaStatement();
        if ($c->Resultado()) {
            $this->setIdConteudos($c->linha['id_Conteudos']);
            $this->setDsTitulo($c->linha['ds_Titulo']);
            $this->setFileCapa($c->linha['file_Capa']);
            $this->setTxResumo($c->linha['tx_Resumo']);
            $this->setTxConteudo($c->linha['tx_Conteudo']);
            $this->setDhCadastro($c->linha['dh_Cadastro']);
            $this->setIsAtivo($c->linha['is_Ativo']);
            $this->setIsDestaque($c->linha['is_Destaque']);
            $this->setIsLiberarComentarios($c->linha['is_LiberarComentarios']);
            $this->setIsPublico($c->linha['is_Publico']);
            $this->setIsEnviado($c->linha['is_Enviado']);
            $this->setDsPermalink($c->linha['ds_Permalink']);
            $this->setIdConteudosFk($c->linha['id_Conteudos_fk']);
            $this->setIdCategoriasPrincipalFk($c->linha['id_CategoriasPrincipal_fk']);
            $this->setIdCategoriasFk($c->linha['id_Categorias_fk']);
            $this->setIdUsuariosFk($c->linha['id_Usuarios_fk']);
            $this->setLogCRIADOPOR($c->linha['log_CRIADOPOR']);
            $this->setLogALTERADOPOR($c->linha['log_ALTERADOPOR']);
            $this->setLogDATA($c->linha['log_DATA']);

            return TRUE;
        }
        return FALSE;
    }
    public function VisualizarPorPermalinkCategoriaPrivado($permalink,$categoria)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Conteudos, ds_Titulo, file_Capa, tx_Resumo, tx_Conteudo, DATE_FORMAT(dh_Cadastro, '%d/%m/%Y %H:%i') dh_Cadastro, is_Ativo, is_Destaque, is_LiberarComentarios, is_Publico ,is_Enviado, ds_Permalink, id_Conteudos_fk, id_CategoriasPrincipal_fk, id_Categorias_fk, id_Usuarios_fk, log_CRIADOPOR, log_ALTERADOPOR, log_DATA FROM Conteudos WHERE ds_Permalink = ? and id_CategoriasPrincipal_fk = ? and is_Ativo = TRUE ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($permalink);
        $c->adicionaParametros($categoria);
        $c->executaStatement();
        if ($c->Resultado()) {
            $this->setIdConteudos($c->linha['id_Conteudos']);
            $this->setDsTitulo($c->linha['ds_Titulo']);
            $this->setFileCapa($c->linha['file_Capa']);
            $this->setTxResumo($c->linha['tx_Resumo']);
            $this->setTxConteudo($c->linha['tx_Conteudo']);
            $this->setDhCadastro($c->linha['dh_Cadastro']);
            $this->setIsAtivo($c->linha['is_Ativo']);
            $this->setIsDestaque($c->linha['is_Destaque']);
            $this->setIsLiberarComentarios($c->linha['is_LiberarComentarios']);
            $this->setIsPublico($c->linha['is_Publico']);
            $this->setIsEnviado($c->linha['is_Enviado']);
            $this->setDsPermalink($c->linha['ds_Permalink']);
            $this->setIdConteudosFk($c->linha['id_Conteudos_fk']);
            $this->setIdCategoriasPrincipalFk($c->linha['id_CategoriasPrincipal_fk']);
            $this->setIdCategoriasFk($c->linha['id_Categorias_fk']);
            $this->setIdUsuariosFk($c->linha['id_Usuarios_fk']);
            $this->setLogCRIADOPOR($c->linha['log_CRIADOPOR']);
            $this->setLogALTERADOPOR($c->linha['log_ALTERADOPOR']);
            $this->setLogDATA($c->linha['log_DATA']);

            return TRUE;
        }
        return FALSE;
    }

    public function Comentarios()
    {
        return Comentarios::ListarTodos("WHERE id_Conteudos_fk = " . $this->getIdConteudos()." ORDER BY dh_Comentario DESC");
    }

    public function Arquivos(){
        return Arquivos::ListarTodos("WHERE id_Conteudos_fk = " . $this->getIdConteudos());
    }


}