<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

class Parametros extends tblParametros
{

    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Parametros, ds_Descricao, ds_Valor FROM Parametros WHERE id_Parametros = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdParametros());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdParametros($c->linha['id_Parametros']);
            $this->setDsDescricao($c->linha['ds_Descricao']);
            $this->setDsValor($c->linha['ds_Valor']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula
     * @return array
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Parametros, ds_Descricao, ds_Valor FROM Parametros $where ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Parametros(NULL);
            $tbl->setIdParametros($c->linha['id_Parametros']);
            $tbl->setDsDescricao($c->linha['ds_Descricao']);
            $tbl->setDsValor($c->linha['ds_Valor']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool
     */
    public function Salvar()
    {
        if ($this->getIdParametros() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO Parametros (ds_Descricao, ds_Valor ) VALUES (?, ?)  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsDescricao());
        $c->adicionaParametros($this->getDsValor());
        if ($c->executaStatement()) {
            $this->setIdParametros($c->last_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou
     */
    private function Alterar()
    {
        $c   = Conexao::getInstance();
        $sql = "UPDATE Parametros SET ds_Descricao = ?, ds_Valor = ? WHERE id_Parametros = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsDescricao());
        $c->adicionaParametros($this->getDsValor());
        // PK
        $c->adicionaParametros($this->getIdParametros());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Parametros
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu
     */
    public static function Remover($id_Parametros)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Parametros WHERE id_Parametros = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Parametros);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

}