<?php

/**
 * Created by MVC Engine Generator by I�ri Gustavo - gustavo.iuri@havit.com.br.
 */

class Menus extends tblMenus
{

    /**
     * Busca atrav�s do ID da PK j� definido na classe
     * @return bool - TRUE se achou registro e FALSE se n�o achou 
     */
    public function VisualizarPorId()
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Menus, ds_Titulo, ds_Url, nu_Posicao, is_Ativo, id_Conteudos_fk, id_Menus_fk FROM Menus WHERE id_Menus = ?   ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdMenus());
        $c->executaStatement();

        if ($c->Resultado()) {
            $this->setIdMenus($c->linha['id_Menus']);
            $this->setDsTitulo($c->linha['ds_Titulo']);
            $this->setDsUrl($c->linha['ds_Url']);
            $this->setNuPosicao($c->linha['nu_Posicao']);
            $this->setIsAtivo($c->linha['is_Ativo']);
            $this->setIdConteudosFk($c->linha['id_Conteudos_fk']);
            $this->setIdMenusFk($c->linha['id_Menus_fk']);

            return TRUE;
        }
        return FALSE;
    }

    /**
     * Busca por todos os registros com ou sem cl�usula
     * @param null $where Cl�usula 
     * @return array 
     */
    public static function ListarTodos($where = NULL)
    {
        $c   = Conexao::getInstance();
        $sql = "SELECT id_Menus, ds_Titulo, ds_Url, nu_Posicao, is_Ativo, id_Conteudos_fk, id_Menus_fk FROM Menus $where ";
        $c->Consulta($sql);
        $arrayList = array();

        while ($c->Resultado()) {
            $tbl = new Menus(NULL);
            $tbl->setIdMenus($c->linha['id_Menus']);
            $tbl->setDsTitulo($c->linha['ds_Titulo']);
            $tbl->setDsUrl($c->linha['ds_Url']);
            $tbl->setNuPosicao($c->linha['nu_Posicao']);
            $tbl->setIsAtivo($c->linha['is_Ativo']);
            $tbl->setIdConteudosFk($c->linha['id_Conteudos_fk']);
            $tbl->setIdMenusFk($c->linha['id_Menus_fk']);
            array_push($arrayList, $tbl);
        }
        return $arrayList;
    }

    /**
     * Fun��o p�blica para enviar as altera��es, caso n�o tiver ID ele adiciona um registro novo, caso tiver ID ele altera o registro
     * @return bool 
     */
    public function Salvar()
    {
        if ($this->getIdMenus() > 0) {
            return $this->Alterar();
        } else {
            return $this->Criar();
        }
    }

    /**
     * Adiciona um Registro na Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou 
     */
    private function Criar()
    {
        $c   = Conexao::getInstance();
        $sql = "INSERT INTO Menus (id_Menus, ds_Titulo, ds_Url, nu_Posicao, is_Ativo, id_Conteudos_fk, id_Menus_fk ) VALUES (?, ?, ?, ?, ?, ?, ?)  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getIdMenus());
        $c->adicionaParametros($this->getDsTitulo());
        $c->adicionaParametros($this->getDsUrl());
        $c->adicionaParametros($this->getNuPosicao());
        $c->adicionaParametros($this->getIsAtivo());
        $c->adicionaParametros($this->getIdConteudosFk());
        $c->adicionaParametros($this->getIdMenusFk());
        if ($c->executaStatement()) {
            $this->setIdMenus($c->last_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Altera um Registro da Tabela
     * @return bool - TRUE se criou registro e FALSE se n�o criou 
     */
    private function Alterar()
    {
        $c   = Conexao::getInstance();
        $sql = "UPDATE Menus SET ds_Titulo = ?, ds_Url = ?, nu_Posicao = ?, is_Ativo = ?, id_Conteudos_fk = ?, id_Menus_fk = ? WHERE id_Menus = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($this->getDsTitulo());
        $c->adicionaParametros($this->getDsUrl());
        $c->adicionaParametros($this->getNuPosicao());
        $c->adicionaParametros($this->getIsAtivo());
        $c->adicionaParametros($this->getIdConteudosFk());
        $c->adicionaParametros($this->getIdMenusFk());
        // PK
        $c->adicionaParametros($this->getIdMenus());
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um Registro da Tabela
     * @param $id_Menus
     * @return bool - TRUE se removeu registro e FALSE se n�o removeu 
     */
    public static function Remover($id_Menus)
    {
        $c   = Conexao::getInstance();
        $sql = "DELETE FROM Menus WHERE id_Menus = ?  ";
        $c->preparaStatement($sql);
        $c->adicionaParametros($id_Menus);
        if ($c->executaStatement()) {
            return TRUE;
        }
        return FALSE;
    }

}